 package br.com.ecommerceinformatica.fatec.web.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dao.CarrinhoDAO;
import br.com.ecommerceinformatica.fatec.dao.CartaoCreditoDAO;
import br.com.ecommerceinformatica.fatec.dao.CategoriaDAO;
import br.com.ecommerceinformatica.fatec.dao.ClienteDAO;
import br.com.ecommerceinformatica.fatec.dao.ContatoDAO;
import br.com.ecommerceinformatica.fatec.dao.EmailTrocarSenhaDAO;
import br.com.ecommerceinformatica.fatec.dao.EnderecoClienteDAO;
import br.com.ecommerceinformatica.fatec.dao.EnderecoDAO;
import br.com.ecommerceinformatica.fatec.dao.EstoqueDAO;
import br.com.ecommerceinformatica.fatec.dao.FornecedorDAO;
import br.com.ecommerceinformatica.fatec.dao.IDAO;
import br.com.ecommerceinformatica.fatec.dao.PedidoDAO;
import br.com.ecommerceinformatica.fatec.dao.ProdutoDAO;
import br.com.ecommerceinformatica.fatec.dao.TrocaPedidoDAO;
import br.com.ecommerceinformatica.fatec.dao.UsuarioClienteDAO;
import br.com.ecommerceinformatica.fatec.dominio.Cartao;
import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.EmailTrocarSenha;
import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EnderecoCliente;
import br.com.ecommerceinformatica.fatec.dominio.Fornecedor;
import br.com.ecommerceinformatica.fatec.dominio.ItensCarrinho;
import br.com.ecommerceinformatica.fatec.dominio.ItensEstoque;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.dominio.TrocaPedido;
import br.com.ecommerceinformatica.fatec.dominio.Usuario;
import br.com.ecommerceinformatica.fatec.strategy.DataCadastro;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.strategy.ValidarExistencia;
import br.com.ecommerceinformatica.fatec.strategy.carrinho.ValidarExistenciaItemCarrinho;
import br.com.ecommerceinformatica.fatec.strategy.carrinho.ValidarQuantidadeItemCarrinho;
import br.com.ecommerceinformatica.fatec.strategy.cartao.ValidarDadosCartao;
import br.com.ecommerceinformatica.fatec.strategy.categoria.ValidarDadosCategoria;
import br.com.ecommerceinformatica.fatec.strategy.categoria.ValidarExisteAlgumProdutoAtivoDelete;
import br.com.ecommerceinformatica.fatec.strategy.categoria.ValidarExisteProdutoAtivoUpdateInativo;
import br.com.ecommerceinformatica.fatec.strategy.cliente.ValidarCPF;
import br.com.ecommerceinformatica.fatec.strategy.cliente.ValidarDadosCartaoCliente;
import br.com.ecommerceinformatica.fatec.strategy.cliente.ValidarDadosCliente;
import br.com.ecommerceinformatica.fatec.strategy.cliente.ValidarSenha;
import br.com.ecommerceinformatica.fatec.strategy.cliente.endereco.ValidarDadosObrigatorio;
import br.com.ecommerceinformatica.fatec.strategy.cliente.endereco.ValidarExistenciaPedidoFinalizando;
import br.com.ecommerceinformatica.fatec.strategy.estoque.DataCompra;
import br.com.ecommerceinformatica.fatec.strategy.fornecedor.ValidarDadosFornecedor;
import br.com.ecommerceinformatica.fatec.strategy.fornecedor.ValidarExistenciaFornecedor;
import br.com.ecommerceinformatica.fatec.strategy.login.ValidarLogin;
import br.com.ecommerceinformatica.fatec.strategy.pedido.AtualizarCodigo;
import br.com.ecommerceinformatica.fatec.strategy.pedido.CalcularFrete;
import br.com.ecommerceinformatica.fatec.strategy.pedido.CalcularPrecoParcela;
import br.com.ecommerceinformatica.fatec.strategy.pedido.EnviarEmailConfirmacaoPedido;
import br.com.ecommerceinformatica.fatec.strategy.pedido.GerarCupomDescontoComADiferencaDoAntigo;
import br.com.ecommerceinformatica.fatec.strategy.pedido.GerarCupomTroca;
import br.com.ecommerceinformatica.fatec.strategy.pedido.NotificarStatusPedido;
import br.com.ecommerceinformatica.fatec.strategy.pedido.PreencherCodigoPedido;
import br.com.ecommerceinformatica.fatec.strategy.pedido.ValidarCartoesPedido;
import br.com.ecommerceinformatica.fatec.strategy.pedido.ValidarCupomPedido;
import br.com.ecommerceinformatica.fatec.strategy.pedido.ValidarDadosPedido;
import br.com.ecommerceinformatica.fatec.strategy.pedido.ValidarDiasParaEntrega;
import br.com.ecommerceinformatica.fatec.strategy.pedido.ValidarEnderecos;
import br.com.ecommerceinformatica.fatec.strategy.pedido.ValidarFormaPagamento;
import br.com.ecommerceinformatica.fatec.strategy.pedido.ValidarQntItensEstoque;
import br.com.ecommerceinformatica.fatec.strategy.pedido.ValidarValorCupom;
import br.com.ecommerceinformatica.fatec.strategy.produto.CriarEstoqueDoProduto;
import br.com.ecommerceinformatica.fatec.strategy.produto.ValidarDadosProdutos;
import br.com.ecommerceinformatica.fatec.strategy.produto.ValidarExistenciaProduto;
import br.com.ecommerceinformatica.fatec.strategy.produto.ValidarSeProdutoTaEmAlgumPedidoAtivoDELETAR;
import br.com.ecommerceinformatica.fatec.strategy.trocapedido.ValidarQuantidadeTrocaCancelada;
import br.com.ecommerceinformatica.fatec.strategy.trocapedido.cancelamento.AlterarPedidoCancelamento;
import br.com.ecommerceinformatica.fatec.strategy.trocapedido.cancelamento.CancelarPedido;
import br.com.ecommerceinformatica.fatec.strategy.trocapedido.troca.AlterarPedidoTroca;

public class FacadeUtil {
	
	public static HashMap<String, IDAO> carregarMapDao(){
		HashMap<String, IDAO> mapDao = new HashMap<String, IDAO>();
		
		mapDao.put(Fornecedor.class.getName(), new FornecedorDAO());
		mapDao.put(Contato.class.getName(), new ContatoDAO());
		mapDao.put(Produto.class.getName(), new ProdutoDAO());
		mapDao.put(Endereco.class.getName(), new EnderecoDAO());
		mapDao.put(Categoria.class.getName(), new CategoriaDAO());
		mapDao.put(Cliente.class.getName(), new ClienteDAO());
		mapDao.put(ItensCarrinho.class.getName(), new CarrinhoDAO());
		mapDao.put(Pedido.class.getName(), new PedidoDAO());
		mapDao.put(ItensEstoque.class.getName(), new EstoqueDAO());
		mapDao.put(Usuario.class.getName(), new UsuarioClienteDAO());
		mapDao.put(EnderecoCliente.class.getName(), new EnderecoClienteDAO());
		mapDao.put(Cartao.class.getName(), new CartaoCreditoDAO());
		mapDao.put(EmailTrocarSenha.class.getName(), new EmailTrocarSenhaDAO());
		mapDao.put(TrocaPedido.class.getName(), new TrocaPedidoDAO());
		
		
		return mapDao;
	}

	
	public static Map<String, Map<String, List<IStrategy>>> carregarMapRegrasNegocio(){
		Map<String, Map<String, List<IStrategy>>> mapRgns = new HashMap<String, Map<String, List<IStrategy>>>();
		
		// Criar um mapa com as regras de negocio baseado no que �
		//--------------------------------------------------
		// 					CARTAO
		List<IStrategy> listRngCartaoBeforeInsert = new ArrayList<IStrategy>();
		listRngCartaoBeforeInsert.add(new ValidarDadosCartao());
		
		Map<String, List<IStrategy>> mapRngCartao = new HashMap<String, List<IStrategy>>();
		mapRngCartao.put("BEFORE INSERT", listRngCartaoBeforeInsert);
		
		
		// --------------------------------------------------------------------------------

				// 					TROCA PEDIDO
				// Regras de neg�cio
				//      SALVAR
		Map<String, List<IStrategy>> mapaRngTrocaPedido = new HashMap<String, List<IStrategy>>();
		List<IStrategy> listRgnTrocaPedidoBeforeInsert = new ArrayList<IStrategy>();
		listRgnTrocaPedidoBeforeInsert.add(new ValidarQuantidadeTrocaCancelada());
		
		List<IStrategy> listRgnTrocaPedidoAfterUpdate = new ArrayList<IStrategy>();
		listRgnTrocaPedidoAfterUpdate.add(new AlterarPedidoTroca());
		listRgnTrocaPedidoAfterUpdate.add(new AlterarPedidoCancelamento());
		listRgnTrocaPedidoAfterUpdate.add(new CancelarPedido());
		
		
		mapaRngTrocaPedido.put("BEFORE INSERT", listRgnTrocaPedidoBeforeInsert);
		mapaRngTrocaPedido.put("AFTER UPDATE", listRgnTrocaPedidoAfterUpdate);
		
		
		
		// --------------------------------------------------------------------------------

		// 					PEDIDO
		// Regras de neg�cio
		//      SALVAR
		List<IStrategy> listRgnPedidoBeforeInsert = new ArrayList<IStrategy>();
		listRgnPedidoBeforeInsert.add(new PreencherCodigoPedido());
		listRgnPedidoBeforeInsert.add(new ValidarEnderecos());
		listRgnPedidoBeforeInsert.add(new CalcularFrete());
		listRgnPedidoBeforeInsert.add(new ValidarCupomPedido());
		listRgnPedidoBeforeInsert.add(new ValidarValorCupom());
		listRgnPedidoBeforeInsert.add(new ValidarDadosPedido());
		listRgnPedidoBeforeInsert.add(new ValidarDiasParaEntrega());
		listRgnPedidoBeforeInsert.add(new ValidarQntItensEstoque());
		listRgnPedidoBeforeInsert.add(new DataCadastro());
		listRgnPedidoBeforeInsert.add(new ValidarCartoesPedido());
		listRgnPedidoBeforeInsert.add(new CalcularPrecoParcela());
		
		List<IStrategy> listRgnPedidoAfterInsert = new ArrayList<IStrategy>();
		listRgnPedidoAfterInsert.add(new AtualizarCodigo());
		listRgnPedidoAfterInsert.add(new ValidarFormaPagamento());
		listRgnPedidoAfterInsert.add(new GerarCupomDescontoComADiferencaDoAntigo());
		listRgnPedidoAfterInsert.add(new EnviarEmailConfirmacaoPedido());
		
		List<IStrategy> listRgnPedidoAfterUpdate = new ArrayList<IStrategy>();
		listRgnPedidoAfterUpdate.add(new NotificarStatusPedido());
		
		
		Map<String, List<IStrategy>> mapaRngPedido = new HashMap<String, List<IStrategy>>();
		mapaRngPedido.put("BEFORE INSERT", listRgnPedidoBeforeInsert);
		mapaRngPedido.put("AFTER INSERT", listRgnPedidoAfterInsert);
		mapaRngPedido.put("AFTER UPDATE", listRgnPedidoAfterUpdate);
		
		
		
		// --------------------------------------------------------------------------------

		// 					FORNECEDOR
		// Regras de neg�cio

		//					 SALVAR
		List<IStrategy> listRngFornecedorBeforeInsert = new ArrayList<IStrategy>();
		listRngFornecedorBeforeInsert.add(new DataCadastro());
		listRngFornecedorBeforeInsert.add(new ValidarExistenciaFornecedor());
		listRngFornecedorBeforeInsert.add(new ValidarDadosFornecedor());

		Map<String, List<IStrategy>> mapaRngFornecedor = new HashMap<String, List<IStrategy>>();
		mapaRngFornecedor.put("BEFORE INSERT", listRngFornecedorBeforeInsert);
		
		//					ALTERAR
		List<IStrategy> listRngFornecedorBeforeUpdate = new ArrayList<IStrategy>();
		listRngFornecedorBeforeUpdate.add(new ValidarDadosFornecedor());
		mapaRngFornecedor.put("BEFORE UPDATE", listRngFornecedorBeforeUpdate);
		
		
		// --------------------------------------------------------------------------------

		// 					CATEGORIA
		// Regras de neg�cio
		//					 SALVAR
		List<IStrategy> listRngCategoriaBeforeInsert = new ArrayList<IStrategy>();
		listRngCategoriaBeforeInsert.add(new DataCadastro());
		listRngCategoriaBeforeInsert.add(new ValidarExistencia());
		listRngCategoriaBeforeInsert.add(new ValidarDadosCategoria());

		Map<String, List<IStrategy>> mapaRngCategoria = new HashMap<String, List<IStrategy>>();
		mapaRngCategoria.put("BEFORE INSERT", listRngCategoriaBeforeInsert);
		
		//					Before update
		List<IStrategy> listRngCategoriaBeforeUpdate = new ArrayList<IStrategy>();
		listRngCategoriaBeforeUpdate.add(new ValidarDadosCategoria());
		listRngCategoriaBeforeUpdate.add(new ValidarExisteProdutoAtivoUpdateInativo());
		mapaRngCategoria.put("BEFORE UPDATE", listRngCategoriaBeforeUpdate);
		
		//					BEFORE DELETE
		List<IStrategy> listRngCategoriaBeforeDelete = new ArrayList<IStrategy>();
		listRngCategoriaBeforeDelete.add(new ValidarExisteAlgumProdutoAtivoDelete());
		
		mapaRngCategoria.put("BEFORE DELETE", listRngCategoriaBeforeDelete);
		
		
		// --------------------------------------------------------------------------------

		// 					PRODUTO
		// Regras de neg�cio
		// 					BEFORE INSERT
		List<IStrategy> listRngProdutoBeforeInsert = new ArrayList<IStrategy>();
		listRngProdutoBeforeInsert.add(new DataCadastro());
		listRngProdutoBeforeInsert.add(new ValidarExistencia());
		listRngProdutoBeforeInsert.add(new ValidarDadosProdutos());		
		listRngProdutoBeforeInsert.add(new ValidarExistenciaProduto());		

		Map<String, List<IStrategy>> mapaRngProduto = new HashMap<String, List<IStrategy>>();
		mapaRngProduto.put("BEFORE INSERT", listRngProdutoBeforeInsert);
		
		//					AFTER INSERT
		List<IStrategy> listRngProdutoAfterInsert = new ArrayList<IStrategy>();
		listRngProdutoAfterInsert.add(new CriarEstoqueDoProduto());
		mapaRngProduto.put("AFTER INSERT", listRngProdutoAfterInsert);
		
		//  
		//					ALTERAR
		List<IStrategy> listRngProdutoBeforeUpdate = new ArrayList<IStrategy>();
		listRngProdutoBeforeUpdate.add(new ValidarDadosProdutos());
		
		mapaRngProduto.put("BEFORE UPDATE", listRngProdutoBeforeUpdate);
		
		//
		//					DELETAR
		List<IStrategy> listRgnProdutoBeforeDelete = new ArrayList<IStrategy>();
		listRgnProdutoBeforeDelete.add(new ValidarSeProdutoTaEmAlgumPedidoAtivoDELETAR());
		
		mapaRngProduto.put("BEFORE DELETE", listRgnProdutoBeforeDelete);

		// --------------------------------------------------------------------------------

		// 					ENDERECO FORNECEDOR
		// Regras de neg�cio
		// 					SALVAR
		List<IStrategy> listRngEnderecoBeforeInsert = new ArrayList<IStrategy>();
		listRngEnderecoBeforeInsert.add(new DataCadastro());
		listRngEnderecoBeforeInsert.add(new ValidarExistencia());

		Map<String, List<IStrategy>> mapaRngEndereco = new HashMap<String, List<IStrategy>>();
		mapaRngEndereco.put("BEFORE INSERT", listRngEnderecoBeforeInsert);
		
		
		// --------------------------------------------------------------------------------

		// 					ENDERECO CLIENTE
		// Regras de neg�cio
		// 					SALVAR
		List<IStrategy> listRngEnderecoClienteBeforeInsert = new ArrayList<IStrategy>();
		listRngEnderecoClienteBeforeInsert.add(new ValidarDadosObrigatorio());
		listRngEnderecoClienteBeforeInsert.add(new DataCadastro());
		
		//					ALTERAR	
		List<IStrategy> listRngEnderecoClienteBeforeUpdate = new ArrayList<IStrategy>();
		listRngEnderecoClienteBeforeUpdate.add(new ValidarExistenciaPedidoFinalizando());
		
		Map<String, List<IStrategy>> mapRngEnderecoCliente = new HashMap<String, List<IStrategy>>();
		mapRngEnderecoCliente.put("BEFORE INSERT", listRngEnderecoClienteBeforeInsert);
		mapRngEnderecoCliente.put("BEFORE UPDATE", listRngEnderecoClienteBeforeUpdate);
				
		
		// --------------------------------------------------------------------------------
		
		//					CLIENTE
		// regras de negocio
		//					SALVAR
		List<IStrategy> listRngClienteBeforeInsert = new ArrayList<IStrategy>();
		listRngClienteBeforeInsert.add(new ValidarSenha());
		listRngClienteBeforeInsert.add(new DataCadastro());
		listRngClienteBeforeInsert.add(new ValidarExistencia());
		listRngClienteBeforeInsert.add(new ValidarDadosCliente());
		listRngClienteBeforeInsert.add(new ValidarDadosCartaoCliente());
		listRngClienteBeforeInsert.add(new ValidarCPF());
		
		Map<String, List<IStrategy>> mapaRngCliente = new HashMap<String, List<IStrategy>>();
		mapaRngCliente.put("BEFORE INSERT", listRngClienteBeforeInsert);
		
		//
		// 					ALTERAR
		List<IStrategy> listRngClienteBeforeUpdate = new ArrayList<IStrategy>();
		listRngClienteBeforeUpdate.add(new ValidarDadosCliente());
		
		mapaRngCliente.put("BEFORE UPDATE", listRngClienteBeforeUpdate);
		
		//
		//					USUARIO	
		List<IStrategy> listRngUsuarioBeforeRead= new ArrayList<IStrategy>();
		listRngUsuarioBeforeRead.add(new ValidarLogin());
		
		Map<String, List<IStrategy>> mapRngUsuario = new HashMap<String, List<IStrategy>>();
		mapRngUsuario.put("BEFORE READ", listRngUsuarioBeforeRead);
		
		
		
		//
		//					CARRINHO
		Map<String, List<IStrategy>> mapaRngCarrinho = new HashMap<String, List<IStrategy>>();
		List<IStrategy> listRngCarrinhoBeforeInsert = new ArrayList<IStrategy>();
		listRngCarrinhoBeforeInsert.add(new ValidarExistenciaItemCarrinho());
		
		List<IStrategy> listRngCarrinhoBeforeUpdate = new ArrayList<IStrategy>();
		listRngCarrinhoBeforeUpdate.add(new ValidarQuantidadeItemCarrinho());
		
		mapaRngCarrinho.put("BEFORE INSERT", listRngCarrinhoBeforeInsert);
		mapaRngCarrinho.put("BEFORE UPDATE", listRngCarrinhoBeforeUpdate);
		
		
		
		
		// 
		//					ITENS ESTOQUE
		Map<String, List<IStrategy>> mapaRngItensEstoque = new HashMap<String, List<IStrategy>>();
		List<IStrategy> listRngItensEstoqueBeforeInsert = new ArrayList<IStrategy>();
		listRngItensEstoqueBeforeInsert.add(new DataCompra());
		
		// BEFORE
		mapaRngItensEstoque.put("BEFORE UPDATE", listRngItensEstoqueBeforeInsert);
		
		
		// --------------------------------------------------------------------------------

		// COLOCAR MAPA DE CADA CLASSE NO MAPA GERAL
		mapRgns = new HashMap<String, Map<String,List<IStrategy>>>();
		// FORNECEDOR
		mapRgns.put(Fornecedor.class.getName(), mapaRngFornecedor);
		// CATEGORIA
		mapRgns.put(Categoria.class.getName(), mapaRngCategoria);
		// PRODUTO
		mapRgns.put(Produto.class.getName(), mapaRngProduto);
		// ENDERECO
		mapRgns.put(Endereco.class.getName(), mapaRngEndereco);
		// ENDERE�O CLIENTE
		mapRgns.put(EnderecoCliente.class.getName(), mapRngEnderecoCliente);
		// CLIENTE
		mapRgns.put(Cliente.class.getName(), mapaRngCliente);
		// CARRINHO
		mapRgns.put(ItensCarrinho.class.getName(), mapaRngCarrinho);
		// PEDIDO
		mapRgns.put(Pedido.class.getName(), mapaRngPedido);	
		// Usuario
		mapRgns.put(Usuario.class.getName(), mapRngUsuario);	
		// Cartao
		mapRgns.put(Cartao.class.getName(), mapRngCartao);
		// ITENS ESTOQUE
		mapRgns.put(ItensEstoque.class.getName(), mapaRngItensEstoque);
		// TROCA PEDIDO
		mapRgns.put(TrocaPedido.class.getName(), mapaRngTrocaPedido);
		
		return mapRgns;
	}
	
}
