package br.com.ecommerceinformatica.fatec.web.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import br.com.ecommerceinformatica.fatec.dao.CategoriaDAO;
import br.com.ecommerceinformatica.fatec.dao.ClienteDAO;
import br.com.ecommerceinformatica.fatec.dao.EnderecoClienteDAO;
import br.com.ecommerceinformatica.fatec.dao.FornecedorDAO;
import br.com.ecommerceinformatica.fatec.dao.PedidoDAO;
import br.com.ecommerceinformatica.fatec.dao.ProdutoDAO;
import br.com.ecommerceinformatica.fatec.dao.TrocaPedidoDAO;
import br.com.ecommerceinformatica.fatec.dominio.Banco;
import br.com.ecommerceinformatica.fatec.dominio.Cartao;
import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.Cidade;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.EnderecoCliente;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Estado;
import br.com.ecommerceinformatica.fatec.dominio.Fornecedor;
import br.com.ecommerceinformatica.fatec.dominio.ItensPedido;
import br.com.ecommerceinformatica.fatec.dominio.Pais;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.dominio.StatusCompra;
import br.com.ecommerceinformatica.fatec.dominio.TrocaPedido;
import br.com.ecommerceinformatica.fatec.dominio.Usuario;

public class Util {
	
	// get list itens troca
	public static List<EntidadeDominio> getListItensTrocaADM(HttpServletRequest request){
		List<EntidadeDominio> listItens;
		TrocaPedido troca = new TrocaPedido(null, -1, null, "");
		listItens = new TrocaPedidoDAO().consultar(troca);
		
		return listItens;
		
	}
	//getPedidos
		public static List<EntidadeDominio> getListPedidoADM(HttpServletRequest request){
			List<EntidadeDominio> listPedidos;
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			int idADm = cliente.getId(); 
			
			cliente.setId(0);
			Pedido pedido = new Pedido(null, null, null, null, null, cliente, "", 0, 0, null, "", 0);
			pedido.setId(0);
			
			listPedidos = new PedidoDAO().consultar(pedido);
			
			cliente.setId(idADm);
			
			return listPedidos;
		}
	
	// get categorias ativas
	public static List<EntidadeDominio> getCategoriasAtiva(){
		Categoria categoria = new Categoria("", 0);
		categoria.setTipoFiltroAtivo(1);
		return new CategoriaDAO().consultar(categoria);
	}
	
	// retornar dia de hoje
	public static String getToday() {
		Date dataHoje = new Date(System.currentTimeMillis());
		DateFormat formatarData = new SimpleDateFormat("dd/MM/yyyy");
		
		return formatarData.format(dataHoje);
		
	}
	
	public static HashMap<Integer, EnderecoCliente> getMapEnderecos(List<EnderecoCliente> listEndCobranca, List<EnderecoCliente> listEndEntrega){
		HashMap<Integer, EnderecoCliente> mapEnderecos = new HashMap<Integer, EnderecoCliente>();
		
		if (listEndCobranca != null)
			for (EnderecoCliente endereco : listEndCobranca) 
				if (mapEnderecos.get(endereco.getId()) == null)
					mapEnderecos.put(endereco.getId(), endereco);
			
		
		if (listEndEntrega != null)
			for (EnderecoCliente endereco : listEndEntrega) 
				if (mapEnderecos.get(endereco.getId()) == null)
					mapEnderecos.put(endereco.getId(), endereco);
		
		return mapEnderecos;
	}
	
	// Carregar os fornecedores
	public static List<EntidadeDominio> carregarFornecedores() {
		FornecedorDAO forDao = new FornecedorDAO();
		
		Fornecedor fornecedor = new Fornecedor(null, null, null, null);

		return forDao.consultar(fornecedor);
		
	}
	
	// Carregar as categorias
	public static List<EntidadeDominio> carregarCategorias(){
		CategoriaDAO catDao = new CategoriaDAO();

		// criar categoria para fazer a consulta
		Categoria categoria = new Categoria("", 0);

		return catDao.consultar(categoria);
		
	}
	
	// Carregar os fornecedores ATIVOS
		public static List<EntidadeDominio> carregarFornecedoresAtivos() {
			FornecedorDAO forDao = new FornecedorDAO();
			
			Fornecedor fornecedor = new Fornecedor(null, null, null, null);
			fornecedor.setTipoFiltroAtivo(1);

			return forDao.consultar(fornecedor);
			
		}
		
		// Carregar as categorias ATIVAS
		public static List<EntidadeDominio> carregarCategoriasAtivas(){
			CategoriaDAO catDao = new CategoriaDAO();

			// criar categoria para fazer a consulta
			Categoria categoria = new Categoria("", 0);
			categoria.setTipoFiltroAtivo(1);

			return catDao.consultar(categoria);
			
		}
	
	
	
	public static List<EntidadeDominio> getProdutoAtivo(){
		Produto produto = new Produto(null, 0, "", "", "", "", "", null, 0, "");
		produto.setTipoFiltroAtivo(1);
		return new ProdutoDAO().consultar(produto);
		
	}
	
	public static List<EntidadeDominio> getProduto(){
		return new ProdutoDAO().consultar(null);
		
	}
	
	public static Cliente getCliente() {
		
		Usuario usuario = new Usuario("bbbbbb@nice.com.br", "", true);
		Cliente cliente = new Cliente("", null, "", usuario, "");
		
		EntidadeDominio entidade = new ClienteDAO().consultar(cliente).get(0);
		
		return (Cliente) entidade;
		
		
	}
	
	public static Boolean venceu(String dataVencimento) {				
		try {
			Date dataHoje = new Date(System.currentTimeMillis());
			DateFormat formatarData = new SimpleDateFormat("dd/MM/yyyy"); 
			Date diaVencimento = formatarData.parse(dataVencimento);
		
			return (dataHoje.compareTo(diaVencimento) <= 0) ? false : true;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	
	
	
}
