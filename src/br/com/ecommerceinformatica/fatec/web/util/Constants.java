package br.com.ecommerceinformatica.fatec.web.util;

public class Constants {
	
	public static final String NOME_EMPRESA_CABECALHO = "ECOMMERCE";
	public static final String NOME_EMPRESA_RODAPE = "ECOMMERCE";
	
	public static final String TITULO_SITE = "Ecommerce";
	
	public static final int ENDERECO_ENTREGA = 0;
	public static final int ENDERECO_COBRANCA = 1;
	public static final int ENDERECO_ENTREGA_COBRANCA = 2;
	
	
	
	public static final String CODIGO_PEDIDO = "CODIGO_PEDIDO";
	public static final String ID_TROCA_PEDIDO = "ID_TROCA_PEDIDO";
	
	
	
}
