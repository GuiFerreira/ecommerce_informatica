package br.com.ecommerceinformatica.fatec.web.util;

import java.util.HashMap;

import br.com.ecommerceinformatica.fatec.command.CreateCommand;
import br.com.ecommerceinformatica.fatec.command.DeleteCommand;
import br.com.ecommerceinformatica.fatec.command.ICommand;
import br.com.ecommerceinformatica.fatec.command.ReadCommand;
import br.com.ecommerceinformatica.fatec.command.UpdateCommand;
import br.com.ecommerceinformatica.fatec.web.viewhelper.ClienteVH;
import br.com.ecommerceinformatica.fatec.web.viewhelper.EstoqueVH;
import br.com.ecommerceinformatica.fatec.web.viewhelper.IViewHelperPost;
import br.com.ecommerceinformatica.fatec.web.viewhelper.PedidoVH;
import br.com.ecommerceinformatica.fatec.web.viewhelper.ProdutoVH;

public class ControlerUtil {
	
	public static HashMap<String, ICommand> carregarMapCommands(){
		HashMap<String, ICommand> commands = new HashMap<String, ICommand>();
		
		// 					CADASTRAR
		commands.put("CADASTRAR PRODUTO", new CreateCommand());
		commands.put("CADASTRAR FORNECEDOR", new CreateCommand());
		commands.put("CADASTRAR CATEGORIA", new CreateCommand());
		commands.put("CADASTRAR CLIENTE", new CreateCommand());
		commands.put("CADASTRAR CLIENTE COMPRA", new CreateCommand());
		commands.put("CADASTRAR ENDERECO", new CreateCommand());
		commands.put("CADASTRAR CARTAO", new CreateCommand());
		commands.put("COLOCAR CARRINHO", new CreateCommand());
		commands.put("FINALIZAR PEDIDO", new CreateCommand());
		commands.put("ESQUECEU SENHA", new CreateCommand());
		commands.put("TROCAR ITEM", new CreateCommand());
		commands.put("CANCELAR ITEM", new CreateCommand());
		
		
		
		//					CONSULTA
		commands.put("FORNECEDOR", new ReadCommand());
		commands.put("PRODUTO", new ReadCommand());
		commands.put("CATEGORIA", new ReadCommand());
		commands.put("CLIENTE", new ReadCommand());
		commands.put("PESQUISAR CATEGORIA", new ReadCommand());
		commands.put("PESQUISAR FORNECEDOR", new ReadCommand());
		commands.put("PESQUISAR PRODUTO", new ReadCommand());
		commands.put("PESQUISAR CLIENTE", new ReadCommand());
		commands.put("PESQUISAR ESTOQUE", new ReadCommand());
		commands.put("LOGAR", new ReadCommand());
		
		
		
		//					EXCLUIR
		commands.put("EXCLUIR PRODUTO", new DeleteCommand());
		commands.put("EXCLUIR CATEGORIA", new DeleteCommand());
		commands.put("EXCLUIR FORNECEDOR", new DeleteCommand());
		commands.put("EXCLUIR CLIENTE", new DeleteCommand());
		commands.put("EXCLUIR CARRINHO", new DeleteCommand());
		
		
		
		//					ALTERAR 
		commands.put("ALTERAR PRODUTO", new UpdateCommand());
		commands.put("ALTERAR CATEGORIA", new UpdateCommand());
		commands.put("ALTERAR FORNECEDOR", new UpdateCommand());
		commands.put("ALTERAR CLIENTE", new UpdateCommand());
		commands.put("ALTERAR SENHA", new UpdateCommand());
		commands.put("ALTERAR CARRINHO", new UpdateCommand());
		commands.put("REGISTRAR COMPRA", new UpdateCommand());
		commands.put("EDITAR PEDIDO", new UpdateCommand());
		commands.put("EDITAR TROCA", new UpdateCommand());
		
		
		
		return commands;
	}

	
	public static HashMap<String, IViewHelperPost> carregarMapView(){
		HashMap<String, IViewHelperPost> views = new HashMap<String, IViewHelperPost>();
		
		views.put("/Ecommerce_Informatica/CadastrarProduto", new ProdutoVH());
		views.put("/Ecommerce_Informatica/CadastrarFornecedor", new ProdutoVH());
		views.put("/Ecommerce_Informatica/CadastrarCategoria", new ProdutoVH());
		views.put("/Ecommerce_Informatica/CadastarCliente", new ClienteVH());
		views.put("/Ecommerce_Informatica/CadastarEndereco", new ClienteVH());
		
		views.put("/Ecommerce_Informatica/pesquisarCategoria", new ProdutoVH());
		views.put("/Ecommerce_Informatica/pesquisarFornecedor", new ProdutoVH());
		views.put("/Ecommerce_Informatica/pesquisarProduto", new ProdutoVH());
		views.put("/Ecommerce_Informatica/pesquisarCliente", new ClienteVH());
		views.put("/Ecommerce_Informatica/pesquisarEstoque", new EstoqueVH());
		
		views.put("/Ecommerce_Informatica/ExcluirProduto", new ProdutoVH());
		views.put("/Ecommerce_Informatica/ExcluirCategoria", new ProdutoVH());
		views.put("/Ecommerce_Informatica/ExcluirFornecedor", new ProdutoVH());
		views.put("/Ecommerce_Informatica/ExcluirCliente", new ClienteVH());
		
		views.put("/Ecommerce_Informatica/AlterarProduto", new ProdutoVH());
		views.put("/Ecommerce_Informatica/AlterarCategoria", new ProdutoVH());
		views.put("/Ecommerce_Informatica/AlterarFornecedor", new ProdutoVH());
		views.put("/Ecommerce_Informatica/AlterarCliente", new ClienteVH());
		views.put("/Ecommerce_Informatica/AlterarSenha", new ClienteVH());
		views.put("/Ecommerce_Informatica/AlterarEndereco", new ClienteVH());
		views.put("/Ecommerce_Informatica/AlterarStatusPedido", new PedidoVH());
		views.put("/Ecommerce_Informatica/AlterarStatusTroca", new PedidoVH());

		views.put("/Ecommerce_Informatica/LoginUsuario", new ClienteVH());
		views.put("/Ecommerce_Informatica/Cartao", new ClienteVH());
		
		views.put("/Ecommerce_Informatica/Carrinho", new ProdutoVH());
		views.put("/Ecommerce_Informatica/Compra", new PedidoVH());
		views.put("/Ecommerce_Informatica/SolicitarTroca", new PedidoVH());
		views.put("/Ecommerce_Informatica/SolicitarCancelamento", new PedidoVH());
		views.put("/Ecommerce_Informatica/Estoque", new EstoqueVH());
		
		views.put("/Ecommerce_Informatica/EsqueceuSenha", new ClienteVH());
		
		
		return views;
		
		
	}
	
	
}
