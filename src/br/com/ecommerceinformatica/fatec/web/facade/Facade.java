package br.com.ecommerceinformatica.fatec.web.facade;

import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dao.IDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.web.util.FacadeUtil;

public class Facade implements IFacade {

	Map<String, IDAO> mapDao;
	Map<String, Map<String, List<IStrategy>>> mapRgns;
	Resultado resultado;

	public Facade() {
		// instanciar o mapa e colocar o retorno uma instancia de objeto do tipo IDAO
		mapDao = FacadeUtil.carregarMapDao();
		
		mapRgns = FacadeUtil.carregarMapRegrasNegocio();
	}

	private String executarRGN(EntidadeDominio entidade, List<IStrategy> listaRGN) {

		StringBuilder erroRgn = new StringBuilder();
		erroRgn.append("");

		for (IStrategy rgn : listaRGN) {
			String erro = rgn.processar(entidade);
			if (erro != null && !erro.trim().equals(""))
				erroRgn.append(erro);
		}

		return erroRgn.toString();
	}

	@Override
	public Resultado create(EntidadeDominio entidade) {
		resultado = new Resultado();
		// DAO do objeto instanciado
		IDAO dao = mapDao.get(entidade.getClass().getName());

		// lista com as regras de neg�cio de cada classe E para a op��o BEFORE INSERT
		Map<String, List<IStrategy>> mapRegrasNegocio = mapRgns.get(entidade.getClass().getName());
		List<IStrategy> rgnBefore = null;
		List<IStrategy> rgnAfter = null;
		String erro = "";

		
		// pegar regra de neg�cio Before Insert
		if (mapRegrasNegocio != null) {
			rgnBefore = mapRegrasNegocio.get("BEFORE INSERT");
			rgnAfter = mapRegrasNegocio.get("AFTER INSERT");			
		}

		// Rodar as regras de neg�cio Before Insert
		if (rgnBefore != null)
			erro = executarRGN(entidade, rgnBefore);

		if (erro.equals("")) {
			erro += dao.salvar(entidade);
			
			// n�o teve erro ao salvar nem nas regras de neg�cio
			// rodar as after insert
			if (erro.equals("") && rgnAfter != null)
				executarRGN(entidade, rgnAfter);
			
		}
		
		resultado.setMsgErro(erro);
		resultado.add(entidade);
		
		return resultado;
		
	}

	@Override
	public Resultado update(EntidadeDominio entidade) {
		resultado = new Resultado();
		// DAO do objeto instanciado
		IDAO dao = mapDao.get(entidade.getClass().getName());

		// lista com as regras de neg�cio de cada classe E para a op��o BEFORE INSERT
		Map<String, List<IStrategy>> mapRegrasNegocio = mapRgns.get(entidade.getClass().getName());
		List<IStrategy> rgnBefore = null;
		List<IStrategy> rgnAfter = null;
		String erro = "";

		
		// pegar regra de neg�cio Before Update
		if (mapRegrasNegocio != null) {
			rgnBefore = mapRegrasNegocio.get("BEFORE UPDATE");
			rgnAfter = mapRegrasNegocio.get("AFTER UPDATE");			
		}

		// Rodar as regras de neg�cio Before Update
		if (rgnBefore != null)
			erro = executarRGN(entidade, rgnBefore);

		if (erro.equals("")) {
			erro += dao.alterar(entidade);
			
			// n�o teve erro ao salvar nem nas regras de neg�cio
			// rodar as after update
			if (erro.equals("") && rgnAfter != null)
				executarRGN(entidade, rgnAfter);
			
		}
		
		resultado.setMsgErro(erro);
		resultado.add(entidade);
		
		return resultado;
	}

	@Override
	public Resultado delete(EntidadeDominio entidade) {
		resultado = new Resultado();
		// DAO do objeto instanciado
		IDAO dao = mapDao.get(entidade.getClass().getName());

		// lista com as regras de neg�cio de cada classe E para a op��o BEFORE INSERT
		Map<String, List<IStrategy>> mapRegrasNegocio = mapRgns.get(entidade.getClass().getName());
		List<IStrategy> rgnBefore = null;
		List<IStrategy> rgnAfter = null;
		String erro = "";

		
		// pegar regra de neg�cio Before Delete
		if (mapRegrasNegocio != null) {
			rgnBefore = mapRegrasNegocio.get("BEFORE DELETE");
			rgnAfter = mapRegrasNegocio.get("AFTER DELETE");			
		}

		// Rodar as regras de neg�cio Before Delete
		if (rgnBefore != null)
			erro = executarRGN(entidade, rgnBefore);

		if (erro.equals("")) {
			erro += dao.excluir(entidade);
			
			// n�o teve erro ao salvar nem nas regras de neg�cio
			// rodar as after delete
			if (erro.equals("") && rgnAfter != null)
				executarRGN(entidade, rgnAfter);
			
		}
		
		resultado.setMsgErro(erro);
		resultado.add(entidade);
		
		return resultado;
	}

	@Override
	public Resultado read(EntidadeDominio entidade) {
		resultado = new Resultado();
		// DAO do objeto instanciado
		IDAO dao = mapDao.get(entidade.getClass().getName());

		// lista com as regras de neg�cio de cada classe E para a op��o BEFORE READ
		Map<String, List<IStrategy>> mapRegrasNegocio = mapRgns.get(entidade.getClass().getName());
		List<IStrategy> rgnBefore = null;
		List<IStrategy> rgnAfter = null;
		String erro = "";

		
		// pegar regra de neg�cio Before Read
		if (mapRegrasNegocio != null) {
			rgnBefore = mapRegrasNegocio.get("BEFORE READ");
			rgnAfter = mapRegrasNegocio.get("AFTER READ");			
		}

		// Rodar as regras de neg�cio Before Read
		if (rgnBefore != null)
			erro = executarRGN(entidade, rgnBefore);

		if (erro.equals("")) {
			resultado.setEntidades(dao.consultar(entidade));
			
			// n�o teve erro ao salvar nem nas regras de neg�cio
			// rodar as after read
			if (erro.equals("") && rgnAfter != null)
				executarRGN(entidade, rgnAfter);
			
		}
		
		resultado.setMsgErro(erro);
		
		
		return resultado;
	}

}
