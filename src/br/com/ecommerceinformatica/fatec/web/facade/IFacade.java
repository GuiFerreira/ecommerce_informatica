package br.com.ecommerceinformatica.fatec.web.facade;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;

public interface IFacade {
	
	public Resultado create(EntidadeDominio entidade);
	public Resultado update(EntidadeDominio entidade);
	public Resultado delete(EntidadeDominio entidade);
	public Resultado read(EntidadeDominio entidade);
	
}
