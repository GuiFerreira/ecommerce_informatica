package br.com.ecommerceinformatica.fatec.web.controle;


import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ecommerceinformatica.fatec.command.ICommand;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;
import br.com.ecommerceinformatica.fatec.web.util.ControlerUtil;
import br.com.ecommerceinformatica.fatec.web.viewhelper.DoGetVH;
import br.com.ecommerceinformatica.fatec.web.viewhelper.IViewHelperGet;
import br.com.ecommerceinformatica.fatec.web.viewhelper.IViewHelperPost;

/**
 * Servlet implementation class Controle
 */
@WebServlet("/Controle")
public class Controle extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Map<String, ICommand> commands;
	private Map<String, IViewHelperPost> views;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Controle() {
		super();
		commands = ControlerUtil.carregarMapCommands();
		views = ControlerUtil.carregarMapView();
		
	}

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		IViewHelperGet vh = new DoGetVH();
		
		vh.setAttribute(request);
		
		vh.setView(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		
		String uri = request.getRequestURI();
		String operacao = request.getParameter("OPERACAO");
		
//		System.out.println("Do Post: \n\tURI = " + uri + "\n\tOperacao = " + operacao);
		
		Resultado resultado = null;
		EntidadeDominio entidade = null;

		// SETAR O OBJETO DEPENDENDO DE QM SEJA
		IViewHelperPost vh = views.get(uri);
		entidade = vh.getEntidade(request);
		
		ICommand cmd = commands.get(operacao);
		resultado = cmd.executar(entidade);

		// definir para onde vai ser direcionado minha pagina
		vh.setView(resultado, request, response);

	}

}
