package br.com.ecommerceinformatica.fatec.web.viewhelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ecommerceinformatica.fatec.dominio.Cartao;
import br.com.ecommerceinformatica.fatec.dominio.CartaoPedido;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Cupom;
import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensCarrinho;
import br.com.ecommerceinformatica.fatec.dominio.ItensPedido;
import br.com.ecommerceinformatica.fatec.dominio.ItensTroca;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;
import br.com.ecommerceinformatica.fatec.dominio.StatusCompra;
import br.com.ecommerceinformatica.fatec.dominio.StatusTrocaCancelamento;
import br.com.ecommerceinformatica.fatec.dominio.TrocaPedido;
import br.com.ecommerceinformatica.fatec.singleton.CarrinhoSingleton;
import br.com.ecommerceinformatica.fatec.web.util.Util;

public class PedidoVH implements IViewHelperPost {

	public EntidadeDominio getEntidade(HttpServletRequest request) {

		String operacao = request.getParameter("OPERACAO");
		
		if (operacao.equals("FINALIZAR PEDIDO")) {
			CarrinhoSingleton carrinho = (CarrinhoSingleton) request.getSession().getAttribute("carrinho");
			List<CartaoPedido> listCartaoPedido = new ArrayList<CartaoPedido>();
			Map<String, String[]> mapParametros = request.getParameterMap();
			Map<String, Cartao> mapCartaoCliente = new HashMap<String, Cartao>();
			double valorFrete = 0;
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			if (cliente.getListaCartoes() != null)
				for (Cartao cartao : cliente.getListaCartoes())
					mapCartaoCliente.put(cartao.getNumero(), cartao);
			
			int idEndEntrega = Integer.valueOf(request.getParameter("usarEnderecoEntrega"));
			int idEndCobranca = Integer.valueOf(request.getParameter("usarEnderecoCobranca"));
			Endereco endEntrega = new Endereco("", "", "", null, "", null, "", "");
			endEntrega.setId(idEndEntrega);
			Endereco endCobranca = new Endereco("", "", "", null, "", null, "", "");
			endCobranca.setId(idEndCobranca);
					
			
			//-------------------------
			if (mapParametros.get("cartaoCredito") != null) {
				int qntCartoes = mapParametros.get("cartaoCredito").length;
				// Cartao de Cr�dito
				for (String numCartao : mapParametros.get("cartaoCredito")) {
					int qntParcelas = Integer.valueOf(request.getParameter("qntParcelas:" + numCartao));
					CartaoPedido cartaoPedido = new CartaoPedido(0, mapCartaoCliente.get(numCartao), qntParcelas,
							0);
					listCartaoPedido.add(cartaoPedido);
				}
				
			}
			
			
			String codigoCupom = request.getParameter("cupomDesconto");
			Cupom cupomDesconto = new Cupom((codigoCupom != null && codigoCupom != "0") ? codigoCupom : "");
			cupomDesconto.setId((codigoCupom != null && codigoCupom != "0" && !codigoCupom.trim().equals("")) ? Integer.valueOf(codigoCupom) : 0);
			
			
			
			StatusCompra status = StatusCompra.PAGAMENTO_PENDENTE;
			
			List<EntidadeDominio> listProdutos = CarrinhoSingleton.getInstance().getInstances();
			
			List<ItensPedido> listItensPedido = new ArrayList<ItensPedido>();
			
			double precoTotalCompra = 0;
			
			for (EntidadeDominio e : listProdutos) {
				ItensCarrinho itemCarrinho = (ItensCarrinho) e;
				ItensPedido itemPedido = new ItensPedido(0, itemCarrinho.getProduto(),
														itemCarrinho.getPrecoItem() / itemCarrinho.getQuantidade(), 
														itemCarrinho.getPrecoItem(),
														itemCarrinho.getQuantidade(),
														Util.getToday());
				itemPedido.setId(itemCarrinho.getId());
				listItensPedido.add(itemPedido);
				precoTotalCompra += itemPedido.getPrecoTotal();
				
			}
			
			precoTotalCompra += valorFrete;
					
					
			Pedido pedido = new Pedido(listItensPedido, cupomDesconto, status, endEntrega, endCobranca, cliente, Util.getToday(), precoTotalCompra, 0, listCartaoPedido, "", valorFrete);
			
			return pedido;
			
		}
		
		
		else if (operacao.equals("TROCAR ITEM") || operacao.equals("CANCELAR ITEM")) {
			Pedido pedido = (Pedido) request.getSession().getAttribute("pedido");
			List<ItensTroca> listItensTroca = new ArrayList<ItensTroca>();
			Map<String, String[]> mapParametros = request.getParameterMap();
			Map<Integer, ItensPedido> mapItensItensPedidoByIdProduto = new HashMap<Integer, ItensPedido>();
			for (ItensPedido item : pedido.getListProdutosPedido()) {
				mapItensItensPedidoByIdProduto.put(item.getProduto().getId(), item);
			}
			
			for (String idProduto : mapParametros.get("idProduto")) {
				int qntTrocada = Integer.parseInt(request.getParameter("qntCancelado:" + idProduto));
				ItensPedido itemPedido = mapItensItensPedidoByIdProduto.get(Integer.parseInt(idProduto));
				ItensTroca item = new ItensTroca(itemPedido.getProduto(), qntTrocada, itemPedido.getPrecoItem(), "",
						(operacao.equals("CANCELAR ITEM") ? StatusTrocaCancelamento.CANCELAMENTO_PENDENTE :StatusTrocaCancelamento.TROCA_PENDENTE));
				listItensTroca.add(item);
			}
			
			TrocaPedido troca = new TrocaPedido(pedido, pedido.getCliente().getId(), listItensTroca,
					Util.getToday());
			
			return troca;
		}
		
		else if (operacao.equals("EDITAR PEDIDO")) {
			int idPedido = Integer.valueOf(request.getParameter("idPedido"));
			String status = request.getParameter("statusPedido");
			Pedido pedido = null;
			List<EntidadeDominio> listPedidos = (List<EntidadeDominio>) request.getSession().getAttribute("listPedidos");
			for (EntidadeDominio ent : listPedidos) { 
				Pedido ped = (Pedido) ent;
				if (idPedido == ped.getId()) {
					pedido = ped;
					break;
				}
			}
			
			pedido.setStatusCompra(StatusCompra.valueOf(status));
			return pedido;
			
		}
		
		else if (operacao.equals("EDITAR TROCA")) {
			int idTroca = Integer.valueOf(request.getParameter("idTroca"));
			int idProduto = Integer.valueOf(request.getParameter("idProduto"));
			String status = request.getParameter("statusTrocaPedido");
			Cliente cliente = new Cliente("", null, "", null, "");
			cliente.setId(Integer.valueOf(request.getParameter("idCliente")));
			
			TrocaPedido troca = null;

			List<EntidadeDominio> listTrocaFront = (List<EntidadeDominio>) request.getSession().getAttribute("listItensTroca");
			for (EntidadeDominio ent : listTrocaFront) {
				TrocaPedido t = (TrocaPedido) ent;
				  
				if (t.getId() == idTroca) {
					for (ItensTroca i: t.getListItensTroca()) {
						if (i.getProduto().getId() == idProduto) {
							troca = t;
							break;
						}
						
					}
					
					break;
				}
				
			}
			
			troca.getListItensTroca().get(0).setStatus(StatusTrocaCancelamento.valueOf(status));
			
			
			return troca;
		}
		
		
		return null;
	}


	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String operacao = request.getParameter("OPERACAO");
		RequestDispatcher dispatcher = null;
		
		if (resultado.getMsgErro() != null && !resultado.getMsgErro().trim().equals("")) {
			request.setAttribute("erro", resultado.getMsgErro());
			dispatcher = request.getRequestDispatcher("erro.jsp");
			
		}
		
		else if (operacao.trim().equals("FINALIZAR PEDIDO")) {
			CarrinhoSingleton.getInstance().limparCarrinho();
			dispatcher = request.getRequestDispatcher("index.jsp");
		}
		
		else if (operacao.trim().equals("CANCELAR ITEM") || operacao.trim().equals("TROCAR ITEM")) {
			dispatcher = request.getRequestDispatcher("index.jsp");
		}
		
		
		else if (operacao.trim().equals("EDITAR PEDIDO") || operacao.trim().equals("EDITAR TROCA")) {
			response.sendRedirect("indexAdemir.jsp");
			return;
		}
		
		
		
		dispatcher.forward(request, response);
		
	}

}
