package br.com.ecommerceinformatica.fatec.web.viewhelper;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ecommerceinformatica.fatec.dao.EstoqueDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensEstoque;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;

public class EstoqueVH implements IViewHelperPost {

	@Override
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		
		String operacao = request.getParameter("OPERACAO");
		
		if (operacao.equals("PESQUISAR ESTOQUE")) {
			int idProduto = Integer.valueOf(request.getParameter("idProduto"));
			
			ItensEstoque itemEstoque = new ItensEstoque(null, 0, "");
			itemEstoque.setId(idProduto);
			
			return itemEstoque;
			
		}
		
		else if(operacao.equals("REGISTRAR COMPRA")) {
			int idProduto = Integer.parseInt(request.getParameter("idProduto"));
			int qntComprada = Integer.parseInt(request.getParameter("qntComprada"));
			
			ItensEstoque itemConsulta = new ItensEstoque(null, 0, "");
			itemConsulta.setId(idProduto);
			
			EstoqueDAO estDAO = new EstoqueDAO();
			
			ItensEstoque itemEstoqueBD = (ItensEstoque) estDAO.consultar(itemConsulta).get(0);
			
			// n�o vai ser nulo pq qnd eu criar um novo produto ele vai ser criado automaticamente
			itemEstoqueBD.setQndEstoque(itemEstoqueBD.getQndEstoque() + qntComprada);
			
			return itemEstoqueBD;
			
		}
		
		
		return null;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		RequestDispatcher dispatcher = null;

		String operacao = request.getParameter("OPERACAO");

		if (resultado.getMsgErro() != null && !resultado.getMsgErro().trim().equals("")) {
			request.setAttribute("erro", resultado.getMsgErro());
			dispatcher = request.getRequestDispatcher("erro.jsp");
		}
		

		else if (operacao.equals("REGISTRAR COMPRA")) {
			response.sendRedirect("indexAdemir.jsp");
			return;
		}
		
		else if (operacao.equals("PESQUISAR ESTOQUE")) {
			request.getSession().setAttribute("resultado", resultado);
			dispatcher = request.getRequestDispatcher("resultadoPesquisa.jsp");
		}
		
		dispatcher.forward(request, response);

	}

}
