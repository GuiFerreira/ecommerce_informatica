package br.com.ecommerceinformatica.fatec.web.viewhelper;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;
import br.com.ecommerceinformatica.fatec.dominio.Usuario;

public class UsuarioVH implements IViewHelperPost {

	@Override
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		
		String operacao = request.getParameter("OPERACAO");
		
		if (operacao.trim().equals("LOGAR")) {
			String email = request.getParameter("login");
			String senha = request.getParameter("senha");
			Usuario usuario = new Usuario(email, senha, false);
			usuario.setId(0);
			
			Cliente cliente = new Cliente("", null, "", usuario, "");
			cliente.setId(0);
			
			return cliente;
			
		}
		
		
		return null;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		String operacao = request.getParameter("OPERACAO");
		
		RequestDispatcher dispatcher = null;
		
		if (!resultado.getMsgErro().equals("")) {
			request.setAttribute("erro", resultado.getMsgErro());
			dispatcher = request.getRequestDispatcher("erro.jsp");
		}
		
		else if (operacao.equals("LOGAR")) {
			List<EntidadeDominio> listCliente = resultado.getEntidades();

			if (!listCliente.isEmpty()) {
				Cliente cliente = (Cliente) listCliente.get(0);
				request.getSession().setAttribute("clienteLogado", cliente);
				dispatcher = request.getRequestDispatcher("index.jsp");
			}
			else
				dispatcher = request.getRequestDispatcher("registrar.jsp");
			
			
		}
		

		dispatcher.forward(request, response);
		
	}

}
