package br.com.ecommerceinformatica.fatec.web.viewhelper;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ecommerceinformatica.fatec.dao.ProdutoDAO;
import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.Cidade;
import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Estado;
import br.com.ecommerceinformatica.fatec.dominio.Fornecedor;
import br.com.ecommerceinformatica.fatec.dominio.ItensCarrinho;
import br.com.ecommerceinformatica.fatec.dominio.Logradouro;
import br.com.ecommerceinformatica.fatec.dominio.Pais;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;
import br.com.ecommerceinformatica.fatec.singleton.CarrinhoSingleton;

public class ProdutoVH implements IViewHelperPost {

	@Override
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		
//		System.out.println("\tProdutoVH");

		String operacao = request.getParameter("OPERACAO");

		// ---------------------------------------------------------------------------------------------------
		// PRODUTO
		if (operacao.equals("CADASTRAR PRODUTO")) {
			String nomeProduto = request.getParameter("nomeProduto");
			String codProduto = request.getParameter("codigoProduto");
			String descricao = request.getParameter("descricaoProduto");
			String especificacoes = request.getParameter("especificacoesProduto");
			Double precoComprado = Double.parseDouble(request.getParameter("precoCompra"));
			String uriImagem = "img/produtos/" + request.getParameter("imagemProduto");

			int idFornecedor = Integer.parseInt(request.getParameter("fornecedorProduto"));
			String marca = request.getParameter("marcaProduto");

			int idCategoria = Integer.parseInt(request.getParameter("categoriaProduto"));

			Categoria categoria = new Categoria("", 0);
			categoria.setId(idCategoria);
			Fornecedor fornecedor = new Fornecedor("", null, null, null);
			fornecedor.setId(idFornecedor);

			Produto produto = new Produto(categoria, precoComprado, nomeProduto, codProduto, descricao, especificacoes,
					marca, fornecedor, 0, uriImagem);

			return produto;

		}

		else if (operacao.equals("PESQUISAR PRODUTO")) {
			String nomeProduto = request.getParameter("nomeProduto");
			String codProduto = request.getParameter("codigoProduto");
			String descricao = request.getParameter("descricaoProduto");
			String especificacoes = request.getParameter("especificacoesProduto");
			int ativoInativo = Integer.parseInt(request.getParameter("ativo"));

			int idFornecedor = Integer.parseInt(request.getParameter("fornecedorProduto"));
			String marca = request.getParameter("marcaProduto");

			int idCategoria = Integer.parseInt(request.getParameter("categoriaProduto"));

			Categoria categoria = new Categoria("", 0);
			categoria.setId(idCategoria);
			Fornecedor fornecedor = new Fornecedor("", null, null, null);
			fornecedor.setId(idFornecedor);

			Produto produto = new Produto(categoria, 0, nomeProduto, codProduto, descricao, especificacoes, marca,
					fornecedor, 0, "");
			
			produto.setTipoFiltroAtivo(ativoInativo);

			return produto;

		} else if (operacao.equals("EXCLUIR PRODUTO")) {

			Produto produtoSessao = (Produto) request.getSession().getAttribute("produto");
			Produto produto = new Produto(null, 0, "", "", "", "", "", null, 0, "");
			produto.setId(produtoSessao.getId());

			return produto;

		} else if (operacao.equals("ALTERAR PRODUTO")) {
			String nomeProduto = request.getParameter("nomeProduto");
			String codProduto = request.getParameter("codigoProduto");
			String descricao = request.getParameter("descricaoProduto");
			String especificacoes = request.getParameter("especificacoesProduto");
			Double precoComprado = Double.parseDouble(request.getParameter("precoCompra"));

			int idFornecedor = Integer.parseInt(request.getParameter("fornecedorProduto"));
			String marca = request.getParameter("marcaProduto");

			int idCategoria = Integer.parseInt(request.getParameter("categoriaProduto"));

			Categoria categoria = new Categoria("", 0);
			categoria.setId(idCategoria);
			Fornecedor fornecedor = new Fornecedor("", null, null, null);
			fornecedor.setId(idFornecedor);

			Produto produto = new Produto(categoria, precoComprado, nomeProduto, codProduto, descricao, especificacoes,
					marca, fornecedor, Double.parseDouble(request.getParameter("descontoProduto")), "");
			
			String ativo = request.getParameter("ativo");
			produto.setFlgAtivo((ativo == null) ? false : true);

			produto.setId(Integer.parseInt(request.getParameter("idProduto")));

			return produto;

		}

		// ---------------------------------------------------------------------------------------------------
		// FORNECEDOR
		else if (operacao.equals("CADASTRAR FORNECEDOR")) {
			String CNPJ = request.getParameter("cnpj");

			String nome = request.getParameter("nomeFornecedor");

			String email = request.getParameter("email");
			String numTelefone = request.getParameter("telefone");
			String ddd = request.getParameter("DDD");

			String nomePais = request.getParameter("pais");
			String sgPais = request.getParameter("siglaPais");
			String nomeEstado = request.getParameter("estado");
			String sgEstado = request.getParameter("siglaEstado");
			String nomeCidade = request.getParameter("cidade");
			String rua = request.getParameter("rua");
			String bairro = request.getParameter("bairro");
			String numero = request.getParameter("numero");
			String cep = request.getParameter("cep");

			Contato contato = new Contato(ddd, numTelefone, email);

			String tpLogradouro = request.getParameter("tipoLogradouro");
			String nomeLogradouro = request.getParameter("logradouro");
			Logradouro logradouro = new Logradouro(tpLogradouro, nomeLogradouro);

			Pais pais = new Pais(nomePais, sgPais);
			Estado estado = new Estado(nomeEstado, sgEstado, pais);
			Cidade cidade = new Cidade(nomeCidade, estado);
			String observacoes = request.getParameter("observacoes");
			Endereco endereco = new Endereco(rua, bairro, numero, cidade, cep, logradouro, observacoes, "");

			Fornecedor fornecedor = new Fornecedor(nome, contato, CNPJ, endereco);

			return fornecedor;

		} else if (operacao.equals("PESQUISAR FORNECEDOR")) {
			String CNPJ = request.getParameter("cnpj");

			String nome = request.getParameter("nomeFornecedor");

			Fornecedor fornecedor = new Fornecedor(nome, null, CNPJ, null);

			return fornecedor;

		} else if (operacao.equals("EXCLUIR FORNECEDOR")) {
			Fornecedor fornecedorSessao = (Fornecedor) request.getSession().getAttribute("fornecedor");
			Fornecedor fornecedor = new Fornecedor("", null, "", null);
			fornecedor.setId(fornecedorSessao.getId());

			return fornecedor;
		}

		else if (operacao.equals("ALTERAR FORNECEDOR")) {
			String CNPJ = request.getParameter("cnpj");

			String nome = request.getParameter("nomeFornecedor");

			String email = request.getParameter("email");
			String numTelefone = request.getParameter("telefone");
			String ddd = request.getParameter("DDD");

			String nomePais = request.getParameter("pais");
			String sgPais = request.getParameter("siglaPais");
			String nomeEstado = request.getParameter("estado");
			String sgEstado = request.getParameter("siglaEstado");
			String nomeCidade = request.getParameter("cidade");
			String rua = request.getParameter("rua");
			String bairro = request.getParameter("bairro");
			String numero = request.getParameter("numero");
			String cep = request.getParameter("cep");

			Contato contato = new Contato(ddd, numTelefone, email);
			contato.setId(Integer.parseInt(request.getParameter("idContato")));

			String tpLogradouro = request.getParameter("tipoLogradouro");
			String nomeLogradouro = request.getParameter("logradouro");
			Logradouro logradouro = new Logradouro(tpLogradouro, nomeLogradouro);

			Pais pais = new Pais(nomePais, sgPais);
			Estado estado = new Estado(nomeEstado, sgEstado, pais);
			Cidade cidade = new Cidade(nomeCidade, estado);
			String observacoes = request.getParameter("observacoes");
			Endereco endereco = new Endereco(rua, bairro, numero, cidade, cep, logradouro, observacoes, "");
			endereco.setId(Integer.parseInt(request.getParameter("idEndereco")));

			Fornecedor fornecedor = new Fornecedor(nome, contato, CNPJ, endereco);
			fornecedor.setId(Integer.parseInt(request.getParameter("idFornecedor")));

			return fornecedor;

		}

		// ---------------------------------------------------------------------------------------------------
		// CATEGORIA
		else if (operacao.equals("CADASTRAR CATEGORIA")) {
			String nomeCategoria = request.getParameter("nome");
			double margemLucro = Double.parseDouble(request.getParameter("margemLucro"));

			Categoria categoria = new Categoria(nomeCategoria, margemLucro);

			return categoria;

		}

		else if (operacao.equals("PESQUISAR CATEGORIA")) {
			String nomeCategoria = request.getParameter("nome");
			double margemLucro = Double.parseDouble(request.getParameter("margemLucro"));
			
			Categoria categoria = new Categoria(nomeCategoria, margemLucro);
			
			categoria.setTipoFiltroAtivo( Integer.parseInt(request.getParameter("ativo")) );
			
			return categoria;

		}

		else if (operacao.equals("EXCLUIR CATEGORIA")) {
			Categoria categoriaSessao = (Categoria) request.getSession().getAttribute("categoria");
			Categoria categoria = new Categoria(null, 0);
			categoria.setId(categoriaSessao.getId());

			return categoria;

		}

		else if (operacao.equals("ALTERAR CATEGORIA")) {
			
			String nomeCategoria = request.getParameter("nome");
			double margemLucro = Double.parseDouble(request.getParameter("margemLucro"));

			Categoria categoria = new Categoria(nomeCategoria, margemLucro);
			categoria.setId(Integer.parseInt(request.getParameter("id")));
			
			String ativo = request.getParameter("ativo");
			categoria.setFlgAtivo((ativo == null) ? false : true);
			
			return categoria;

		}
		
		else if(operacao.equals("COLOCAR CARRINHO")) {
			Produto produto = (Produto) request.getSession().getAttribute("produto");
			int idProduto = produto.getId();
			ItensCarrinho carrinho = new ItensCarrinho(produto, 1, 0);
			carrinho.setPrecoItem(carrinho.getQuantidade() * 
						(
								(carrinho.getProduto().getPrecoComprado() * carrinho.getProduto().getCategoria().getMargemLucro())
								- carrinho.getProduto().getDesconto())
						);
			carrinho.setId(idProduto);
			
			return carrinho;
		}
		
		else if(operacao.equals("ALTERAR CARRINHO")) {
			Produto produto = new Produto(null, 0, "", "", "", "", "", null, 0, "");
			produto.setId(Integer.parseInt(request.getParameter("idProdutoAlterar")));
			produto = (Produto) new ProdutoDAO().consultar(produto).get(0);
			
			return new ItensCarrinho(produto, Integer.parseInt(request.getParameter("quantidadeProduto")), 0);
		}
		
		else if(operacao.equals("EXCLUIR CARRINHO")) {
			int id = Integer.parseInt(request.getParameter("idProdutoAlterar"));
			Produto produto = new Produto(null, 0, "", "", "", "", "", null, 0, "");
			produto.setId(id);
			
			return new ItensCarrinho(produto, 0, 0);
			
		}

		return null;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		RequestDispatcher dispatcher = null;

		String operacao = request.getParameter("OPERACAO");

		if (resultado.getMsgErro() != null && !resultado.getMsgErro().trim().equals("")) {
			if (resultado.getMsgErro().contains("Produto j� cadastrado")) {
				response.sendRedirect("carrinho.jsp");
				return;
			}
			request.setAttribute("erro", resultado.getMsgErro());
			dispatcher = request.getRequestDispatcher("erro.jsp");
		}

		// ---------------------------------------------------------------------------------------------
		// CADASTRAR E RESULTADO
		else if (operacao.equals("CADASTRAR PRODUTO")) {
			Produto produto = (Produto) resultado.getEntidades().get(0);
			request.getSession().setAttribute("produto", produto);
			dispatcher = request.getRequestDispatcher("produtoInformacoes.jsp");
		}

		else if (operacao.equals("CADASTRAR FORNECEDOR")) {
			// vai direcionar para a pagina desse fornecedor cadastrado
			Fornecedor fornecedor = (Fornecedor) resultado.getEntidades().get(0);
			request.getSession().setAttribute("fornecedor", fornecedor);
			dispatcher = request.getRequestDispatcher("fornecedorInformacoes.jsp");
		}

		else if (operacao.equals("CADASTRAR CATEGORIA")) {
			Categoria categoria = (Categoria) resultado.getEntidades().get(0);
			request.getSession().setAttribute("categoria", categoria);
			dispatcher = request.getRequestDispatcher("categoriaInformacoes.jsp");
		}
		else if (operacao.equals("FORNECEDOR")) {
			// vai direcionar para a pagina desse fornecedor cadastrado
			Fornecedor fornecedor = (Fornecedor) resultado.getEntidades().get(0);
			request.getSession().setAttribute("fornecedor", fornecedor);
			dispatcher = request.getRequestDispatcher("fornecedorInformacoes.jsp");
		}

		else if (operacao.equals("CATEGORIA")) {
			Categoria categoria = (Categoria) resultado.getEntidades().get(0);
			request.getSession().setAttribute("categoria", categoria);
			dispatcher = request.getRequestDispatcher("categoriaInformacoes.jsp");
		}

		// ---------------------------------------------------------------------------------------------
		// PESQUISAR para RESULTADO
		else if (operacao.equals("PESQUISAR PRODUTO")) {
			request.getSession().setAttribute("resultado", resultado);
			dispatcher = request.getRequestDispatcher("resultadoPesquisa.jsp");
		}

		else if (operacao.equals("PESQUISAR FORNECEDOR")) {
			request.getSession().setAttribute("resultado", resultado);
			dispatcher = request.getRequestDispatcher("resultadoPesquisa.jsp");
		}

		else if (operacao.equals("PESQUISAR CATEGORIA")) {
			request.getSession().setAttribute("resultado", resultado);
			dispatcher = request.getRequestDispatcher("resultadoPesquisa.jsp");
		}

		// ---------------------------------------------------------------------------------------------
		// ALTERAR
		else if(operacao.equals("ALTERAR PRODUTO")) {
			Produto produto = (Produto) resultado.getEntidades().get(0);
			request.getSession().setAttribute("produto", produto);
			dispatcher = request.getRequestDispatcher("indexAdemir.jsp");
		}
		
		else if (operacao.equals("ALTERAR CATEGORIA")) {
			Categoria categoria = (Categoria) resultado.getEntidades().get(0);
			request.getSession().setAttribute("categoria", categoria);
			dispatcher = request.getRequestDispatcher("indexAdemir.jsp");
		}
		
		else if (operacao.equals("ALTERAR FORNECEDOR")) {
			response.sendRedirect("indexAdemir.jsp");
			return;
		}

		// ---------------------------------------------------------------------------------------------
		// EXCLUIR
		else if (operacao.equals("EXCLUIR PRODUTO") || operacao.equals("EXCLUIR CATEGORIA")
				|| operacao.equals("EXCLUIR FORNECEDOR")) {
			response.sendRedirect("indexAdemir.jsp");
			return;
		}
		
		
		// --------------------------------------------------------------------------------------------
		//  CARRINHO
		else if(operacao.equals("COLOCAR CARRINHO") || 
				operacao.equals("ALTERAR CARRINHO") ||
				operacao.equals("EXCLUIR CARRINHO"))
		{
			request.getSession().setAttribute("carrinho", CarrinhoSingleton.getInstance());
			response.sendRedirect("carrinho.jsp");
			return;
		}
		
		
		dispatcher.forward(request, response);

	}

}
