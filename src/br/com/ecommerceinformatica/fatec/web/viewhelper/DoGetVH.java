package br.com.ecommerceinformatica.fatec.web.viewhelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ecommerceinformatica.fatec.dao.CupomDAO;
import br.com.ecommerceinformatica.fatec.dao.PedidoDAO;
import br.com.ecommerceinformatica.fatec.dao.ProdutoDAO;
import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Cupom;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Fornecedor;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;
import br.com.ecommerceinformatica.fatec.singleton.CarrinhoSingleton;
import br.com.ecommerceinformatica.fatec.web.util.Util;

public class DoGetVH implements IViewHelperGet {

	@Override
	public void setAttribute(HttpServletRequest request) {
		// TODO Auto-generated method stub
		
		String pagina = request.getRequestURI();
		String padrao = "/Ecommerce_Informatica/";
		
		List<EntidadeDominio> listaCategoria = null;
		List<EntidadeDominio> listaFornecedor = null;
		List<EntidadeDominio> listProduto = null;
		
		if (pagina.equals(padrao + "cadastrarProduto") || 
				pagina.equals(padrao + "pesquisar") ||
				pagina.equals(padrao + "AlterarProduto")) {
		
			listaCategoria = Util.carregarCategorias();
			listaFornecedor = Util.carregarFornecedores();
		
			
			request.setAttribute("listaCategorias", listaCategoria);
			request.setAttribute("listaFornecedores", listaFornecedor);
		}
		
		// pagina do adm
		else if(pagina.equals(padrao + "ademirRequest")){
			String operacao = request.getParameter("OPERACAO");
			
			if (operacao.equals("Cadastrar Produto")) {
				listaCategoria = Util.carregarCategoriasAtivas();
				listaFornecedor = Util.carregarFornecedoresAtivos();
					
				request.setAttribute("listaCategorias", listaCategoria);
				request.setAttribute("listaFornecedores", listaFornecedor);
			}
			
			else if(operacao.equals("Pesquisar")) {
				listaCategoria = Util.carregarCategorias();
				listaFornecedor = Util.carregarFornecedores();
				listProduto = Util.getProdutoAtivo();
				
					
				request.setAttribute("listaCategorias", listaCategoria);
				request.setAttribute("listaFornecedores", listaFornecedor);
				request.setAttribute("listProdutos", listProduto);
			}
			
			else if (operacao.equals("Desconto Produto")) {
				request.getSession().setAttribute("listProdutos", Util.getProdutoAtivo());
				request.getSession().setAttribute("listCategorias", Util.getCategoriasAtiva());
				
			}
				
			else if (operacao.equals("Registrar Compra Mercadoria"))
				request.getSession().setAttribute("listProdutos", Util.getProdutoAtivo());
		}
		
		// produtos
		else if(pagina.equals(padrao + "produtos")) {
			request.getSession().setAttribute("listProdutos", Util.getProdutoAtivo());
			request.getSession().setAttribute("listCategorias", Util.getCategoriasAtiva());
			
		}
		
		else if(pagina.equals(padrao + "produto")) {
			List<EntidadeDominio> listProdutos = (List<EntidadeDominio>) request.getSession().getAttribute("listProdutos");
			
			int idProduto = Integer.parseInt(request.getParameter("id"));
			
			for (EntidadeDominio e : listProdutos) {
				Produto produto = (Produto) e;
				
				if (produto.getId() == idProduto) {
					request.getSession().setAttribute("produto", produto);
					break;
				}
				
			}
			
		}
		
		else if(pagina.equals(padrao + "carrinho")) {
			request.getSession().setAttribute("carrinho", CarrinhoSingleton.getInstance());
		}
		
		else if (pagina.equals(padrao + "DadosProduto")) {

			Produto produtoIdTabela = new Produto(null, 0, "", "", "", "", "", null, 0, "");
			produtoIdTabela.setId(Integer.parseInt(request.getParameter("id")));
			
			Resultado resultado = (Resultado) request.getSession().getAttribute("resultado");
			List<EntidadeDominio> listProdutos = resultado.getEntidades();
			
			Produto produto = null;
			
			// fazer um for each procurando o dono do ID
			for (EntidadeDominio ed : listProdutos) {
				Produto pro = (Produto) ed;
				if (pro.getId() == produtoIdTabela.getId()) {
					produto = pro;
					break;
				}
				
			}
			
			
			request.getSession().setAttribute("produto", produto);

		}
		else if (pagina.equals(padrao + "DadosFornecedor")) {

			Fornecedor fornecedorIdTable = new Fornecedor("", null, "", null);
			fornecedorIdTable.setId(Integer.parseInt(request.getParameter("id")));
			
			
			Resultado resultado = (Resultado)request.getSession().getAttribute("resultado");
			List<EntidadeDominio> listFornecedores = resultado.getEntidades();
			 
			Fornecedor fornecedor = null;
			
			for (EntidadeDominio ed : listFornecedores) {
				Fornecedor forn = (Fornecedor) ed;
				if (forn.getId() == fornecedorIdTable.getId()) {
					fornecedor = forn;
					break;
				}
			}
			
			request.getSession().setAttribute("fornecedor", fornecedor);

		}
		else if (pagina.equals(padrao + "DadosCategoria")) {

			Categoria categoriaIdTable = new Categoria(null, 0);
			categoriaIdTable.setId(Integer.parseInt(request.getParameter("id")));
			
			Resultado resultado = (Resultado) request.getSession().getAttribute("resultado");
			List<EntidadeDominio> listCategorias = resultado.getEntidades();
			
			Categoria categoria = null;
			
			for (EntidadeDominio ed : listCategorias) {
				Categoria cat = (Categoria) ed;
				if (cat.getId() == categoriaIdTable.getId()) {
					categoria = cat;
					break;
				}
			}

			request.getSession().setAttribute("categoria", categoria);

		}
		
		else if (pagina.equals(padrao + "DadosCliente")) {
			Cliente clienteIdTable = new Cliente("", null, "", null, "");
			clienteIdTable.setId(Integer.parseInt(request.getParameter("id")));
			
			Resultado resultado = (Resultado) request.getSession().getAttribute("resultado");
			List<EntidadeDominio> listClientes = resultado.getEntidades();
			
			Cliente cliente = null;
			
			for (EntidadeDominio ed : listClientes) {
				Cliente cli = (Cliente) ed;
				if (cli.getId() == clienteIdTable.getId()) {
					cliente = cli;
					break;
				}
			}
			
			request.getSession().setAttribute("cliente", cliente);
			
		}
		
		else if (pagina.equals(padrao + "Deslogar")) {
				request.getSession().setAttribute("clienteLogado", null);
		}
		
		else if (pagina.equals(padrao + "FiltrarCategoriaProduto")) {
			String filtrarCategoria = request.getParameter("categoria");
			System.out.println("|" + filtrarCategoria + "|");
			if (filtrarCategoria.equals("Todas")){
				request.getSession().setAttribute("listProdutos", Util.getProdutoAtivo());
				return;
			}
			
			List<EntidadeDominio> listEntidade = (List<EntidadeDominio>) request.getSession().getAttribute("listCategorias");
			Categoria categoria = null;
			
			for (EntidadeDominio entidade : listEntidade) {
				Categoria cCategoria = (Categoria) entidade;
				if (cCategoria.getNome().replace(" ", "_").equals(filtrarCategoria)) {
					categoria = cCategoria;
					break;
				}
				
			}
			
			// achou uma categoria para filtrar.. ent�o 
			if (categoria != null) {
				Produto produto = new Produto(categoria, 0, "", "", "", "", "", null, 0, "");
				produto.setTipoFiltroAtivo(1);
				List<EntidadeDominio> listEntidades = new ProdutoDAO().consultar(produto);
				request.getSession().setAttribute("listProdutos", listEntidades);
			}
			
		}
		
		else if (pagina.equals(padrao + "Perfil")) {
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			Boolean eraAdm = cliente.getUsuario().isAdministrador();
			
			cliente.getUsuario().setAdministrador(false);
			cliente.setListPedidos(new ArrayList<Pedido>());
			
			Pedido pedido = new Pedido(null, null, null, null, null, cliente, "", 0, 0, null, null, 0);
			
			List<EntidadeDominio> listPedidos = new PedidoDAO().consultar(pedido);
			
			for (EntidadeDominio ent : listPedidos) {
				Pedido ped = (Pedido) ent;
				
				cliente.getListPedidos().add(ped);
			}
			
			cliente.getUsuario().setAdministrador(eraAdm);
			
			Cupom cupom = new Cupom("");
			cupom.setIdCliente(cliente.getId());
			List<EntidadeDominio> listCupons = new CupomDAO().consultar(cupom);
			
			cliente.setListCupons(new ArrayList<Cupom>());
			for (EntidadeDominio ent : listCupons) {
				Cupom cup = (Cupom) ent;
				cliente.getListCupons().add(cup);
			}
			
			request.getSession().setAttribute("clienteLogado", cliente);
			
		}
		
		else if (pagina.equals(padrao + "Compra") && request.getParameter("OPERACAO").toString().equals("FINALIZAR COMPRA")) {
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			if (cliente == null) return;
			Boolean eraAdm = cliente.getUsuario().isAdministrador();
			
			cliente.getUsuario().setAdministrador(false);
			cliente.setListPedidos(new ArrayList<Pedido>());
			
			Pedido pedido = new Pedido(null, null, null, null, null, cliente, "", 0, 0, null, null, 0);
			
			List<EntidadeDominio> listPedidos = new PedidoDAO().consultar(pedido);
			
			for (EntidadeDominio ent : listPedidos) {
				Pedido ped = (Pedido) ent;
				
				cliente.getListPedidos().add(ped);
			}
			
			cliente.getUsuario().setAdministrador(eraAdm);
			
			Cupom cupom = new Cupom("");
			cupom.setIdCliente(cliente.getId());
			List<EntidadeDominio> listCupons = new CupomDAO().consultar(cupom);
			
			cliente.setListCupons(new ArrayList<Cupom>());
			for (EntidadeDominio ent : listCupons) {
				Cupom cup = (Cupom) ent;
				cliente.getListCupons().add(cup);
			}
			
			request.getSession().setAttribute("clienteLogado", cliente);
		}
		
		else if (pagina.equals(padrao + "Pedido")) {
			String codPedido = request.getParameter("codigo");
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			Pedido pedido = null;
			
			List<EntidadeDominio> listPedidos = (List<EntidadeDominio>) request.getSession().getAttribute("listPedidos");
			
			for (EntidadeDominio ent : listPedidos) {
				Pedido p = (Pedido) ent;
				if (p.getCodPedido().equals(codPedido)) {
					pedido = p;
					break;
				}
			}
			
			
			request.getSession().setAttribute("pedido", pedido);
			
		}
		
		else if (pagina.equals(padrao + "SolicitarCancelamento") || pagina.equals(padrao + "SolicitarTroca")) {
			String codPedido = request.getParameter("codigo");
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			Pedido pedido = null;
			
			for (EntidadeDominio entidade : cliente.getListPedidos()) {
				Pedido entPedido = (Pedido) entidade;
				if (entPedido.getCodPedido().equals(codPedido)) {
					pedido = entPedido;
					break;
				}
			}
			
			request.getSession().setAttribute("pedido", pedido);
			
		}
		
		else if(pagina.equals(padrao) || pagina.equals(padrao + "index.jsp") || pagina.equals(padrao + "paginaPrincipal"))  
			request.getSession().setAttribute("listProdutos", Util.getProdutoAtivo());
		
		
	}

	@Override
	public void setView(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher = null;
		
		String requisicao = request.getRequestURI();
		String padrao = "/Ecommerce_Informatica/";
		
		if (requisicao.equals(padrao + "FiltrarCategoriaProduto")) {
			dispatcher = request.getRequestDispatcher("produtos.jsp");
		}
		
		else if (requisicao.equals(padrao + "Pedido")) {
			dispatcher = request.getRequestDispatcher("pedido.jsp");
			
		}
		else if (requisicao.equals(padrao + "SolicitarCancelamento")) {
			dispatcher = request.getRequestDispatcher("solicitarCancelamento.jsp");
		}
		else if (requisicao.equals(padrao + "SolicitarTroca")) {
			dispatcher = request.getRequestDispatcher("solicitarTroca.jsp");
		}
		
		// cadastrar produto
		else if (requisicao.equals(padrao + "cadastrarProduto"))
			dispatcher = request.getRequestDispatcher("cadastrarProduto.jsp");
		
		else if(requisicao.equals(padrao) || requisicao.equals(padrao + "index.jsp") || requisicao.equals(padrao + "paginaPrincipal")) {
			dispatcher = request.getRequestDispatcher("index.jsp");
		}
		
		// alterar fornecedor
		else if (requisicao.equals(padrao + "AlterarFornecedor"))
			dispatcher = request.getRequestDispatcher("alterarFornecedor.jsp");
		
		// pesquisar tudo
		else if (requisicao.equals(padrao + "pesquisar"))
			dispatcher = request.getRequestDispatcher("pesquisar.jsp");
		
		// alterar Categoria
		else if (requisicao.equals(padrao + "AlterarCategoria"))
			dispatcher = request.getRequestDispatcher("alterarCategoria.jsp");
		
		// alterar Produto
		else if (requisicao.equals(padrao + "AlterarProduto"))
			dispatcher = request.getRequestDispatcher("alterarProduto.jsp");
		
		// Ademir Request
		else if (requisicao.equals(padrao + "ademirRequest")){
			String operacao = request.getParameter("OPERACAO");
			
			if (operacao.equals("Cadastrar Produto")) 
				dispatcher = request.getRequestDispatcher("cadastrarProduto.jsp");
			
			else if (operacao.equals("Cadastrar Fornecedor"))
				dispatcher = request.getRequestDispatcher("cadastrarFornecedor.jsp");
			
			else if (operacao.equals("Cadastrar Categoria"))
				dispatcher = request.getRequestDispatcher("cadastrarCategoria.jsp");
			
			else if(operacao.equals("Pesquisar")) 
				dispatcher = request.getRequestDispatcher("pesquisar.jsp");
			else if (operacao.equals("Desconto Produto"))
				dispatcher = request.getRequestDispatcher("criarDesconto.jsp");
			
			else if (operacao.equals("Registrar Compra Mercadoria"))
				dispatcher = request.getRequestDispatcher("registrarCompra.jsp");
			
		}
		
		else if (requisicao.equals(padrao + "indexAdemir") ||
				requisicao.equals(padrao + "indexAdemir.jsp"))
				dispatcher = request.getRequestDispatcher("indexAdemir.jsp");
		
		else if(requisicao.equals(padrao + "navBar.jsp")) {
			dispatcher = request.getRequestDispatcher("navBar.jsp");
		}
		
		else if(requisicao.equals(padrao + "produtos")) {
			dispatcher = request.getRequestDispatcher("produtos.jsp");
		}
		
		else if(requisicao.equals(padrao + "produto")) {
			dispatcher = request.getRequestDispatcher("produtoInformacoes.jsp");
		}
		
		else if (requisicao.equals(padrao + "Carrinho")) {
			dispatcher = request.getRequestDispatcher("carrinho.jsp");
		}
		
		else if(requisicao.equals(padrao + "Compra")) {
			String operacao = request.getParameter("OPERACAO");
			
			if (operacao.equals("FINALIZAR COMPRA")) {
				dispatcher = request.getRequestDispatcher("finalizarCompra.jsp");
			}
			
			
		}
		// ---------------------------------------------------------------------------------------------
		// RESULTADO PARA INFORMACAO
		else if (requisicao.equals(padrao + "DadosProduto")) {
			response.sendRedirect("produtoInformacoes.jsp");
			return;
		}
		else if (requisicao.equals(padrao + "DadosFornecedor")) {
			response.sendRedirect("fornecedorInformacoes.jsp");
			return;
		}

		else if (requisicao.equals(padrao + "DadosCategoria")) {
			response.sendRedirect("categoriaInformacoes.jsp");
			return;
		}
		
		else if (requisicao.equals(padrao + "DadosCliente")) {
			response.sendRedirect("clienteInformacoes.jsp");
			return;
		}
		
		
		//---------------------------------------------------------------------------------------
		// DESLOGAR
		else if (requisicao.equals(padrao + "Deslogar")) {
				dispatcher = request.getRequestDispatcher("index.jsp");
		}
		
		//
		// PERFIL
		else if (requisicao.equals(padrao + "Perfil")) {
			dispatcher = request.getRequestDispatcher("perfil.jsp");
		}
		
		dispatcher.forward(request, response);
		
		
	}

	

}
