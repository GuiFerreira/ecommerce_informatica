package br.com.ecommerceinformatica.fatec.web.viewhelper;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IViewHelperGet {
	
	public void setAttribute(HttpServletRequest request);
	public void setView(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException ;
	
	
}
