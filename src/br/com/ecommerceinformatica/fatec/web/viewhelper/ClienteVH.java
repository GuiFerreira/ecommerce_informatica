package br.com.ecommerceinformatica.fatec.web.viewhelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.ecommerceinformatica.fatec.dominio.Banco;
import br.com.ecommerceinformatica.fatec.dominio.Cartao;
import br.com.ecommerceinformatica.fatec.dominio.Cidade;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.EnderecoCliente;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Estado;
import br.com.ecommerceinformatica.fatec.dominio.Logradouro;
import br.com.ecommerceinformatica.fatec.dominio.Pais;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;
import br.com.ecommerceinformatica.fatec.dominio.Usuario;
import br.com.ecommerceinformatica.fatec.web.util.Constants;
import br.com.ecommerceinformatica.fatec.web.util.Util;

public class ClienteVH implements IViewHelperPost {

	@Override
	public EntidadeDominio getEntidade(HttpServletRequest request) {
		
		String operacao = request.getParameter("OPERACAO");
		
		
		if (operacao.equals("CADASTRAR CLIENTE")) {
			String email = request.getParameter("email");
			String senha = request.getParameter("senha").toString();
			Usuario usuario = new Usuario(email, senha, false);
			
			
			String ddd = request.getParameter("DDD").toString();
			String telefone = request.getParameter("telefone").toString();
			Contato contato = new Contato(ddd, telefone, email);
			
			
			String nome = request.getParameter("nomeCliente").toString();
			String cpf = request.getParameter("CPF").toString();
			String dataNascimento = request.getParameter("dataNascimento").toString();
			
			Cliente cliente = new Cliente(nome, contato, cpf, usuario, dataNascimento);
			
			cliente.setListaEnderecoCobranca(null);
			cliente.setListaCartoes(null);
			cliente.setListaEnderecoEntrega(null);
			
			return cliente;
			
		}
		
		else if (operacao.equals("CADASTRAR CLIENTE COMPRA")) 
		{
			String email = request.getParameter("email");
			String senha = request.getParameter("senha");
			Usuario usuario = new Usuario(email, senha, false);
			
			String ddd = request.getParameter("DDD").toString();
			String telefone = request.getParameter("telefone").toString();
			Contato contato = new Contato(ddd, telefone, email);
			
			
			String nome = request.getParameter("nomeCliente").toString();
			String cpf = request.getParameter("CPF").toString();
			String dataNascimento = request.getParameter("dataNascimento").toString();
			
			Cliente cliente = new Cliente(nome, contato, cpf, usuario, dataNascimento);
			
			
			// dados do endere�o
			List<EnderecoCliente> listEnderecoEntrega = new ArrayList<EnderecoCliente>();
			List<EnderecoCliente> listEnderecoCobranca = new ArrayList<EnderecoCliente>();
			
			
			//				ENDERE�O DE ENTREGA
				String nomePaisEntrega = request.getParameter("paisEntrega");
					// pegar caracteres da sigla do pais
					char[] sigla = nomePaisEntrega.toCharArray();
				
				String siglaPaisEntrega = "";
				siglaPaisEntrega += sigla[0];
				siglaPaisEntrega += sigla[1];
				
				Pais paisEntrega = new Pais(nomePaisEntrega, siglaPaisEntrega);
				
				String nomeEstadoEntrega = request.getParameter("estadoEntrega");
				sigla = nomeEstadoEntrega.toCharArray();
				
				String siglaEstadoEntrega = "";
				
				siglaEstadoEntrega += sigla[0];
				siglaEstadoEntrega += sigla[1];
				
				Estado estadoEntrega = new Estado(nomeEstadoEntrega, siglaEstadoEntrega, paisEntrega);
				
				String nomeCidadeEntrega = request.getParameter("cidadeEntrega");
				
				Cidade cidadeEntrega = new Cidade(nomeCidadeEntrega, estadoEntrega);
				
				// logradouro
				String nomeLogradouroEntrega = request.getParameter("logradouroEntrega");
				String tipoLogradouroEntrega = request.getParameter("tipoLogradouroEntrega");
				
				Logradouro logradouroEntrega = new Logradouro(nomeLogradouroEntrega, tipoLogradouroEntrega);
				
				
				String nomeRuaEntrega = request.getParameter("ruaEntrega");
				String nomeBairroEntrega = request.getParameter("bairroEntrega");
				String numeroEntrega = request.getParameter("numEntrega");
				String cepEntrega = request.getParameter("cepEntrega");
				String observacoesEntrega = request.getParameter("observacoesEntrega");
				String apelidoEnderecoEntrega = request.getParameter("apelidoEntrega");
				
				
				// verificar se ele qr usar o endereço de entrega como o endereço de cobrança
				String usarEnderecoEntrega = request.getParameter("usarEnderecoEntrega");

				EnderecoCliente enderecoEntrega = new EnderecoCliente(nomeRuaEntrega, nomeBairroEntrega, numeroEntrega,
						cidadeEntrega, cepEntrega, logradouroEntrega, observacoesEntrega, apelidoEnderecoEntrega, 
								(usarEnderecoEntrega != null ? Constants.ENDERECO_ENTREGA_COBRANCA : Constants.ENDERECO_ENTREGA));
				
			
			listEnderecoEntrega.add(enderecoEntrega);
			
			cliente.setListaEnderecoEntrega(listEnderecoEntrega);
			
			
			//					ENDERE�O COBRAN�A
			
			if (usarEnderecoEntrega != null) {
				listEnderecoCobranca.add(enderecoEntrega);
				
				cliente.setListaEnderecoCobranca(listEnderecoCobranca);
					
			}
				
			else {
				String nomePaisCobranca = request.getParameter("paisCobranca");
				// pegar caracteres da sigla do pais
				sigla = nomePaisCobranca.toCharArray();
			
				String siglaPaisCobranca = "";
				siglaPaisCobranca += sigla[0];
				siglaPaisCobranca += sigla[1];
				siglaPaisCobranca += sigla[2];
				
				Pais paisCobranca = new Pais(nomePaisCobranca, siglaPaisCobranca);
				
				String nomeEstadoCobranca = request.getParameter("estadoCobranca");
				sigla = nomeEstadoCobranca.toCharArray();
				
				String siglaEstadoCobranca = "";
				
				siglaEstadoCobranca += sigla[0];
				siglaEstadoCobranca += sigla[1];
				siglaEstadoCobranca += sigla[2];
				
				Estado estadoCobranca = new Estado(nomeEstadoCobranca, siglaEstadoCobranca, paisCobranca);
				
				String nomeCidadeCobranca = request.getParameter("cidadeCobranca");
				
				Cidade cidadeCobranca = new Cidade(nomeCidadeCobranca, estadoCobranca);
				
				// logradouro
				String nomeLogradouroCobranca = request.getParameter("logradouroCobranca");
				String tipoLogradouroCobranca = request.getParameter("tipoLogradouroCobranca");
				
				Logradouro logradouroCobranca = new Logradouro(nomeLogradouroCobranca, tipoLogradouroCobranca);
				
				
				String nomeRuaCobranca = request.getParameter("ruaCobranca");
				String nomeBairroCobranca = request.getParameter("bairroCobranca");
				String numeroCobranca = request.getParameter("numCobranca");
				String cepCobranca = request.getParameter("cepCobranca");
				String observacoesCobranca = request.getParameter("observacoesCobranca");
				String apelidoEnderecoCobranca = request.getParameter("apelidoCobranca");
				
				
				EnderecoCliente enderecoCobranca = new EnderecoCliente(nomeRuaCobranca, nomeBairroCobranca, numeroCobranca,
						cidadeCobranca, cepCobranca, logradouroCobranca, observacoesCobranca, apelidoEnderecoCobranca, Constants.ENDERECO_COBRANCA);
				
				listEnderecoCobranca.add(enderecoCobranca);
				
				cliente.setListaEnderecoCobranca(listEnderecoCobranca);
			}
			
				
			// pegar cart�o de cr�dito
			String numAgencia = request.getParameter("numAgencia");
			String nomeBanco = request.getParameter("nomeBanco");
			String nomeImpresso = request.getParameter("nomeImpresso");
			String numCartao = request.getParameter("numCartao");
			String codSeguranca = request.getParameter("codSeguranca");
			
			Banco banco = new Banco(nomeBanco, numAgencia);
			Cartao cartao = new Cartao(numCartao, nomeImpresso, codSeguranca, banco);
			
			cliente.setListaCartoes(new ArrayList<Cartao>());
			cliente.getListaCartoes().add(cartao);
			
			return cliente;
			
			
		}
		
		else if (operacao.equals("PESQUISAR CLIENTE")) {

			String email = request.getParameter("email");
			String senha = "";
			Usuario usuario = new Usuario(email, senha, false);
			
			
			String ddd = "";
			String telefone = "";
			Contato contato = new Contato(ddd, telefone, email);
			
			
			String nome = request.getParameter("nomeCliente").toString();
			String cpf = request.getParameter("CPF").toString();
			String dataNascimento = "";
			
			Cliente cliente = new Cliente(nome, contato, cpf, usuario, dataNascimento);
			cliente.setId(0);
			
			return cliente;
			
			
		}
		
		else if (operacao.equals("EXCLUIR CLIENTE")) {
			Cliente clienteSessao = (Cliente) request.getSession().getAttribute("Cliente");
			Cliente cliente = new Cliente("", null, "", null, "");
			cliente.setId(clienteSessao.getId());
			
			return cliente;

		} else if (operacao.equals("ALTERAR CLIENTE")) {
			String nomeCliente = request.getParameter("nome");
			String dataNascimento = request.getParameter("dataNascimento");
			String cpf = request.getParameter("cpf");
			
			Cliente cliente = new Cliente(nomeCliente, null, cpf, null, dataNascimento);
			
			return cliente;

		}
		
		else if (operacao.equals("CADASTRAR ENDERECO")) {
			int endCobranca = Integer.valueOf(request.getParameter("tipoEndereco"));
			
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			 
			//				ENDERE�O
				String nomePaisEntrega = request.getParameter("paisEntrega");
					// pegar caracteres da sigla do pais
					char[] sigla = nomePaisEntrega.toCharArray();
				
				String siglaPaisEntrega = "";
				siglaPaisEntrega += sigla[0];
				siglaPaisEntrega += sigla[1];
				
				Pais paisEntrega = new Pais(nomePaisEntrega, siglaPaisEntrega);
				
				String nomeEstadoEntrega = request.getParameter("estadoEntrega");
				sigla = nomeEstadoEntrega.toCharArray();
				
				String siglaEstadoEntrega = "";
				
				siglaEstadoEntrega += sigla[0];
				siglaEstadoEntrega += sigla[1];
				
				Estado estadoEntrega = new Estado(nomeEstadoEntrega, siglaEstadoEntrega, paisEntrega);
				
				String nomeCidadeEntrega = request.getParameter("cidadeEntrega");
				
				Cidade cidadeEntrega = new Cidade(nomeCidadeEntrega, estadoEntrega);
				
				// logradouro
				String nomeLogradouroEntrega = request.getParameter("logradouroEntrega");
				String tipoLogradouroEntrega = request.getParameter("tipoLogradouroEntrega");
				
				Logradouro logradouroEntrega = new Logradouro(nomeLogradouroEntrega, tipoLogradouroEntrega);
				
				
				String nomeRuaEntrega = request.getParameter("ruaEntrega");
				String nomeBairroEntrega = request.getParameter("bairroEntrega");
				String numeroEntrega = request.getParameter("numEntrega");
				String cepEntrega = request.getParameter("cepEntrega");
				String observacoesEntrega = request.getParameter("observacoesEntrega");
				String apelidoEnderecoEntrega = request.getParameter("apelidoEntrega");
				
				
				EnderecoCliente enderecoEntrega = new EnderecoCliente(nomeRuaEntrega, nomeBairroEntrega, numeroEntrega,
						cidadeEntrega, cepEntrega, logradouroEntrega, observacoesEntrega, apelidoEnderecoEntrega, endCobranca);
				
				enderecoEntrega.setId(cliente.getId());
				enderecoEntrega.setIdCliente(cliente.getId());
				
			return enderecoEntrega;
			
			
			
		}
		
		else if (operacao.equals("LOGAR")) {
			String login = request.getParameter("login");
			String senha = request.getParameter("senha");
			
			Usuario usuario = new Usuario(login, senha, false);
			usuario.setTipoFiltroAtivo(1);
			
			return usuario;
			
		}
		
		else if (operacao.equals("CADASTRAR CARTAO")) {
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			
			String numAgencia = request.getParameter("numAgencia");
			String nomeBanco = request.getParameter("nomeBanco");
			String nomeImpresso = request.getParameter("nomeImpresso");
			String numCartao = request.getParameter("numCartao");
			String codSeguranca = request.getParameter("codSeguranca");
			
			Banco banco = new Banco(nomeBanco, numAgencia);
			Cartao cartao = new Cartao(numCartao, nomeImpresso, codSeguranca, banco);
			
			cartao.setId(cliente.getId());
			
			return cartao;
		}
		
		else if (operacao.equals("ESQUECEU SENHA")) {
			
			
		}
		
		else if (operacao.equals("ALTERAR ENDERECO")) {
			int endCobranca = Integer.valueOf(request.getParameter("tipoEndereco"));
			
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			 
			//				ENDERE�O
				String nomePaisEntrega = request.getParameter("paisEntrega");
					// pegar caracteres da sigla do pais
					char[] sigla = nomePaisEntrega.toCharArray();
				
				String siglaPaisEntrega = "";
				siglaPaisEntrega += sigla[0];
				siglaPaisEntrega += sigla[1];
				
				Pais paisEntrega = new Pais(nomePaisEntrega, siglaPaisEntrega);
				
				String nomeEstadoEntrega = request.getParameter("estadoEntrega");
				sigla = nomeEstadoEntrega.toCharArray();
				
				String siglaEstadoEntrega = "";
				
				siglaEstadoEntrega += sigla[0];
				siglaEstadoEntrega += sigla[1];
				
				Estado estadoEntrega = new Estado(nomeEstadoEntrega, siglaEstadoEntrega, paisEntrega);
				
				String nomeCidadeEntrega = request.getParameter("cidadeEntrega");
				
				Cidade cidadeEntrega = new Cidade(nomeCidadeEntrega, estadoEntrega);
				
				// logradouro
				String nomeLogradouroEntrega = request.getParameter("logradouroEntrega");
				String tipoLogradouroEntrega = request.getParameter("tipoLogradouroEntrega");
				
				Logradouro logradouroEntrega = new Logradouro(nomeLogradouroEntrega, tipoLogradouroEntrega);
				
				
				String nomeRuaEntrega = request.getParameter("ruaEntrega");
				String nomeBairroEntrega = request.getParameter("bairroEntrega");
				String numeroEntrega = request.getParameter("numEntrega");
				String cepEntrega = request.getParameter("cepEntrega");
				String observacoesEntrega = request.getParameter("observacoesEntrega");
				String apelidoEnderecoEntrega = request.getParameter("apelidoEntrega");
				
				
				EnderecoCliente enderecoEntrega = new EnderecoCliente(nomeRuaEntrega, nomeBairroEntrega, numeroEntrega,
						cidadeEntrega, cepEntrega, logradouroEntrega, observacoesEntrega, apelidoEnderecoEntrega, endCobranca);
				
				int idEndereco = Integer.parseInt(request.getParameter("idEndereco"));
				enderecoEntrega.setId(idEndereco);
				enderecoEntrega.setIdCliente(cliente.getId());
				
			return enderecoEntrega;
		}
		
		
		return null;
	}

	@Override
	public void setView(Resultado resultado, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		
		RequestDispatcher dispatcher = null;
		
		
		String operacao = request.getParameter("OPERACAO");
		if (resultado.getMsgErro() != null && !resultado.getMsgErro().trim().equals("")) {
			request.setAttribute("erro", resultado.getMsgErro());
			dispatcher = request.getRequestDispatcher("erro.jsp");
		}
		else if (operacao.equals("CADASTRAR CLIENTE")) {
			request.getSession().setAttribute("clienteLogado", resultado.getEntidades().get(0));
			dispatcher = request.getRequestDispatcher("index.jsp");
		}
		
		else if (operacao.equals("CADASTRAR CLIENTE COMPRA")) {
			request.getSession().setAttribute("clienteLogado", resultado.getEntidades().get(0));
			dispatcher = request.getRequestDispatcher("finalizarCompra.jsp");
		}
		
		else if (operacao.equals("CADASTRAR ENDERECO")) {
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			EnderecoCliente endCliente = (EnderecoCliente) resultado.getEntidades().get(0);
			
			if (endCliente.getEndCobranca() == 1) {
				if (cliente.getListaEnderecoCobranca() == null)
					cliente.setListaEnderecoCobranca(new ArrayList<EnderecoCliente>());
				
				cliente.getListaEnderecoCobranca().add(endCliente);
			}
			else if (endCliente.getEndCobranca() == 0){
				if (cliente.getListaEnderecoEntrega() == null)
					cliente.setListaEnderecoEntrega(new ArrayList<EnderecoCliente>());
				
				cliente.getListaEnderecoEntrega().add(endCliente);	
			}
			else if (endCliente.getEndCobranca() == 2) {
				if (cliente.getListaEnderecoCobranca() == null)
					cliente.setListaEnderecoCobranca(new ArrayList<EnderecoCliente>());
				
				cliente.getListaEnderecoCobranca().add(endCliente);
				
				if (cliente.getListaEnderecoEntrega() == null)
					cliente.setListaEnderecoEntrega(new ArrayList<EnderecoCliente>());
				
				cliente.getListaEnderecoEntrega().add(endCliente);
			}
			request.getSession().setAttribute("clienteLogado", cliente);
			response.sendRedirect("perfil.jsp");
			return;
		}
		
		else if (operacao.equals("PESQUISAR CLIENTE")) {
			request.getSession().setAttribute("resultado", resultado);
			dispatcher = request.getRequestDispatcher("resultadoPesquisa.jsp");
		}
		
		else if (operacao.equals("EXCLUIR  CLIENTE")) {
			dispatcher = request.getRequestDispatcher("index.jsp");
		}
		
		else if (operacao.equals("ALTERAR CLIENTE")) {
			Cliente clienteAtualizado = (Cliente) resultado.getEntidades().get(0);
			Cliente clienteSession = (Cliente) request.getSession().getAttribute("clienteLogado");
			
			clienteSession.setNome(clienteAtualizado.getNome());
			clienteSession.setCpf(clienteAtualizado.getCpf());
			clienteSession.setDataNascimento(clienteAtualizado.getDataNascimento());
			
			request.getSession().setAttribute("clienteLogado", clienteSession);
			
			dispatcher = request.getRequestDispatcher("perfil.jsp");
			
			
		}
		
		else if (operacao.equals("ALTERAR SENHA")) {
			
			
		}
		
		else if (operacao.equals("ALTERAR ENDERECO")) { 
			EnderecoCliente endereco = (EnderecoCliente) resultado.getEntidades().get(0);
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			
			for (EnderecoCliente end : cliente.getListaEnderecoEntrega()) {
				if (end.getId() == endereco.getId()) {
					System.out.println("Encontrou o endere�o na lista entrega: " + cliente.getListaEnderecoEntrega().remove(end));
					
					// antes de colocar o novo endere�o na lista
					// verifica se ele � do tipo Entrega
					if (endereco.getEndCobranca() == 0 || endereco.getEndCobranca() == 2)
						cliente.getListaEnderecoEntrega().add(endereco);
					
					break;
				}
			}
			
			for (EnderecoCliente end : cliente.getListaEnderecoCobranca()) {
				if (end.getId() == endereco.getId()) {
					System.out.println("Encontrou o endere�o na lista cobran�a: " + cliente.getListaEnderecoCobranca().remove(end));
					
					// antes de colocar o novo endere�o na lista
					// verifica se ele � do tipo Entrega
					if (endereco.getEndCobranca() == 1 || endereco.getEndCobranca() == 2)
						cliente.getListaEnderecoCobranca().add(endereco);
					
					break;
				}
			}
			
			request.getSession().setAttribute("clienteLogado", cliente);
			
			response.sendRedirect("perfil.jsp");
			
		}	
		
		else if (operacao.equals("LOGAR")) {
			Cliente cliente = (Cliente) resultado.getEntidades().get(0);
			request.getSession().setAttribute("clienteLogado", cliente);
			request.getSession().setAttribute("listProdutos", Util.getProdutoAtivo());
			response.sendRedirect("index.jsp");
			return;
			
		}
		
		else if (operacao.equals("CADASTRAR CARTAO")) {
			Cliente cliente = (Cliente) request.getSession().getAttribute("clienteLogado");
			if (cliente.getListaCartoes() == null)
				cliente.setListaCartoes(new ArrayList<Cartao>());
			
			Cartao cartao = (Cartao) resultado.getEntidades().get(0);
			cliente.getListaCartoes().add(cartao);
			
			request.getSession().setAttribute("clienteLogado", cliente);
			
			response.sendRedirect("perfil.jsp");
			return;
		}
		
		
		dispatcher.forward(request, response);

	}

}
