package br.com.ecommerceinformatica.fatec.singleton;

import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;

public interface ISingleton{
	
	public List<EntidadeDominio> getInstances();	
	public void addList(EntidadeDominio entidade);
	public EntidadeDominio pesquisarLista(EntidadeDominio entidade);
	
}
