package br.com.ecommerceinformatica.fatec.singleton;

import java.util.ArrayList;
import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensCarrinho;
import br.com.ecommerceinformatica.fatec.dominio.Produto;

public class CarrinhoSingleton extends EntidadeDominio implements ISingleton {

	private static CarrinhoSingleton carrinho = null;
	private double precoCarrinho;
	private List<EntidadeDominio> listProdutos = null;
	
	private CarrinhoSingleton() {}
	
	public static synchronized CarrinhoSingleton getInstance() {
		if (carrinho == null)
			carrinho = new CarrinhoSingleton();
		return carrinho;
	}

	@Override
	public List<EntidadeDominio> getInstances() {
		if (listProdutos == null)
			listProdutos = new ArrayList<EntidadeDominio>();
		return listProdutos;
	}
	

	public double getPrecoCarrinho() {
		return precoCarrinho;
	}

	@Override
	public void addList(EntidadeDominio entidade) {
		if (listProdutos == null) {
			listProdutos = new ArrayList<EntidadeDominio>();
		}
		listProdutos.add(entidade);
		calcularPrecoCarrinho();
	}
	
	public void calcularPrecoCarrinho() {
		precoCarrinho = 0;
		if (listProdutos != null) {
			for (EntidadeDominio e : listProdutos) {
				ItensCarrinho i = (ItensCarrinho) e;
				precoCarrinho += i.getPrecoItem();
				
			}
		}
		
		
	}
	
	public void limparCarrinho() {
		listProdutos = null;
	}
	
	public ItensCarrinho findById(int id) {
		
		for (EntidadeDominio e : getInstances()) {
			ItensCarrinho ic = (ItensCarrinho) e;
			if (id == ic.getProduto().getId())
				return ic;	
		}
		return null;
		
	}

	@Override
	public EntidadeDominio pesquisarLista(EntidadeDominio entidade) {
		Produto produto = (Produto) entidade;
		Produto procurado = null;
		for(EntidadeDominio e : listProdutos) {
			ItensCarrinho itemCarrinho = (ItensCarrinho) e;
			if (produto.getNome().equals(itemCarrinho.getProduto().getNome())) {
				procurado = itemCarrinho.getProduto();
				break;
			}
				
			
		}
		
		return procurado;
	}
	
	public void remove(int idProduto) {
		List<EntidadeDominio> listAuxiliar = new ArrayList<EntidadeDominio>();
		
		for (EntidadeDominio e : listProdutos) {
			ItensCarrinho ic = (ItensCarrinho) e;
			if (idProduto != ic.getId())
				listAuxiliar.add(ic);
		}
		
		listProdutos = listAuxiliar;
		
	}

}
