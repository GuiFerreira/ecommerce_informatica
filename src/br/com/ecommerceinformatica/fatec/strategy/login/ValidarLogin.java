package br.com.ecommerceinformatica.fatec.strategy.login;

import java.util.List;

import br.com.ecommerceinformatica.fatec.dao.UsuarioDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Usuario;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarLogin implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		String msg = "";
		Usuario usuario = (Usuario) entidade;
		usuario.setTipoFiltroAtivo(1);
		
		List<EntidadeDominio> listUsuario = new UsuarioDAO().consultar(usuario);
		
		if (listUsuario.isEmpty())
			msg = "Login ou senha inválidos";
		
		else {
			Usuario usuarioBD = (Usuario) listUsuario.get(0);
			if (!usuario.getEmail().equals(usuarioBD.getEmail()) || !usuario.getSenha().equals(usuarioBD.getSenha()))
				msg = "Login ou senha inválido";
		}
		
		return msg;
	}

}
