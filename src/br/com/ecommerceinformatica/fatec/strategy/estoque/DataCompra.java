package br.com.ecommerceinformatica.fatec.strategy.estoque;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensEstoque;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.web.util.Util;

public class DataCompra implements IStrategy{

	@Override
	public String processar(EntidadeDominio entidade) {
		
		entidade.setDataCadastro(Util.getToday());
		
		ItensEstoque item = (ItensEstoque) entidade;
		item.setDataUltimaCompra(item.getDataCadastro());
		
		
		return "";
	}

	
	
}
