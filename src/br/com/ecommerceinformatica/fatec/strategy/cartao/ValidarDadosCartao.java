package br.com.ecommerceinformatica.fatec.strategy.cartao;

import br.com.ecommerceinformatica.fatec.dominio.Cartao;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarDadosCartao implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Cartao cartao = (Cartao) entidade;
		
		String msg = "";
		
		if (cartao != null) {
			if (cartao.getBanco().getNome().trim().equals(""))
				msg += "<p style='color: red;'>Nome do banco inv�lido</p><br/>";
			if (cartao.getBanco().getNumAgencia().trim().equals(""))
				msg += "<p style='color: red;'>N�mero da Ag�ncia inv�lido</p><br/>";
			if (cartao.getCodSeguranca().trim().equals(""))
				msg += "<p style='color: red;'><br/>C�digo de Seguran�a inv�lido</p><br/>";
			if (cartao.getNomeImpresso().trim().equals(""))
				msg += "<p style='color: red;'><br/>Nome impresso inv�lido</p><br/>";
			if (cartao.getNumero().trim().equals(""))
				msg += "<p style='color: red;'><br/>N�mero do cart�o inv�lido</p><br/>";
			
		}
		else
			msg = "<p style='color: red;'>N�o existe cart�o</p><br/>";
		
		return msg;
	}

}
