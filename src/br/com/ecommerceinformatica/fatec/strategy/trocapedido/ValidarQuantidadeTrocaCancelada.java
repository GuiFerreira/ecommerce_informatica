package br.com.ecommerceinformatica.fatec.strategy.trocapedido;

import java.util.ArrayList;
import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensTroca;
import br.com.ecommerceinformatica.fatec.dominio.TrocaPedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarQuantidadeTrocaCancelada implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		TrocaPedido troca = (TrocaPedido) entidade;
		
		List<ItensTroca> listItens = new ArrayList<ItensTroca>();
		
		boolean flgTemItem = false;
		
		for (ItensTroca item : troca.getListItensTroca()) {
			if (item.getQntTrocada() == 0) 
				continue;
			
			listItens.add(item);
			flgTemItem = true;
		}
		
		troca.setListItensTroca(listItens);
		
		if (!flgTemItem)
			return "N�o tem itens para trocar...";
		
		
		
		return "";
	}

}
