package br.com.ecommerceinformatica.fatec.strategy.trocapedido.troca;

import br.com.ecommerceinformatica.fatec.dao.ClienteDAO;
import br.com.ecommerceinformatica.fatec.dao.CupomDAO;
import br.com.ecommerceinformatica.fatec.dao.PedidoDAO;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Cupom;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensTroca;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.StatusCompra;
import br.com.ecommerceinformatica.fatec.dominio.StatusTrocaCancelamento;
import br.com.ecommerceinformatica.fatec.dominio.TrocaPedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.web.util.Constants;
import br.com.ecommerceinformatica.fatec.web.util.Email;

public class AlterarPedidoTroca implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		String msgErro = "";
		TrocaPedido trocaPedido = (TrocaPedido) entidade;
		Pedido pedido = trocaPedido.getPedido();
		Cupom cupom = null;
		String assunto = "";
		String corpoEmail = "";
		
		for (ItensTroca item : trocaPedido.getListItensTroca()) {
			// se troca for pendente: muda status do pedido para pendente
			if (item.getStatus() == StatusTrocaCancelamento.TROCA_PENDENTE) {
				
				// enviar email de confirma��o de troca
				Cliente cliente = new Cliente("", null, "", null, "");
				cliente.setId(trocaPedido.getIdCliente());
				cliente = (Cliente) new ClienteDAO().consultar(cliente).get(0);
				
				assunto = "Solicita��o de Troca Feita - Pedido: " + pedido.getCodPedido();
				corpoEmail = "Sua solicita��o de troca do pedido: " + pedido.getCodPedido() + " foi feita. \nPor favor, envie-nos os produtos:\n\n";
				
				corpoEmail += "<table>";
				corpoEmail += "<thead>";
				corpoEmail += "<tr><td>Nome Produto:</td> <td>Quantidade</td></tr>";
				corpoEmail += "</thead>";
				corpoEmail += "<tbody>";
				
					corpoEmail += "<tr>";
					corpoEmail += "<td>" + item.getProduto().getNome() + "</td>";
					corpoEmail += "<td>" + item.getQntTrocada() + "</td>";				
					corpoEmail += "</tr>";
				
				
				corpoEmail += "</tbody>";
				corpoEmail += "</table>";
				
				corpoEmail += "\n\nPara o endere�o: Rua XXXXX numero XXX Bairro: XXX, Cidade XXX, Estado: XXX, CEP: XXXXX\n\nAtenciosamente,\n " 
						+ Constants.NOME_EMPRESA_CABECALHO;
				Email.EnviarEmail(cliente.getUsuario().getEmail(), assunto, corpoEmail);
			}
			
			
			// se troca for igual a recusado, muda pedido para troca recusada
			else if (item.getStatus() == StatusTrocaCancelamento.TROCA_RECUSADA) {
				pedido.setStatusCompra(StatusCompra.TROCA_RECUSADA);
				msgErro = new PedidoDAO().alterar(pedido);
				
				// enviar email de recusa de troca
				Cliente cliente = new Cliente("", null, "", null, "");
				cliente.setId(trocaPedido.getIdCliente());
				cliente = (Cliente) new ClienteDAO().consultar(cliente).get(0);
				
				assunto = "Troca Recusada - Pedido: " + pedido.getCodPedido();
				corpoEmail = "A solicita��o de troca do pedido: " + pedido.getCodPedido() + " foi recusada";
				Email.EnviarEmail(cliente.getUsuario().getEmail(), assunto, corpoEmail);
			}
			
			// se troca for igual a aprovada muda o pedido para troca aprovada e cria cupom de troca
			else if (item.getStatus() == StatusTrocaCancelamento.TROCA_APROVADA) {
				double valorTotal = 0;
				
				valorTotal += item.getValorUnitario() * item.getQntTrocada();
				
				cupom = new Cupom("Troca de Mercadoria - C�d: " + pedido.getCodPedido(), trocaPedido.getIdCliente(),  valorTotal, 120, 1);
				String retorno = new CupomDAO().salvar(cupom);
				msgErro = (retorno.trim().equals("")) ? "" : "Erro ao gerar o cupom da diferen�a... <br/>" + retorno;
				
				pedido.setStatusCompra(StatusCompra.TROCA_APROVADA);
				retorno = new PedidoDAO().alterar(pedido);
				
				msgErro += (retorno.trim().equals("")) ? "" : "Erro ao alterar produto status TrocaAprovado...";
				
				// enviar email de confirma��o de troca
				Cliente cliente = new Cliente("", null, "", null, "");
				cliente.setId(trocaPedido.getIdCliente());
				cliente = (Cliente) new ClienteDAO().consultar(cliente).get(0);
				
				assunto = "Troca Aceita - Pedido: " + pedido.getCodPedido();
				corpoEmail = "A solicita��o de troca do pedido: " + pedido.getCodPedido() + " foi aceita";
				Email.EnviarEmail(cliente.getUsuario().getEmail(), assunto, corpoEmail);
				
			}
			
		}
		
		
		return msgErro;
	}

}
