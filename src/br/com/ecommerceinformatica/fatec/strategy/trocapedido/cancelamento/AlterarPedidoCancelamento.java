package br.com.ecommerceinformatica.fatec.strategy.trocapedido.cancelamento;

import br.com.ecommerceinformatica.fatec.dao.ClienteDAO;
import br.com.ecommerceinformatica.fatec.dao.CupomDAO;
import br.com.ecommerceinformatica.fatec.dao.PedidoDAO;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Cupom;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensTroca;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.StatusTrocaCancelamento;
import br.com.ecommerceinformatica.fatec.dominio.TrocaPedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.web.util.Email;

public class AlterarPedidoCancelamento implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		String msgErro = "";
		TrocaPedido trocaPedido = (TrocaPedido) entidade;
		Pedido pedido = trocaPedido.getPedido();
		Cupom cupom = null;
		String assunto = "";
		String corpoEmail = "";
		
		for (ItensTroca item : trocaPedido.getListItensTroca()) {
			// se troca for pendente: muda status do pedido para pendente
			if (item.getStatus() == StatusTrocaCancelamento.CANCELAMENTO_PENDENTE) {
				
				// enviar email de confirmação de troca
				Cliente cliente = new Cliente("", null, "", null, "");
				cliente.setId(trocaPedido.getIdCliente());
				cliente = (Cliente) new ClienteDAO().consultar(cliente).get(0);
				
				assunto = "Solicitação de Cancelamento Feita - Pedido: " + pedido.getCodPedido();
				corpoEmail = "Sua solicitação de cancelamento do pedido: " + pedido.getCodPedido() + " foi feita.\n";
				
				corpoEmail += "<table>";
				corpoEmail += "<thead>";
				corpoEmail += "<tr><td>Nome Produto:</td> <td>Quantidade</td></tr>";
				corpoEmail += "</thead>";
				corpoEmail += "<tbody>";
				
					corpoEmail += "<tr>";
					corpoEmail += "<td>" + item.getProduto().getNome() + "</td>";
					corpoEmail += "<td>" + item.getQntTrocada() + "</td>";				
					corpoEmail += "</tr>";
				
				
				corpoEmail += "</tbody>";
				corpoEmail += "</table>";
				
				Email.EnviarEmail(cliente.getUsuario().getEmail(), assunto, corpoEmail);
			}
			
			
			// se troca for igual a recusado, muda pedido para troca recusada
			else if (item.getStatus() == StatusTrocaCancelamento.CANCELAMENTO_RECUSADO) {
				// enviar email de recusa de troca
				Cliente cliente = new Cliente("", null, "", null, "");
				cliente.setId(trocaPedido.getIdCliente());
				cliente = (Cliente) new ClienteDAO().consultar(cliente).get(0);
				
				assunto = "Cancelamento Recusada - Pedido: " + pedido.getCodPedido();
				corpoEmail = "A solicitação de cancelamento do pedido: " + pedido.getCodPedido() + " foi recusada";
				Email.EnviarEmail(cliente.getUsuario().getEmail(), assunto, corpoEmail);
			}
			
			// se troca for igual a aprovada muda o pedido para troca aprovada e cria cupom de troca
			else if (item.getStatus() == StatusTrocaCancelamento.CANCELAMENTO_APROVADO) {
				double valorTotal = 0;
				
				valorTotal += item.getValorUnitario() * item.getQntTrocada();
				
				cupom = new Cupom("Cancelamento de Mercadoria - Cód: " + pedido.getCodPedido(), trocaPedido.getIdCliente(),  valorTotal, 120, 1);
				String retorno = new CupomDAO().salvar(cupom);
				msgErro = (retorno.trim().equals("")) ? "" : "Erro ao gerar o cupom da diferença... <br/>" + retorno;
				// enviar email de confirmação de troca
				Cliente cliente = new Cliente("", null, "", null, "");
				cliente.setId(trocaPedido.getIdCliente());
				cliente = (Cliente) new ClienteDAO().consultar(cliente).get(0);
				
				assunto = "Cancelamento Aceito - Pedido: " + pedido.getCodPedido();
				corpoEmail = "A solicitação de cancelamento do pedido: " + pedido.getCodPedido() + " foi aceita";
				Email.EnviarEmail(cliente.getUsuario().getEmail(), assunto, corpoEmail);
				
			}
			
		}
		
		
		return msgErro;
	}

}
