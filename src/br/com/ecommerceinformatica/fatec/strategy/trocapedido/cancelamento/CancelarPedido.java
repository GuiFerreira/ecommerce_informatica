package br.com.ecommerceinformatica.fatec.strategy.trocapedido.cancelamento;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dao.PedidoDAO;
import br.com.ecommerceinformatica.fatec.dao.TrocaPedidoDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensPedido;
import br.com.ecommerceinformatica.fatec.dominio.ItensTroca;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.StatusCompra;
import br.com.ecommerceinformatica.fatec.dominio.StatusTrocaCancelamento;
import br.com.ecommerceinformatica.fatec.dominio.TrocaPedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class CancelarPedido implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		boolean flgTemProdutoEntregar = false;
		TrocaPedido troca = (TrocaPedido) entidade;
		Pedido pedido = troca.getPedido();
		pedido = (Pedido) new PedidoDAO().consultar(pedido).get(0);
		int idCliente = troca.getIdCliente();
		Map<Integer, Integer> mapQntProdutoByIdProduto = new HashMap<Integer, Integer>();
		for (ItensPedido item : pedido.getListProdutosPedido()) {
			if (mapQntProdutoByIdProduto.get(item.getProduto().getId()) == null)
				mapQntProdutoByIdProduto.put(item.getProduto().getId(), item.getQuantidade());
		}
		if (!troca.getListItensTroca().get(0).getStatus().toString().equals(StatusTrocaCancelamento.CANCELAMENTO_APROVADO.toString()))
			return "";
		
		troca.setIdCliente(0);
		// validar se todos os itens foram cancelados
		List<EntidadeDominio> listTrocasPedido = new TrocaPedidoDAO().consultar(troca);
		troca.setIdCliente(idCliente);
		
		for (EntidadeDominio ent : listTrocasPedido) {
			TrocaPedido tro = (TrocaPedido) ent;
			
			for (ItensTroca item : tro.getListItensTroca()) {
				if (!item.getStatus().toString().equals(StatusTrocaCancelamento.CANCELAMENTO_APROVADO.toString()))
					continue;
				int qnt = mapQntProdutoByIdProduto.get(item.getProduto().getId());
				
				qnt -= item.getQntTrocada();
				
				mapQntProdutoByIdProduto.put(item.getProduto().getId(), qnt);
				
			}
			
			
		}
		
		for (Integer idProduto : mapQntProdutoByIdProduto.keySet()) {
			if (mapQntProdutoByIdProduto.get(idProduto) > 0) {
				flgTemProdutoEntregar = true;
				break;
			}
		}
		
		
		// cancelar pedido
		if (!flgTemProdutoEntregar) {
			pedido.setStatusCompra(StatusCompra.CANCELADO);
			new PedidoDAO().alterar(pedido);
		}
		
		
		
		return "";
	}

}
