package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dominio.CartaoPedido;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class CalcularPrecoParcela implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Pedido pedido = (Pedido) entidade;
		System.out.println(pedido.getPrecoTotal());
		int qntCartao = 0;
		
		for (CartaoPedido car : pedido.getListCartoes()) {
			qntCartao += 1;
			
		}
		System.out.println(qntCartao);
		
		double valorPorCartao = pedido.getPrecoTotal() / qntCartao; 
		System.out.println(valorPorCartao);
		
		for (CartaoPedido car : pedido.getListCartoes()) {
			car.setValorTotal(valorPorCartao);
			car.setPrecoParcela(valorPorCartao / car.getQntParcelas());
			
		}
		
		
		
		
		return "";
	}

}
