package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.web.util.Email;

public class EnviarEmailConfirmacaoPedido implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		Pedido pedido = (Pedido) entidade;
		String assunto = "Confirma��o de Compra";
		String corpoEmail = "Parab�nsm, sua solicita��o de compra foi feita. \nC�digo do Pedido: " + pedido.getCodPedido();
		String destinatario = pedido.getCliente().getUsuario().getEmail();
		
		return Email.EnviarEmail(destinatario, assunto, corpoEmail);
	}

}
