package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class CalcularFrete implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Pedido pedido = (Pedido) entidade;
		
		pedido.setValorFrete(15);
		
		double valor = pedido.getPrecoTotal() + pedido.getValorFrete();
		pedido.setPrecoTotal(valor);
		
		return "";
	}

}
