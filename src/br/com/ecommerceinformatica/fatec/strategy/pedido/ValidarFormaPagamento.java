package br.com.ecommerceinformatica.fatec.strategy.pedido;

import java.util.concurrent.TimeUnit;

import br.com.ecommerceinformatica.fatec.dao.PedidoDAO;
import br.com.ecommerceinformatica.fatec.dominio.CartaoPedido;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.StatusCompra;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.web.util.Email;

public class ValidarFormaPagamento implements IStrategy {
	
	// M�TODO PARA VALIDAR SE O CART�O PODE EFETUAR ESSA COMPRA
	@Override
	public String processar(EntidadeDominio entidade) {
		
		Pedido pedido = (Pedido) entidade;
		
		new Thread() {
		    @Override
		    public void run() {
		    	try {
					TimeUnit.SECONDS.sleep(15);
					
					boolean flgDeuCerto = true;
					for (CartaoPedido cartao : pedido.getListCartoes()) {
						char vetCodigo[] = cartao.getCartao().getCodSeguranca().toCharArray();
						
						int valor = 0;
						for (char c : vetCodigo)
							valor += Integer.valueOf(c);
						
						if (valor % 2 == 1) {
							pedido.setStatusCompra(StatusCompra.PAGAMENTO_REJEITADO);
							flgDeuCerto = false;
							break;
						}
						
					}
					
					if (flgDeuCerto)
						pedido.setStatusCompra(StatusCompra.PAGAMENTO_PROCESSADO);
					new PedidoDAO().alterar(pedido);
					
					String destinatario = pedido.getCliente().getUsuario().getEmail();
					String corpoEmail = "O pagamento do seu pedido: " + pedido.getCodPedido() + " foi " + 
							(pedido.getStatusCompra().toString().equals("PAGAMENTO_PROCESSADO") ? "APROVADO" : "REPROVADO\nPor favor, crie um outro pedido e com um cart�o v�lido");
					String assunto = "PAGAMENTO DO PEDIDO";
					
					Email.EnviarEmail(destinatario, assunto, corpoEmail);
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       
		    }
		    
		}.start();
		
		
		
		
		return "";
	}

}
