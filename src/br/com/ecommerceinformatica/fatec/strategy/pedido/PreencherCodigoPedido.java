package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dao.SystemConfigDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.web.util.Constants;

public class PreencherCodigoPedido implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Pedido pedido = (Pedido) entidade;
		
		String codigoPedido = new SystemConfigDAO().consultar(Constants.CODIGO_PEDIDO);
		
		pedido.setCodPedido(codigoPedido);
		
		return "";
	}

}
