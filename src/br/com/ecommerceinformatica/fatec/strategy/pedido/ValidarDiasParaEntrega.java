package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarDiasParaEntrega implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Pedido pedido = (Pedido) entidade;
		
		// 4 dias uteis no minimo
		// para cada produto colocar 1 dia util
		pedido.setDiasEntrega(15 + pedido.getListProdutosPedido().size());
		
		return "";
	}

}
