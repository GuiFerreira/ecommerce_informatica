package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dominio.Cupom;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarValorCupom implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {

		Pedido pedido = (Pedido) entidade;
		Cupom cupom;
		
		
		if (pedido.getCupom() == null || pedido.getCupom().getCodigo().trim().equals(""))
			return "";
		
		if (pedido.getPrecoTotal() < pedido.getCupom().getValor()) 
			pedido.setPrecoTotal(0);
		
				
				
		return "";
	}

}
