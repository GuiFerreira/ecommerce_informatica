package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dao.SystemConfigDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.web.util.Constants;

public class AtualizarCodigo implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		new SystemConfigDAO().alterar(Constants.CODIGO_PEDIDO);
		
		
		return "";
	}

}
