package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarCartoesPedido implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		Pedido pedido = (Pedido) entidade;
		
		if (pedido.getListCartoes() == null || pedido.getListCartoes().isEmpty()) {
			return "<p style='color: red;'>� obrigat�rio colocar ao menos 1 cart�o de cr�dito!</p><br/>";
			
		}
		
		
		return "";
	}

}
