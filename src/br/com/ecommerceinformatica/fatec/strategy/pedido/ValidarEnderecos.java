package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarEnderecos implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Pedido pedido = (Pedido) entidade;
		String msgErro = "";
		
		if (pedido.getEndEntrega() == null || pedido.getEndEntrega().getId() == 0) {
			msgErro += "Ender�o de Entrega � obrigat�rio<br/>";
		}
		
		if (pedido.getEndCobranca() == null || pedido.getEndCobranca().getId() == 0) {
			msgErro += "Ender�o de Cobran�a � obrigat�rio<br/>";
		}
		
		
		return msgErro.equals("") ? "" : "<p style='color: red;'>" + msgErro + "</p>";
	}

}
