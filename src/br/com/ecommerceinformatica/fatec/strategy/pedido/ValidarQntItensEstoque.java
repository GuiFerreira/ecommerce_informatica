package br.com.ecommerceinformatica.fatec.strategy.pedido;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dao.EstoqueDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensEstoque;
import br.com.ecommerceinformatica.fatec.dominio.ItensPedido;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarQntItensEstoque implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		String msgErro = "";
		Pedido pedido = (Pedido) entidade;
		List<EntidadeDominio> listItens = new ArrayList<EntidadeDominio>();
		Map<Integer, ItensPedido> mapItensPedidoByIdProduto = new HashMap<Integer, ItensPedido>();
		
		for (ItensPedido item : pedido.getListProdutosPedido()) {
			EntidadeDominio ent = new EntidadeDominio();
			ent.setId(item.getProduto().getId());
			
			listItens.add(ent);
			mapItensPedidoByIdProduto.put(item.getProduto().getId(), item);
			
		}
		
		for (EntidadeDominio ent : new EstoqueDAO().consultar(listItens)) {
			ItensEstoque estoque = (ItensEstoque) ent;
			ItensPedido item = mapItensPedidoByIdProduto.get(estoque.getProduto().getId());
			
			msgErro += (estoque.getQndEstoque() < item.getQuantidade()) ? "<br/> Produto: " + item.getProduto().getNome() + " Insuficiente no Estoque" : "";
			
		}
		
		
		return msgErro;
	}

}
