package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dao.CupomDAO;
import br.com.ecommerceinformatica.fatec.dominio.Cupom;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensPedido;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class GerarCupomDescontoComADiferencaDoAntigo implements IStrategy {

	@Override
	// REGRA DE VALIDA��O :
	// valida se o cupom usado na compra tinha um valor muito maior do q o valor do pedido
	// se o valor do cupom for maior do q o valor do pedido
	// criar um cupom com o valor da diferen�a
	public String processar(EntidadeDominio entidade) {
		Pedido pedido = (Pedido) entidade;
		double valorTotalPedido = 0;
		
		if (pedido.getCupom() == null || pedido.getCupom().getCodigo().trim().equals(""))
			return "";
		
		
		for (ItensPedido item : pedido.getListProdutosPedido()) {
			valorTotalPedido += item.getPrecoTotal();
		}
		
		valorTotalPedido += pedido.getValorFrete();
		
		if (valorTotalPedido >= pedido.getCupom().getValor()) 
			return "";
		
		Cupom cupom = pedido.getCupom();
		Cupom cupomDiferenca = new Cupom(cupom.getNome() + " Vers�o 2", cupom.getValor() - valorTotalPedido, 
										cupom.getVencimento(), pedido.getCliente().getId(), 1);
		
		String msgErro = new CupomDAO().salvar(cupomDiferenca);
				
				
		return msgErro.equals("") ? msgErro : msgErro + "";
	}

}
