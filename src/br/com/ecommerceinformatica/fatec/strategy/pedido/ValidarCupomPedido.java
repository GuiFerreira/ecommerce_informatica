package br.com.ecommerceinformatica.fatec.strategy.pedido;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.ecommerceinformatica.fatec.dao.CupomDAO;
import br.com.ecommerceinformatica.fatec.dominio.Cupom;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarCupomPedido implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Pedido pedido = (Pedido) entidade;
		String msgErro = "";
		
		if (!pedido.getCupom().getCodigo().trim().equals("")) {
			
			CupomDAO cupomDao = new CupomDAO();
			
			Cupom cupomParaValidar = null;
			List<EntidadeDominio> listCupons = cupomDao.consultar(pedido.getCupom());
			
			if (listCupons.size() > 0) {
				
				cupomParaValidar = (Cupom) listCupons.get(0);
				DateFormat formatadoData = new SimpleDateFormat("dd/MM/yyyy");
				
				try {
					Date vencimentoCupom = formatadoData.parse(cupomParaValidar.getVencimento());
					Date dataAtual = new Date(System.currentTimeMillis());
					
					if (dataAtual.after(vencimentoCupom)) 
						msgErro += "Cupom vencido!\n";
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				if (cupomParaValidar.getIdCliente() != pedido.getCliente().getId())
					msgErro += "Esse cupom n�o pertence a vc...\n";
				
				
				if (msgErro.trim().equals("")) {
					double valor = pedido.getPrecoTotal() - cupomParaValidar.getValor();
					pedido.setPrecoTotal(valor);
					pedido.setCupom(cupomParaValidar);
				}
				
		
			}
			else
				msgErro += "Cupom n�o existe...\n";
		}
		
		return msgErro;
		
	}

}
