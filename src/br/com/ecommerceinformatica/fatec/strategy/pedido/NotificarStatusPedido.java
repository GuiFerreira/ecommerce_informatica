package br.com.ecommerceinformatica.fatec.strategy.pedido;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.web.util.Email;

public class NotificarStatusPedido implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Pedido pedido = (Pedido) entidade;
		String assunto = "Status do Pedido: " + pedido.getCodPedido();
		String corpoEmail = "Seu pedido est� no status: " + pedido.getStatusCompra().toString().replace("_", " ");
		String destinatario = pedido.getCliente().getUsuario().getEmail();
		
		return Email.EnviarEmail(destinatario, assunto, corpoEmail);
		
		
	}

}
