package br.com.ecommerceinformatica.fatec.strategy.produto;

import java.util.List;

import br.com.ecommerceinformatica.fatec.dao.ProdutoDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarExistenciaProduto implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Produto produto = (Produto) entidade;
		Produto validarProduto = new Produto(null, 0, "", produto.getCodProduto(), "", "", "", null, 0, "");
		
		List<EntidadeDominio> listProdutosParecidos = new ProdutoDAO().consultar(validarProduto);
		
		if (!listProdutosParecidos.isEmpty())
			return "J� existe um produto com esse c�digo";
		
		return "";
	}

}
