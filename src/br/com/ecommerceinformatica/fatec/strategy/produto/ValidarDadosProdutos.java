package br.com.ecommerceinformatica.fatec.strategy.produto;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarDadosProdutos implements IStrategy {

	/*
	 * Guilherme Ferreira
	 * @see br.com.ecommerceinformatica.fatec.strategy.IStrategy#processar(br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio)
	 * 
	 * Regra de Neg�cio: RN0011
	 * Validar Campos Obrigat�rios para o cadastro de Produto
	 * 
	 */
	
	
	@Override
	public String processar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		
		String erro = "";
		
		Produto produto = (Produto)entidade;
		
		if(produto != null) {
			if (produto.getNome() == null || produto.getNome().trim().equals("")) 
				erro += "Produto sem nome!\n";
			if (produto.getCodProduto() == null || produto.getCodProduto().trim().equals(""))
				erro += "Produto sem C�digo!\n";
			if (produto.getDescricao() == null || produto.getDescricao().trim().equals(""))
				erro+="Produto sem Descrição!\n";
			if (produto.getEspecificacoes() == null || produto.getEspecificacoes().trim().equals(""))
				erro += "Produto sem Especifica��es!\n";
			if (produto.getMarca() == null || produto.getMarca().trim().equals(""))
				erro += "Produto sem Marca!\n";
			if (produto.getPrecoComprado() == 0)
				erro += "Produto sem Preço de Compra!\n";
			if (produto.getCategoria() == null || produto.getCategoria().getId() == 0)
				erro+="Produto sem categoria!\n";
			if (produto.getFornecedor() == null || produto.getFornecedor().getId() == 0)
				erro+="Produto sem fornecedor!\n";
			
		}
		else
			erro = "N�o criou nenhum produto!";
		
		
		return erro;
	}

}
