package br.com.ecommerceinformatica.fatec.strategy.produto;

import br.com.ecommerceinformatica.fatec.dao.EstoqueDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensEstoque;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.web.util.Util;

public class CriarEstoqueDoProduto implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Produto produto = (Produto) entidade;
		
		ItensEstoque itemEstoque = new ItensEstoque(produto, 0, Util.getToday());
		
		new EstoqueDAO().salvar(itemEstoque);
		
		return "";
	}

}
