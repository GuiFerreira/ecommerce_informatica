package br.com.ecommerceinformatica.fatec.strategy;

import java.util.List;

import br.com.ecommerceinformatica.fatec.dao.CategoriaDAO;
import br.com.ecommerceinformatica.fatec.dao.ClienteDAO;
import br.com.ecommerceinformatica.fatec.dao.FornecedorDAO;
import br.com.ecommerceinformatica.fatec.dao.ProdutoDAO;
import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Fornecedor;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.dominio.Usuario;

public class ValidarExistencia implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub]
		
		String classe = entidade.getClass().getName();
		
		StringBuilder erros = new StringBuilder();
		erros.append("");
		
		// Pegar data atual
		
		if (classe.equals(Categoria.class.getName())) {
			Categoria categoriaEntidade = (Categoria) entidade;
			
			Categoria categoria = new Categoria(categoriaEntidade.getNome(), 0);
			
			CategoriaDAO catDao = new CategoriaDAO();
			
			
			List<EntidadeDominio> listCategoria = catDao.consultar(categoria);
			
			if (!listCategoria.isEmpty()) {
				erros.append("<p style='color: red;'>Essa categoria j� foi Cadastrada... </p><br/>");
				
			}
			
				
				
		}
		else if (classe.equals(Produto.class.getName())) {
			Produto produtoEntidade = (Produto) entidade;
			
			Produto produto = new Produto(null, 0, "", produtoEntidade.getCodProduto(), "", "", "", null, 0, "");
			
			ProdutoDAO proDao = new ProdutoDAO();
			
			List<EntidadeDominio> listProduto = proDao.consultar(produto);
			
			if (!listProduto.isEmpty()) {
				erros.append("<p style='color: red;'>C�digo do Produto j� Cadastrado </p><br/>");
				
			}
			
		}
		
		else if(classe.equals(Cliente.class.getName())) {
			Cliente clienteEntidade = (Cliente) entidade;
			// Cliente para procurar no bd
			Usuario usuario = new Usuario(clienteEntidade.getUsuario().getEmail(), "", false);
			Cliente cliente = new Cliente("", null, "", usuario, "");
			// tirar alguns campos do usu�rio
			
			// Cliente dao
			ClienteDAO cliDAO = new ClienteDAO();
			
			List<EntidadeDominio> listCliente = cliDAO.consultar(cliente);
			
			
			if (listCliente != null) {
				if (!listCliente.isEmpty())
					erros.append("<p style='color: red;'>Email j� cadastrado! </p><br>");
				
			}
			
			Usuario usuarioCpf = new Usuario("", "", false);
			Cliente clienteCPF = new Cliente("", null, clienteEntidade.getCpf(), usuarioCpf, "");
			
			listCliente = cliDAO.consultar(clienteCPF);
			
			if (listCliente != null) {
				if (!listCliente.isEmpty())
					erros.append("<p style='color: red;'>CPF j� cadastrado! </p><br/>");
				
			}
			
		}
		
		
		return erros.toString();
	}

}
