package br.com.ecommerceinformatica.fatec.strategy;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;

public interface IStrategy {
	
	public String processar(EntidadeDominio entidade);
	
	
}
