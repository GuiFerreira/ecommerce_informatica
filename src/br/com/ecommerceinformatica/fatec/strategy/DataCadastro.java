package br.com.ecommerceinformatica.fatec.strategy;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.ecommerceinformatica.fatec.dominio.Cartao;
import br.com.ecommerceinformatica.fatec.dominio.CartaoPedido;
import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Fornecedor;
import br.com.ecommerceinformatica.fatec.dominio.ItensPedido;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.Produto;

public class DataCadastro implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		
		// Pegar data atual
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Date data = Calendar.getInstance().getTime();
		
		String dataFormatada = sdf.format(data);
		
		entidade.setDataCadastro(dataFormatada);
		
		
		String classe = entidade.getClass().getName();
		
		
		// Pegar data atual
		
		if (classe.equals(Fornecedor.class.getName())) {
			Fornecedor fornecedor = (Fornecedor)entidade;
			fornecedor.getContato().setDataCadastro(dataFormatada);
			fornecedor.getEndereco().setDataCadastro(dataFormatada);
			
		}
		else if (classe.equals(Cliente.class.getName())) {
			Cliente cliente = (Cliente) entidade;
			cliente.setDataCadastro(dataFormatada);
			cliente.getUsuario().setDataCadastro(dataFormatada);
			cliente.getContato().setDataCadastro(dataFormatada);
		}
		else if (classe.equals(Pedido.class.getName())) {
			Pedido pedido = (Pedido) entidade;
			pedido.setDataCompra(dataFormatada);
			for (ItensPedido itemPedido: pedido.getListProdutosPedido())
				itemPedido.setDataCompra(dataFormatada);
			for (CartaoPedido cartao : pedido.getListCartoes()) {
				cartao.setDataCadastro(dataFormatada);
			}
			
			
		}
		
		
		
		return new String("");
	}

}
