package br.com.ecommerceinformatica.fatec.strategy.categoria;

import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarDadosCategoria implements IStrategy {

	
	/*
	 * Guilherme Ferreira
	 * @see br.com.ecommerceinformatica.fatec.strategy.IStrategy#processar(br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio)
	 * 
	 * Regra de Neg�cio: RN0017
	 * Validar Campos Obrigat�rios para o cadastro de Categoria
	 * 
	 */
	
	
	@Override
	public String processar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		
		String erro = "";
		
		Categoria categoria = (Categoria)entidade;
		
		if (categoria != null) {
			if (categoria.getNome() == null || categoria.getNome().trim().equals(""))
				erro += "Categoria sem Nome!\n";
			if (categoria.getMargemLucro() == 0)
				erro += "Categoria n�o pode ter margem de lucro igual a 0 (Zero)...\n";
			
		}
		else 
			erro = "Categoria nula...";
		
		
		return erro;
	}

}
