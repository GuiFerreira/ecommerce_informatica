package br.com.ecommerceinformatica.fatec.strategy.categoria;

import java.util.List;

import br.com.ecommerceinformatica.fatec.dao.ProdutoDAO;
import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarExisteAlgumProdutoAtivoDelete implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		Categoria categoria = (Categoria) entidade;
		
		Produto produto = new Produto(categoria, 0, "", "", "", "", "", null, 0, "");
		produto.setTipoFiltroAtivo(1);
		
		List<EntidadeDominio> listProdutosComEssaCategoria = new ProdutoDAO().consultar(produto);
		
		if (listProdutosComEssaCategoria.size() > 0)
			return "Existem produtos ativos com essa categoria...\n";
		
		return "";
	}

}
