package br.com.ecommerceinformatica.fatec.strategy.cliente;

import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarDadosCliente implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {

		String erro =  "";
		
		Cliente cliente = (Cliente) entidade;
		
		if (cliente != null) {
			if (cliente.getCpf() == null || cliente.getCpf().trim().equals(""))
				erro += "Cpf inv�lido! \n";
			
			if (cliente.getDataNascimento() == null || cliente.getDataNascimento().trim().equals(""))
				erro += "Data de Nascimento inv�lida! \n";
			
			if (cliente.getNome() == null || cliente.getNome().trim().equals(""))
				erro += "Nome inv�lido! \n";
			
			if (cliente.getContato() != null) {
				if (cliente.getContato().getDDD() == null || cliente.getContato().getDDD().trim().equals(""))
					erro += "DDD inv�lido!\n";
				else if (cliente.getContato().getEmail() == null || cliente.getContato().getEmail().trim().equals(""))
					erro += "Email inv�lido! \n";
				else if (cliente.getContato().getTelefone() == null || cliente.getContato().getTelefone().trim().equals(""))
					erro += "Telefone inv�lido!\n";
				
			}
			
			else
				erro += "Cliente sem informações de Contato! \n";
			
			if (cliente.getUsuario() != null) {
				if (cliente.getUsuario().getEmail() == null || cliente.getUsuario().getEmail().trim().equals(""))
					erro += "Cliente sem Email de Usu�rio!\n";
				if (cliente.getUsuario().getSenha() == null || cliente.getUsuario().getSenha().trim().equals(""))
					erro += "Cliente sem Senha de Usu�rio!\n";
			}
			
			else
				erro += "Cliente sem Usu�rio!\n";
			
			
		}
		
		else
			erro = "Cliente não instanciado...";
		
		
		return erro;
	}

}
