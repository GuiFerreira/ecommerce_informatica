package br.com.ecommerceinformatica.fatec.strategy.cliente;

import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;
import br.com.ecommerceinformatica.fatec.strategy.cartao.ValidarDadosCartao;

public class ValidarDadosCartaoCliente implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		String msg = "";
		Cliente cliente = (Cliente) entidade;
		
		if (cliente.getListaCartoes() != null) {
			if (cliente.getListaCartoes().get(0) != null) {
				msg = new ValidarDadosCartao().processar(cliente.getListaCartoes().get(0));
			}
		}
		
		
		return msg;
	}

}
