package br.com.ecommerceinformatica.fatec.strategy.fornecedor;

import java.util.List;

import br.com.ecommerceinformatica.fatec.dao.FornecedorDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Fornecedor;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarExistenciaFornecedor implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		StringBuilder erros = new StringBuilder();
		Fornecedor fornecedorEntidade = (Fornecedor)entidade;
		Fornecedor fornecedor = new Fornecedor(fornecedorEntidade.getNome(), null, "", null);
		
		FornecedorDAO fornDAO = new FornecedorDAO();
		// validar se existe um fornecedor com esse nome
		List<EntidadeDominio> listFornecedores =  fornDAO.consultar(fornecedor);
		// verificar se ela ta vazia
		if (!listFornecedores.isEmpty()) {
			erros.append("Fornecedor j� cadastrado pelo Nome... \n");
		}
		
		// validar se existe um fornecedor com esse cnpj
		fornecedor.setNome("");
		fornecedor.setCNPJ(fornecedorEntidade.getCNPJ());
		listFornecedores = fornDAO.consultar(fornecedor);
		// verificar se a lista esta vazia
		if (!listFornecedores.isEmpty()) {
			erros.append("Fornecedor j� cadastrado com esse CNPJ \n");
		}
		
		
		return erros.toString();
	}

}
