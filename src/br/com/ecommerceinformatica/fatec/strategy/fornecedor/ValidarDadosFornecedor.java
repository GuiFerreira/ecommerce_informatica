package br.com.ecommerceinformatica.fatec.strategy.fornecedor;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Fornecedor;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarDadosFornecedor implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		
		String erro = "";
		
		Fornecedor fornecedor = (Fornecedor)entidade;
		
		if (fornecedor != null) {
			if (fornecedor.getNome() == null || fornecedor.getNome().trim().equals(""))
				erro += "Fornecedor sem Nome!\n";
			
			if (fornecedor.getCNPJ() == null || fornecedor.getCNPJ().trim().equals(""))
				erro += "Fornecedor sem CNPJ!\n";
			
			if (fornecedor.getContato().getDDD() == null || fornecedor.getContato().getDDD().trim().equals(""))
				erro += "Fornecedor sem DDD!\n";
			
			if (fornecedor.getContato().getEmail() == null || fornecedor.getContato().getEmail().trim().equals(""))
				erro += "Fornecedor sem Email!\n";
			
			if (fornecedor.getContato().getTelefone() == null || fornecedor.getContato().getTelefone().trim().equals(""))
				erro += "Fornecedor sem Telefone!\n";
			
			if (fornecedor.getEndereco().getRua() == null || fornecedor.getEndereco().getRua().trim().equals(""))
				erro += "Fornecedor sem RUA!\n";
			
			if (fornecedor.getEndereco().getBairro() == null || fornecedor.getEndereco().getBairro().trim().equals(""))
				erro += "Fornecedor sem Bairro!\n";
			
			if (fornecedor.getEndereco().getCep() == null || fornecedor.getEndereco().getCep().trim().equals(""))
				erro += "Fornecedor sem CEP!\n";
			
			if (fornecedor.getEndereco().getNumero() == null || fornecedor.getEndereco().getNumero().trim().equals(""))
				erro+= "Fornecedor sem N�mero!\n";
			
			if (fornecedor.getEndereco().getLogradouro().getLogradouro() == null ||
						fornecedor.getEndereco().getLogradouro().getLogradouro().trim().equals(""))
				erro += "Fornecedor sem Logradouro!\n";
			
			if (fornecedor.getEndereco().getLogradouro().getTpLogradouro() == null ||
						fornecedor.getEndereco().getLogradouro().getTpLogradouro().trim().equals(""))
				erro += "Fornecedor sem Tipo Logradouro!\n";
			
			if (fornecedor.getEndereco().getCidade().getNome() == null || fornecedor.getEndereco().getCidade().getNome().trim().equals(""))
				erro += "Fornecedor sem CIdade!\n";
			
			if (fornecedor.getEndereco().getCidade().getEstado().getNome() == null || 
						fornecedor.getEndereco().getCidade().getEstado().getNome().trim().equals(""))
				erro += "FOrnecedor sem Estado!\n";
			
			if (fornecedor.getEndereco().getCidade().getEstado().getSigla() == null ||
						fornecedor.getEndereco().getCidade().getEstado().getSigla().trim().equals(""))
				erro += "Fornecedor sem Sigla para Estado!\n";
			
			if (fornecedor.getEndereco().getCidade().getEstado().getPais().getNome() == null ||
						fornecedor.getEndereco().getCidade().getEstado().getPais().getNome().trim().equals(""))
				erro += "Fornecedor sem Pais!\n";
			
			if (fornecedor.getEndereco().getCidade().getEstado().getPais().getSigla() == null || 
						fornecedor.getEndereco().getCidade().getEstado().getPais().getSigla().trim().equals(""))
				erro += "Fornecedor sem Sigla para Pais!\n";
			
			
		}
		else
			erro = "Nenhum fornecedor";
		
		
		return erro;
	}

}
