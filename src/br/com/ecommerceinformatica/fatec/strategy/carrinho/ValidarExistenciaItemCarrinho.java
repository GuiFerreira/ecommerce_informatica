package br.com.ecommerceinformatica.fatec.strategy.carrinho;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensCarrinho;
import br.com.ecommerceinformatica.fatec.singleton.CarrinhoSingleton;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarExistenciaItemCarrinho implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		ItensCarrinho ItensCarrinho = null;
		
		if (entidade != null)
			ItensCarrinho = (ItensCarrinho) entidade;
		else
			return "<p style='color: red;'>Objeto ItensCarrinho nulo</p><br/>";
		
		if (ItensCarrinho.getProduto() != null) {
			return (CarrinhoSingleton.getInstance().findById(ItensCarrinho.getProduto().getId()) == null) ? "" : "<p style='color: red;'>Produto j� cadastrado</p><br/>";
			
		}
		
		
		return "<p style='color: red;'>N�o existe Produto</p><br/>";
	}

}
