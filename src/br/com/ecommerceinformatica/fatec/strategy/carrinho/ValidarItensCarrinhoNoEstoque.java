package br.com.ecommerceinformatica.fatec.strategy.carrinho;

import java.util.List;

import br.com.ecommerceinformatica.fatec.dao.EstoqueDAO;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensCarrinho;
import br.com.ecommerceinformatica.fatec.dominio.ItensEstoque;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarItensCarrinhoNoEstoque implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {

		EstoqueDAO estoqueDao = new EstoqueDAO();
		
		ItensCarrinho itemCarrinho = (ItensCarrinho) entidade;
		
		ItensEstoque itemEstoqueParaValidacao = new ItensEstoque(null, 0, "");
		itemEstoqueParaValidacao.setId(itemCarrinho.getProduto().getId());
		
		List<EntidadeDominio> listProdutosEstoque = estoqueDao.consultar(itemEstoqueParaValidacao);
		
		ItensEstoque itemEstoqueBD = (ItensEstoque) listProdutosEstoque.get(0);
		
		return (itemEstoqueBD.getQndEstoque() < itemCarrinho.getQuantidade()) ?
					"<p style='color: red;'>N�o tem itens suficiente no estoque desse produto</p><br/>" : "";
		
	}

}
