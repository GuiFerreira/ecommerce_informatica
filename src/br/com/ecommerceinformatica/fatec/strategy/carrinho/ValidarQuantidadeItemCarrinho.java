package br.com.ecommerceinformatica.fatec.strategy.carrinho;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensCarrinho;
import br.com.ecommerceinformatica.fatec.strategy.IStrategy;

public class ValidarQuantidadeItemCarrinho implements IStrategy {

	@Override
	public String processar(EntidadeDominio entidade) {
		
		ItensCarrinho ItensCarrinho = (entidade != null) ? (ItensCarrinho) entidade : null;
		
		return (ItensCarrinho != null) ? ((ItensCarrinho.getQuantidade() > 3) ? "<p style='color: red;'>Quantidade de Produtos excedido</p><br/>" : "") : "<p style='color: red;'>Objeto Nulo</p><br/>"; 
		
	}

}
