package br.com.ecommerceinformatica.fatec.dominio;

public class CartaoPedido extends EntidadeDominio {
	
	private Cartao cartao;
	private int qntParcelas;
	private double precoParcela;
	private double valorTotal;
	private int idPedido;
	
	
	public CartaoPedido(int idPedido, Cartao cartao, int qntParcelas, double precoParcela) {
		super();
		this.cartao = cartao;
		this.idPedido = idPedido;
		this.qntParcelas = qntParcelas;
		this.precoParcela = precoParcela;
		this.valorTotal = precoParcela * qntParcelas;
	}
	
	public int getIdPedido() {
		return idPedido;
	}

	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Cartao getCartao() {
		return cartao;
	}
	public void setCartao(Cartao cartao) {
		this.cartao = cartao;
	}
	public int getQntParcelas() {
		return qntParcelas;
	}
	public void setQntParcelas(int qntParcelas) {
		this.qntParcelas = qntParcelas;
	}
	public double getPrecoParcela() {
		return precoParcela;
	}
	public void setPrecoParcela(double precoParcela) {
		this.precoParcela = precoParcela;
	}
	
	
	
}
