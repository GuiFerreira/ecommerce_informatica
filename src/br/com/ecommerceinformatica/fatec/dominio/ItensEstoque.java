package br.com.ecommerceinformatica.fatec.dominio;

public class ItensEstoque extends EntidadeDominio{

	private Produto produto;
	private int qndEstoque;
	private String dataUltimaCompra;
	
	public ItensEstoque(Produto produto, int qndEstoque, String dataUltimaCompra) {
		super();
		this.produto = produto;
		this.qndEstoque = qndEstoque;
		this.dataUltimaCompra = dataUltimaCompra;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public int getQndEstoque() {
		return qndEstoque;
	}
	public void setQndEstoque(int qndEstoque) {
		this.qndEstoque = qndEstoque;
	}
	public String getDataUltimaCompra() {
		return dataUltimaCompra;
	}
	public void setDataUltimaCompra(String dataUltimaCompra) {
		this.dataUltimaCompra = dataUltimaCompra;
	}
	
	
}
