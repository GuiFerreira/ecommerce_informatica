package br.com.ecommerceinformatica.fatec.dominio;

/**
 * @author gui17
 *
 */
public class Produto extends EntidadeDominio{
	

	private Categoria categoria;
	private double precoComprado;
	
	private String nome;
	private String codProduto;
	private String descricao;
	private String especificacoes;
	private String marca;
	private Fornecedor fornecedor;
	private double desconto;
	private String uriImagem;
	
	public Produto(Categoria categoria, double precoComprado, String nome, String codProduto, String descricao,
			String especificacoes, String marca, Fornecedor fornecedor, double desconto, String uriImagem) {
		super();
		this.categoria = categoria;
		this.precoComprado = precoComprado;
		this.nome = nome;
		this.codProduto = codProduto;
		this.descricao = descricao;
		this.especificacoes = especificacoes;
		this.marca = marca;
		this.fornecedor = fornecedor;
		this.desconto = desconto;
		this.uriImagem = uriImagem;
	}
	
	public String getUriImagem() {
		return uriImagem;
	}

	public void setUriImagem(String uriImagem) {
		this.uriImagem = uriImagem;
	}
	
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}


	

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	public String getEspecificacoes() {
		return especificacoes;
	}

	public void setEspecificacoes(String especificacoes) {
		this.especificacoes = especificacoes;
	}
	
	public double getDesconto() {
		return desconto;
	}
	
	
	public void setDesconto(double desconto) {
		this.desconto = desconto;
	}
	
	
	public String getCodProduto() {
		return codProduto;
	}


	public void setCodProduto(String codProduto) {
		this.codProduto = codProduto;
	}


	public double getPrecoComprado() {
		return precoComprado;
	}
	public void setPrecoComprado(double precoComprado) {
		this.precoComprado = precoComprado;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	
	
	
}
