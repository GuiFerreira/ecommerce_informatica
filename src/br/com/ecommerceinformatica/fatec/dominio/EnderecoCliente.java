package br.com.ecommerceinformatica.fatec.dominio;

public class EnderecoCliente extends Endereco {
	
	private int idCliente;
	// se 0 entrega
	// se 1 cobran�a
	// se 2 entrega e cobran�a
	private int endCobranca = 0;
	
	
	public EnderecoCliente(String rua, String bairro, String numero, Cidade cidade, String cep, Logradouro logradouro,
			String obsercacoes, String apelidoEndereco, int endCobranca) {
		super(rua, bairro, numero, cidade, cep, logradouro, obsercacoes, apelidoEndereco);
		
		this.endCobranca = endCobranca;
		
	}

	
	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}



	public int getEndCobranca() {
		return endCobranca;
	}


	public void setEndCobranca(int endCobranca) {
		this.endCobranca = endCobranca;
	}
	
	
	
	
	
}
