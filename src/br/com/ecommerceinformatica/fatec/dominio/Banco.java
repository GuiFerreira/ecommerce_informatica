package br.com.ecommerceinformatica.fatec.dominio;

public class Banco extends EntidadeDominio {
	
	private String nome;
	private String numAgencia;
	
	
	public Banco() {
		super();
	}
	public Banco(String nome, String numAgencia) {
		super();
		this.nome = nome;
		this.numAgencia = numAgencia;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNumAgencia() {
		return numAgencia;
	}
	public void setNumAgencia(String numAgencia) {
		this.numAgencia = numAgencia;
	}
	
	
	
	
	
}
