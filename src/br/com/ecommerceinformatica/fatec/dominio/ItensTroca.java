package br.com.ecommerceinformatica.fatec.dominio;

public class ItensTroca extends EntidadeDominio {
	
	private Produto produto;
	private int qntTrocada;
	private double valorUnitario;
	private String dataTroca;
	private StatusTrocaCancelamento status;
	
	public ItensTroca(Produto produto, int qntTrocada, double valorUnitario, String dataTroca, StatusTrocaCancelamento status) {
		super();
		this.produto = produto;
		this.qntTrocada = qntTrocada;
		this.valorUnitario = valorUnitario;
		this.dataTroca = dataTroca;
		this.status = status;
	}
	
	public String getDataTroca() {
		return dataTroca;
	}

	public void setDataTroca(String dataTroca) {
		this.dataTroca = dataTroca;
	}

	public StatusTrocaCancelamento getStatus() {
		return status;
	}

	public void setStatus(StatusTrocaCancelamento status) {
		this.status = status;
	}

	public double getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(double valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public int getQntTrocada() {
		return qntTrocada;
	}

	public void setQntTrocada(int qntTrocada) {
		this.qntTrocada = qntTrocada;
	}
	
	
}
