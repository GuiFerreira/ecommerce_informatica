package br.com.ecommerceinformatica.fatec.dominio;

public class Cartao extends EntidadeDominio {

	private String numero;
	private String nomeImpresso;
	private String codSeguranca;
	private Banco banco;

	public Cartao(String numero, String nomeImpresso, String codSeguranca, Banco banco) {
		super();
		this.numero = numero;
		this.nomeImpresso = nomeImpresso;
		this.codSeguranca = codSeguranca;
		this.banco = banco;
	}

	public Banco getBanco() {
		return banco;
	}

	public void setBanco(Banco banco) {
		this.banco = banco;
	}

	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getNomeImpresso() {
		return nomeImpresso;
	}
	public void setNomeImpresso(String nomeImpresso) {
		this.nomeImpresso = nomeImpresso;
	}
	public String getCodSeguranca() {
		return codSeguranca;
	}
	public void setCodSeguranca(String codSeguranca) {
		this.codSeguranca = codSeguranca;
	}
	
	
}
