package br.com.ecommerceinformatica.fatec.dominio;

public class ItensCarrinho extends EntidadeDominio {
	
	private Produto produto;
	private double precoItem;
	private int quantidade;
	
	public ItensCarrinho(Produto produto, int quantidade, double precoItem) {
		super();
		this.produto = produto;
		this.precoItem = precoItem;
		this.quantidade = quantidade;
	}

	public double getPrecoItem() {
		return precoItem;
	}
	public void setPrecoItem(double precoItem) {
		this.precoItem = precoItem;
	}
	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	
	
}
