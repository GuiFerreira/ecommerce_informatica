package br.com.ecommerceinformatica.fatec.dominio;

public class Logradouro {
	
	private String tpLogradouro;
	private String logradouro;
	
	
	
	public Logradouro(String tpLogradouro, String logradouro) {
		super();
		this.tpLogradouro = tpLogradouro;
		this.logradouro = logradouro;
	}
	
	public String getTpLogradouro() {
		return tpLogradouro;
	}
	public void setTpLogradouro(String tpLogradouro) {
		this.tpLogradouro = tpLogradouro;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	
	
}
