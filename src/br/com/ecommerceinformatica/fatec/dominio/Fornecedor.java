package br.com.ecommerceinformatica.fatec.dominio;

public class Fornecedor extends Pessoa{
	
	private String CNPJ;
	private Endereco endereco;
	

	public Fornecedor(String nome, Contato contato, String cNPJ, Endereco endereco) {
		super(nome, contato);
		CNPJ = cNPJ;
		this.endereco = endereco;
	}


	public String getCNPJ() {
		return CNPJ;
	}


	public void setCNPJ(String CNPJ) {
		this.CNPJ = CNPJ;
	}


	public Endereco getEndereco() {
		return endereco;
	}


	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}


	
	
	
}
