package br.com.ecommerceinformatica.fatec.dominio;

public class Endereco extends EntidadeDominio{
	
	private String rua;
	private String bairro;
	private String numero;
	private Cidade cidade;
	private String cep;
	private Logradouro logradouro;
	private String obsercacoes;
	private String apelidoEndereco;


	public Endereco(String rua, String bairro, String numero, Cidade cidade, String cep, Logradouro logradouro,
			String obsercacoes, String apelidoEndereco) {
		super();
		this.rua = rua;
		this.bairro = bairro;
		this.numero = numero;
		this.cidade = cidade;
		this.cep = cep;
		this.logradouro = logradouro;
		this.obsercacoes = obsercacoes;
		this.apelidoEndereco = apelidoEndereco;
	}
	

	public String getApelidoEndereco() {
		return apelidoEndereco;
	}

	public void setApelidoEndereco(String apelidoEndereco) {
		this.apelidoEndereco = apelidoEndereco;
	}

	
	public Logradouro getLogradouro() {
		return logradouro;
	}


	public void setLogradouro(Logradouro logradouro) {
		this.logradouro = logradouro;
	}




	public String getObsercacoes() {
		return obsercacoes;
	}


	public void setObsercacoes(String obsercacoes) {
		this.obsercacoes = obsercacoes;
	}




	public String getCep() {
		return cep;
	}


	public void setCep(String cep) {
		this.cep = cep;
	}


	public String getRua() {
		return rua;
	}
	public void setRua(String rua) {
		this.rua = rua;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Cidade getCidade() {
		return cidade;
	}
	public void setCidade(Cidade cidade) {
		this.cidade = cidade;
	}
	
	
	
}
