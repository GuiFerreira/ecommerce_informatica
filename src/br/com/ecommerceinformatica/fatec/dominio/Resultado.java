package br.com.ecommerceinformatica.fatec.dominio;

import java.util.ArrayList;
import java.util.List;


public class Resultado {

	private List<EntidadeDominio> entidades;
	private String msgErro;

	public List<EntidadeDominio> add(EntidadeDominio entidade) {

		if (entidades == null) 
			entidades = new ArrayList<EntidadeDominio>();
			
		entidades.add(entidade);

		return entidades;
	}

	public List<EntidadeDominio> getEntidades() {
		return entidades;
	}

	public void setEntidades(List<EntidadeDominio> entidades) {
		this.entidades = entidades;
	}

	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

}
