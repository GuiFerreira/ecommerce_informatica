package br.com.ecommerceinformatica.fatec.dominio;

public enum StatusCompra {
	PAGAMENTO_PENDENTE(false, 0), PAGAMENTO_PROCESSADO(true, 2), PAGAMENTO_REJEITADO(false, 10),
	
	EM_SEPARA��O(true, 3),
	
	ENVIADO_A_TRANSPORTADORA(true, 4), 
	
	EM_TRANSPORTE(true, 5), 

	ENTREGUE(true, 6),
	
	CANCELADO(false, 10),
	
	TROCA_APROVADA(true, 8), TROCA_RECUSADA(true, 8), TROCA_PENDENTE(true, 7);
	
	
	
	private boolean permitirAlteracaoADM;
	// a ideia � sempre exibir o status q ta + 1 (para o adm)
	private int ordemStatus;
	
	StatusCompra(boolean permitirAlteracaoADM, int ordemStatus) {
		this.permitirAlteracaoADM = permitirAlteracaoADM;
		this.ordemStatus = ordemStatus;
	}
	
	public boolean getPermitirAlteracaoADM() {
		return permitirAlteracaoADM;
	}

	public int getOrdemStatus() {
		return ordemStatus;
	}
	
	
	
}
