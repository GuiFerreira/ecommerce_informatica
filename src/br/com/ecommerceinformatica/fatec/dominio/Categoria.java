package br.com.ecommerceinformatica.fatec.dominio;

public class Categoria extends EntidadeDominio{

	private String nome;
	private double margemLucro;

	public Categoria(String nome, double margemLucro) {
		this.nome = nome;
		this.margemLucro = margemLucro;
	}

	public double getMargemLucro() {
		return margemLucro;
	}

	public void setMargemLucro(double margemLucro) {
		this.margemLucro = margemLucro;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
