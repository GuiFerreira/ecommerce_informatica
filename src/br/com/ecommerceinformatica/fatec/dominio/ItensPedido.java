package br.com.ecommerceinformatica.fatec.dominio;

public class ItensPedido extends EntidadeDominio {
	
	private Produto produto;
	private double precoItem;
	private double precoTotal;
	private int quantidade;
	private String dataCompra;
	private int idPedido;
	
	public int getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
	public String getDataCompra() {
		return dataCompra;
	}
	public void setDataCompra(String dataCompra) {
		this.dataCompra = dataCompra;
	}
	public ItensPedido(int idPedido, Produto produto, double precoItem, double precoTotal, int quantidade, String dataCompra) {
		super();
		this.idPedido = idPedido;
		this.produto = produto;
		this.precoItem = precoItem;
		this.precoTotal = precoTotal;
		this.quantidade = quantidade;
		this.dataCompra = dataCompra;
	}
	public Produto getProduto() {
		return produto;
	}
	public void setProduto(Produto produto) {
		this.produto = produto;
	}
	public double getPrecoItem() {
		return precoItem;
	}
	public void setPrecoItem(double precoItem) {
		this.precoItem = precoItem;
	}
	public double getPrecoTotal() {
		return precoTotal;
	}
	public void setPrecoTotal(double precoTotal) {
		this.precoTotal = precoTotal;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	
	
}
