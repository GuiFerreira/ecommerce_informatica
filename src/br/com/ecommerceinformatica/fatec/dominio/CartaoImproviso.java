package br.com.ecommerceinformatica.fatec.dominio;

public class CartaoImproviso extends EntidadeDominio {
	
	private String numCartao;
	private String nomeConta;
	private String codSeguranca;
	
	public CartaoImproviso(String numCartao, String nomeConta, String codSeguranca) {
		super();
		this.numCartao = numCartao;
		this.nomeConta = nomeConta;
		this.codSeguranca = codSeguranca;
	}
	
	
	public String getCodSeguranca() {
		return codSeguranca;
	}


	public void setCodSeguranca(String codSeguranca) {
		this.codSeguranca = codSeguranca;
	}


	public String getNumCartao() {
		return numCartao;
	}
	public void setNumCartao(String numCartao) {
		this.numCartao = numCartao;
	}
	public String getNomeConta() {
		return nomeConta;
	}
	public void setNomeConta(String nomeConta) {
		this.nomeConta = nomeConta;
	}
	
	
	
}
