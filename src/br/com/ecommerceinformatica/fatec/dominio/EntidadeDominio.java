package br.com.ecommerceinformatica.fatec.dominio;

public class EntidadeDominio {
	
	private int id;
	private String dataCadastro;
	private boolean flgAtivo;
	
	// vari�vel usada para saber se traz os ativos, os inativos ou os dois
	// se 1: traz ativo
	// se -1: traz inativo
	// se for qlqr outro n�mero: traz inativo E ativo
	private int tipoFiltroAtivo = 0;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(String dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public boolean isFlgAtivo() {
		return flgAtivo;
	}
	public void setFlgAtivo(boolean flgAtivo) {
		this.flgAtivo = flgAtivo;
	}
	public int getTipoFiltroAtivo() {
		return tipoFiltroAtivo;
	}
	public void setTipoFiltroAtivo(int tipoFiltroAtivo) {
		this.tipoFiltroAtivo = tipoFiltroAtivo;
	}
	
	
	
}
