package br.com.ecommerceinformatica.fatec.dominio;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Cupom extends EntidadeDominio {
	
	private String nome;
	private String codigo;
	private double valor;
	private String vencimento;
	private boolean usado = false;
	private int idCliente;
	
	// 0 = TROCA
	// 1  = DESCONTO
	private int tipoCupom; 
	
	
	public Cupom(String nome, int idCliente, double valor, int qntDias, int tipoCupom) {
		super();
		this.nome = nome;
		this.idCliente = idCliente;
		this.valor = valor;
		this.tipoCupom = tipoCupom;
		setVencimento(qntDias);
	}
	
	
	
	public Cupom(String nome, double valor, String vencimento, int idCliente, int tipoCupom) {
		super();
		this.nome = nome;
		this.valor = valor;
		this.vencimento = vencimento;
		this.idCliente = idCliente;
		this.tipoCupom = tipoCupom;
	}



	public Cupom(String codigo) {
		super();
		this.nome = "";
		this.codigo = codigo;
		this.valor = 0;
		this.tipoCupom = -1;
		this.vencimento = "";
	}



	public int getIdCliente() {
		return idCliente;
	}



	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}



	public void setVencimento(int qntDias) {
		Calendar vencimento = Calendar.getInstance();
		vencimento.setTime(new Date(System.currentTimeMillis()));
		vencimento.add(Calendar.DATE, qntDias);
		
		DateFormat formatarData = new SimpleDateFormat("dd/MM/yyyy");
		
		this.vencimento = formatarData.format(vencimento.getTime());
				
		
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public double getValor() {
		return valor;
	}


	public void setValor(double valor) {
		this.valor = valor;
	}


	public String getVencimento() {
		return vencimento;
	}


	public boolean isUsado() {
		return usado;
	}


	public void setUsado(boolean usado) {
		this.usado = usado;
	}


	public int getTipoCupom() {
		return tipoCupom;
	}


	public void setTipoCupom(int tipoCupom) {
		this.tipoCupom = tipoCupom;
	}



	public void setVencimento(String vencimento) {
		this.vencimento = vencimento;
	}
	
	public String getNomeTipoCupom() {
		return (tipoCupom == 0) ? "Troca" : "Desconto";
	}

	
	
	
}
