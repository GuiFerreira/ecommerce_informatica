package br.com.ecommerceinformatica.fatec.dominio;

public class Usuario extends EntidadeDominio {
	
	private String email;
	private String senha;
	private boolean administrador;
	
	

	public Usuario(String email, String senha, boolean administrador) {
		super();
		this.email = email;
		this.senha = senha;
		this.administrador = administrador;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}


	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public boolean isAdministrador() {
		return administrador;
	}

	public void setAdministrador(boolean administrador) {
		this.administrador = administrador;
	}
	
	
	
	
}
