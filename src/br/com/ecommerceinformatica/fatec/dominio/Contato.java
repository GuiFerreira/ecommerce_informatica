package br.com.ecommerceinformatica.fatec.dominio;

public class Contato extends EntidadeDominio{
	
	private String DDD;
	private String telefone;
	private String email;
	
	public Contato(String dDD, String telefone, String email) {
		super();
		DDD = dDD;
		this.telefone = telefone;
		this.email = email;
	}
	
	public String getDDD() {
		return DDD;
	}
	public void setDDD(String dDD) {
		DDD = dDD;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		StringBuilder json = new StringBuilder();
		
		json.append("{");
		json.append("\"id\" : \"" + this.getId() + "\",");
		json.append("\"telefone\" : \"" + this.getTelefone() + "\"");
		json.append("}");
		
		
		
		
		return json.toString();
		
	}
	
	
	
}
