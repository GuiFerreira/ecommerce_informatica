package br.com.ecommerceinformatica.fatec.dominio;

public class EmailTrocarSenha extends EntidadeDominio {

	private String emailCliente;
	private Boolean usouCodigo;
	
	public EmailTrocarSenha(String emailCliente, Boolean usouCodigo) {
		super();
		this.emailCliente = emailCliente;
		this.usouCodigo = usouCodigo;
	}
	
	public EmailTrocarSenha(String emailCliente, Boolean usouCodigo, int codigo) {
		super();
		this.setId(codigo);
		this.emailCliente = emailCliente;
		this.usouCodigo = usouCodigo;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}

	public Boolean getUsouCodigo() {
		return usouCodigo;
	}

	public void setUsouCodigo(Boolean usouCodigo) {
		this.usouCodigo = usouCodigo;
	}

}
