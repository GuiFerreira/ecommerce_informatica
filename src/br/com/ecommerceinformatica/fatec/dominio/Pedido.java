package br.com.ecommerceinformatica.fatec.dominio;

import java.util.List;

public class Pedido extends EntidadeDominio {
	
	private List<ItensPedido> listProdutosPedido;
	private Cupom cupom;
	private String codPedido;
	private StatusCompra statusCompra;
	private Endereco endEntrega;
	private Endereco endCobranca;
	private Cliente cliente;
	private String dataCompra;
	private double precoTotal;
	private int diasEntrega;
	private List<CartaoPedido> listCartoes;
	private double valorFrete;
	private List<TrocaPedido> listTrocaPedido;
	
	
	public Pedido(List<ItensPedido> listProdutosPedido, Cupom cupom, StatusCompra statusCompra,
			Endereco endEntrega, Endereco endCobranca, Cliente cliente, String dataCompra, double precoTotal,
			int diasEntrega, List<CartaoPedido> listCartoes, String codPedido, double valorFrete) {
		super();
		this.listProdutosPedido = listProdutosPedido;
		this.cupom = cupom;
		this.statusCompra = statusCompra;
		this.endEntrega = endEntrega;
		this.endCobranca = endCobranca;
		this.cliente = cliente;
		this.dataCompra = dataCompra;
		this.precoTotal = precoTotal;
		this.diasEntrega = diasEntrega;
		this.listCartoes = listCartoes;
		this.codPedido = codPedido;
		this.valorFrete = valorFrete;
	}
	

	public List<TrocaPedido> getListTrocaPedido() {
		return listTrocaPedido;
	}


	public void setListTrocaPedido(List<TrocaPedido> listTrocaPedido) {
		this.listTrocaPedido = listTrocaPedido;
	}


	public double getValorFrete() {
		return valorFrete;
	}


	public void setValorFrete(double valorFrete) {
		this.valorFrete = valorFrete;
	}

	public String getCodPedido() {
		return codPedido;
	}

	public void setCodPedido(String codPedido) {
		this.codPedido = codPedido;
	}

	public List<CartaoPedido> getListCartoes() {
		return listCartoes;
	}




	public void setListCartoes(List<CartaoPedido> listCartoes) {
		this.listCartoes = listCartoes;
	}


	
	public int getDiasEntrega() {
		return diasEntrega;
	}
	public void setDiasEntrega(int diasEntrega) {
		this.diasEntrega = diasEntrega;
	}
	public double getPrecoTotal() {
		return precoTotal;
	}
	public void setPrecoTotal(double precoTotal) {
		this.precoTotal = precoTotal;
	}
	public String getDataCompra() {
		return dataCompra;
	}
	public void setDataCompra(String dataCompra) {
		this.dataCompra = dataCompra;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public List<ItensPedido> getListProdutosPedido() {
		return listProdutosPedido;
	}
	public void setListProdutosPedido(List<ItensPedido> listProdutosPedido) {
		this.listProdutosPedido = listProdutosPedido;
	}
	public StatusCompra getStatusCompra() {
		return statusCompra;
	}
	public void setStatusCompra(StatusCompra statusCompra) {
		this.statusCompra = statusCompra;
	}
	public Endereco getEndEntrega() {
		return endEntrega;
	}
	public void setEndEntrega(Endereco endEntrega) {
		this.endEntrega = endEntrega;
	}
	public Endereco getEndCobranca() {
		return endCobranca;
	}
	public void setEndCobranca(Endereco endCobranca) {
		this.endCobranca = endCobranca;
	}




	public Cupom getCupom() {
		return cupom;
	}




	public void setCupom(Cupom cupom) {
		this.cupom = cupom;
	}
	
	
	
	
	
	
	
}
