package br.com.ecommerceinformatica.fatec.dominio;

import java.util.List;

public class TrocaPedido extends EntidadeDominio {
	
	private Pedido pedido;
	private int idCliente;
	private List<ItensTroca> listItensTroca;
	private String dataSolicitacao;
	
	public TrocaPedido(Pedido idPedido, int idCliente, List<ItensTroca> listItensTroca, String dataSolicitacao) {
		super();
		this.pedido = idPedido;
		this.idCliente = idCliente;
		this.listItensTroca = listItensTroca;
		this.dataSolicitacao = dataSolicitacao;
	}
	
	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	
	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public List<ItensTroca> getListItensTroca() {
		return listItensTroca;
	}
	public void setListItensTroca(List<ItensTroca> listItensTroca) {
		this.listItensTroca = listItensTroca;
	}

	public String getDataSolicitacao() {
		return dataSolicitacao;
	}

	public void setDataSolicitacao(String dataSolicitacao) {
		this.dataSolicitacao = dataSolicitacao;
	}
	
	
	
}
