package br.com.ecommerceinformatica.fatec.dominio;

public enum StatusTrocaCancelamento {
	
	CANCELAMENTO_PENDENTE(1,0), CANCELAMENTO_RECUSADO(1, 1), CANCELAMENTO_APROVADO(1, 1),
	
	TROCA_PENDENTE(0, 4), TROCA_APROVADA(0, 5), TROCA_RECUSADA(0, 5);
	
	public int tipoStatus;
	private int ordemStatus;
	
	StatusTrocaCancelamento(int tipoStatus, int ordemStatus) {
		this.tipoStatus = tipoStatus;
		this.ordemStatus = ordemStatus;
	}

	public int getOrdemStatus() {
		return ordemStatus;
	}
	
	
	
}
