package br.com.ecommerceinformatica.fatec.dominio;

import java.util.List;

public class Cliente extends Pessoa {
	
	private String cpf;
	private Usuario usuario;
	private String dataNascimento;
	private List<Cartao> listaCartoes;
	private List<EnderecoCliente> listaEnderecoCobranca;
	private List<EnderecoCliente> listaEnderecoEntrega;
	private List<Pedido> listPedidos;
	private List<Cupom> listCupons;
	
	
	public Cliente(String nome, Contato contato, String cpf, Usuario usuario, String dataNascimento) {
		super(nome, contato);
		this.cpf = cpf;
		this.usuario = usuario;
		this.dataNascimento = dataNascimento;
	}
	
	public Cliente(String nome, Contato contato, String cpf, Usuario usuario, String dataNascimento,
			List<Cartao> listaCartoes, List<EnderecoCliente> listaEnderecoCobranca, List<EnderecoCliente> listaEnderecoEntrega) {
		super(nome, contato);
		this.cpf = cpf;
		this.usuario = usuario;
		this.dataNascimento = dataNascimento;
		this.listaCartoes = listaCartoes;
		this.listaEnderecoCobranca = listaEnderecoCobranca;
		this.listaEnderecoEntrega = listaEnderecoEntrega;
	}
	
	
	
	public List<Cupom> getListCupons() {
		return listCupons;
	}

	public void setListCupons(List<Cupom> listCupons) {
		this.listCupons = listCupons;
	}

	public List<Pedido> getListPedidos() {
		return listPedidos;
	}

	public void setListPedidos(List<Pedido> listPedidos) {
		this.listPedidos = listPedidos;
	}

	public String getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	
	
	public List<Cartao> getListaCartoes() {
		return listaCartoes;
	}
	public void setListaCartoes(List<Cartao> listaCartoes) {
		this.listaCartoes = listaCartoes;
	}
	public List<EnderecoCliente> getListaEnderecoCobranca() {
		return listaEnderecoCobranca;
	}
	public void setListaEnderecoCobranca(List<EnderecoCliente> listaEnderecoCobranca) {
		this.listaEnderecoCobranca = listaEnderecoCobranca;
	}
	public List<EnderecoCliente> getListaEnderecoEntrega() {
		return listaEnderecoEntrega;
	}
	public void setListaEnderecoEntrega(List<EnderecoCliente> listaEnderecoEntrega) {
		this.listaEnderecoEntrega = listaEnderecoEntrega;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
		
	
	
}
