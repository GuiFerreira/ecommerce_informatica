package br.com.ecommerceinformatica.fatec.command;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;

public class ReadCommand extends ACommand {

	@Override
	public Resultado executar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return facade.read(entidade);
	}

}
