package br.com.ecommerceinformatica.fatec.command;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;

public class CreateCommand extends ACommand{

	@Override
	public Resultado executar(EntidadeDominio entidade) {
		return facade.create(entidade);
	}
	
	
	
}
