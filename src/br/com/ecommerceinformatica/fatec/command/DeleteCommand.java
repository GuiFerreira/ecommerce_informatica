package br.com.ecommerceinformatica.fatec.command;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;

public class DeleteCommand extends ACommand {

	@Override
	public Resultado executar(EntidadeDominio entidade) {
		return facade.delete(entidade);
	}

}
