package br.com.ecommerceinformatica.fatec.command;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Resultado;

public interface ICommand {

	public Resultado executar(EntidadeDominio entidade);
	
}
