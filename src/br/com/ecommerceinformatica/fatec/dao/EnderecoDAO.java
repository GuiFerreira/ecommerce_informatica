package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.Cidade;
import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Estado;
import br.com.ecommerceinformatica.fatec.dominio.Logradouro;
import br.com.ecommerceinformatica.fatec.dominio.Pais;

public class EnderecoDAO extends AbstractDAO {

	public EnderecoDAO() {
		super("end_id", "ENDERECOS");
		// TODO Auto-generated constructor stub
	}

	public EnderecoDAO(Connection con) {
		super("end_id", "ENDERECOS", con);
		ctrlTransacao = false;
	}

	private String gerarQuery(Endereco endereco) {
		String query = "SELECT * FROM " + nomeTable;

		List<Integer> listPesquisa = new ArrayList<Integer>();
		Map<Integer, String> mapQuery = new HashMap<Integer, String>();
		boolean maisDeUmWhere = false;

		// verificar se tem algo para filtro
		if (endereco.getId() != 0) {
			listPesquisa.add(1);
			mapQuery.put(1, "end_id = " + endereco.getId());
		} else {
			if (endereco.getRua() != null) {
				if (!endereco.getRua().trim().equals("")) {
					listPesquisa.add(2);
					mapQuery.put(2, "end_rua LIKE '%" + endereco.getRua() + "%'");
				}
			}
			if (endereco.getNumero() != null) {
				if (!endereco.getNumero().trim().equals("")) {
					listPesquisa.add(3);
					mapQuery.put(3, "end_numero LIKE '%" + endereco.getRua() + "%'");
				}
			}
			if (endereco.getBairro() != null) {
				if (!endereco.getBairro().trim().equals("")) {
					listPesquisa.add(4);
					mapQuery.put(4, "end_bairro LIKE '%" + endereco.getBairro() + "%'");
				}
			}
			if (endereco.getCep() != null) {
				if (!endereco.getCep().trim().equals("")) {
					listPesquisa.add(5);
					mapQuery.put(5, "end_cep LIKE '%" + endereco.getCep() + "%'");
				}
			}
			if (endereco.getCidade() != null) {
				if (endereco.getCidade().getNome() != null) {
					if (!endereco.getCidade().getNome().trim().equals("")) {
						listPesquisa.add(6);
						mapQuery.put(6, "end_cidade LIKE '%" + endereco.getCidade().getNome() + "%'");
					}
				}
				if (endereco.getCidade().getEstado().getNome() != null) {
					if (!endereco.getCidade().getEstado().getNome().trim().equals("")) {
						listPesquisa.add(7);
						mapQuery.put(7, "end_estado LIKE '%" + endereco.getCidade().getEstado().getNome() + "%'");
					}
				}
				if (endereco.getCidade().getEstado().getSigla() != null) {
					if (!endereco.getCidade().getEstado().getSigla().trim().equals("")) {
						listPesquisa.add(8);
						mapQuery.put(8, "end_sigla_estado LIKE '%" + endereco.getCidade().getEstado().getSigla() + "%'");
					}
				}
				if (endereco.getCidade().getEstado().getPais().getNome() != null) {
					if (!endereco.getCidade().getEstado().getPais().getNome().trim().equals("")) {
						listPesquisa.add(9);
						mapQuery.put(9, "end_pais LIKE '%" + endereco.getCidade().getEstado().getPais().getNome() + "%'");
					}
				}
				if (endereco.getCidade().getEstado().getPais().getSigla() != null) {
					if (!endereco.getCidade().getEstado().getPais().getSigla().trim().equals("")) {
						listPesquisa.add(10);
						mapQuery.put(10,
								"end_sigla_pais LIKE '%" + endereco.getCidade().getEstado().getPais().getSigla() + "%'");
					}
				}
			}
			if (endereco.getLogradouro() != null) {
				if (endereco.getLogradouro().getLogradouro() != null) {
					if (!endereco.getLogradouro().getLogradouro().trim().equals("")) {
						listPesquisa.add(11);
						mapQuery.put(11, "end_logradouro LIKE '%" + endereco.getLogradouro().getLogradouro() + "%'");
					}
				}
				if (endereco.getLogradouro().getTpLogradouro() != null) {
					if (!endereco.getLogradouro().getTpLogradouro().trim().equals("")) {
						listPesquisa.add(12);
						mapQuery.put(12,
								"end_tipo_logradouro LIKE '%" + endereco.getLogradouro().getTpLogradouro() + "%'");
					}
				}
			}
			if (endereco.getObsercacoes() != null) {
				if (!endereco.getObsercacoes().trim().equals("")) {
					listPesquisa.add(13);
					mapQuery.put(13, "end_observacoes LIKE '%" + endereco.getObsercacoes() + "%'");
				}
			}
			if (endereco.getApelidoEndereco() != null) {
				if (!endereco.getApelidoEndereco().trim().equals("")) {
					listPesquisa.add(14);
					mapQuery.put(14, "end_apelido LIKE '%" + endereco.getApelidoEndereco() + "%'");
				}
			}
			
		}
		if (!listPesquisa.isEmpty()) {
			query += " WHERE";
			for (Integer a : listPesquisa) {
				if (maisDeUmWhere) {
					query += " AND";
				}
				query += " " + mapQuery.get(a);
				maisDeUmWhere = true;
			}
		}

		return query;
	}
	
	public List<EntidadeDominio> consultar(List<EntidadeDominio> listEntidades) {
		abrirConexao();

		List<EntidadeDominio> listaEndereco = new ArrayList<EntidadeDominio>();

		String sql = "SELECT * FROM " + nomeTable + " WHERE end_id IN (";
		boolean flgMaisDeUm = false;
		for (EntidadeDominio entidade : listEntidades) {
			if (flgMaisDeUm) sql += ", ";
			
			sql += entidade.getId();
			flgMaisDeUm = true;
		}
		sql += ");";

		PreparedStatement ps = null;

		try {
			ps = conexao.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			Endereco endereco = null;

			while (rs.next()) {
				Pais pais = new Pais(rs.getString("end_pais"), rs.getString("end_sigla_pais"));
				Estado estado = new Estado(rs.getString("end_estado"), rs.getString("end_sigla_estado"), pais);
				Cidade cidade = new Cidade(rs.getString("end_cidade"), estado);

				endereco = new Endereco(rs.getString("end_rua"), rs.getString("end_bairro"), rs.getString("end_numero"),
						cidade, rs.getString("end_cep"),
						new Logradouro(rs.getString("end_tipo_logradouro"), rs.getString("end_logradouro")),
						rs.getString("end_observacoes"), rs.getString("end_apelido"));

				endereco.setId(rs.getInt("end_id"));
				endereco.setDataCadastro(rs.getString("end_data_cadastro"));

				listaEndereco.add(endereco);

			}

			return listaEndereco;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
		
		
	}
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub

		abrirConexao();

		Endereco enderecoQuery = (Endereco) entidade;
		List<EntidadeDominio> listaEndereco = new ArrayList<EntidadeDominio>();

		String sql = gerarQuery(enderecoQuery);

		PreparedStatement ps = null;

		try {
			ps = conexao.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			Endereco endereco = null;

			while (rs.next()) {
				Pais pais = new Pais(rs.getString("end_pais"), rs.getString("end_sigla_pais"));
				Estado estado = new Estado(rs.getString("end_estado"), rs.getString("end_sigla_estado"), pais);
				Cidade cidade = new Cidade(rs.getString("end_cidade"), estado);

				endereco = new Endereco(rs.getString("end_rua"), rs.getString("end_bairro"), rs.getString("end_numero"),
						cidade, rs.getString("end_cep"),
						new Logradouro(rs.getString("end_tipo_logradouro"), rs.getString("end_logradouro")),
						rs.getString("end_observacoes"), rs.getString("end_apelido"));

				endereco.setId(rs.getInt("end_id"));
				endereco.setDataCadastro(rs.getString("end_data_cadastro"));

				listaEndereco.add(endereco);

			}

			return listaEndereco;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		abrirConexao();

		StringBuilder sql = new StringBuilder();
		PreparedStatement ps = null;

		Endereco endereco = (Endereco) entidade;

		try {
			conexao.setAutoCommit(false);

			sql.append("INSERT INTO " + nomeTable + "(end_rua, end_bairro, end_numero, end_cep,"
					+ "end_pais, end_sigla_pais, end_estado, end_sigla_estado, end_cidade, end_data_cadastro,"
					+ "end_tipo_logradouro, end_logradouro, end_observacoes, end_apelido, ativo)" + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?, true)");

			ps = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, endereco.getRua());
			ps.setString(2, endereco.getBairro());
			ps.setString(3, endereco.getNumero());
			ps.setString(4, endereco.getCep());
			ps.setString(5, endereco.getCidade().getEstado().getPais().getNome());
			ps.setString(6, endereco.getCidade().getEstado().getPais().getSigla());
			ps.setString(7, endereco.getCidade().getEstado().getNome());
			ps.setString(8, endereco.getCidade().getEstado().getSigla());
			ps.setString(9, endereco.getCidade().getNome());
			ps.setString(10, endereco.getDataCadastro());
			ps.setString(11, endereco.getLogradouro().getTpLogradouro());
			ps.setString(12, endereco.getLogradouro().getLogradouro());
			ps.setString(13, endereco.getObsercacoes());
			ps.setString(14, endereco.getApelidoEndereco());

			ps.executeUpdate();

			// obter o id do endereco
			ResultSet rs = ps.getGeneratedKeys();

			int idEnd = 0;
			while (rs.next())
				idEnd = rs.getInt(1);

			endereco.setId(idEnd);

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
				return "N�o foi poss�vel salvar o Endereco";
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return "";
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		abrirConexao();

		Endereco endereco = (Endereco) entidade;

		String sql = "UPDATE " + nomeTable + " SET end_rua = ?, end_bairro = ?, " + 
				"end_numero = ?, end_cep = ?, end_pais = ?, end_sigla_pais = ?, end_estado = ?, " + 
				"end_sigla_estado = ?, end_cidade = ?, end_tipo_logradouro = ?, end_logradouro = ?, " + 
				"end_observacoes = ? WHERE " + idTable + " = " + endereco.getId();

		PreparedStatement ps = null;

		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			ps.setString(1, endereco.getRua());
			ps.setString(2, endereco.getBairro());
			ps.setString(3, endereco.getNumero());
			ps.setString(4, endereco.getCep());
			ps.setString(5, endereco.getCidade().getEstado().getPais().getNome());
			ps.setString(6, endereco.getCidade().getEstado().getPais().getSigla());
			ps.setString(7, endereco.getCidade().getEstado().getNome());
			ps.setString(8, endereco.getCidade().getEstado().getSigla());
			ps.setString(9, endereco.getCidade().getNome());
			ps.setString(10, endereco.getLogradouro().getLogradouro());
			ps.setString(11, endereco.getLogradouro().getTpLogradouro());
			ps.setString(12, endereco.getObsercacoes());
				
			
			ps.executeUpdate();

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			return "ERRO";
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return "";

	}
	
	public String excluir(EntidadeDominio entidade) {
		return desativarItem(entidade);
	}

}
