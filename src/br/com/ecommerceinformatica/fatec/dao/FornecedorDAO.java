package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Fornecedor;

public class FornecedorDAO extends AbstractDAO {

	public FornecedorDAO() {
		super("for_id", "FORNECEDORES");
	}

	public FornecedorDAO(Connection con) {
		super("for_id", "FORNECEDORES", con);
		ctrlTransacao = false;
	}

	private String gerarQuery(Fornecedor fornecedor) {
		String query = "SELECT * FROM " + nomeTable;

		List<Integer> listPesquisa = new ArrayList<Integer>();
		Map<Integer, String> mapQuery = new HashMap<Integer, String>();
		boolean maisDeUmWhere = false;

		// verificar se tem algo para filtro
		if (fornecedor.getId() != 0) {
			listPesquisa.add(1);
			mapQuery.put(1, "for_id = " + fornecedor.getId());
		} else {
			if (fornecedor.getCNPJ() != null) {
				if (!fornecedor.getCNPJ().trim().equals("")) {
					listPesquisa.add(2);
					mapQuery.put(2, "for_cnpj LIKE '%" + fornecedor.getCNPJ() + "%'");
				}
			}
			if (fornecedor.getNome() != null) {
				if (!fornecedor.getNome().trim().equals("")) {
					listPesquisa.add(3);
					mapQuery.put(3, "for_nome LIKE '%" + fornecedor.getNome() + "%'");
				}
			}
		}
		if (!listPesquisa.isEmpty()) {
			query += " WHERE";
			for (Integer a : listPesquisa) {
				if (maisDeUmWhere) {
					query += " AND";
				}
				query += " " + mapQuery.get(a);
				maisDeUmWhere = true;
			}
		}

		return query;
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		abrirConexao();

		Fornecedor fornecedorQuery = (Fornecedor) entidade;

		String sqlQuery = gerarQuery(fornecedorQuery);

		PreparedStatement ps = null;

		List<EntidadeDominio> listaFornecedor = new ArrayList<EntidadeDominio>();

		try {
			ps = conexao.prepareStatement(sqlQuery);

			ResultSet rs = ps.executeQuery();

			Fornecedor fornecedor = null;
			// fazer uma busca de todos os fornecedores de acordo com a query
			while (rs.next()) {
				// pegar id do contato do fornecedor
				Contato contato = new Contato("", "", "");
				contato.setId(rs.getInt("for_con_id"));

				// pegar id do endereco do fornecedor
				Endereco endereco = new Endereco("", "", "", null, "", null, null, "");
				endereco.setId(rs.getInt("for_end_id"));

				// instanciar o fornecedor
				fornecedor = new Fornecedor(rs.getString("for_nome"), contato, rs.getString("for_cnpj"), endereco);
				fornecedor.setId(rs.getInt("for_id"));
				fornecedor.setDataCadastro(rs.getString("for_data_cadastro"));

				// adicionar fornecedor a lista
				listaFornecedor.add(fornecedor);

			}

			// fazer uma busca com o contato dos fornecedores
			ContatoDAO contDAO = new ContatoDAO(conexao);
			contDAO.ctrlTransacao = false;
			Contato contato = new Contato("", "", "");
			List<EntidadeDominio> listaContatos = contDAO.consultar(contato);

			// para cada fornecedor colocar seu contato
			for (EntidadeDominio forn : listaFornecedor) {
				fornecedor = (Fornecedor) forn;
				for (EntidadeDominio cont : listaContatos) {
					contato = (Contato) cont;
					if (fornecedor.getContato().getId() == contato.getId()) {
						fornecedor.setContato(contato);
						break;
					}

				}
			}

			// fazer uma busca com o endere�o dos fornecedores
			EnderecoDAO endDAO = new EnderecoDAO(conexao);
			endDAO.ctrlTransacao = false;
			Endereco endereco = new Endereco("", "", "", null, "", null, null, "");
			List<EntidadeDominio> listaEnd = endDAO.consultar(endereco);

			// para cada fornecedor colocar seu endereco
			for (EntidadeDominio forn : listaFornecedor) {
				fornecedor = (Fornecedor) forn;
				for (EntidadeDominio end : listaEnd) {
					endereco = (Endereco) end;
					if (fornecedor.getEndereco().getId() == endereco.getId()) {
						fornecedor.setEndereco(endereco);
						break;
					}

				}
			}

			return listaFornecedor;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		abrirConexao();

		StringBuilder sql = new StringBuilder();
		PreparedStatement ps = null;

		Fornecedor fornecedor = (Fornecedor) entidade;
		Endereco endereco = fornecedor.getEndereco();
		Contato contato = fornecedor.getContato();

		try {
			conexao.setAutoCommit(false);
			// salvar o endere�o e retornar o id dele
			EnderecoDAO endDAO = new EnderecoDAO(conexao);
			// Salvando ele no banco de dados eu ja tenho o ID do endereco
			endDAO.ctrlTransacao = false;
			endDAO.salvar(endereco);

			// salvar contato do fornecedor
			ContatoDAO contDAO = new ContatoDAO(conexao);
			contDAO.ctrlTransacao = false;
			contDAO.salvar(contato);

			sql.append("INSERT INTO " + nomeTable + "(for_nome, for_cnpj, for_con_id, for_end_id, for_data_cadastro, ativo)"
					+ " VALUES(?,?,?,?,?, true)");

			ps = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, fornecedor.getNome());
			ps.setString(2, fornecedor.getCNPJ());
			ps.setInt(3, contato.getId());
			ps.setInt(4, endereco.getId());
			ps.setString(5, fornecedor.getDataCadastro());

			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			int idForncedor = 0;
			while (rs.next())
				idForncedor = rs.getInt(1);
			fornecedor.setId(idForncedor);

			ctrlTransacao = true;

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return "";
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		abrirConexao();

		Fornecedor fornecedor = (Fornecedor) entidade;

		String sql = "UPDATE " + nomeTable + " SET for_nome = ?, for_cnpj = ? WHERE " + idTable + " = " + fornecedor.getId();

		PreparedStatement ps = null;

		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			ps.setString(1, fornecedor.getNome());
			ps.setString(2, fornecedor.getCNPJ());

			String erro = "";

			EnderecoDAO endDAO = new EnderecoDAO(conexao);
			endDAO.ctrlTransacao = false;
			erro = endDAO.alterar(fornecedor.getEndereco());
			
			if (!erro.equals("")) {
				try {
					conexao.rollback();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return "ERRO";
			}
			
			
			ContatoDAO conDAO = new ContatoDAO(conexao);
			conDAO.ctrlTransacao = false;
			erro = conDAO.alterar(fornecedor.getContato());
			
			if (!erro.equals("")) {
				try {
					conexao.rollback();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				return "ERRO";
			}
			
			
			ps.executeUpdate();
			
			if(ctrlTransacao)
			conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			return "ERRO";
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return "";
	}

}
