package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EnderecoCliente;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;

public class EnderecoClienteDAO extends AbstractDAO {
	
	public EnderecoClienteDAO() {
		super("cliend_id", "clientes_enderecos");
		
	}
	
	public EnderecoClienteDAO(Connection conexao) {
		super("cliend_id", "clientes_enderecos", conexao);
		ctrlTransacao = false;
		
	}
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		
		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();
		
		EnderecoCliente endCliente = (EnderecoCliente) entidade;
		String sql = "INSERT INTO " + nomeTable + "(cliend_cli_id, cliend_end_id, cliend_cobranca) VALUES(?,?,?);";
		PreparedStatement ps = null;
		String erro = "";
		
		try {
			conexao.setAutoCommit(false);
			Endereco endereco = new Endereco(endCliente.getRua(), endCliente.getBairro(),
					endCliente.getNumero(), endCliente.getCidade(), endCliente.getCep(),
					endCliente.getLogradouro(), endCliente.getObsercacoes(), endCliente.getApelidoEndereco());
			
			erro = new EnderecoDAO().salvar(endereco);
			
			if (!erro.trim().equals(""))
				return "Erro ao salvar Endere�o Cliente";
			
			ps = conexao.prepareStatement(sql);
			
			ps.setInt(1, endCliente.getIdCliente());
			ps.setInt(2, endereco.getId());
			ps.setInt(3, endCliente.getEndCobranca());
			
			
			ps.execute();
			
			
			
			if (ctrlTransacao)
				conexao.commit();
			
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				return "erro ao dar rollback EnderecoCLienteDAO";
			}
			e.printStackTrace();
			return "Erro de catch ao salvar EnderecoCliente";
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return "Erro ao fechar conexao ou ps do EnderecoCliente em salvar";
				}
			}
			
		}
		
		return "";
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return null;
	}

}
