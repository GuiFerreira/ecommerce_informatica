package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;

public class ContatoDAO extends AbstractDAO {

	public ContatoDAO() {
		// TODO Auto-generated constructor stub
		super("con_id", "CONTATOS");
	}

	public ContatoDAO(Connection con) {
		
		super("con_id", "CONTATOS", con);
		ctrlTransacao = false;
	}

	private String gerarQuery(Contato contato) {
		String query = "SELECT * FROM " + nomeTable;

		List<Integer> listPesquisa = new ArrayList<Integer>();
		Map<Integer, String> mapQuery = new HashMap<Integer, String>();
		boolean maisDeUmWhere = false;

		// verificar se tem algo para filtro
		if (contato.getId() != 0) {
			listPesquisa.add(1);
			mapQuery.put(1, "con_id = " + contato.getId());
		} else {
			if (contato.getDDD() != null) {
				if (!contato.getDDD().trim().equals("")) {
					listPesquisa.add(2);
					mapQuery.put(2, "con_ddd LIKE '" + contato.getDDD() + "'");
				}
			}
			if (contato.getEmail() != null) {
				if (!contato.getEmail().trim().equals("")) {
					listPesquisa.add(3);
					mapQuery.put(3, "con_email LIKE '" + contato.getEmail() + "'");
				}
			}
			if (contato.getTelefone() != null) {
				if (!contato.getTelefone().trim().equals("")) {
					listPesquisa.add(4);
					mapQuery.put(4, "con_telefone LIKE '" + contato.getTelefone() + "'");
				}
			}
		}

		if (!listPesquisa.isEmpty()) {
			query += " WHERE";
			for (Integer a : listPesquisa) {
				if (maisDeUmWhere) {
					query += " AND";
				}
				query += " " + mapQuery.get(a);
				maisDeUmWhere = true;
			}
		}

		return query;
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		abrirConexao();

		List<EntidadeDominio> listaContato = new ArrayList<EntidadeDominio>();
		Contato contatoQuery = (Contato) entidade;

		String sql = gerarQuery(contatoQuery);

		PreparedStatement ps = null;

		try {
			ps = conexao.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			Contato contato = null;

			while (rs.next()) {
				contato = new Contato(rs.getString("con_ddd"), rs.getString("con_telefone"), rs.getString("con_email"));

				contato.setId(rs.getInt("con_id"));
				contato.setDataCadastro(rs.getString("con_data_cadastro"));

				listaContato.add(contato);

			}

			return listaContato;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		abrirConexao();

		StringBuilder sql = new StringBuilder();
		PreparedStatement ps = null;

		Contato contato = (Contato) entidade;

		try {
			conexao.setAutoCommit(false);
			
			sql.append("INSERT INTO " + nomeTable + "(con_ddd, con_telefone, con_email, con_data_cadastro, ativo)"
					+ " VALUES(?,?,?,?, true)");

			System.out.println("Sql Contato = " + sql);
			
			ps = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, contato.getDDD());
			ps.setString(2, contato.getTelefone());
			ps.setString(3, contato.getEmail());
			ps.setString(4, contato.getDataCadastro());

			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			int idContato = 0;
			while (rs.next())
				idContato = rs.getInt(1);
			System.out.println("id contato = " + idContato);
			contato.setId(idContato);

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			try {
				conexao.rollback();
				return "N�o foi poss�vel salvar o contato";
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return "";

	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		abrirConexao();

		Contato contato= (Contato) entidade;

		String sql = "UPDATE " + nomeTable + " SET con_ddd = ?, con_telefone = ?," + 
					" con_email = ? WHERE " + idTable + " = " + contato.getId();

		PreparedStatement ps = null;

		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			ps.setString(1, contato.getDDD());
			ps.setString(2, contato.getTelefone());
			ps.setString(3, contato.getEmail());
			

			ps.executeUpdate();

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			return "ERRO";
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return "";
	}

}
