package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.Banco;
import br.com.ecommerceinformatica.fatec.dominio.Cartao;
import br.com.ecommerceinformatica.fatec.dominio.CartaoPedido;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensTroca;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.Produto;
import br.com.ecommerceinformatica.fatec.dominio.StatusTrocaCancelamento;
import br.com.ecommerceinformatica.fatec.dominio.TrocaPedido;
import br.com.ecommerceinformatica.fatec.web.util.Constants;

public class TrocaPedidoDAO extends AbstractDAO {

	public TrocaPedidoDAO() {
		super("itro_ped_id", "ITENS_TROCA");
	}
	
	public TrocaPedidoDAO(Connection conexao) {
		super("itro_ped_id", "ITENS_TROCA", conexao);
	}
	
	private String gerarQuery(TrocaPedido troca) {
		String query = "SELECT * FROM " + nomeTable + " LEFT JOIN PRODUTOS AS PRO ON PRO.pro_id = itro_pro_id LEFT JOIN PEDIDO ON ped_id = itro_ped_id" +
					" LEFT JOIN Clientes ON itro_cli_id = cli_id";

		List<Integer> listPesquisa = new ArrayList<Integer>();
		Map<Integer, String> mapQuery = new HashMap<Integer, String>();
		boolean maisDeUmWhere = false;

		// verificar se tem algo para filtro
		// id de um adm
		if (troca.getIdCliente() == -1) {
			listPesquisa.add(1);
			mapQuery.put(1, "itro_status = '" + StatusTrocaCancelamento.CANCELAMENTO_PENDENTE.toString() + "' OR " +
						"itro_status = '" + StatusTrocaCancelamento.TROCA_PENDENTE.toString() + "'");
		}
		// por cliente
		else if (troca.getIdCliente() != 0){
			listPesquisa.add(1);
			mapQuery.put(1, "itro_cli_id = " + troca.getIdCliente());
		}
		else if (troca.getPedido() != null && troca.getPedido().getId() != 0){
			listPesquisa.add(1);
			mapQuery.put(1, "itro_ped_id = " + troca.getPedido().getId());
		}
			
		
		if (!listPesquisa.isEmpty()) {
			query += " WHERE";
			for (Integer a : listPesquisa) {
				if (maisDeUmWhere) {
					query += " AND";
				}
				query += " " + mapQuery.get(a);
				maisDeUmWhere = true;
			}
		}

		return query;
	}

	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		abrirConexao();

		TrocaPedido contatoQuery = (TrocaPedido) entidade;
		List<EntidadeDominio> listaItensTroca = new ArrayList<EntidadeDominio>();
		Map<Integer, Map<Integer,TrocaPedido>> mapTrocaPedidoByIdPedido = new HashMap<Integer, Map<Integer,TrocaPedido>>();
		Map<Integer, Pedido> mapPedido = new HashMap<Integer, Pedido>();
		
		String sql = gerarQuery(contatoQuery);
		PreparedStatement ps = null;

		try {
			ps = conexao.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			ItensTroca item = null;

			while (rs.next()) {
				int idPedido = rs.getInt("itro_ped_id");
				int idTroca = rs.getInt("itro_id");
				
				if (mapTrocaPedidoByIdPedido.get(idPedido) == null) {
					Pedido pedido = new Pedido(null, null, null, null, null, null, "", 0, 0, null, "", 0);
					pedido.setCodPedido(rs.getString("ped_codigo"));
					pedido.setId(rs.getInt("ped_id"));
					
					mapPedido.put(idPedido, pedido);
					
					mapTrocaPedidoByIdPedido.put(idPedido, new HashMap<Integer, TrocaPedido>());
					if (mapTrocaPedidoByIdPedido.get(idPedido).get(idTroca) == null)
						mapTrocaPedidoByIdPedido.get(idPedido).put(idTroca, new TrocaPedido(pedido, rs.getInt("cli_id"), new ArrayList<ItensTroca>(), 
							rs.getString("itro_data_solicitacao")));
						
					mapTrocaPedidoByIdPedido.get(idPedido).get(idTroca).setId(rs.getInt("itro_id"));
				}
				else if (mapTrocaPedidoByIdPedido.get(idPedido).get(idTroca) == null) {
					mapTrocaPedidoByIdPedido.get(idPedido).put(idTroca, new TrocaPedido(mapPedido.get(idPedido), rs.getInt("cli_id"), new ArrayList<ItensTroca>(), 
							rs.getString("itro_data_solicitacao")));
				}
				
				TrocaPedido troca = mapTrocaPedidoByIdPedido.get(idPedido).get(idTroca);
				
				Produto produto = new Produto(null, 0, rs.getString("pro_nome"), rs.getString("pro_codigo"), "", "", rs.getString("pro_marca"),
						null, 0, rs.getString("pro_imagem"));
				produto.setId(rs.getInt("pro_id"));
				
				item = new ItensTroca(produto, rs.getInt("itro_quantidade"), rs.getDouble("itro_valor_unitario"), rs.getString("itro_data_troca"),
						StatusTrocaCancelamento.valueOf(rs.getString("itro_status")));
				
				troca.getListItensTroca().add(item);
				
			}
			
			for (Map<Integer,TrocaPedido> map : mapTrocaPedidoByIdPedido.values()) {
				for (int id : map.keySet())
					listaItensTroca.add(map.get(id));
			}

			return listaItensTroca;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}
	
	
	public List<EntidadeDominio> consultar(List<EntidadeDominio> listEntidades) {
		abrirConexao();

		String sql = "SELECT * FROM " + nomeTable + 
					" LEFT JOIN PRODUTOS AS PRO ON PRO.pro_id = itro_pro_id LEFT JOIN PEDIDO ON ped_id = itro_ped_id" +
				" LEFT JOIN Clientes ON itro_cli_id = cli_id " + 
						"WHERE " + idTable + " IN (";
		boolean flgMaisDeUm = false;
		for (EntidadeDominio entidade : listEntidades) {
			if (flgMaisDeUm) sql += ", ";
			
			sql += entidade.getId();
			flgMaisDeUm = true;
		}
		sql += ");";
		PreparedStatement ps = null;
		
		List<EntidadeDominio> listaItensTroca = new ArrayList<EntidadeDominio>();
		Map<Integer, Map<Integer,TrocaPedido>> mapTrocaPedidoByIdPedido = new HashMap<Integer, Map<Integer,TrocaPedido>>();
		Map<Integer, Pedido> mapPedido = new HashMap<Integer, Pedido>();

		try {
			ps = conexao.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			ItensTroca item = null;

			while (rs.next()) {
				int idPedido = rs.getInt("itro_ped_id");
				int idTroca = rs.getInt("itro_id");
				
				if (mapTrocaPedidoByIdPedido.get(idPedido) == null) {
					Pedido pedido = new Pedido(null, null, null, null, null, null, "", 0, 0, null, "", 0);
					pedido.setCodPedido(rs.getString("ped_codigo"));
					pedido.setId(rs.getInt("ped_id"));
					
					mapPedido.put(idPedido, pedido);
					
					mapTrocaPedidoByIdPedido.put(idPedido, new HashMap<Integer, TrocaPedido>());
					if (mapTrocaPedidoByIdPedido.get(idPedido).get(idTroca) == null) {
						mapTrocaPedidoByIdPedido.get(idPedido).put(idTroca, new TrocaPedido(pedido, rs.getInt("cli_id"), new ArrayList<ItensTroca>(), 
								rs.getString("itro_data_solicitacao")));
					}
						
					mapTrocaPedidoByIdPedido.get(idPedido).get(idTroca).setId(rs.getInt("itro_id"));
				}
				else if (mapTrocaPedidoByIdPedido.get(idPedido).get(idTroca) == null) {
					mapTrocaPedidoByIdPedido.get(idPedido).put(idTroca, new TrocaPedido(mapPedido.get(idPedido), rs.getInt("cli_id"), new ArrayList<ItensTroca>(), 
							rs.getString("itro_data_solicitacao")));
					
					mapTrocaPedidoByIdPedido.get(idPedido).get(idTroca).setId(rs.getInt("itro_id"));
				}
				
				TrocaPedido troca = mapTrocaPedidoByIdPedido.get(idPedido).get(idTroca);
				
				Produto produto = new Produto(null, 0, rs.getString("pro_nome"), rs.getString("pro_codigo"), "", "", rs.getString("pro_marca"),
						null, 0, rs.getString("pro_imagem"));
				produto.setId(rs.getInt("pro_id"));
				
				item = new ItensTroca(produto, rs.getInt("itro_quantidade"), rs.getDouble("itro_valor_unitario"), rs.getString("itro_data_troca"),
						StatusTrocaCancelamento.valueOf(rs.getString("itro_status")));
				
				troca.getListItensTroca().add(item);
				
			}
			
			for (Map<Integer,TrocaPedido> map : mapTrocaPedidoByIdPedido.values()) {
				for (int id : map.keySet())
					listaItensTroca.add(map.get(id));
			}

			return listaItensTroca;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
		
	}
	
	

	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement ps = null;
		SystemConfigDAO sysConfig = new SystemConfigDAO(conexao);
		TrocaPedido troca = (TrocaPedido) entidade;
		
		int idTroca = Integer.valueOf(sysConfig.consultar(Constants.ID_TROCA_PEDIDO));
		
		try {
			conexao.setAutoCommit(false);

			sql.append("INSERT INTO " + nomeTable + "(itro_ped_id, itro_pro_id, itro_status, itro_quantidade, itro_data_solicitacao,"
					+ " itro_valor_unitario, itro_cli_id, itro_id) VALUES");
			
			boolean flgPrimeiro = false;
			for (ItensTroca item : troca.getListItensTroca()) {
				if (flgPrimeiro) sql.append(", ");
				sql.append("(" + troca.getPedido().getId() + ", " + item.getProduto().getId() + ", '" + item.getStatus().toString() + "', " + 
							item.getQntTrocada() + ", '" + troca.getDataSolicitacao() + "', " + item.getValorUnitario() + 
							", " + troca.getIdCliente() + ", " + idTroca + ")");
				flgPrimeiro = true;
			}
			
			System.out.println(sql);
			
			ps = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			
			
			ps.executeUpdate();

			// obter o id do endereco
			ResultSet rs = ps.getGeneratedKeys();

			int idEnd = 0;
			while (rs.next())
				idEnd = rs.getInt(1);

			troca.setId(idEnd);

			if (ctrlTransacao)
				conexao.commit();
			
			sysConfig.alterar(Constants.ID_TROCA_PEDIDO);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
				return "N�o foi poss�vel salvar o ItensPedido";
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		return "";
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		abrirConexao();

		TrocaPedido troca = (TrocaPedido) entidade;

		String sql = "UPDATE " + nomeTable + " SET itro_status = '" + troca.getListItensTroca().get(0).getStatus().toString() + 
					"' WHERE itro_id = " + troca.getId() + " AND itro_ped_id = " + troca.getPedido().getId() + 
					" AND itro_pro_id = " + troca.getListItensTroca().get(0).getProduto().getId();

		PreparedStatement ps = null;
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			

			ps.executeUpdate();

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			return "ERRO AO ALTERAR A TROCA";
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return "";
	}

}
