package br.com.ecommerceinformatica.fatec.dao;

import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Usuario;

public class UsuarioClienteDAO extends AbstractDAO {
	
	public UsuarioClienteDAO() {
		super("aa", "aa");
	}
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		ClienteDAO cliDao = new ClienteDAO();
		Cliente cliente = new Cliente("", null, "", (Usuario)entidade, "");
		
		return cliDao.consultar(cliente);
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return null;
	}

}
