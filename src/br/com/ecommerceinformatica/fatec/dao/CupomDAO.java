package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.Cupom;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;

public class CupomDAO extends AbstractDAO {
	
	public CupomDAO() {
		super("cup_id", "CUPOM");
	}
	
	public CupomDAO(Connection conexao) {
		super("cup_id", "CUPOM", conexao);
		ctrlTransacao = false;
	}
	
	private String gerarQuery(EntidadeDominio entidade) {
		String query = "SELECT * FROM " + nomeTable;
		
		if (entidade.getClass().getName() == Cupom.class.getName()) {
			Cupom cupom = (Cupom) entidade;
			
			if (cupom.getId() != 0) {
				query += " WHERE cup_id = " + cupom.getId();
			}
			else if (cupom.getCodigo() != null && !cupom.getCodigo().trim().equals("")){
				query += " WHERE cup_id = " + cupom.getCodigo() + "";
			}
			else if(cupom.getIdCliente() != 0) {
				query += " WHERE cup_cli_id = " + cupom.getIdCliente();
			}
			
		}
		
		
		return query;
	}
	
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		abrirConexao();
		
		List<EntidadeDominio> listCupom = new ArrayList<EntidadeDominio>();
		Cupom cupom;

		String sql = gerarQuery(entidade);

		PreparedStatement ps = null;

		try {
			ps = conexao.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				cupom = new Cupom(String.valueOf(rs.getInt("cup_id")));
				cupom.setId(rs.getInt("cup_id"));
				cupom.setNome(rs.getString("cup_nome"));
				cupom.setValor(rs.getDouble("cup_valor"));
				cupom.setTipoCupom(rs.getBoolean("cup_troca") ? 0 : 1);
				cupom.setUsado(rs.getBoolean("cup_usado"));
				cupom.setVencimento(rs.getString("cup_data_vencimento"));
				cupom.setIdCliente(rs.getInt("cup_cli_id"));

				listCupom.add(cupom);

			}

			return listCupom;

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();

		Cupom cupom = (Cupom) entidade;
		
		String sql = "INSERT INTO " + nomeTable + "(cup_nome, cup_valor, cup_data_vencimento, cup_usado, cup_desconto, cup_troca, cup_cli_id)"
					+ " VALUES('" + cupom.getNome() + "', " + cupom.getValor() + ", '" + cupom.getVencimento() + "', false, " 
					+ (cupom.getTipoCupom() == 1 ? "true, false, " : "false, true, " ) + cupom.getIdCliente() + ")";
		PreparedStatement ps = null;


		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);

			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			int idContato = 0;
			while (rs.next())
				idContato = rs.getInt(1);

			cupom.setId(idContato);

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			try {
				conexao.rollback();
				return "N�o foi poss�vel salvar o CUPOM";
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return "";
		
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		abrirConexao();

		Cupom cupom= (Cupom) entidade;

		String sql = "UPDATE " + nomeTable + " SET cup_nome = ?, cup_valor = ?, cup_data_vencimento = ?, cup_usado = ?" + 
					" WHERE " + idTable + " = " + cupom.getId();

		PreparedStatement ps = null;

		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			ps.setString(1, cupom.getNome());
			ps.setDouble(2, cupom.getValor());
			ps.setString(3, cupom.getVencimento());
			ps.setBoolean(4, cupom.isUsado());
			

			ps.executeUpdate();

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			return "ERRO - ALTERAR CUPOM...";
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return "";
	}

}
