package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;

public class SystemConfigDAO extends AbstractDAO {

	
	public SystemConfigDAO() {
		super("sys_nome", "SYSTEM_CONFIG");
	}
	
	public SystemConfigDAO(Connection conexao) {
		super("sys_nome", "SYSTEM_CONFIG", conexao);
		ctrlTransacao = false;
	}
	
	public String alterar(String nome) {
		abrirConexao();
		ctrlTransacao = false;
		String valor = consultar(nome);
		ctrlTransacao = true;
		valor = String.valueOf(Integer.valueOf(valor) + 1);
		
		String sql = "UPDATE " + nomeTable + " SET sys_valor = '" + valor + "' WHERE " + idTable + " = '" + nome + "'";
		
		PreparedStatement ps = null;

		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			

			ps.executeUpdate();

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			return "ERRO";
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		
		
		return "";
	}
	
	public String consultar(String nome) {
		String sql = "SELECT * FROM " + nomeTable + " WHERE " + idTable + " = '" + nome + "'";
		
		abrirConexao();
		
		String valor = "";
		
		PreparedStatement ps = null;

		try {
			ps = conexao.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				valor = rs.getString("sys_valor");

			}

			return valor;

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
		
	}
	
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		
		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return null;
	}

}
