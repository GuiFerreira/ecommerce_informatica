package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Usuario;

public class UsuarioDAO extends AbstractDAO {
	
	public UsuarioDAO() {
		super("usu_id", "USUARIOS");
	}
	
	public UsuarioDAO(Connection conexao) {
		super("usu_id", "USUARIOS", conexao);
		ctrlTransacao = false;
	}
	
	
	public String gerarQuery(Usuario usuario) {
		
		String query = "SELECT * FROM USUARIOS";
		boolean flgWhere = false;

		List<Integer> listPesquisa = new ArrayList<Integer>();
		Map<Integer, String> mapQuery = new HashMap<Integer, String>();
		boolean maisDeUmWhere = false;
		
		if (usuario != null) {
			if (usuario.getId() != 0) {
				listPesquisa.add(1);
				mapQuery.put(1, "usu_id = " + usuario.getId());
				
			}
			else {
				if (usuario.getEmail() != null) {
					if (!usuario.getEmail().trim().equals("")) {
						listPesquisa.add(2);
						mapQuery.put(2, "usu_email LIKE '%" + usuario.getEmail() + "%'");
					}
					
				}
				
				// se igual a 0 retorna todos
				// se igual a 1 retorna os ativos
				if (usuario.getTipoFiltroAtivo() == 1) {
					listPesquisa.add(3);
					mapQuery.put(3, "ativo = true");
				}
				// se igual a -1 retorna os inativos
				else if (usuario.getTipoFiltroAtivo() == -1) {
					listPesquisa.add(4);
					mapQuery.put(4, "ativo = false");
				}
			}
			
		}
		
		if (!listPesquisa.isEmpty()) {
			query += " WHERE";
			for (Integer a : listPesquisa) {
				if (maisDeUmWhere) {
					query += " AND";
				}
				query += " " + mapQuery.get(a);
				maisDeUmWhere = true;
			}
			
		}
		
		return query;
	}
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		abrirConexao();
		
		Usuario usuario = (Usuario) entidade;
		List<EntidadeDominio> listUsuarios = new ArrayList<EntidadeDominio>();
		String sql = gerarQuery(usuario);
		
		PreparedStatement ps = null;
		
		try {
			ps = conexao.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Usuario usuBd = new Usuario(rs.getString("usu_email"), rs.getString("usu_senha"), rs.getBoolean("usu_adm"));
				
				usuBd.setId(rs.getInt("usu_id"));
				
				listUsuarios.add(usuBd);
				
			}
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		finally {
			if(ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return listUsuarios;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();
		
		String sql = "INSERT INTO " + nomeTable + " (usu_email, usu_senha, usu_adm, ativo) VALUES(?,?,?, true);";
		Usuario usuario = (Usuario) entidade;
		PreparedStatement ps = null;
		
		
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, usuario.getEmail());
			ps.setString(2, usuario.getSenha());
			ps.setBoolean(3, usuario.isAdministrador());
			
			
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			int id = 0;
			while(rs.next())
				id = rs.getInt("usu_id");
			
			usuario.setId(id);
			
			if (ctrlTransacao)
				conexao.commit();
			
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				return "Erro ao dar rollback UsuarioDAO";
			}
			e.printStackTrace();
			return "Erro ao inserir no bando UsuarioDAO";
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return "N�o foi poss�vel fechar conexao ou ps UsuarioDAO";
				}
				
			}
		}
		
		
		return "";
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		return "";
	}

	@Override
	public String excluir(EntidadeDominio entidade) {
		return desativarItem(entidade);
	}

}
