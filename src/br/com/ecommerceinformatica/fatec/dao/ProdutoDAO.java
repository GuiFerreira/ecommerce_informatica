package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Fornecedor;
import br.com.ecommerceinformatica.fatec.dominio.ItensEstoque;
import br.com.ecommerceinformatica.fatec.dominio.Produto;

public class ProdutoDAO extends AbstractDAO {

	public ProdutoDAO() {
		super("pro_id", "PRODUTOS");
	}
	public ProdutoDAO(Connection conexao) {
		super("pro_id", "PRODUTOS", conexao);
		ctrlTransacao = false;
	}

	public String gerarQuery(Produto produto) {
		String query = "SELECT * FROM " + nomeTable + " AS P";
	
		List<Integer> listPesquisa = new ArrayList<Integer>();
		Map<Integer, String> mapQuery = new HashMap<Integer, String>();
		boolean maisDeUmWherePRODUTO = false;

		// verificar se tem algo para filtro
		// -----------------------------------------------------------------------------
		// PRODUTO
		if (produto != null) {
			if (produto.getId() != 0) {
				listPesquisa.add(1);
				mapQuery.put(1, "pro_id = " + produto.getId());
			} else {
				if (produto.getNome() != null) {
					if (!produto.getNome().trim().equals("")) {
						listPesquisa.add(2);
						mapQuery.put(2, "pro_nome LIKE '%" + produto.getNome() + "%'");
					}
				}
				if (produto.getCodProduto() != null) {
					if (!produto.getCodProduto().trim().equals("")) {
						listPesquisa.add(3);
						mapQuery.put(3, "pro_codigo LIKE '%" + produto.getCodProduto() + "%'");
					}
				}
				if (produto.getDescricao() != null) {
					if (!produto.getDescricao().trim().equals("")) {
						listPesquisa.add(4);
						mapQuery.put(4, "pro_descricao LIKE '%" + produto.getDescricao() + "%'");
					}
				}
				if (produto.getEspecificacoes() != null) {
					if (!produto.getEspecificacoes().trim().equals("")) {
						listPesquisa.add(5);
						mapQuery.put(5, "pro_especificacoes LIKE '%" + produto.getEspecificacoes() + "%'");
					}
				}
				if (produto.getMarca() != null) {
					if (!produto.getMarca().trim().equals("")) {
						listPesquisa.add(6);
						mapQuery.put(6, "pro_marca LIKE '%" + produto.getMarca() + "%'");
					}
				}
				
				// se 1 igual traz ativo
				if (produto.getTipoFiltroAtivo() == 1) {
					listPesquisa.add(7);
					mapQuery.put(7, "P.ativo = true");
				}
				// se -1 traz inativo
				else if (produto.getTipoFiltroAtivo() == -1) {
					listPesquisa.add(8);
					mapQuery.put(8, "P.ativo = false");
				}
			}
			
		}
		
		// --------------------------------------------------------------------
		// CATEGORIAS

		// verificar na hora se tem NOME DE CATEGORIA PARA PESQUISA
		boolean flgCategoria = false;
		if (produto != null) {
			if (produto.getCategoria() != null) {
				if (produto.getCategoria().getId() != 0) {
					flgCategoria = true;
				}
			}
		}
		
		if (flgCategoria)
			query += ", (SELECT * FROM categorias WHERE cat_id = " + produto.getCategoria().getId() + ") AS C";

		else
			query += ", categorias AS C";
		
		
		
		// --------------------------------------------------------------------
		// FORNECEDOR
		boolean flgFornecedor = false;
		if (produto != null) {
			if (produto.getFornecedor() != null) {
				if (produto.getFornecedor().getId() != 0) {
					flgFornecedor = true;
				}
			}
		}
		
		if (flgFornecedor) 
			query += ", (SELECT * FROM fornecedores WHERE for_id = " + produto.getFornecedor().getId() + ") AS F";
		else
			query += ", fornecedores AS F";
		
	
		
		// WHERE DE PRODUTO
		query += " WHERE";
		if (!listPesquisa.isEmpty()) {
			for (Integer a : listPesquisa) {
				if (maisDeUmWherePRODUTO) {
					query += " AND";
				}
				query += " " + mapQuery.get(a);
				maisDeUmWherePRODUTO = true;
			}
			
			query += " AND P.pro_cat_id = C.cat_id AND P.pro_for_id = F.for_id";
			
		}
		else
			query += " P.pro_cat_id = C.cat_id AND P.pro_for_id = F.for_id";
		
		return query;
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		abrirConexao();

		Produto produtoQuery = (Produto) entidade;
		Produto produto = null;
		

		String sql = gerarQuery(produtoQuery);
		List<EntidadeDominio> listaProduto = new ArrayList<EntidadeDominio>();
		
		// vai ser usado pq fornecedor tem endere�o...
		List<EntidadeDominio> listaEndereco = null;
		EnderecoDAO endDAO = new EnderecoDAO(conexao);
		endDAO.ctrlTransacao = false;
		Endereco endereco = null;
		List<EntidadeDominio> listContato = null;
		ContatoDAO contDAO = new ContatoDAO(conexao);
		contDAO.ctrlTransacao = false;
		Contato contato = null;
		
		PreparedStatement ps = null;
		
		try {
			ps = conexao.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Categoria categoria = new Categoria(rs.getString("cat_nome"), rs.getDouble("cat_margem_lucro"));
				categoria.setId(rs.getInt("cat_id"));
				categoria.setDataCadastro(rs.getString("cat_data_cadastro"));

				
				
				Fornecedor fornecedor = new Fornecedor(rs.getString("for_nome"),
														null,
														rs.getString("for_cnpj"), 
														null);
				fornecedor.setId(rs.getInt("for_id"));
				fornecedor.setDataCadastro(rs.getString("for_data_cadastro"));
				// PEGANDO ENDERE�O DO FORNECEDOR
				fornecedor.setEndereco(new Endereco("", "", "", null, "", null, "", ""));
				fornecedor.getEndereco().setId(rs.getInt("for_end_id"));
				// vou fazer uma busca na DAO ENDERECO s� q eu sei q so vai me retornar 1 endereco
				// pq eu ja to definindo o id dele
				listaEndereco = endDAO.consultar(fornecedor.getEndereco());
				fornecedor.setEndereco((Endereco)listaEndereco.get(0));
				// PEGANDO CONTATO DO FORNECEDOR
				fornecedor.setContato(new Contato("", "", ""));
				fornecedor.getContato().setId(rs.getInt("for_con_id"));
				// vou fazer uma busca na DAO CONTATO s� q eu sei q so vai me retornar 1 contato
				// pq eu ja to definindo o id dele
				listContato = contDAO.consultar(fornecedor.getContato());
				fornecedor.setContato((Contato) listContato.get(0));
				
				
				produto = new Produto(categoria, rs.getDouble("pro_preco_compra"), rs.getString("pro_nome"),
						rs.getString("pro_codigo"), rs.getString("pro_descricao"), rs.getString("pro_especificacoes"),
						rs.getString("pro_marca"), fornecedor, rs.getDouble("pro_desconto"),
						rs.getString("pro_imagem"));
				produto.setDataCadastro(rs.getString("pro_data_cadastro"));
				produto.setId(rs.getInt("pro_id"));

				listaProduto.add(produto);

			}

			return listaProduto;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		abrirConexao();
		
		String erro = "";
		StringBuilder sb = new StringBuilder();
		Produto produto = (Produto) entidade;

		PreparedStatement ps = null;

		try {
			conexao.setAutoCommit(false);

			// eu ja tenho ID do Fornecedor e o de CategoriaGra�as ao esquema do <option
			// value="">
			// so mandar o c�digo
			sb.append("INSERT INTO " + nomeTable + "(pro_nome, pro_codigo, pro_descricao,"
					+ " pro_especificacoes, pro_marca, pro_desconto, pro_imagem, pro_for_id, pro_cat_id, pro_data_cadastro, pro_preco_compra, ativo) "
					+ "VALUES(?,?,?,?,?,?,?,?,?,?,?, true)");

			ps = conexao.prepareStatement(sb.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, produto.getNome());
			ps.setString(2, produto.getCodProduto());
			ps.setString(3, produto.getDescricao());
			ps.setString(4, produto.getEspecificacoes());
			ps.setString(5, produto.getMarca());
			ps.setDouble(6, produto.getDesconto());
			ps.setString(7, produto.getUriImagem());
			ps.setInt(8, produto.getFornecedor().getId());
			ps.setInt(9, produto.getCategoria().getId());
			ps.setString(10, produto.getDataCadastro());
			ps.setDouble(11, produto.getPrecoComprado());
			
			
			// executei a query
			ps.executeUpdate();
			// pegar o id do produto
			ResultSet rs = ps.getGeneratedKeys();

			int idProduto = 0;
			while (rs.next())
				idProduto = rs.getInt(1);

			produto.setId(idProduto);
			
			
			// salvar esse produto no estoque
			//	 mas com os dados igual a 0
			ItensEstoque itemEstoque = new ItensEstoque(produto, 0, produto.getDataCadastro());
			itemEstoque.setId(produto.getId());
			
			EstoqueDAO estDAO = new EstoqueDAO(conexao);
			String msgErro = estDAO.salvar(itemEstoque);
			
			// verificar se deu merda al salvar
			if (!msgErro.trim().equals(""))
				erro = msgErro;


			if (ctrlTransacao)
				conexao.commit();
			
			// carregar fornecedor
			FornecedorDAO fornDao = new FornecedorDAO(conexao);
			fornDao.ctrlTransacao = false;
			Fornecedor fornecedor = new Fornecedor("", null, "", null);
			fornecedor.setId(produto.getFornecedor().getId());
			List<EntidadeDominio> listFornecedor = fornDao.consultar(fornecedor);
			produto.setFornecedor((Fornecedor)listFornecedor.get(0));
			
			
			// carregar categoria
			CategoriaDAO catDao = new CategoriaDAO(conexao);
			catDao.ctrlTransacao = false;
			Categoria categoria = new Categoria("", 0);
			categoria.setId(produto.getCategoria().getId());
			List<EntidadeDominio> listaCategoria = catDao.consultar(categoria);
			produto.setCategoria((Categoria) listaCategoria.get(0));
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return erro;
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		abrirConexao();
		
		Produto produto = (Produto) entidade;
		
		String sql = "UPDATE " + nomeTable + " SET pro_nome = ?, pro_descricao = ?," +
					" pro_especificacoes = ?, pro_marca = ?, pro_desconto = ?, pro_for_id = ?," + 
				" pro_cat_id = ?, pro_preco_compra = ? " + ", ativo = " + String.valueOf(produto.isFlgAtivo()) + 
				" WHERE " + idTable + " = " + produto.getId();
		
		PreparedStatement ps = null;
		
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			
			ps.setString(1, produto.getNome());
			ps.setString(2, produto.getDescricao());
			ps.setString(3, produto.getEspecificacoes());
			ps.setString(4, produto.getMarca());
			ps.setDouble(5, produto.getDesconto());
			ps.setInt(6, produto.getFornecedor().getId());
			ps.setInt(7, produto.getCategoria().getId());
			ps.setDouble(8, produto.getPrecoComprado());
			
			
			ps.executeUpdate();
			
			if(ctrlTransacao)
			conexao.commit();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			return "ERRO: " + e.getMessage();
		}
		finally {
			if(ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		return "";
	}

	public String excluir(EntidadeDominio entidade) {
		return desativarItem(entidade);
	}
	
	
}
