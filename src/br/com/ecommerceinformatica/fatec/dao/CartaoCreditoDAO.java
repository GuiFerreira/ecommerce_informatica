package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.Cartao;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;

public class CartaoCreditoDAO extends AbstractDAO {

	public CartaoCreditoDAO() {
		super ("car_id", "CARTAO_CREDITO");
	}
	
	public CartaoCreditoDAO(Connection conexao) {
		super ("car_id", "CARTAO_CREDITO", conexao);
		ctrlTransacao = false;
	}
	
	public String gerarQuery(Cartao cartao) {
		
		String query = "SELECT * FROM CARTAO_CREDITO";
		
		List<Integer> listPesquisa = new ArrayList<Integer>();
		Map<Integer, String> mapQuery = new HashMap<Integer, String>();
		boolean maisDeUmWhere = false;
		
		
		
		
		return query;
	}
	
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
//		abrirConexao();
//		
//		Cartao cartao = (Cartao) entidade;
//		String sql = gerarQuery(cartao);
//		
		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();
		
		Cartao cartao = (Cartao) entidade;
		String sql = "INSERT INTO " + nomeTable + "(car_numero," + 
				"car_cli_id," + 
				"car_cod_seguranca," + 
				"car_nome_impresso," + 
				"car_nome_banco," + 
				"car_agencia, ativo)" + 
				"VALUES (?,?,?,?,?,?, true)";
		
		PreparedStatement ps = null;
		
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			
			ps.setString(1, cartao.getNumero());
			ps.setInt(2, cartao.getId());
			ps.setString(3, cartao.getCodSeguranca());
			ps.setString(4, cartao.getNomeImpresso());
			ps.setString(5, cartao.getBanco().getNome());
			ps.setString(6, cartao.getBanco().getNumAgencia());
			
			ps.execute();
			
			
			if (ctrlTransacao)
				conexao.commit();
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				return "n�o deu pra dar rollback CartaoCreditoDAO";
			}
			e.printStackTrace();
			return "Erro ao salvar CartaoCreditoDAO";
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return "n�o deu pra fechar conexao Cart�oCr�ditoDAO";
				}
			}
			
		}
		
		
		return "";
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return "";
	}
	

}
