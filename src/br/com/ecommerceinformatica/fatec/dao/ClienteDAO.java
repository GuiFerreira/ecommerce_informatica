package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.Banco;
import br.com.ecommerceinformatica.fatec.dominio.Cartao;
import br.com.ecommerceinformatica.fatec.dominio.Cidade;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EnderecoCliente;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.Estado;
import br.com.ecommerceinformatica.fatec.dominio.Logradouro;
import br.com.ecommerceinformatica.fatec.dominio.Pais;
import br.com.ecommerceinformatica.fatec.dominio.Usuario;

public class ClienteDAO extends AbstractDAO {

	public ClienteDAO() {
		super("cli_id", "CLIENTES");
		// TODO Auto-generated constructor stub
	}
	
	public ClienteDAO(Connection conexao) {
		super("cli_id", "CLIENTES", conexao);
		ctrlTransacao = false;
	}
	
	
	public String gerarQuery(Cliente cliente) {
		String query = "SELECT * FROM " + nomeTable + " AS C LEFT JOIN CONTATOS AS CO ON CO.con_id = C.cli_con_id LEFT JOIN USUARIOS AS US ON cli_usu_id = usu_id ";

		List<Integer> listPesquisa = new ArrayList<Integer>();
		Map<Integer, String> mapQuery = new HashMap<Integer, String>();
		boolean maisDeUmWhere = false;

		boolean temUsuario = false;
		
		// verificar se tem algo para filtro
		if (cliente.getId() != 0) {
			listPesquisa.add(1);
			mapQuery.put(1, "C.cli_id = " + cliente.getId());
			
		} else {
			if (cliente.getCpf() != null) {
				if(!cliente.getCpf().trim().equals("")) {
					listPesquisa.add(2);
					mapQuery.put(2, "C.cli_cpf LIKE '%" + cliente.getCpf() + "%'");				
				}
			}
			if (cliente.getNome() != null) {
				if(!cliente.getNome().trim().equals("")) {
					listPesquisa.add(3);
					mapQuery.put(3, "C.cli_nome LIKE '%" + cliente.getNome() + "%'");
				}
			}
			if (cliente.getDataNascimento() != null) {
				if(!cliente.getDataNascimento().trim().equals("")) {
					listPesquisa.add(4);
					mapQuery.put(4, "C.cli_data_nascimento LIKE '%" + cliente.getDataNascimento() + "%'");				
				}
			
			}
			// se igual a 1 traz ativo
			if (cliente.getTipoFiltroAtivo() == 1) {
				listPesquisa.add(5);
				mapQuery.put(5, "C.ativo = true");		
			}
			// se igual a -1 traz inativo
			else if (cliente.getTipoFiltroAtivo() == -1) {
				listPesquisa.add(6);
				mapQuery.put(6, "C.ativo = false");		
			}
			
			// existe algum usuario
			if (cliente.getUsuario() != null) {
				if(cliente.getUsuario().getEmail() != null) {
					if (!cliente.getUsuario().getEmail().trim().equals("")) {
						if (!temUsuario)
							query += " WHERE usu_email LIKE '%" + cliente.getUsuario().getEmail() + "%'";
						else
							query += " AND '%" + cliente.getUsuario().getEmail() + "%'"; 
					}
				}
			}
			
			
			
		}
		
		
		if (!listPesquisa.isEmpty()) {
			query += " WHERE";
			for (Integer a : listPesquisa) {
				if (maisDeUmWhere) {
					query += " AND";
				}
				query += " " + mapQuery.get(a);
				maisDeUmWhere = true;
			}
			
		}

		
		return query;
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		abrirConexao();
		
		Cliente clienteQuery = (Cliente)entidade;

		String sql = gerarQuery(clienteQuery);
		
		PreparedStatement ps = null;
		
		List<EntidadeDominio> listClientes = new ArrayList<EntidadeDominio>();
		
		try {
			ps = conexao.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			Map<Integer, Cliente> mapClientes = new HashMap<Integer, Cliente>();
			List<Integer> listIds = new ArrayList<Integer>();
			// Pegar Lista de Clientes
			while(rs.next()) {
				
				Cliente cliente = new Cliente(rs.getString("cli_nome"),
												null,
												rs.getString("cli_cpf"),
												null,
												rs.getString("cli_data_nascimento"));
				cliente.setId(rs.getInt("cli_id"));
				
				cliente.setUsuario(new Usuario(rs.getString("usu_email"), rs.getString("usu_senha"), rs.getBoolean("usu_adm")));
				cliente.getUsuario().setId(rs.getInt("usu_id"));
				
				cliente.setContato(new Contato(rs.getString("con_ddd"), rs.getString("con_telefone"), rs.getString("con_email")));
				cliente.getContato().setId(rs.getInt("con_id"));
				
				
				listIds.add(cliente.getId());
				mapClientes.put(cliente.getId(), cliente);
				
				
			}
			
			if (!listIds.isEmpty()) {
				sql = "SELECT * FROM CLIENTES_ENDERECOS AS CE, ENDERECOS AS E WHERE CE.cliend_end_id = E.end_id AND cliend_cli_id IN (";
				
				boolean flgMaisDeUm = false;
				for (Integer id : listIds) {
					if (flgMaisDeUm)
						sql += ",";
					sql += id;
					flgMaisDeUm = true;
				}
				
				sql += ");";
				PreparedStatement ps2 = conexao.prepareStatement(sql);
				
				
				rs = ps2.executeQuery();
				
				while(rs.next()) {
					EnderecoCliente endereco = null;
					Pais pais = new Pais(rs.getString("end_pais"), rs.getString("end_sigla_pais"));
					Estado estado = new Estado(rs.getString("end_estado"), rs.getString("end_sigla_estado"), pais);
					Cidade cidade = new Cidade(rs.getString("end_cidade"), estado);
					
					int isCobranca = rs.getInt("cliend_cobranca");
					
					endereco = new EnderecoCliente(rs.getString("end_rua"), rs.getString("end_bairro"), rs.getString("end_numero"),
							cidade, rs.getString("end_cep"),
							new Logradouro(rs.getString("end_tipo_logradouro"), rs.getString("end_logradouro")),
							rs.getString("end_observacoes"), rs.getString("end_apelido"), isCobranca);

					endereco.setIdCliente(rs.getInt("cliend_cli_id"));
					endereco.setId(rs.getInt("end_id"));
					
					endereco.setDataCadastro(rs.getString("end_data_cadastro"));

					if (isCobranca == 1) {
						if (mapClientes.get(endereco.getIdCliente()).getListaEnderecoCobranca() == null)
							mapClientes.get(endereco.getIdCliente()).setListaEnderecoCobranca(new ArrayList<EnderecoCliente>());
						
						mapClientes.get(endereco.getIdCliente()).getListaEnderecoCobranca().add(endereco);
						
					}
					else if (isCobranca == 0){
						if (mapClientes.get(endereco.getIdCliente()).getListaEnderecoEntrega() == null)
							mapClientes.get(endereco.getIdCliente()).setListaEnderecoEntrega(new ArrayList<EnderecoCliente>());
						
						mapClientes.get(endereco.getIdCliente()).getListaEnderecoEntrega().add(endereco);
						
					}
					
					else if (isCobranca == 2) {
						if (mapClientes.get(endereco.getIdCliente()).getListaEnderecoCobranca() == null)
							mapClientes.get(endereco.getIdCliente()).setListaEnderecoCobranca(new ArrayList<EnderecoCliente>());
						
						mapClientes.get(endereco.getIdCliente()).getListaEnderecoCobranca().add(endereco);
						
						if (mapClientes.get(endereco.getIdCliente()).getListaEnderecoEntrega() == null)
							mapClientes.get(endereco.getIdCliente()).setListaEnderecoEntrega(new ArrayList<EnderecoCliente>());
						
						mapClientes.get(endereco.getIdCliente()).getListaEnderecoEntrega().add(endereco);
						
					}
						
				}
				
				
				// pegar cart�o
				sql = "SELECT * FROM CARTAO_CREDITO WHERE car_cli_id IN (";
				
				flgMaisDeUm = false;
				for (Integer id : listIds) {
					if (flgMaisDeUm)
						sql += ",";
					sql += id;
					flgMaisDeUm = true;
				}
				
				sql += ");";
				PreparedStatement ps3 = conexao.prepareStatement(sql);
				
				
				rs = ps3.executeQuery();

				
				while(rs.next()) {
					Banco banco = new Banco(rs.getString("car_nome_banco"), rs.getString("car_agencia"));
					Cartao cartao = new Cartao(rs.getString("car_numero"), rs.getString("car_nome_impresso"),
										rs.getString("car_cod_seguranca"), banco);
					
					int idCliente = rs.getInt("car_cli_id");
					
					if(mapClientes.get(idCliente).getListaCartoes() == null)
						mapClientes.get(idCliente).setListaCartoes(new ArrayList<Cartao>());
					
					mapClientes.get(idCliente).getListaCartoes().add(cartao);
					
					
				}
				
				
			}
			
			
			
			// colocar todos os cliente na lista de clientes
			
			for (Integer id : mapClientes.keySet()) 
				listClientes.add(mapClientes.get(id));
			
			
			return listClientes;
			
			
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();
		
		String sql = "INSERT INTO " + nomeTable + " (cli_nome, cli_cpf, cli_data_nascimento, cli_con_id, cli_usu_id, ativo) " + 
					"VALUES (?,?,?,?,?, true)";
		PreparedStatement ps = null;
		
		Cliente cliente = (Cliente) entidade;
		
		String erro = "";
		
		// antes de cadastrar o cliente tem que ter o id de contato
		ContatoDAO conDAO = new ContatoDAO(conexao);
		conDAO.ctrlTransacao = false;
		erro = conDAO.salvar(cliente.getContato());
		
		UsuarioDAO usuDAO = new UsuarioDAO(conexao);
		usuDAO.ctrlTransacao = false;
		erro += usuDAO.salvar(cliente.getUsuario());
			
		
		if (!erro.trim().equals("")) {
			try {
				conexao.rollback();
				conexao.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return erro + "\nErro ao dar rollback ou fechar a conexao do Cliente Dao";
			}
			return erro;
		}
		
		try {
			
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			ps.setString(1, cliente.getNome());
			ps.setString(2, cliente.getCpf());
			ps.setString(3, cliente.getDataNascimento());
			ps.setInt(4, cliente.getContato().getId());
			ps.setInt(5, cliente.getUsuario().getId());
			
			// executei a query
			ps.executeUpdate();
			// pegar o id do produto
			ResultSet rs = ps.getGeneratedKeys();
			int id = 0;
			while(rs.next())
				id = rs.getInt("cli_id");
			cliente.setId(id);
			
			
			EnderecoClienteDAO endDAO = new EnderecoClienteDAO(conexao);
			if (cliente.getListaEnderecoCobranca() != null) 
				if (cliente.getListaEnderecoCobranca().get(0) != null) {
					cliente.getListaEnderecoCobranca().get(0).setIdCliente(id);
					
					erro += endDAO.salvar(cliente.getListaEnderecoCobranca().get(0));	
					
				}
			
			if (cliente.getListaEnderecoEntrega() != null) 
				if (cliente.getListaEnderecoEntrega().get(0) != null) {
					cliente.getListaEnderecoEntrega().get(0).setIdCliente(id);
					
					erro += endDAO.salvar(cliente.getListaEnderecoEntrega().get(0));
					
				}
			
			if (cliente.getListaCartoes() != null) {
				if (cliente.getListaCartoes().get(0) != null) {
					cliente.getListaCartoes().get(0).setId(id);
					CartaoCreditoDAO carDAO = new CartaoCreditoDAO(conexao);
					erro += carDAO.salvar(cliente.getListaCartoes().get(0));
				}
			}
			
			
			if (!erro.trim().equals("")) {
				conexao.rollback();
				return "erro ao salvar o endere�o do cliente ClienteDAO";
			}
			
			
			if (ctrlTransacao) 
				conexao.commit();
			
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
				return "N�o foi poss�vel salvar o Cliente";
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return "";
		
		
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		abrirConexao();
		
		String sql = "UPDATE " + nomeTable + "";
		
		
		
		return null;
	}
	
	@Override
	public String excluir(EntidadeDominio entidade) {
		abrirConexao();
		
		String erro = "";
		Cliente cliente = (Cliente) entidade;
		Usuario usuario = cliente.getUsuario();
		erro = desativarItem(entidade);
		
		if (erro.equals("")) {
			// desativar usu�rio
			erro = new UsuarioDAO(conexao).excluir(usuario);
			List<EntidadeDominio> listEntidade = null;
			
			// desativar cart�es
			if (!cliente.getListaCartoes().isEmpty()) {
				listEntidade = new ArrayList<EntidadeDominio>();
				for (Cartao cartao : cliente.getListaCartoes()) {
					EntidadeDominio entidadeCartao = new EntidadeDominio();
					entidadeCartao.setId(cartao.getId());
					listEntidade.add(entidadeCartao);
				}
				
				erro = (!cliente.getListaCartoes().isEmpty()) ? new CartaoCreditoDAO(conexao).excluir(listEntidade) : "";
				
			}
			
			// desativar lista de endere�os de cobran�a e entrega
			if (!cliente.getListaEnderecoCobranca().isEmpty()) {
				listEntidade = new ArrayList<EntidadeDominio>();
				for (EnderecoCliente endereco : cliente.getListaEnderecoEntrega()) {
					EntidadeDominio entidadeCartao = new EntidadeDominio();
					entidadeCartao.setId(endereco.getId());
					listEntidade.add(entidadeCartao);
				}
			}
			if (!cliente.getListaEnderecoEntrega().isEmpty()) {
				listEntidade = new ArrayList<EntidadeDominio>();
				for (EnderecoCliente endereco : cliente.getListaEnderecoCobranca()) {
					EntidadeDominio entidadeCartao = new EntidadeDominio();
					entidadeCartao.setId(endereco.getId());
					listEntidade.add(entidadeCartao);
				}
			}
			
			erro += (listEntidade.isEmpty()) ? "" : new EnderecoClienteDAO(conexao).excluir(listEntidade);
			
		}
		
		
		return erro;
	}
	

}
