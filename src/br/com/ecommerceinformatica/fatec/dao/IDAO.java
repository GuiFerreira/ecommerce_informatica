package br.com.ecommerceinformatica.fatec.dao;

import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;


public interface IDAO {

	public List<EntidadeDominio> consultar(EntidadeDominio entidade);
	public String salvar(EntidadeDominio entidade);
	public String alterar(EntidadeDominio entidade);
	public String excluir(EntidadeDominio entidade);
	public String excluir(List<EntidadeDominio> listEntidade);
	
	
}
