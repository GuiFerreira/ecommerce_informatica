package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.CartaoPedido;
import br.com.ecommerceinformatica.fatec.dominio.Cliente;
import br.com.ecommerceinformatica.fatec.dominio.Contato;
import br.com.ecommerceinformatica.fatec.dominio.Cupom;
import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensPedido;
import br.com.ecommerceinformatica.fatec.dominio.Pedido;
import br.com.ecommerceinformatica.fatec.dominio.StatusCompra;
import br.com.ecommerceinformatica.fatec.dominio.TrocaPedido;
import br.com.ecommerceinformatica.fatec.dominio.Usuario;

public class PedidoDAO extends AbstractDAO{
	
	public PedidoDAO() {
		super("ped_id", "PEDIDO");
	}
	public PedidoDAO(Connection conexao) {
		super("ped_id", "PEDIDO", conexao);
		ctrlTransacao = false;
	}
	
	private String getQuery(Pedido pedido) {
		String query = "SELECT * FROM " + nomeTable +  
						" LEFT JOIN CLIENTES AS CL ON cl.cli_id = ped_cli_id" +
						" LEFT JOIN CUPOM AS CU ON cup_id = ped_cupom_desconto_id" + 
						" LEFT JOIN CONTATOS AS CO ON CL.cli_con_id = con_id" +
						" LEFT JOIN USUARIOS AS US ON CL.cli_usu_id = usu_id";
		
		List<Integer> listCondicoes = new ArrayList<Integer>();
		Map<Integer, String> mapCondicoes = new HashMap<Integer, String>();
		boolean flgControleCondicoes = false;
		
		if (pedido != null) {
			if (pedido.getId() != 0) { 
				listCondicoes.add(0);
				mapCondicoes.put(0, "ped_id = " + pedido.getId());
			}
			else {
				// status do pedido
				// validar se � Usu�rio adm
				if (pedido.getCliente() != null && pedido.getCliente().getUsuario() != null && pedido.getCliente().getUsuario().isAdministrador()) {
					listCondicoes.add(2);
					String filtro = "ped_status_compra != '" + StatusCompra.ENTREGUE.toString()+ "' AND ped_status_compra != '" + StatusCompra.CANCELADO.toString() +
							"' AND ped_status_compra != '" + StatusCompra.PAGAMENTO_REJEITADO.toString() + "' AND "
							+ "ped_status_compra != '" + StatusCompra.TROCA_APROVADA.toString() + "'";
					mapCondicoes.put(2, filtro);
				}
				
				if (pedido.getCliente() != null && pedido.getCliente().getId() != 0) {
					listCondicoes.add(1);
					mapCondicoes.put(1, "ped_cli_id = " + pedido.getCliente().getId());
				}
				
				
			}
		}
		
		if (!listCondicoes.isEmpty()) {
			query += " WHERE ";
			for (Integer i : listCondicoes) {
				if (flgControleCondicoes)
					query += " AND ";
				
				query += mapCondicoes.get(i);
				
				flgControleCondicoes = true;
			}
		}
		
		query += " ORDER BY ped_id";
		return query;
	}
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		abrirConexao();
		
		Pedido pedidoEntidade = (Pedido) entidade;
		Endereco endCobranca, endEntrega;
		Cupom cupomDesconto;
		
		String query = getQuery(pedidoEntidade);
		PreparedStatement ps = null;
		
		List<EntidadeDominio> listaPedidos = new ArrayList<EntidadeDominio>();
		List<EntidadeDominio> listaEnderecos = new ArrayList<EntidadeDominio>();
		Map<Integer, Endereco> mapEndereco = new HashMap<Integer, Endereco>();
		Map<Integer, Pedido> mapPedidos = new HashMap<Integer, Pedido>();
		Map<Integer, List<TrocaPedido>> mapTroca = new HashMap<Integer, List<TrocaPedido>>();
		
		try {
			ps = conexao.prepareStatement(query);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				int idPedido = rs.getInt("ped_id");
				Pedido pedido = mapPedidos.get(idPedido);
				
				if (pedido == null) {
					endCobranca = new Endereco("", "", "", null, "", null, "", "");
					endCobranca.setId(rs.getInt("ped_end_cobranca_id"));
					endEntrega = new Endereco("", "", "", null, "", null, "", "");
					endEntrega.setId(rs.getInt("ped_end_entrega_id"));
					listaEnderecos.add(endEntrega);
					listaEnderecos.add(endCobranca);
					
					cupomDesconto = new Cupom("");
					cupomDesconto.setCodigo(String.valueOf((rs.getInt("cup_id"))));
					cupomDesconto.setId((rs.getInt("cup_id")));
					cupomDesconto.setValor(rs.getDouble("cup_valor"));
					cupomDesconto.setNome(rs.getString("cup_nome"));
					cupomDesconto.setVencimento(rs.getString("cup_data_vencimento"));
					cupomDesconto.setTipoCupom(rs.getBoolean("cup_troca") ? 0 : 1);
					
					Contato contato = new Contato(rs.getString("con_ddd"), rs.getString("con_telefone"), rs.getString("con_email"));
					contato.setDataCadastro(rs.getString("con_data_cadastro"));
					Usuario usuario = new Usuario(rs.getString("usu_email"), "", rs.getBoolean("usu_adm"));
					Cliente cliente = new Cliente(rs.getString("cli_nome"), contato, rs.getString("cli_cpf"), usuario, rs.getString("cli_data_nascimento"));
					cliente.setId(rs.getInt("cli_id"));
					
					String statusCompra = rs.getString("ped_status_compra");
					
					double valorFrete = rs.getDouble("ped_valor_frete");
					
					pedido = new Pedido(new ArrayList<ItensPedido>(), cupomDesconto, StatusCompra.valueOf(statusCompra), endEntrega, 
								endCobranca, cliente, rs.getString("ped_data_compra"),
								rs.getDouble("ped_preco_total"), rs.getInt("ped_dias_entrega"), new ArrayList<CartaoPedido>(), rs.getString("ped_codigo"), valorFrete);
					
					
					pedido.setDiasEntrega(rs.getInt("ped_dias_entrega"));
					pedido.setDataCompra(rs.getString("ped_data_compra"));
					pedido.setId(rs.getInt("ped_id"));
					
					// colocar pedido no mapa
					mapPedidos.put(pedido.getId(), pedido);
					listaPedidos.add(pedido);
					
					
				}
				
				
			}
			
			if (listaPedidos.isEmpty()) {
				return listaPedidos;
			}
			
			// obter enderecos entrega e cobranca
			for (EntidadeDominio endereco : new EnderecoDAO(conexao).consultar(listaEnderecos)) {
				if (mapEndereco.get(endereco.getId()) == null) 
					mapEndereco.put(endereco.getId(), (Endereco) endereco);
				
			}
			
			
			// obter itens pedido
			for (EntidadeDominio entItem : new ItensPedidoDAO(conexao).consultar(listaPedidos)) {
				ItensPedido item = (ItensPedido) entItem;
				mapPedidos.get(item.getIdPedido()).getListProdutosPedido().add(item);
				
			}
			
			
			for (EntidadeDominio entCar : new CartoesPedidoDAO(conexao).consultar(listaPedidos)) {
				CartaoPedido carPedido = (CartaoPedido) entCar;
				
				mapPedidos.get(carPedido.getIdPedido()).getListCartoes().add(carPedido);
				
			}
			
			// trocas
			for (EntidadeDominio entTroca : new TrocaPedidoDAO(conexao).consultar(listaPedidos)) {
				TrocaPedido troca = (TrocaPedido) entTroca;
				if (mapTroca.get(troca.getPedido().getId()) == null)
					mapTroca.put(troca.getPedido().getId(), new ArrayList<TrocaPedido>());
					
				mapTroca.get(troca.getPedido().getId()).add(troca);
			}
			
			
			// PEDIDO
			for (EntidadeDominio entidadePedido : listaPedidos) {
				Pedido pedido = (Pedido) entidadePedido;
				pedido.setEndCobranca(mapEndereco.get(pedido.getEndCobranca().getId()));
				pedido.setEndEntrega(mapEndereco.get(pedido.getEndEntrega().getId()));
				pedido.setListTrocaPedido(mapTroca.get(pedido.getId()));
				
				
			}
		
			
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		return listaPedidos;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();
		
		Pedido pedido = (Pedido) entidade;
		
		Cupom cupomDesconto = pedido.getCupom();
		
		String sql = "INSERT INTO " + nomeTable + "(ped_preco_total, ped_status_compra, ped_cli_id, "+
						"ped_end_cobranca_id, ped_end_entrega_id, ped_data_compra, ped_dias_entrega, ped_codigo, ped_valor_frete" + 
						// cupom desconto
					((cupomDesconto.getId() != 0) ? ", ped_cupom_desconto_id" : "") +
					") VALUES(?,?,?,?,?,?,?,?,?" + 
					// cupom desconto 
					((cupomDesconto.getId() != 0) ? ",?" : "") +
					");";
		
		PreparedStatement ps = null;
		
		
		
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setDouble(1, pedido.getPrecoTotal());
			ps.setString(2, pedido.getStatusCompra().toString());
			ps.setInt(3, pedido.getCliente().getId());
			ps.setInt(4, pedido.getEndCobranca().getId());
			ps.setInt(5, pedido.getEndEntrega().getId());
			ps.setString(6, pedido.getDataCompra());
			ps.setInt(7, pedido.getDiasEntrega());
			ps.setString(8, pedido.getCodPedido());
			ps.setDouble(9, pedido.getValorFrete());
			if (cupomDesconto.getId() != 0)
				ps.setInt(10, pedido.getCupom().getId());
			
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			
			int idPedido = 0;
			
			while(rs.next())
				idPedido = rs.getInt(1);
			
			pedido.setId(idPedido);
			
			
			CartoesPedidoDAO cartaoDAO = new CartoesPedidoDAO(conexao);
			for (CartaoPedido cartao : pedido.getListCartoes()) {
				cartao.setIdPedido(idPedido);
				String erro = cartaoDAO.salvar(cartao); 
				if (!erro.equals(""))
					return erro;
				
			}
			
			ItensPedidoDAO itemDAO = new ItensPedidoDAO(conexao);
			for (ItensPedido item : pedido.getListProdutosPedido()) {
				item.setIdPedido(pedido.getId());
				String erro = itemDAO.salvar(item); 
				if (!erro.equals(""))
					return erro;
				
			}
			
			
			CupomDAO cupDAO = new CupomDAO(conexao);
			pedido.getCupom().setUsado(true);
			String erro = cupDAO.alterar(pedido.getCupom());
			if (!erro.equals("")) return erro;
			
			if (ctrlTransacao)
				conexao.commit();
			
			
		}catch (SQLException e) {
			try {
				e.printStackTrace();
				conexao.rollback();
				return "N�o foi poss�vel salvar o PEDIDO";
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		return "";
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		abrirConexao();

		Pedido pedido = (Pedido) entidade;

		String sql = "UPDATE " + nomeTable + " SET ped_status_compra = '" + pedido.getStatusCompra().toString() + "' WHERE " + idTable + " = " + pedido.getId();

		PreparedStatement ps = null;

		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);

			ps.executeUpdate();

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			return "ERRO AO ALTERAR O PEDIDO";
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return "";

	}

	
	
}
