package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensEstoque;
import br.com.ecommerceinformatica.fatec.dominio.Produto;

public class EstoqueDAO extends AbstractDAO {

	public EstoqueDAO() {
		super("est_pro_id", "ESTOQUE");
	}
	public EstoqueDAO(Connection conexao) {
		super("est_pro_id", "ESTOQUE", conexao);
		ctrlTransacao = false;
	}
	
	public String gerarQuery(ItensEstoque produto) {
		String query = "SELECT * FROM " + nomeTable + " as E JOIN PRODUTOS as P ON E." + idTable + " = P.pro_id";
		
		boolean flgMaisDeUmFiltro = false;
		
		List<Integer> listNumeros = new ArrayList<Integer>();
		Map<Integer, String> mapWhere = new HashMap<Integer, String>();
		
		
		if (produto != null) {
			if (produto.getId() != 0) {
				listNumeros.add(1);
				mapWhere.put(1, "est_pro_id = " + produto.getId());
			}
			else {
				
				
			}
			
		}
		
		
		if (!listNumeros.isEmpty()) {
			query += " WHERE ";
			
			for (Integer i : listNumeros) {
				if (flgMaisDeUmFiltro)
					query += " AND ";
				query += mapWhere.get(i);
			}
		}
		
		return query;
	}
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		ItensEstoque itemConsulta = (ItensEstoque) entidade;
		
		String sql = gerarQuery(itemConsulta);
		
		return consultar(sql);
	}
	
	public List<EntidadeDominio> consultar(List<EntidadeDominio> listEntidades) {
		
		String query = "SELECT * FROM " + nomeTable + " JOIN PRODUTOS ON est_pro_id = pro_id WHERE est_pro_id IN (" ;
		boolean flgMaisDeUm = false;
		for (EntidadeDominio entidade : listEntidades) {
			if (flgMaisDeUm)
				query += ",";
			
			query += entidade.getId();
		}
		
		query += ");";
			
		
		return consultar(query);
	}

	
	public List<EntidadeDominio> consultar(String query) {
		abrirConexao();
		PreparedStatement ps = null;
		
		List<EntidadeDominio> listEntidade = new ArrayList<EntidadeDominio>();
		
		try {
			ps = conexao.prepareStatement(query);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Produto produto = new Produto(null, 0, "", "", "", "", "", null, 0, "");
				produto.setNome(rs.getString("pro_nome"));
				produto.setId(rs.getInt("est_pro_id"));
				
				ItensEstoque itemEstoque = new ItensEstoque(produto, 
														rs.getInt("est_qnt_estoque"),
														rs.getString("est_data_ultima_compra"));
				
				itemEstoque.setId(produto.getId());
				
				listEntidade.add(itemEstoque);
			}
			
			
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
		return listEntidade;
		
	}
	
	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();
		PreparedStatement ps = null;
		
		ItensEstoque itemEstoque = (ItensEstoque) entidade;
		String sql = "INSERT INTO " + nomeTable + " VALUES(?,?,?);";
		
		
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			
			ps.setInt(1, itemEstoque.getId());
			ps.setInt(2, itemEstoque.getQndEstoque());
			ps.setString(3, itemEstoque.getDataUltimaCompra());
			
			ps.executeUpdate();
			
			if (ctrlTransacao)
				conexao.commit();
		
		} catch (SQLException e) {
			e.printStackTrace();
			return "N�o foi poss�vel salvar no Estoque";
		}
		finally {
			if(ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return "N�o foi poss�vel fechar a conex�o do BD no Estoque";
				}
			}
		}
		
		return "";
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		abrirConexao();
		
		PreparedStatement ps = null;
		ItensEstoque itemEstoque = (ItensEstoque) entidade;
		String sql = "UPDATE " + nomeTable + " SET est_qnt_estoque = ?, est_data_ultima_compra = ? " + 
							" WHERE est_pro_id = ?";
		
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			ps.setInt(1, itemEstoque.getQndEstoque());
			ps.setString(2, itemEstoque.getDataUltimaCompra());
			ps.setInt(3, itemEstoque.getId());
			
			ps.executeUpdate();
			
			if (ctrlTransacao)
				conexao.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
			return "N�o foi poss�vel alterar Estoque";
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return "";
	}
	

}
