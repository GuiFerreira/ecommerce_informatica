package br.com.ecommerceinformatica.fatec.dao;

import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensCarrinho;
import br.com.ecommerceinformatica.fatec.singleton.CarrinhoSingleton;

public class CarrinhoDAO extends AbstractDAO {

	public CarrinhoDAO() {
		super("", "");
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		return CarrinhoSingleton.getInstance().getInstances();
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		CarrinhoSingleton.getInstance().addList(entidade);
		return "";
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		ItensCarrinho ItensCarrinho = (ItensCarrinho) entidade;		
		ItensCarrinho itemDaLista = (ItensCarrinho) CarrinhoSingleton.getInstance().findById(ItensCarrinho.getProduto().getId());
		
		if (ItensCarrinho != null) {
			itemDaLista.setQuantidade(ItensCarrinho.getQuantidade());
			itemDaLista.setPrecoItem(itemDaLista.getQuantidade() * 
						((itemDaLista.getProduto().getPrecoComprado() * itemDaLista.getProduto().getCategoria().getMargemLucro()) 
								- itemDaLista.getProduto().getDesconto()));
			
			CarrinhoSingleton.getInstance().calcularPrecoCarrinho();
			return "";
		}
		
		return "n�o encontrei o ItensCarrinho";
	}
	
	@Override
	public String excluir(EntidadeDominio entidade) {
		ItensCarrinho ItensCarrinho = (ItensCarrinho) entidade;
		
		ItensCarrinho itemParaTirar = CarrinhoSingleton.getInstance().findById(ItensCarrinho.getProduto().getId());
		
		if (itemParaTirar != null) {
			CarrinhoSingleton.getInstance().remove(itemParaTirar.getId());
			CarrinhoSingleton.getInstance().calcularPrecoCarrinho();
			return "";
		}
		
		return "N�o existe esse produto no carrinho";
		
	}

}
