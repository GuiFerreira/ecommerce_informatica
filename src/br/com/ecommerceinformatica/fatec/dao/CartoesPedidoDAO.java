package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.Banco;
import br.com.ecommerceinformatica.fatec.dominio.Cartao;
import br.com.ecommerceinformatica.fatec.dominio.CartaoPedido;
import br.com.ecommerceinformatica.fatec.dominio.Endereco;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;

public class CartoesPedidoDAO extends AbstractDAO {

	public CartoesPedidoDAO(Connection conexao) {
		super("carped_ped_id", "cartoes_pedido", conexao);
		ctrlTransacao = false;
		
	}
	
	public CartoesPedidoDAO() {
		super("carped_ped_id", "cartoes_pedido");
		
	}
	
	public List<EntidadeDominio> consultar(List<EntidadeDominio> listEntidades) {
		abrirConexao();

		List<EntidadeDominio> listaCartoes = new ArrayList<EntidadeDominio>();

		String sql = "SELECT * FROM " + nomeTable + 
						" JOIN CARTAO_CREDITO AS CAR ON carped_car_id = car_numero " + 
						"WHERE " + idTable + " IN (";
		boolean flgMaisDeUm = false;
		for (EntidadeDominio entidade : listEntidades) {
			if (flgMaisDeUm) sql += ", ";
			
			sql += entidade.getId();
			flgMaisDeUm = true;
		}
		sql += ");";

		PreparedStatement ps = null;

		try {
			ps = conexao.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				Banco banco = new Banco();
				banco.setNome( rs.getString("car_nome_banco") );
				banco.setNumAgencia( rs.getString("car_agencia") );
				
				Cartao cartao = new Cartao(rs.getString("car_numero"), rs.getString("car_nome_impresso"), "", banco);
				
				CartaoPedido carPedido = new CartaoPedido(rs.getInt("carped_ped_id"), cartao, rs.getInt("carped_qnt_parcela"), rs.getDouble("carped_valor_parcela"));
				listaCartoes.add(carPedido);
			}

			return listaCartoes;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
		
	}
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement ps = null;
		
		CartaoPedido cartao = (CartaoPedido) entidade;
		
		try {
			conexao.setAutoCommit(false);

			sql.append("INSERT INTO " + nomeTable + "(carped_ped_id, carped_car_id, carped_valor_total, carped_valor_parcela, carped_qnt_parcela) " + 
							" VALUES (?, ?, ?, ?, ?)");

			ps = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, cartao.getIdPedido());
			ps.setString(2, cartao.getCartao().getNumero());
			ps.setDouble(3, cartao.getValorTotal());
			ps.setDouble(4, cartao.getPrecoParcela());
			ps.setInt(5, cartao.getQntParcelas());
			
			ps.executeUpdate();

			// obter o id do endereco
			ResultSet rs = ps.getGeneratedKeys();

			int idEnd = 0;
			while (rs.next())
				idEnd = rs.getInt(1);

			cartao.setId(idEnd);

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
				return "N�o foi poss�vel salvar o Cart�o Pedido";
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		return "";
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return null;
	}

}
