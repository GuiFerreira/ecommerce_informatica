package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.EmailTrocarSenha;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;

public class EmailTrocarSenhaDAO extends AbstractDAO {
	
	public EmailTrocarSenhaDAO() {
		super("email_cod", "EMAILTROCARSENHA");
	}
	public EmailTrocarSenhaDAO(Connection conexao) {
		super("email_cod", "EMAILTROCARSENHA", conexao);
		ctrlTransacao = false;
	}
	
	public String gerarQuery(EmailTrocarSenha email) {
		
		String query = "SELECT * FROM " + nomeTable; 
		boolean flgWhere = false;

		List<Integer> listPesquisa = new ArrayList<Integer>();
		Map<Integer, String> mapQuery = new HashMap<Integer, String>();
		boolean maisDeUmWhere = false;
		
		if (email != null) {
			if (email.getId() != 0) {
				listPesquisa.add(1);
				mapQuery.put(1, idTable + " = " + email.getId());
				
			}
			else {
				if (email.getEmailCliente() != null) {
					if (!email.getEmailCliente().trim().equals("")) {
						listPesquisa.add(2);
						mapQuery.put(2, "email_cliente LIKE '%" + email.getEmailCliente() + "%'");
					}
					
				}
				
				// se igual a verdadeiro retorna os j� usados
				if (email.getUsouCodigo()) {
					listPesquisa.add(3);
					mapQuery.put(3, "email_usou = true");
				}
				// se igual a false retorna os que n�o foram usados
				else if (!email.getUsouCodigo()) {
					listPesquisa.add(4);
					mapQuery.put(4, "email_usou = false");
				}
			}
			
		}
		
		if (!listPesquisa.isEmpty()) {
			query += " WHERE";
			for (Integer a : listPesquisa) {
				if (maisDeUmWhere) {
					query += " AND";
				}
				query += " " + mapQuery.get(a);
				maisDeUmWhere = true;
			}
			
		}
		
		return query;
	}
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		abrirConexao();
		
		EmailTrocarSenha email = (EmailTrocarSenha) entidade;
		List<EntidadeDominio> listEmail = new ArrayList<EntidadeDominio>();
		String sql = gerarQuery(email);
		
		PreparedStatement ps = null;
		
		try {
			ps = conexao.prepareStatement(sql);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				EmailTrocarSenha emailConsulta = new EmailTrocarSenha(rs.getString("email_cliente"), 
													rs.getBoolean("email_usou"), 
													rs.getInt("email_cod"));
				
				listEmail.add(emailConsulta);
			}
			
			
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				System.out.println("Erro ao dar rollback EmailTrocarSenhaDAO");
			}
			e.printStackTrace();
			System.out.println("Erro ao inserir no bando EmailTrocarSenhaDAO");
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println("N�o foi poss�vel fechar conexao ou ps EmailTrocarSenhaDAO");
				}
				
			}
		}
		
		
		
		return listEmail;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();
		
		EmailTrocarSenha email = (EmailTrocarSenha) entidade;
		String sql = "INSERT INTO " + nomeTable + "(" + idTable + ", email_cliente, email_usou) VALUES(?, ?, false)";
		
		PreparedStatement ps = null;
		
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			
			ps.setInt(1, email.getId());
			ps.setString(2, email.getEmailCliente());
			
			
			ps.executeUpdate();
			
			if (ctrlTransacao)
				conexao.commit();
			
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				return "Erro ao dar rollback EmailTrocarSenhaDAO";
			}
			e.printStackTrace();
			return "Erro ao inserir no bando EmailTrocarSenhaDAO";
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return "N�o foi poss�vel fechar conexao ou ps EmailTrocarSenhaDAO";
				}
				
			}
		}
		
		
		return "";
		
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		abrirConexao();

		EmailTrocarSenha email = (EmailTrocarSenha) entidade;

		String sql = "UPDATE " + nomeTable + " SET email_usou = ? WHERE " + idTable
				+ " = " + email.getId();

		PreparedStatement ps = null;

		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			ps.setBoolean(1, email.getUsouCodigo());
			
			
			ps.executeUpdate();

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
				return "ERRO! Erro ao dar rollback EmailTrocarSenhaDAO";
			}
			e.printStackTrace();
			return "ERRO! Erro ao inserir no bando EmailTrocarSenhaDAO";
		}
		finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return "ERRO! N�o foi poss�vel fechar conexao ou ps EmailTrocarSenhaDAO";
				}
				
			}
		}
		
		return "";
	}

}
