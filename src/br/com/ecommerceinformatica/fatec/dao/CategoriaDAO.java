package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;

public class CategoriaDAO extends AbstractDAO {

	public CategoriaDAO() {
		super("cat_id", "CATEGORIAS");
	}

	public CategoriaDAO(Connection con) {
		super("cat_id", "CATEGORIAS", con);
		ctrlTransacao = false;
	}

	private String gerarQuery(Categoria categoria) {
		String query = "SELECT * FROM " + nomeTable;

		List<Integer> listPesquisa = new ArrayList<Integer>();
		Map<Integer, String> mapQuery = new HashMap<Integer, String>();
		boolean maisDeUmWhere = false;

		// verificar se tem algo para filtro
		if (categoria.getId() != 0) {
			listPesquisa.add(1);
			mapQuery.put(1, "cat_id = " + categoria.getId());
		} else {
			if (categoria.getNome() != null) {
				if (!categoria.getNome().trim().equals("")) {
					listPesquisa.add(2);
					mapQuery.put(2, "cat_nome LIKE '%" + categoria.getNome() + "%'");
				}
			}
			if (categoria.getMargemLucro() > 0) {
				listPesquisa.add(3);
				mapQuery.put(3, "cat_margem_lucro > " + categoria.getMargemLucro());
			}
			
			// se 1 s� ativo
			if (categoria.getTipoFiltroAtivo() == 1) {
				listPesquisa.add(4);
				mapQuery.put(4, "ativo = true");
			}
			// se -1 s� inativo
			else if (categoria.getTipoFiltroAtivo() == -1) {
				listPesquisa.add(5);
				mapQuery.put(5, "ativo = false");
			}
			
		}

		if (!listPesquisa.isEmpty()) {
			query += " WHERE";
			for (Integer a : listPesquisa) {
				if (maisDeUmWhere) {
					query += " AND";
				}
				query += " " + mapQuery.get(a);
				maisDeUmWhere = true;
			}
		}

		return query;
	}

	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		abrirConexao();

		Categoria categoriaFiltro = (Categoria) entidade;

		String sql = gerarQuery(categoriaFiltro);
		PreparedStatement ps = null;

		// Lista das categorias
		List<EntidadeDominio> listCategoria = new ArrayList<EntidadeDominio>();

		try {
			ps = conexao.prepareStatement(sql);

			// Recebi todos os resultados
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				// setar nome e margem de lucro
				Categoria categoria = new Categoria(rs.getString("cat_nome"), rs.getDouble("cat_margem_lucro"));
				// setar id
				categoria.setId(rs.getInt("cat_id"));
				// setar data cadastro
				categoria.setDataCadastro(rs.getString("cat_data_cadastro"));

				listCategoria.add(categoria);

			}

			return listCategoria;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return listCategoria;

	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		abrirConexao();

		StringBuilder sql = new StringBuilder();
		PreparedStatement ps = null;

		Categoria categoria = (Categoria) entidade;

		try {
			conexao.setAutoCommit(false);

			sql.append(
					"INSERT INTO " + nomeTable + "(cat_nome, cat_margem_lucro, cat_data_cadastro, ativo)" + " VALUES(?,?,?, true)");

			ps = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, categoria.getNome());
			ps.setDouble(2, categoria.getMargemLucro());
			ps.setString(3, categoria.getDataCadastro());

			ps.executeUpdate();

			ResultSet rs = ps.getGeneratedKeys();
			int id_categoria = 0;
			while (rs.next())
				id_categoria = rs.getInt(1);
			categoria.setId(id_categoria);

			conexao.commit();

		} catch (SQLException e) {

			try {
				conexao.rollback();
				return "N�o foi poss�vel salvar a Categoria";
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return "";
		
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		abrirConexao();

		Categoria categoria = (Categoria) entidade;

		String sql = "UPDATE " + nomeTable + " SET cat_nome = ?, cat_margem_lucro = ?, ativo = ? WHERE " + idTable
				+ " = " + categoria.getId();

		PreparedStatement ps = null;

		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			ps.setString(1, categoria.getNome());
			ps.setDouble(2, categoria.getMargemLucro());
			ps.setBoolean(3, categoria.isFlgAtivo());
			
			
			ps.executeUpdate();

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			return "ERRO";
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return "";
	}
	
	public String excluir(EntidadeDominio entidade) {
		return desativarItem(entidade);
	}

}
