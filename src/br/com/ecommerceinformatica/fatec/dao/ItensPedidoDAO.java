package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.CartaoPedido;
import br.com.ecommerceinformatica.fatec.dominio.Categoria;
import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.dominio.ItensPedido;
import br.com.ecommerceinformatica.fatec.dominio.Produto;

public class ItensPedidoDAO extends AbstractDAO {

	public ItensPedidoDAO() {
		super("iped_ped_id", "ITENS_PEDIDO");
	}
	
	public ItensPedidoDAO(Connection conexao) {
		super("iped_ped_id", "ITENS_PEDIDO", conexao);
		ctrlTransacao = false;
	}
	
	
	public List<EntidadeDominio> consultar(List<EntidadeDominio> listEntidades) {
		abrirConexao();

		List<EntidadeDominio> listaItensPedido = new ArrayList<EntidadeDominio>();

		String sql = "SELECT * FROM " + nomeTable + " JOIN PRODUTOS AS PRO ON pro_id = iped_pro_id " + 
						"JOIN CATEGORIAS AS CAT ON cat_id = pro_cat_id " + 
						"WHERE " + idTable + " IN (";
		boolean flgMaisDeUm = false;
		for (EntidadeDominio entidade : listEntidades) {
			if (flgMaisDeUm) sql += ", ";
			
			sql += entidade.getId();
			flgMaisDeUm = true;
		}
		sql += ");";

		PreparedStatement ps = null;

		try {
			ps = conexao.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			
			while (rs.next()) {
				Categoria categoria = new Categoria(rs.getString("cat_nome"), rs.getDouble("cat_margem_lucro"));
				
				Produto produto = new Produto(categoria, 0, rs.getString("pro_nome"), rs.getString("pro_codigo"), 
						rs.getString("pro_descricao"), rs.getString("pro_especificacoes"), rs.getString("pro_marca"), 
						null, 0, rs.getString("pro_imagem"));
				
				produto.setId(rs.getInt("pro_id"));
				
				ItensPedido item = new ItensPedido(rs.getInt("iped_ped_id"), produto, rs.getDouble("iped_preco_unitario"), rs.getDouble("iped_preco_total"), 
							rs.getInt("iped_qnt_produto"), rs.getString("iped_data_compra"));
				
				listaItensPedido.add(item);
			}

			return listaItensPedido;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;
		
	}
	
	
	@Override
	public List<EntidadeDominio> consultar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String salvar(EntidadeDominio entidade) {
		abrirConexao();
		
		StringBuilder sql = new StringBuilder();
		PreparedStatement ps = null;
		
		ItensPedido itemPedido = (ItensPedido) entidade;
		
		try {
			conexao.setAutoCommit(false);

			sql.append("INSERT INTO " + nomeTable + "(iped_ped_id, iped_pro_id, iped_qnt_produto, iped_preco_unitario, iped_preco_total, iped_data_compra) " + 
							" VALUES (?, ?, ?, ?, ?, ?)");

			ps = conexao.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, itemPedido.getIdPedido());
			ps.setInt(2, itemPedido.getProduto().getId());
			ps.setInt(3, itemPedido.getQuantidade());
			ps.setDouble(4, itemPedido.getPrecoItem());
			ps.setDouble(5, itemPedido.getPrecoTotal());
			ps.setString(6, itemPedido.getDataCompra());
			
			
			ps.executeUpdate();

			// obter o id do endereco
			ResultSet rs = ps.getGeneratedKeys();

			int idEnd = 0;
			while (rs.next())
				idEnd = rs.getInt(1);

			itemPedido.setId(idEnd);

			if (ctrlTransacao)
				conexao.commit();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
				return "N�o foi poss�vel salvar o ItensPedido";
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		} finally {
			if (ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		return "";
		
	}

	@Override
	public String alterar(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		return null;
	}

}
