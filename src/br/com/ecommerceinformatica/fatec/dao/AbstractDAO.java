package br.com.ecommerceinformatica.fatec.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.com.ecommerceinformatica.fatec.dominio.EntidadeDominio;
import br.com.ecommerceinformatica.fatec.web.util.Conexao;

public abstract class AbstractDAO implements IDAO {

	protected String idTable;
	protected String nomeTable;
	protected boolean ctrlTransacao = true;
	protected Connection conexao;

	public AbstractDAO(String idTable, String nomeTable, Connection conexao) {
		super();
		this.idTable = idTable;
		this.nomeTable = nomeTable;
		this.conexao = conexao;
	}

	public AbstractDAO(String idTable, String nomeTable) {
		super();
		this.idTable = idTable;
		this.nomeTable = nomeTable;
	}
	
	public List<EntidadeDominio> getEntidadeByListIds(List<Integer> listEntidades, String nomeTabela){
		abrirConexao();
		
		List<EntidadeDominio> listaEntidadesConsulta = new ArrayList<EntidadeDominio>();
		
		
		
		return listaEntidadesConsulta;
		
	}
	
	@Override
	public String excluir(EntidadeDominio entidade) {
		// TODO Auto-generated method stub
		abrirConexao();
		
		String sql = "DELETE FROM " + nomeTable + " WHERE " + idTable + " = " + entidade.getId();		
		
		String erro = "";
		
		PreparedStatement ps = null;
		
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			
			ps.executeUpdate();
			
			conexao.commit();
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			erro = "ERRO: " + e.getMessage();
			
		}
		finally {
			try {
				ps.close();
				conexao.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return erro;
		
	}
	
	public String excluir(List<EntidadeDominio> listEntidade) {
		// TODO Auto-generated method stub
		abrirConexao();
		
		String sql = "DELETE FROM " + nomeTable + " WHERE " + idTable + " IN (";
		boolean flg = false;
		for (EntidadeDominio entidade : listEntidade) {
			if (flg)
				sql += ",";
			sql += "'" + entidade.getId() + "'";
			flg = true;
		}
		
		sql += ");";
		
		String erro = "";
		
		PreparedStatement ps = null;
		
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sql);
			
			ps.executeUpdate();
			
			conexao.commit();
			
		} catch (SQLException e) {
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			erro = "ERRO-Excluir listEntidade: " + e.getMessage();
			
		}
		finally {
			try {
				ps.close();
				conexao.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return erro;
		
	}
	
	public String desativarItem(EntidadeDominio entidade) {
		String erro = "";
		abrirConexao();
		
		String sqlDesativarItem = "UPDATE " + nomeTable + " SET ativo = false WHERE " + idTable + " = " + entidade.getId();
		
		PreparedStatement ps = null;
		
		try {
			conexao.setAutoCommit(false);
			ps = conexao.prepareStatement(sqlDesativarItem);
			
			ps.executeUpdate();
			
			if(ctrlTransacao)
				conexao.commit();
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			try {
				conexao.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
			erro =  "ERRO no M�todo desativarItem na classe " + entidade.getClass().getName() + "DAO";
		}
		finally {
			if(ctrlTransacao) {
				try {
					ps.close();
					conexao.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		
		return erro;
	}
	
	protected void abrirConexao() {
		try {
			if (conexao == null || conexao.isClosed())
				conexao = Conexao.getConexao();
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
