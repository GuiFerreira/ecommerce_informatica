<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<!--  	IMPORTAR O MENU    -->
	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<h3 class="center">Promo��es e Novidades</h3>
	<br>

	<div class="container">
		<form action="ProdutosServlet" method="POST">
			<div class="row">
				<% 
				
						
                    	
                    	List<EntidadeDominio> listEntidades = (List<EntidadeDominio>) session.getAttribute("listProdutos");
						// verificar se meus produtos est�o carregados na sess�o
    					if (listEntidades == null){
    						listEntidades = Util.getProdutoAtivo();
    						session.setAttribute("listProdutos", listEntidades);
    						
    					}
    						
						
						
						Produto produto = null;
						
						for (EntidadeDominio e : listEntidades){
							produto = (Produto) e;
							
							out.print("<div class='col s6 m3'>");
								out.print("<div class='card grey darken-1 z-depth-5'>");
									out.print("<a href='produto?id="+ produto.getId() + "'>");
										out.print("<div class='card-image'>");
											out.print("<img class='imgProduto' src='" + produto.getUriImagem() +
													"' alt='" + produto.getNome() + "'>");
										out.print("</div>");
										out.print("<div class='tituloProduto'>" + produto.getNome());
										out.print("</div>");
									out.print("</a>");
								out.print("<div class='descricaoProduto'>" + produto.getDescricao());		    									
								out.print("</div>");
								out.print("<div class='precoProduto'>R$ " + String.format("%.2f", produto.getPrecoComprado() * produto.getCategoria().getMargemLucro()));		    									
								out.print("</div>");		    								
								out.print("</div>");
							out.print("</div>");
							
							
						}
						
    						
    		
    		
                    %>
				
				

			</div>



		</form>
	</div>
	</main>



	<%@include file="footer.jsp"%>


	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>