<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>
	<!-- HEADER -->
	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<div class="container">
		<div class="row">

			<h3>Informacoes da Categoria</h3>

			<%
               		Categoria categoria = (Categoria)session.getAttribute("categoria");
                %>

			<fieldset>
				<legend>Informacoes da Categoria</legend>
				<div class="col s12 m6">
					<h6>
						Nome da Categoria:

						<%
                        		out.print(" " + categoria.getNome());
                            %>

					</h6>
				</div>
				<div class="col s12 m6">
					<h6>
						Margem de Lucro: R$

						<%
                        	    out.print(" " + categoria.getMargemLucro());
                            %>
					</h6>
				</div>
			</fieldset>
			<br />
			<table>
				<th>
					<form action="AlterarCategoria" method="GET">
						<input class="waves-light btn grey" type="submit"
							value="ALTERAR CATEGORIA">
					</form>
				</th>
				<th>
					<form action="ExcluirCategoria" method="POST">
						<input class="waves-light btn grey" type="submit" id="OPERACAO"
							name="OPERACAO" value="EXCLUIR CATEGORIA">
					</form>
				</th>
			
			</table>


		</div>


	</div>

	</main>


	<!-- FOOTER -->
	<%@include file="footer.jsp"%>


	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>