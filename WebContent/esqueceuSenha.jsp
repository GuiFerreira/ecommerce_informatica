<!DOCTYPE html>
<html>
<head>
</head>
<body>
	<%@include file="navBar.jsp"%>
	
	
	<main class="corpo">
		<div class="container">
			<div class="row  center-align ">
	
				<h3>Esqueceu a Senha?</h3>
	
				<div class="col s6 offset-s3  center-align ">
					<fieldset>
	
						<img src="img/user_icon.png" alt="user icon" width="30%"
							height="100px">
	
						<form action="EsqueceuSenha" method="POST">	
							<label for="email">Email para recuperar Senha:</label> 
							<input type="text" id="email" name="email" required> 
							<br />
							<br />
							
							<input class="waves-light btn grey" type="submit" id="OPERACAO"
								name="OPERACAO" value="ESQUECEU SENHA" /> <br> <br>
						</form>
	
					</fieldset>
				</div>
	
			</div>


		</div>
	
	</main>
	
	<%@include file="footer.jsp"%>

	
</body>
</html>