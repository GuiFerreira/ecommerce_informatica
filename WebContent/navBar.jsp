<%@page import="br.com.ecommerceinformatica.fatec.web.util.Constants"%>
<%@page import="br.com.ecommerceinformatica.fatec.web.util.Util"%>
<%@page import="java.util.*"%>
<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>

<!DOCTYPE html>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />
	
<title><% out.print(Constants.TITULO_SITE); %></title>

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>


	<!-- PEGAR O ADM RSRS -->
	<%
		boolean adm = false;
    	Cliente clienteLogado = (Cliente) session.getAttribute("clienteLogado");
    	
		
    	/*
    	if (clienteLogado == null){
    		clienteLogado = Util.getCliente();
    		session.setAttribute("clienteLogado", clienteLogado);
    	}
    	*/
    	if (clienteLogado != null){
    		if (clienteLogado.getUsuario() != null)
    			if (clienteLogado.getUsuario().isAdministrador())
    				adm = true;
				    		
    	}
    		

	%>

	<!-- HEADER -->
	<nav class="grey darken-3">
		<div class="nav-wrapper">
			<a href="paginaPrincipal" class="brand-logo valign-wrapper logo">
				<div class="valign-wrapper"><% out.print(Constants.NOME_EMPRESA_CABECALHO); %></div>
			</a> <a href="paginaPrincipal" data-target="mobile-demo"
				class="sidenav-trigger"><i class="material-icons">menu</i></a>
			<ul class="right hide-on-med-and-down">
				<%
            
            	if (adm)
					out.print("<li><a href='indexAdemir.jsp'>Administrador</a></li>");
            		
            %>
				<li><a href="produtos">Produtos</a></li>
				<%
					if (clienteLogado == null){
						out.print("<li><a href='login.jsp'>Entrar</a></li>" + 
               			"<li><a href='registrar.jsp'>Registrar</a></li>");
	                }
                	else
                		out.print("<li><a href='Perfil'>Perfil</a></li>");
					
                
                %>
				<li><a href="Carrinho">Carrinho</a></li>
				<li><a href="suporte.jsp">Suporte</a></li>
				<li><a href="quemsomos.jsp">Quem Somos?</a></li>
				<%
					if (clienteLogado != null)
						out.print("<li><a href='Deslogar'>Deslogar</a></li>");
				%>
			</ul>
		</div>
	</nav>

	<ul class="sidenav" id="mobile-demo">
		<%
            
        	if (adm)
				out.print("<li><a href='indexAdemir.jsp'>Administrador</a></li>");
            		
        %>
		<li><a href="produtos">Produtos</a></li>
		<%
			if(clienteLogado == null){
            	out.print("<li><a href='login.jsp'>Entrar</a></li>" + 
            	"<li><a href='registrar.jsp'>Registrar</a></li>");
	        }
        	else
            	out.print("<li><a href='Perfil'>Perfil</a></li>");
			        
                
		%>
		<li><a href="Carrinho">Carrinho</a></li>
		<li><a href="suporte.jsp">Suporte</a></li>
		<li><a href="quemsomos.jsp">Quem Somos?</a></li>
		<%
			if (clienteLogado != null)
				out.print("<li><a href='Deslogar'>Deslogar</a></li>");
		%>
	</ul>


</body>

</html>