<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<div class="container">
		<div class="center row">
			<h3>Lan�ar compra de Produto</h3>

			<br>
			<br>
			<form action="Estoque" method="POST">
				<div class="input-field col s12 m6">
					<select id="idProduto" name="idProduto" required>
						<option value="" disabled selected>Escolha um produto</option>
						<%
							
								List<EntidadeDominio> listProdutos = (List<EntidadeDominio>) session.getAttribute("listProdutos");
	    			
	    						for(EntidadeDominio e : listProdutos){
	    							Produto produto = (Produto) e;
	    							out.print("<option value='" + produto.getId() + "'>");
	    							out.print(produto.getNome());
	    							out.print("</option>");
	    							
	    						}
		    						
							
							%>

					</select>
				</div>

				<div class="col s12 m6">
					<label for="qntComprada">Quantidade Comprada de Produto</label> <input
						type="number" id="qntComprada" name="qntComprada"
						placeholder="quantidade comprada" required>
				</div>

				<br>

				<div class="col s12 m6">
					<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO"
						value="REGISTRAR COMPRA">
				</div>

			</form>

		</div>


	</div>

	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>