<%@page
	import="br.com.ecommerceinformatica.fatec.singleton.CarrinhoSingleton"%>
<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>
<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<% 
    	CarrinhoSingleton carrinho = (CarrinhoSingleton) session.getAttribute("carrinho");
    		List<EntidadeDominio> listEntidades = null;
    	if (carrinho != null)
        	listEntidades = carrinho.getInstances();
    %>

	<!-- BODY -->
	<main class="corpo">
	<div class="container">
		<div class="row center">

			<h3>Carrinho</h3>

			<table>
				<thead>
					<tr>
						<td>Imagem do Produto</td>
						<td>Nome do Produto</td>
						<td>Quantidade</td>
						<td>Pre�o</td>
						<td>Editar</td>
						<td>Excluir</td>
					</tr>

				</thead>

				<tbody>
					<%
                        	if (listEntidades != null){
                                                    	ItensCarrinho itemCarrinho = null;
                                                        Produto produto = null;
                                                        for (EntidadeDominio e : listEntidades){
                                                        	itemCarrinho = (ItensCarrinho) e;
                                                            produto = itemCarrinho.getProduto();
                                                            out.print("<tr>");
                                                            out.print("<td><img class='imgCarrinho' src='" + produto.getUriImagem() + "'></td>");
                        							out.print("<td><a href='produto?id=" + produto.getId() + "'>" + produto.getNome() + "</a></td>");
                        							out.print("<td><form action='Carrinho' method='POST'>" +
                        			                                "<select name='quantidadeProduto'>" + 
                        												"<option>" + itemCarrinho.getQuantidade() + "</option>" +
                        			                                    ((itemCarrinho.getQuantidade() == 1) ? "" : "<option>1</option>" )+ 
                        			                                    ((itemCarrinho.getQuantidade() == 2) ? "" : "<option>2</option>" )+
                        			                                    ((itemCarrinho.getQuantidade() == 3) ? "" : "<option>3</option>" )+
                        		                                	"</select>" +
                        	                            		"</td>");
                        							out.print("<td>R$ " + String.format("%.2f", itemCarrinho.getPrecoItem()) + "</td>");
                        							// botao editar
                        							out.print("<td>" +
                        										"<input type='hidden' id='idProdutoAlterar' name='idProdutoAlterar' value=" + produto.getId() + " style='display: none'>"+
                        										"<button type='submit' name='OPERACAO' id='OPERACAO' value='ALTERAR CARRINHO'>" +
                        											"<i class='material-icons'>build</i>" + 
                        										"</button>" +
                        									 "</td>");
                        							// botao excluir
                        							out.print("<td>" +
                        										"<button type='submit' name='OPERACAO' id='OPERACAO' value='EXCLUIR CARRINHO'>" +
                        											"<i class='material-icons'>clear</i>" + 
                        										"</button>" +
                        										"</td>");
                        							
                        							out.print("</form>");
                        							out.print("</tr>");
                                                        }
                                                        
                                                        
                                                    }
                        %>


				</tbody>

			</table>

			<div class="right">
				<h4>
					Sub Total: R$
					<% if (carrinho != null)
											out.print(String.format("%.2f",carrinho.getPrecoCarrinho())); 
									%>
				</h4>
			</div>


			<%
					
					out.print("<br><br>");
	                if (listEntidades != null){
		                if (!listEntidades.isEmpty()){
		                	out.print("<div class='center' style='display: inline'>" + 
		                                	"<form action='Compra' method='GET'> " +
		                            			"<input class='waves-light btn grey' name='OPERACAO' id='OPERACAO' type='submit' value='FINALIZAR COMPRA'>" +
		                            		"</form>" +
		                              "</div>"					
		            		);
		                	
		                }
	                }	
	                
				%>

		</div>


	</div>

	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>