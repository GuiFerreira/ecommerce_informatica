<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>
	<!-- PEGAR O ADM RSRS -->
	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<div class="container">
		<div class="row center">

			<h3>PESQUISAR</h3>

			<br />
			<div class="col s12">
				<!-- -------------------------------------------------------------------------------- -->
				<!--                            ESTOQUE                                            -->
				<fieldset>
					<legend>Pesquisar Estoque</legend>
					<form action="pesquisarEstoque" method="POST">
						<div class="input-field col s12 m6">
							<select id="idProduto" name="idProduto">
								<option value='0'>Todos itens do estoque</option>
								<%
                                		List<EntidadeDominio> listProdutos = (List<EntidadeDominio>) request.getAttribute("listProdutos");
                                		
    									if (listProdutos != null){
	            							for (EntidadeDominio e : listProdutos){
	            								Produto produto = (Produto) e;
	            								
	            								out.print("<option value='" + produto.getId() + "'>");
	            								out.print(produto.getNome());
	            								out.print("</option>");
	            							}
    									}
                                	%>

							</select>


						</div>
						<br /> <input class="waves-light btn grey" type="submit"
							id="OPERACAO" name="OPERACAO" value="PESQUISAR ESTOQUE" /> <br />

					</form>

				</fieldset>
				<!-- -------------------------------------------------------------------------------- -->
				<!--                            PRODUTO                                                 -->
				<fieldset>
					<legend>Pesquisar Produto</legend>
					<form action="pesquisarProduto" method="POST">
						<div class="col s12 m6">
							<label for="nomeProduto">Nome do Produto</label> <input
								type="text" id="nomeProduto" name="nomeProduto">
						</div>
						<div class="col s12 m6">
							<label for="codigoProduto">Código do Produto</label> <input
								type="text" id="codigoProduto" name="codigoProduto">
						</div>

						<div class="col s12">
							<label for="descricaoProduto">Descrição do Produto</label>
							<textarea id="descricaoProduto" name="descricaoProduto"></textarea>
						</div>
						<br />
						<div class="col s12">
							<label for="especificacoesProduto">Especificações do
								Produto</label>
							<textarea id="especificacoesProduto" name="especificacoesProduto"></textarea>
						</div>

						<div class="input-field col s12 m6">
							<select id="fornecedorProduto" name="fornecedorProduto">
								<option value="0">Todos Fornecedores</option>

								<%
                                        List<EntidadeDominio> listaFornecedores = (List<EntidadeDominio>)request.getAttribute("listaFornecedores");
                                        
                                        for (EntidadeDominio e : listaFornecedores){
                                            Fornecedor fornecedor = (Fornecedor) e;
                                            out.print("<option value='" + fornecedor.getId() + "'>" + fornecedor.getNome() + 
                                                    "</option>");
                                        }
                                        
                                    %>
							</select> <label for="fornecedorProduto">Fornecedor do Produto</label>
						</div>


						<div class="input-field col s12 m6">
							<select id="categoriaProduto" name="categoriaProduto">
								<option value="0">Qualquer Categoria</option>

								<%
                                            List<EntidadeDominio> categorias = (List<EntidadeDominio>)request.getAttribute("listaCategorias");
                                                
                                            Categoria categoria = null;
                                                
                                            for(EntidadeDominio e : categorias){
                                                categoria = (Categoria) e;
                                                out.print("<option value='" + categoria.getId() + "'>" + categoria.getNome() + "</option>");
                                                    
                                            }
                                                
                                            
                                            
                                        %>
							</select> <label for="categoriaProduto">Categoria do Produto</label>
						</div>


						<div class="col s12">
							<label for="marcaProduto">Marca do Produto</label> <input
								type="text" id="marcaProduto" name="marcaProduto">
						</div>

						<br />
						<div class="input-field col s12">
							<select id="ativo" name="ativo">
								<option value="0">Ativo/Inativo</option>
								<option value="1">Ativo</option>
								<option value="-1">Inativo</option>
							</select>
						</div>
						<br /> 
						<input class="waves-light btn grey" type="submit"
							id="OPERACAO" name="OPERACAO" value="PESQUISAR PRODUTO" /> <br />

					</form>

				</fieldset>

				<!-- -------------------------------------------------------------------------------- -->
				<!--                            FORNECEDOR                                            -->
				<fieldset>
					<legend>Pesquisar Fornecedor</legend>
					<form action="pesquisarFornecedor" method="POST">
						<div class="col s12 m6">
							<label for="nomeFornecedor">Nome do Fornecedor</label> <input
								class="input-field" type="text" id="nomeFornecedor"
								name="nomeFornecedor">
						</div>

						<div class="col s12 m6">
							<label for="cnpj">CNPJ do Fornecedor</label> <input
								class="input-field" type="text" id="cnpj" name="cnpj">
						</div>
						<br /> <input class="waves-light btn grey" type="submit"
							id="OPERACAO" name="OPERACAO" value="PESQUISAR FORNECEDOR" /> <br />
					</form>

				</fieldset>


				<!-- -------------------------------------------------------------------------------- -->
				<!--                            CATEGORIA                                            -->
				<fieldset>
					<legend>Pesquisar Categoria</legend>
					<form action="pesquisarCategoria" method="POST">
						<div class="col s12 m8">
							<label for="nome">Nome da Categoria</label> <input
								class="input-field" type="text" id="nome" name="nome">
						</div>

						<div class="col s12 m4">
							<label for="margemLucro">Margem de lucro dessa Categoria</label>
							<input class="input-field" value="0" type="number"
								id="margemLucro" name="margemLucro" step="0.01">
						</div>
						<br/>
						<br/>
						<div class="input-field">
							<select id="ativo" name="ativo">
								<option value="0">Ativo/Inativo</option>
								<option value="1">Ativo</option>
								<option value="-1">Inativo</option>
							</select>
						</div>
						<br /> 
						<input class="waves-light btn grey" type="submit"
							id="OPERACAO" name="OPERACAO" value="PESQUISAR CATEGORIA" /> <br />
	
					</form>

				</fieldset>

				<!-- -------------------------------------------------------------------------------- -->
				<!--                            CLIENTE                                            -->
				<fieldset>
					<legend>Pesquisar Cliente</legend>
					<form action="pesquisarCliente" method="POST">
						<div class="col s12 m7">
							<label for="nomeCliente">Nome:</label> <input type="text"
								id="nomeCliente" name="nomeCliente">
						</div>
						<div class="col s12 m5">
							<label for="CPF">CPF:</label> <input type="text" id="CPF"
								name="CPF">
						</div>

						<div class="col s12">
							<label for="email">Email:</label> <input type="text" id=email
								name="email">
						</div>
						<br /> <input class="waves-light btn grey" type="submit"
							id="OPERACAO" name="OPERACAO" value="PESQUISAR CLIENTE" /> <br />

					</form>

				</fieldset>

			</div>


		</div>


	</div>

	</main>


	<!-- FOOTER -->
	<%@include file="footer.jsp"%>


	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>