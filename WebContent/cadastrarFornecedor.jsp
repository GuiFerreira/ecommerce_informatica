<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">

	<div class="container">
		<div class="row center">
			<form action="CadastrarFornecedor" method="POST">

				<h3>Cadastrar Fornecedor</h3>
				<p style="color: red;">Campos com * são obrigat�rios</p>
				<br />

				<fieldset>
					<legend>Dados do Fornecedor</legend>
					<div class="col s12 m8 offset-m2">
						<label for="nomeFornecedor">Nome do Fornecedor *</label> <input
							class="input-field" type="text" id="nomeFornecedor"
							name="nomeFornecedor" required>
					</div>
					<div class="col s12 m8 offset-m2">
						<label for="cnpj">CNPJ do Fornecedor * </label> <input
							class="input-field" type="text" id="cnpj" name="cnpj" required>
					</div>
				</fieldset>

				<fieldset>
					<legend>Contato do Fornecedor *</legend>
					<div class="col s12 m4">
						<div class="row">
							<div class="col s3">
								<label for="DDD">DDD *</label> <input class="input-field"
									type="number" id="DDD" name="DDD" maxlength="99" minlength="10"
									required>
							</div>
							<div class="col s9">
								<label for="telefone">Telefone *</label> <input
									class="input-field" type="number" id="telefone" name="telefone"
									required>
							</div>
						</div>
					</div>

					<div class="col s12 m8">
						<label for="email">Email do Fornecedor *</label> <input
							class="input-field" type="text" id="email" name="email" required>
					</div>


				</fieldset>

				<fieldset>
					<legend>Endereço do Fornecedor</legend>
					<div class="col s12">
						<label for="rua">Rua *</label> <input class="input-field"
							type="text" id="rua" name="rua" required>
					</div>

					<div class="col s12 m8">
						<label for="bairro">Bairro *</label> <input class="input-field"
							type="text" id="bairro" name="bairro" required>
					</div>

					<div class="col s12 m4">
						<label for="numero">Número *</label> <input class="input-field"
							type="text" id="numero" name="numero" required>
					</div>

					<div class="col s12 m8">
						<label for="cidade">Cidade *</label> <input class="input-field"
							type="text" id="cidade" name="cidade" required>
					</div>

					<div class="col s12 m4">
						<label for="cep">CEP *</label> <input class="input-field"
							type="text" id="cep" name="cep" required>
					</div>


					<div class="col s10 m5">
						<label for="estado">Estado *</label> <input class="input-field"
							type="text" id="estado" name="estado" required>
					</div>

					<div class="col s2 m1">
						<label for="siglaEstado">Sigla *</label> <input
							class="input-field" type="text" id="siglaEstado"
							name="siglaEstado" required>
					</div>

					<div class="col s10 m5">
						<label for="pais">País *</label> <input class="input-field"
							type="text" id="pais" name="pais" required>
					</div>

					<div class="col s2 m1">
						<label for="siglaPais">Sigla *</label> <input class="input-field"
							type="text" id="siglaPais" name="siglaPais" required>
					</div>

					<div class="col s12 m6">
						<label for="logradouro">Logradouro *</label> <input
							class="input-field" type="text" id="logradouro" name="logradouro"
							required>
					</div>

					<div class="col s12 m6">
						<label for="tipoLogradouro">Tipo Logradouro *</label> <input
							class="input-field" type="text" id="tipoLogradouro"
							name="tipoLogradouro" required>
					</div>

					<div class="col s12">
						<label for="observacoes">Observações</label>
						<textarea id="observacoes" name="observacoes"></textarea>

					</div>

				</fieldset>
				<br /> <input class="waves-light btn grey" type="submit"
					id="OPERACAO" name="OPERACAO" value="CADASTRAR FORNECEDOR">

			</form>
		</div>
	</div>


	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>