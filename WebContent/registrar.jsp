<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<div class="container">
		<div class="row">
			<h3>Cadastrar!</h3>
			<h6 style="color: red;">Todos os campos s�o obrigatorio!</h6>
			<form action="CadastarCliente" method="POST" class="center">
				<fieldset>
					<legend>Informa�oes B�sicas</legend>
					<div class="col s12">
						<label for="nomeCliente">Nome:</label> <input type="text"
							id="nomeCliente" name="nomeCliente" required>
					</div>
					<div class="col s12 m7">
						<label for="CPF">CPF:</label> <input type="text" id="CPF"
							name="CPF" required>
					</div>
					<div class="col s12 m5">
						<label for="dataNascimento">Data Nascimento:</label> <input
							type="text" id="dataNascimento" name="dataNascimento" required>
					</div>

				</fieldset>

				<fieldset>
					<legend>Informa�oes de Contato</legend>
					<div>
						<div class="col s3 m1">
							<label for="DDD">DDD</label> <input class="input-field"
								type="number" id="DDD" name="DDD" maxlength="99" minlength="10"
								required>
						</div>
						<div class="col s9 m3">
							<label for="telefone">Telefone</label> <input class="input-field"
								type="number" id="telefone" name="telefone" required>
						</div>

					</div>

				</fieldset>


				<fieldset>
					<legend>Usu�rio</legend>
					<div class="col s12 m8">
						<label for="email">Email:</label> <input type="text" id=email
							name="email" required>
					</div>

					<div class="col s12 m6">
						<label for="senha">Senha:</label> <input type="password"
							pattern="((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,15})"
							title="A senha deve conter no minimo 8 caracteres,
                                     ter letras maisculas e minusculas, n�meros além de conter caracteres especiais!"
							id="senha" name="senha" required>
					</div>
					<div class="col s12 m6">
						<label for="confirmacaoSenha">Confirma��o de Senha:</label> <input
							type="password" id="confirmacaoSenha" onblur="validarSenha()"
							name="logconfirmacaoSenhain" required>
					</div>


				</fieldset>


				<br>
				<br> <input class="waves-light btn grey" type="submit"
					id="OPERACAO" name="OPERACAO" value="CADASTRAR CLIENTE"> <br>
				<br>
			</form>
		</div>


	</div>

	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>