<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css" />
<link type="text/css" rel="stylesheet" href="css/estilo.css" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- MAIN -->
	<main class="corpo">
	<h3 class="center">Cadastar Produto</h3>
	<br>

	<div class="container center">
		<div class="row">
			<form action="CadastrarProduto" method="POST">

				<fieldset>
					<legend>Informacoes do Produto</legend>
					<div class="col s12 m6">
						<label for="nomeProduto">Nome do Produto</label> <input
							type="text" class="validate" id="nomeProduto" name="nomeProduto"
							required>
					</div>
					<div class="col s12 m6">
						<label for="codigoProduto">Codigo do Produto</label> <input
							type="text" class="validate" id="codigoProduto"
							name="codigoProduto" required>
					</div>

					<div class="col s12">
						<label for="descricaoProduto">Descricao do Produto</label>
						<textarea id="descricaoProduto" name="descricaoProduto" required></textarea>
					</div>
					<br />
					<div class="col s12">
						<label for="especificacoesProduto">Especificacoes do
							Produto</label>
						<textarea id="especificacoesProduto" name="especificacoesProduto"
							required></textarea>
					</div>
					<br />
					<div class="col s12 m6">
						<label for="precoCompra">Preco de Compra R$</label> <input
							type="number" step="0.01" id="precoCompra" name="precoCompra"
							min="0" required>
					</div>
					<br />
					<div class="col s12 m6">
						<label for="imagemProduto">Selecione a Imagem do Produto</label><br>
						<br> <input type="file" id="imagemProduto"
							name="imagemProduto">
					</div>

				</fieldset>

				<fieldset>
					<legend>Fornecedor do Produto</legend>

					<!-- JSP PARA COMPLETAR AS OPÇÕES -->
					<div class="input-field col s12 m6">
						<select id="fornecedorProduto" name="fornecedorProduto">
							<option value="" disabled selected>Escolha uma opicaoo</option>
							<%
    								List<EntidadeDominio> listaFornecedores = (List<EntidadeDominio>)request.getAttribute("listaFornecedores");
    								
                            		for (EntidadeDominio e : listaFornecedores){
                            			Fornecedor fornecedor = (Fornecedor) e;
                            			out.print("<option value='" + fornecedor.getId() + "'>" + fornecedor.getNome() + 
                            					"</option>");
                            		}
    							
    							%>

						</select> <label for="fornecedorProduto">Fornecedor do Produto</label>
					</div>

					<div class="col s12 m6">
						<br /> <a href="cadastrarFornecedor.jsp">Cadastar um Novo
							Fornecedor?</a>

					</div>

					<div class="col s12">
						<label for="marcaProduto">Marca do Produto</label> <input
							type="text" class="validate" id="marcaProduto"
							name="marcaProduto" required>
					</div>


				</fieldset>

				<fieldset>
					<legend>Categoria do Produto</legend>

					<!-- JSP PARA COMPLETAR AS OPÇÕES -->
					<div class="input-field col s12 m6">
						<select id="categoriaProduto" name="categoriaProduto">
							<option value="" disabled selected>Escolha uma opcaoo</option>
							<%
                                	List<EntidadeDominio> categorias = (List<EntidadeDominio>)request.getAttribute("listaCategorias");
                            		
                            		Categoria categoria = null;
                            		
                            		for(EntidadeDominio e : categorias){
                            			categoria = (Categoria) e;
                            			out.print("<option value='" + categoria.getId() + "'>" + categoria.getNome() + "</option>");
                            			
                            		}
                                	
                                
                                
                                %>

						</select> <label for="categoriaProduto">Categoria do Produto</label>
					</div>

					<div class="col s12 m6">
						<br /> <a href="cadastrarCategoria.jsp">Cadastrar uma Nova
							Categoria?</a>
					</div>


				</fieldset>
				<br>
				<br>
				<center>
					<input class="waves-light btn grey" type="submit" id="OPERACAO"
						name="OPERACAO" value="CADASTRAR PRODUTO">
				</center>
				<br>
				<br>

			</form>

		</div>
	</div>




	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>