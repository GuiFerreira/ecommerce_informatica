<%@page import="br.com.ecommerceinformatica.fatec.dominio.Categoria"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo center"> <%
				
			Categoria categoria = (Categoria)session.getAttribute("categoria");
		
		%>

	<h3>Cadastrar Categoria do Produto</h3>
	<p style="color: red;">Campos com * s�o obrigat�rios</p>
	<br />

	<div class="container">
		<div class="row">


			<form action="AlterarCategoria" method="POST">
				<fieldset>
					<legend>Dados de Categoria</legend>

					<input id="id" name="id"
						value="<% out.print(categoria.getId()); %>" style="display: none" />

					<div class="col s12 m8">
						<label for="nome">Nome da Categoria</label> <input
							class="input-field" type="text" id="nome" name="nome"
							value="<% out.print(categoria.getNome()); %>" required>
					</div>

					<div class="col s12 m4">
						<label for="margemLucro">Margem de lucro dessa Categoria</label> <input
							class="input-field" type="number" id="margemLucro"
							name="margemLucro"
							value="<% out.print(categoria.getMargemLucro()); %>"
							step="0.01" required>
					</div>
					
					<div class="col s12 m6">
						<br/>
						<label>
							<input type="checkbox" id="ativo" name="ativo" checked='"<% out.print((categoria.isFlgAtivo()) ? "checked" : "unchecked");%>"'/>
							<span>Ativo/Inativo</span>
						</label>
					</div>
					
				</fieldset>

				<br /> <input type="submit" class="waves-light btn grey"
					id="OPERACAO" name="OPERACAO" value="ALTERAR CATEGORIA">

			</form>
		</div>

	</div>



	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>