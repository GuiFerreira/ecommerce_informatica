<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
		<%@include file="navBar.jsp"%>
		<%
			Pedido pedido = (Pedido) session.getAttribute("pedido");
			double precoDosProdutos = 0;
		
		%>
		<!-- BODY -->
		<main class="corpo">
		<div class="center">
			<div class="row">
				<div class="container">
					<h3>Solicitar Cancelamento de Itens</h3>
						<% out.print("<h5>C�digo do pedido: " + pedido.getCodPedido() + "</h5>"); %>
		
						<form action="SolicitarCancelamento" method="POST">
							<fieldset>
								<legend><b>Itens do Pedido</b></legend>
								<table>
									<thead>
										<tr>
											<td>Imagem do Produto</td>
											<td>Nome do Produto</td>
											<td>Quantidade Dispon�vel</td>
											<td>Quantidade Cancelada</td>
											<td>Ser� Enviado</td>
										</tr>
									</thead>
									
										<tbody>
											<%
												Map<Integer, Integer> mapQntTrocaByIdProduto = new HashMap<Integer, Integer>();
												for (ItensPedido produto : pedido.getListProdutosPedido()) {
													mapQntTrocaByIdProduto.put(produto.getProduto().getId(), 0);
												}
												if (pedido.getListTrocaPedido() != null && !pedido.getListTrocaPedido().isEmpty())
												for (TrocaPedido troca : pedido.getListTrocaPedido()) {										
													for (ItensTroca item : troca.getListItensTroca()){
														if (item.getStatus().toString().equals(StatusTrocaCancelamento.CANCELAMENTO_RECUSADO.toString()) || 
																item.getStatus().toString().equals(StatusTrocaCancelamento.TROCA_RECUSADA.toString()))
															continue;
															
															
														int qnt = mapQntTrocaByIdProduto.get(item.getProduto().getId());
														qnt += item.getQntTrocada();
														
														mapQntTrocaByIdProduto.put(item.getProduto().getId(), qnt);
														
													}
													
													
												}
											
											
						                     
												for (ItensPedido itemPedido : pedido.getListProdutosPedido()){
													Produto produto = itemPedido.getProduto();
													precoDosProdutos += itemPedido.getPrecoTotal();
													int intQntComprada = (itemPedido.getQuantidade() - mapQntTrocaByIdProduto.get(itemPedido.getProduto().getId()));
													String 	qntComprada 	= "qntComprada:" + itemPedido.getProduto().getId(), 
															qntCancelado 	= "qntCancelado:" + itemPedido.getProduto().getId(),
															qntTrocado 	= "qntTrocado:" + itemPedido.getProduto().getId();
													out.print("<tr>");
														out.print("<td><img class='imgCarrinho' src='" + produto.getUriImagem() + "'></td>");
														out.print("<td><a href='produto?id="+ produto.getId() + "'>" + produto.getNome() + "</a></td>");
														out.print("<input type='hidden' id='idProduto' name='idProduto' value='" + itemPedido.getProduto().getId() + "'/>");
														out.print("<td><input type='number' id='" + qntComprada + "' name='" + qntComprada + "' value='" + intQntComprada + "' disabled='true'/></td>");
														out.print("<td><input type='number' name='" + qntCancelado + "' id='" + qntCancelado + "' max='" + itemPedido.getQuantidade() + "' value='0' onchange='calcularQntEntrega(" + itemPedido.getProduto().getId() + ")' /></td>");
														out.print("<td><input type='number' name='" + qntTrocado + "' id='" + qntTrocado + "' value='" + intQntComprada + "' disabled='true'/></td>");
														
													out.print("</tr>");
												}
					                               
					                                   
					                               
					                        %>
										</tbody>
									
								</table>
								
							</fieldset>
							
							<br/><br/><br/>
				
							
							<input class="waves-light btn grey" name="OPERACAO" id="OPERACAO"
									type="submit" value="CANCELAR ITEM">
						
							
							<br/><br/>
							
						</form>
				
				</div>
			</div>
			
		</div>
	
		</main>
	
		<%@include file="footer.jsp"%>
	
		<!--JavaScript at end of body for optimized loading-->
		<script type="text/javascript" src="js/materialize.min.js"></script>
	</body>
	
	
	<script type="text/javascript">
		function calcularQntEntrega(idProduto) {
			var qntComprada = document.getElementById('qntComprada:' + idProduto).value;
			var qntCancelada = document.getElementById('qntCancelado:' + idProduto).value;
			var qntTrocado = document.getElementById('qntTrocado:' + idProduto).value;
			
			if (qntComprada < qntCancelada) {
				alert('N�o pode cancelar uma quantidade maior do que a comprada...');
				document.getElementById('qntCancelado:' + idProduto).value = 0;
			}
			
			else if (qntCancelada < 0){
				alert('N�o pode colocar quantidades negativas');
				document.getElementById('qntCancelado:' + idProduto).value = 0;
			}
			
			else {
				document.getElementById('qntTrocado:' + idProduto).value = qntCancelada;
				
			}
			
			
			
		}
	</script>
	
	
	
</html>