<%@page
	import="br.com.ecommerceinformatica.fatec.singleton.CarrinhoSingleton"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<div class="center">
		<div class="row">

			<h3>Finalizar Compra</h3>

			<% 
			    	CarrinhoSingleton carrinho = (CarrinhoSingleton) session.getAttribute("carrinho");
			    		List<EntidadeDominio> listEntidades = null;
			    	if (carrinho != null)
			        	listEntidades = carrinho.getInstances();
    			%>

			<fieldset>
				<legend><b>Itens do Pedido</b></legend>
				<table>
					<thead>
						<tr>
							<td>Imagem do Produto</td>
							<td>Nome do Produto</td>
							<td>Quantidade</td>
							<td>Pre�o</td>
						</tr>
					</thead>
					<tbody>
						<%
                        	if (listEntidades != null){
                                                    	ItensCarrinho itensProdutos = null;
                                                        Produto produto = null;
                                                        for (EntidadeDominio e : listEntidades){
                                                        	itensProdutos = (ItensCarrinho) e;
                                                            produto = itensProdutos.getProduto();
                                                            out.print("<tr>");
                                                            out.print("<td><img class='imgCarrinho' src='" + produto.getUriImagem() + "'></td>");
                        							out.print("<td><a href='produto?id="+ produto.getId() + "'>" + produto.getNome() + "</a></td>");
                        							out.print("<td>" + itensProdutos.getQuantidade() + "</td>");
                        							out.print("<td>R$ " + String.format("%.2f", itensProdutos.getPrecoItem()) + "</td>");
                        							
                                                        }
                                                        
                                                        
                                                    }
                        %>
					</tbody>
				</table>
				
				<h6>Pre�o total = R$ <% out.print(String.format("%.2f",carrinho.getPrecoCarrinho())); %></h6>

			</fieldset>


			<br /> <br />

			<form action="Compra" method="POST">
				<div class="col s6">
					<fieldset>
						<legend><b>Dados do Usu�rio</b></legend>
						<%
								if (clienteLogado != null){
							%>
						<fieldset>
							<legend><b>Dados Cadastrais</b></legend>
							<h6>
								Nome:
								<% out.print(clienteLogado.getNome()); %>
							</h6>
							<h6>
								Cpf:
								<% out.print(clienteLogado.getCpf()); %>
							</h6>
							<h6>
								<% out.print("Telefone : (" + clienteLogado.getContato().getDDD() + ") "
												+ clienteLogado.getContato().getTelefone()); %>
							</h6>
							<h6>
								Email:
								<% out.print(clienteLogado.getContato().getEmail()); %>
							</h6>

						</fieldset>
						<br />


						<%
								}
								
								else{
									
							%>

						<h6>
							J� tem acadastro? <a href="login.jsp"> Entre na sua conta por
								aqui! </a>
						</h6>
						<h6>
							N�o tem cadastro? <a href="registrarClienteCompra.jsp">Cadastra-se
								agora!</a>
						</h6>

						<% 
							
								}
								
							%>

						<fieldset>
							<legend><b>Endere�o de Entrega</b></legend>
							<%
									// colocar os endere�os de entrega
									if (clienteLogado != null){
										if (clienteLogado.getListaEnderecoEntrega() != null && !clienteLogado.getListaEnderecoEntrega().isEmpty()){
											String checked = "checked";
											for(EnderecoCliente end : clienteLogado.getListaEnderecoEntrega()){
												out.print("<fieldset>");
												out.print("<legend><b>" + end.getApelidoEndereco() + "</b></legend>");
												out.print("<label>");
													out.print("<input name='usarEnderecoEntrega' type='radio' value='" +
												end.getId() + "'" + checked + " onchange='calcularFrete()'/>");
													out.print("<span>Usar esse endere�o como de Entrega?<span>");
												out.print("</label><br/>");
													out.print("Endereco: " + end.getRua() + "<br/>");
													out.print("Cidade: " + end.getCidade().getNome() + "<br/>");
													out.print("N�mero: " + end.getNumero() + "<br/>");
												out.print("</fieldset>");
												checked = "";
												
											}
										%>
										
											<fieldset>
												<legend><b>Outro Endere�o?</b></legend>
												<h6>
													Cadastrar outro endere�o? <a
														href="cadastrarEndereco.jsp?tipoEndereco=0"> Clique
														aqui! </a>
												</h6>
											</fieldset>
											<br />
											
											<%
										}
										
										else{
											
										%>
											
											<fieldset>
												<legend><b>Endere�o de Entrega</b></legend>
												<h6>
													Cadastrar endere�o de entrega? <a href="cadastrarEndereco.jsp?tipoEndereco=0"> Clique aqui! </a>
												</h6>
											
											</fieldset>	
											
											
										<%
											
											
										}
											
										%>

								<%		
									}
									else
										out.print("<h6> Favor fa�a login!</h6>");	
								
								%>
								
								<fieldset>
									<legend>Frete:</legend>
									
									<input type="text" id="frete" name="frete" disabled="true" value=""/>
									
								</fieldset>
								

						</fieldset>




					</fieldset>

				</div>
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				<!--  ----------------------------------------------------------------------------------------------------------------------- -->
				<!-- 												LADO DIREITO -->
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				<div class="col s6">
					<fieldset>
						<legend><b>Informa��es do Pedido</b></legend>
						<fieldset>
							<legend><b>Forma de Pagamento</b></legend>
							<%
									if (clienteLogado != null){
										if (clienteLogado.getListaCartoes() != null && !clienteLogado.getListaCartoes().isEmpty()){
											String checked = "checked";
											for (Cartao cartao : clienteLogado.getListaCartoes()){
												out.print("<fieldset>");
												out.print("<legend><b>Cart�o de Cr�dito</b></legend>");
												out.print("<div class='col s12 m12 l12 left-align'>");
												out.print("<label>");
													out.print("<input name='cartaoCredito' type='checkbox' value='" + cartao.getNumero() + "'"
															+ checked + "/>");
													out.print("<span>Usar esse Cart�o de Cr�dito?<span>");
												out.print("</label><br/>");
													out.print("Nome impresso: " + cartao.getNomeImpresso() + "<br/>");
													out.print("N�mero do Cart�o: " + cartao.getNumero() + "<br/>");
													out.print("Nome Banco: " + cartao.getBanco().getNome() + "<br/>");
													out.print("Agencia: " + cartao.getBanco().getNumAgencia() + "<br/>");
													out.print("<label>");
														out.print("<span>Quantidade de parcelas:<span>");
														//out.print("<input name='qntParcelas:" + cartao.getNumero() + "' type='number' max='6' min='1' value='1'/>");
														out.print("<select name='qntParcelas:" + cartao.getNumero() + "'>");
														%>
														
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
															<option value="6">6</option>
														</select>
														<%
													out.print("</label><br/>");
												out.print("</div>");
												out.print("</fieldset>");
												checked = "";
												
												
											}
																				
										
								%>
				
											<fieldset>
												<legend><b>Outro Cart�o?</b></legend>
												<h6>
													Cadastrar outro Cart�o? <a href="cadastrarCartao.jsp"> Clique aqui! </a>
												</h6>
											</fieldset>
											<br />

								<%
								
										}
										
										else{
											%>
											
											<fieldset>
												<legend><b>Cart�o de Cr�dito</b></legend>
													<h6>
														Cadastrar Cart�o? <a href="cadastrarCartao.jsp"> Clique aqui! </a>
													</h6>
											
											</fieldset>
											
											
											<%
											
										}
									}
										
									else
										out.print("<h6> Favor fa�a login!</h6>");
								
								
								%>
								

						</fieldset>
						<br />
						<fieldset>
							<legend><b>Cupom</b></legend>
							<%
								if (clienteLogado != null) {
									if (clienteLogado.getListCupons() == null || clienteLogado.getListCupons().isEmpty()) {
							%>
								<h5>Voc� n�o possui cupons para desconto!</h5>		
							<%		
									}
									else {
							%>
							<label for="cupomDesconto">Selecione o Cupom: </label>
							<select name="cupomDesconto" id="cupomDesconto">
								<option value="">Selecione um</option>
								<%
									
										for (Cupom cupom : clienteLogado.getListCupons()) {
											if (cupom.isUsado() || Util.venceu(cupom.getVencimento())) continue;
											String nome = cupom.getCodigo() + ": " + cupom.getNome() + " | R$ " + cupom.getValor() + " | Vencimento = " + cupom.getVencimento();
											out.print("<option value='" + cupom.getCodigo() + "'>" + nome + "</option>");
										}
								
								%>
							</select>
							
							
							<% 
									}
									
								}// if cliente logado != null
								else
									out.print("<h6> Favor fa�a login!</h6>");
							%>
						</fieldset>
						<br />

						<fieldset>
							<legend><b>Endere�o de Cobran�a</b></legend>
							<%
									// colocar os endere�os de entrega
									if (clienteLogado != null){
										if (clienteLogado.getListaEnderecoCobranca() != null && !clienteLogado.getListaEnderecoCobranca().isEmpty()){
											String checked = "checked";
											for(EnderecoCliente end : clienteLogado.getListaEnderecoCobranca()){
												out.print("<fieldset>");
												out.print("<legend><b>" + end.getApelidoEndereco() + "</b></legend>");
												out.print("<label>");
													out.print("<input name='usarEnderecoCobranca' type='radio' value='" + end.getId() + "'"
															+ checked + "/>");
													out.print("<span>Usar esse endere�o como de Cobran�a?<span>");
												out.print("</label><br/>");
													out.print("Endereco: " + end.getRua() + "<br/>");
													out.print("Cidade: " + end.getCidade().getNome() + "<br/>");
													out.print("N�mero: " + end.getNumero() + "<br/>");
												out.print("</fieldset>");
												checked = "";
												
											}
										
										}
											
										
								%>
											<fieldset>
												<legend><b>Outro Endere�o?</b></legend>
												<h6>
													Cadastrar endere�o de cobran�a? <a
														href="cadastrarEndereco.jsp?tipoEndereco=1"> Clique
														aqui! </a>
												</h6>
											</fieldset>
											<br />

											
								<% 	
										
										
									}
							
									else
										out.print("<h6> Favor fa�a login!</h6>");
								
								%>

						</fieldset>



					</fieldset>

				</div>


				<br> <br>

				<h6></h6>
				<h6></h6>
				<h6></h6>

				<%
						if (clienteLogado != null){
					%>
				<input class="waves-light btn grey" name="OPERACAO" id="OPERACAO"
					type="submit" value="FINALIZAR PEDIDO">

				<%
						}
					%>

			</form>

		</div>


	</div>

	</main>
	
	<input type="hidden" id="valorFrete" value="1"/> 

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>


	<script type="text/javascript">
		function calcularFrete(){
			var valorFrete = parseInt(document.getElementById('valorFrete').value);
			var preco = valorFrete * 5;
			var dinheiro = 'R$ ';
			
			document.getElementById('frete').value = dinheiro + preco + ',00';

			document.getElementById('valorFrete').value = valorFrete + 1;
			if (valorFrete === 4)
				document.getElementById('valorFrete').value = 1;
			
			document.getElementById('frete').value = dinheiro + '15' + ',00';
			
		}
		
		calcularFrete();
	</script>


</html>