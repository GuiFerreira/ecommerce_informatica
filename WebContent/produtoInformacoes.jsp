<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo"> <% 
            Produto produto = (Produto) session.getAttribute("produto");
        %>

	<div class="container">
		<div class="row">

			<h3 class="center">Informacoes do Produto</h3>

			<fieldset>
				<legend>Informacoes do Produto</legend>
				<div class="col s12 m6">
					<!-- IMAGEM DO PRODUTO -->

					<%
                            out.print("<img src='" + produto.getUriImagem() + "' alt='" + produto.getNome() + "' width='100%' height='300'/>");
						%>

				</div>

				<div class="col s12 m6">
					<div class="col s12">
						<h6>
							Nome do Produto:
							<% 
                                	out.print(" " + produto.getNome());
                                %>
						</h6>
					</div>
					<div class="col s12">
						<h6>
							Codigo do Produto:
							<% 
	                                    out.print(" " + produto.getCodProduto());
                            	%>
						</h6>
					</div>

					<div class="col s12">
						<h6>
							Descricao do Produto:
							<% 
	                                out.print(" " + produto.getDescricao());    
                            	%>
						</h6>
					</div>

					<div class="col s12">
						<h6>
							Especificacoes do Produto:
							<% 
                                	out.print(" " + produto.getEspecificacoes());
                            	%>
						</h6>
					</div>

					<div class="col s12">
						<h6>
							Marca do Produto:
							<% 
                                    out.print(" " + produto.getMarca());           
                                %>
						</h6>
					</div>

					<%
						if (clienteLogado != null){					
							if (clienteLogado.getUsuario().isAdministrador()){
								out.print("<div class='col s12'>");
								out.print("<h6>");
								out.print("Preco de Compra do Produto: R$ ");
								out.print(produto.getPrecoComprado());
								out.print("</h6>");
								out.print("<div>");
							}
								
						}
					
					%>

					<% 
                        	if (adm){
	                        	out.print("<div class='col s12'>");
	                        		out.print("<h6>Margem de Lucro do Produto: x");
		                                    out.print(String.format(".2f", produto.getCategoria().getMargemLucro())); 
		                            out.print("</h6>");
		                        out.print("</div>");
                        	}
						%>

					<div class="col s12">
						<h6>
							Desconto do Produto: R$
							<!-- PRECCO DE COMPRA * MARGEM LUCRO-->
							<% 
                                out.print(" " + String.format("%.2f", produto.getDesconto()));
                                                
                                %>
						</h6>
					</div>


					<div class="col s12">
						<h6>
							Preco de Venda do Produto: R$
							<!-- PRECCO DE COMPRA * MARGEM LUCRO-->
							<% 
                                out.print(" " + String.format("%.2f", (produto.getPrecoComprado() * produto.getCategoria().getMargemLucro()) - produto.getDesconto()));
                                                
                                %>
						</h6>
					</div>


					<div class="col s12">
						<h6>
							Categoria do Produto:
							<!-- PRECCO DE COMPRA * MARGEM LUCRO-->
							<% 
                                    out.print(" " + produto.getCategoria().getNome());
                                                    
                                %>
						</h6>
					</div>

					<div class="col s12">
						<h6>
							Fornecedor do Produto:
							<% 
                                    out.print(" " + produto.getFornecedor().getNome());
                                                    
                                %>
						</h6>
					</div>

				</div>





			</fieldset>

			<br />
			<div class="center">
				<table>
					<th>
						<form action="Carrinho" method="POST">
							<input class="waves-light btn grey" name="OPERACAO" id="OPERACAO"
								type="submit" value="COLOCAR CARRINHO">
						</form>
					</th>

				<%
         		if (adm){
         		
         			out.print("<th><form action='AlterarProduto' method='GET'>" + 
 		            	   		 "<input class='waves-light btn grey' type='submit' value='ALTERAR PRODUTO'>" + 
               		  			"</form></th>" + 
			               "<th><form  action='ExcluirProduto' method='POST'>" + 
			                   "<input class='waves-light btn grey' type='submit' id='OPERACAO'" + 
			               			"name='OPERACAO' value='EXCLUIR PRODUTO'>" +
			               "</form></th>");
         		}
                	
	                %>
	                
	            </table>
			</div>


		</div>


	</div>



	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>