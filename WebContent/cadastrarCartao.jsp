<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<div class="container">
		<div class="row center">
			<h3>Cadastrar Cartao</h3>
			
			<form action="Cartao" method="POST">
				<fieldset>
					<div class="col s12 m6">
						<label for="nomeBanco">Nome do Banco:</label> 
						<select name="nomeBanco" id="nomeBanco">
							<option value="Itau">Itau</option>
							<option value="Inter">Inter</option>
							<option value="Santander">Santander</option>
							<option value="Bradesco">Bradesco</option>
							<option value="Banco do Brasil">Banco do Brasil</option>
							<option value="Caixa">Caixa</option>
						</select>
					</div>
					
					<div class="col s12 m6">
						<label for="numAgencia">N�mero da Ag�ncia:</label> <input type="text" id="numAgencia"
							name="numAgencia" required>
					</div>

					<div class="col s12">
						<label for="nomeImpresso">Nome Impresso:</label> <input type="text" id="nomeImpresso"
							name="nomeImpresso" required>
					</div>
					
					<div class="col s12 m6">
						<label for="numCartao">N�mero do Cart�o:</label> <input type="text" id="numCartao"
							name="numCartao" required>
					</div>
					
					<div class="col s12 m6">
						<label for="codSeguranca">C�digo de Seguran�a:</label> <input type="text" id="codSeguranca"
							name="codSeguranca" required>
					</div>
					
				<input class="waves-light btn grey" type="submit" id="OPERACAO"
						name="OPERACAO" value="CADASTRAR CARTAO">
					
				</fieldset>
			
			</form>
		</div>


	</div>

	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>