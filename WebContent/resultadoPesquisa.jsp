<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">

	<div class="container">
		<div class="row center">

			<div class="col s12">


				<%
					String nomeClasse = null;

					Resultado resultado = (Resultado) session.getAttribute("resultado");
					List<EntidadeDominio> entidades = null;
					if (resultado != null) {
						entidades = resultado.getEntidades();
						if (entidades != null) {
							if (entidades.isEmpty()){
								out.print("<h3>Nenhum resultado encontrado :(</h3> <br/>");
								out.print("<h6>Essas caracteristicas nao se encontram no nosso banco de dados...");
								nomeClasse = "vazio";
							}
							else if (entidades.get(0).getClass().getName().equals(Produto.class.getName())) {
								out.print("<h3>Resultado de Produtos</h3> <br>");
								nomeClasse = Produto.class.getName();
							} else if (entidades.get(0).getClass().getName().equals(Fornecedor.class.getName())) {
								out.print("<h3>Resultado de Fornecedores</h3> <br>");
								nomeClasse = Fornecedor.class.getName();
							} else if (entidades.get(0).getClass().getName().equals(Categoria.class.getName())) {
								out.print("<h3>Resultado de Categoria</h3> <br>");
								nomeClasse = Categoria.class.getName();
							}
							else if (entidades.get(0).getClass().getName().equals(Cliente.class.getName())){
								out.print("<h3>Resultado de Clientes</h3> <br>");
								nomeClasse = Cliente.class.getName();
							}
							else if (entidades.get(0).getClass().getName().equals(ItensEstoque.class.getName())){
								out.print("<h3>Resultado de Itens do Estoque</h3> <br>");
								nomeClasse = ItensEstoque.class.getName();
							}
						}
					}
					else {
						out.print("<h3>Erro ao fazer a Pesquisa</h3> <br/>");
					}

					
				%>

				<table>
					<thead>
						<tr>

							<%
								if (entidades != null && !nomeClasse.equals("vazio")) {
									if (nomeClasse.equals(Produto.class.getName())) {
										out.print("<td>Codigo Produto</td>" + "<td>Nome do Produto</td>" + "<td>Categoria do Produto</td>"
												+ "<td>Pesquisar</td>");
									} else if (nomeClasse.equals(Fornecedor.class.getName())) {
										out.print("<td>CNPJ do Fornecedor</td>" + "<td>Nome do Fornecedor</td>" + "<td>Pesquisar</td>");
									} else if (nomeClasse.equals(Categoria.class.getName())) {
										out.print("<td>Categoria</td>" + "<td>Margem de Lucro</td>" + "<td>Pesquisar</td>");
									}
									else if (nomeClasse.equals(Cliente.class.getName())){
										out.print("<td>Cliente</td>" + "<td>Cpf</td>" + "<td>Administrador</td>" + "<td>Pesquisar</td>");
									}
									else if (nomeClasse.equals(ItensEstoque.class.getName())){
										out.print("<td>Produto</td>" + "<td>Quantidade Estoque</td>" + "<td>Ultima Data da Compra</td>");
									}

								}
							%>


						</tr>
					</thead>

					<tbody>

						<%
							if (entidades != null && !nomeClasse.equals("vazio")) {
								for (EntidadeDominio e : entidades) {
									out.print("<tr>");
									if (nomeClasse.equals(Produto.class.getName())) {
										Produto produto = (Produto) e;
										out.print("<td>" + produto.getCodProduto() + "</td>" + "<td>" + produto.getNome() + "</td>"
												+ "<td>" + produto.getCategoria().getNome() + "</td>"
												+ "<td><form action='DadosProduto' method='GET'><input name='id' id='id' value='"
												+ produto.getId() + "' style='display:none'><input type='submit' id='OPERACAO' "
												+ "name='OPERACAO' value='PRODUTO' class='waves-light btn grey'></form></td>");
									} 
									
									else if (nomeClasse.equals(Fornecedor.class.getName())) {
										Fornecedor fornecedor = (Fornecedor) e;
										out.print("<td>" + fornecedor.getCNPJ() + "</td>" + "<td>" + fornecedor.getNome() + "</td>"
												+ "<td><form action='DadosFornecedor' method='GET'><input name='id' id='id' value='"
												+ fornecedor.getId() + "' style='display:none'><input type='submit' id='OPERACAO' "
												+ "name='OPERACAO' value='FORNECEDOR' class='waves-light btn grey'></form></td>");
									} 
									
									else if (nomeClasse.equals(Categoria.class.getName())) {
										Categoria categoria = (Categoria) e;
										out.print(
												"<td>" + categoria.getNome() + "</td>" + "<td>" + categoria.getMargemLucro() + "</td>"+
														"<td><form action='DadosCategoria' method='GET'><input name='id' id='id' value='"
														+ categoria.getId() + "' style='display:none'><input type='submit' id='OPERACAO' "
														+ "name='OPERACAO' value='CATEGORIA' class='waves-light btn grey'></form></td>");
									}
									
									else if (nomeClasse.equals(Cliente.class.getName())){
										Cliente cliente = (Cliente) e;
										out.print(
												"<td>" + cliente.getNome() + "</td>" + 
												"<td>" + cliente.getCpf() + "</td>" +
												"<td>" + cliente.getUsuario().isAdministrador() + "</td>" +
												"<td><form action='DadosCliente' method='GET'><input name='id' id='id' value='"
												+ cliente.getId() + "' style='display:none'><input type='submit' id='OPERACAO' "
												+ "name='OPERACAO' value='CLIENTE' class='waves-light btn grey'></form></td>"
												);
										
									}
									
									else if(nomeClasse.equals(ItensEstoque.class.getName())){
										ItensEstoque itemEstoque = (ItensEstoque) e;
										out.print("<td>" + itemEstoque.getProduto().getNome() +"</td>");
										out.print("<td>" + itemEstoque.getQndEstoque() +"</td>");
										out.print("<td>" + itemEstoque.getDataUltimaCompra() +"</td>");
										
									}

									out.print("</tr>");
								}

							}
						%>



					</tbody>

				</table>



			</div>



		</div>


	</div>



	</main>


	<%@include file="footer.jsp"%>


	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>