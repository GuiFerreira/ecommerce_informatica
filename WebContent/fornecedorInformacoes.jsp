<%@page import="br.com.ecommerceinformatica.fatec.dominio.Fornecedor"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<div class="container">
		<div class="row">

			<h3>Informacoes do Fornecedor</h3>

			<%
                
                	Fornecedor fornecedor = (Fornecedor) session.getAttribute("fornecedor");
                
                 %>




			<fieldset>
				<legend>Dados do Fornecedor</legend>
				<div class="col s12 m6">
					<h6>
						Nome do Fornecedor:
						<%

								out.print(" " + fornecedor.getNome());

                            %>
					</h6>
				</div>
				<div class="col s12 m6">
					<h6>
						Cnpj do Fornecedor:

						<%
        
                            out.print(" " + fornecedor.getCNPJ());
        
                            %>
					</h6>
				</div>
			</fieldset>

			<fieldset>
				<legend>Contato do Fornecedor</legend>
				<div class="col s12 m6">
					<h6>
						Telefone do Fornecedor:

						<%
                                
                            out.print(" (" + fornecedor.getContato().getDDD() + ") " + fornecedor.getContato().getTelefone());
                
                             %>
					</h6>

				</div>

				<div class="col s12 m6">
					<h6>
						Email do Fornecedor:

						<%
                                    
                        out.print(" " + fornecedor.getContato().getEmail());
                    
                         %>
					</h6>

				</div>

			</fieldset>


			<fieldset>
				<legend>Endereco do Fornecedor</legend>
				<div class="col s12">
					<h6>
						Rua do Fornecedor:

						<%
                                                    
                                            out.print(" " + fornecedor.getEndereco().getRua());
                                    
                                         %>
					</h6>
				</div>

				<div class="col s12 m8">
					<h6>
						Bairro do Fornecedor:

						<%
                                                    
                                        out.print(" " + fornecedor.getEndereco().getBairro());
                                    
                                         %>
					</h6>
				</div>

				<div class="col s12 m4">
					<h6>
						Numero do Fornecedor:

						<%
                                                    
                                        out.print(" " + fornecedor.getEndereco().getNumero());
                                    
                                         %>
					</h6>
				</div>

				<div class="col s12 m8">
					<h6>
						Cidade do Fornecedor:

						<%
                                                    
                                        out.print(" " + fornecedor.getEndereco().getCidade().getNome());
                                    
                                         %>
					</h6>
				</div>

				<div class="col s12 m4">
					<h6>
						CEP do Fornecedor:

						<%
                                                    
                                        out.print(" " + fornecedor.getEndereco().getCep());
                                    
                                         %>
					</h6>
				</div>


				<div class="col s10 m4">
					<h6>
						Estado do Fornecedor:

						<%
                                                    
                                        out.print(" " + fornecedor.getEndereco().getCidade().getEstado().getNome());
                                    
                                         %>
					</h6>
				</div>

				<div class="col s2 m2">
					Sigla:
					<%
                                                    
                                	out.print(" " + fornecedor.getEndereco().getCidade().getEstado().getSigla());            
                                    
                                %>
				</div>

				<div class="col s10 m4">
					<h6>
						Pais do Fornecedor:

						<%
                                                    
                                        out.print(" " + fornecedor.getEndereco().getCidade().getEstado().getPais().getNome());
                                    
                                         %>
					</h6>
				</div>

				<div class="col s2 m2">
					<h6>
						Sigla:

						<%
                                                    
                                        out.print(" " + fornecedor.getEndereco().getCidade().getEstado().getPais().getSigla());
                                    
                                         %>
					</h6>
				</div>

				<div class="col s12 m6">
					<h6>
						Logradouro do Fornecedor:

						<%
                                                    
                                        out.print(" " + fornecedor.getEndereco().getLogradouro().getLogradouro());
                                    
                                         %>
					</h6>
				</div>

				<div class="col s12 m6">
					<h6>
						Tipo de Logradouro do Fornecedor:

						<%
                                            
                                        out.print(" " + fornecedor.getEndereco().getLogradouro().getTpLogradouro());
                                    
                                         %>
					</h6>
				</div>

				<div class="col s12">
					<h6>
						Observacoes do Fornecedor:

						<%
                                                    
                                            out.print(" " + fornecedor.getEndereco().getObsercacoes());
                                    
                                         %>
					</h6>
				</div>

			</fieldset>

			<br/>
			<table>
				<th>
					<form action="AlterarFornecedor" method="GET">
						<input class="waves-light btn grey" type="submit"
							value="ALTERAR FORNECEDOR">
					</form>
				</th>	
				<th>
					<form action="ExcluirFornecedor" method="POST">
						<input class="waves-light btn grey" type="submit" id="OPERACAO"
							name="OPERACAO" value="EXCLUIR FORNECEDOR">
		
					</form>
				</th>
			</table>


		</div>


	</div>

	</main>


	<!-- FOOTER -->
	<%@include file="footer.jsp"%>


	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>