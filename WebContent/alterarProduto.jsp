<%@page import="java.util.*"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css" />
<link type="text/css" rel="stylesheet" href="css/estilo.css" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>
	<%@include file="navBar.jsp"%>


	<!-- MAIN -->
	<main class="corpo">
	<h3 class="center">Alterar Produto</h3>
	<br>

	<%

            Produto produto = (Produto) session.getAttribute("produto");

        %>


	<div class="container center">
		<div class="row">
			<form action="AlterarProduto" method="POST">

				<input name="idProduto" id="idProduto"
					value="<% out.print(produto.getId()); %>" style="display: none">

				<fieldset>
					<legend>Informacoes do Produto</legend>
					<div class="col s12 m6">
						<label for="nomeProduto">Nome do Produto</label> <input
							type="text" id="nomeProduto" name="nomeProduto"
							value="<% out.print(produto.getNome()); %>" required>
					</div>

					<input type="hidden" name="codigoProduto" id="codigoProduto"
						value="<% out.print(produto.getCodProduto()); %>">

					<div class="col s12 m6">
						<label for="descontoProduto">Desconto do Produto</label> <input
							type="number" id="descontoProduto" name="descontoProduto"
							value="0" required>
					</div>

					<div class="col s12">
						<label for="descricaoProduto">Descricao do Produto</label>
						<textarea id="descricaoProduto" name="descricaoProduto" required>
							<% out.print(produto.getDescricao()); %>
						</textarea>
					</div>
					<br />
					<div class="col s12">
						<label for="especificacoesProduto">Especificacoes do
							Produto</label>
						<textarea id="especificacoesProduto" name="especificacoesProduto"
							required>
							<% out.print(produto.getEspecificacoes()); %>
						</textarea>
					</div>
					<br />
					<div class="col s12 m6">
						<label for="precoCompra">Preco de Compra R$</label> <input
							type="number" step="0.01" id="precoCompra" name="precoCompra"
							min="0" value="<% out.print(produto.getPrecoComprado()); %>"
							required>
					</div>
					
					<div class="col s12 m6">
						<br/>
						<label>
							<input type="checkbox" id="ativo" name="ativo" checked="<% out.print((produto.isFlgAtivo()) ? "checked" : "unchecked");%>"/>
							<span>Ativo/Inativo</span>
						</label>
					</div>
				</fieldset>

				<fieldset>
					<legend>Fornecedor do Produto</legend>

					<!-- JSP PARA COMPLETAR AS OPÇÕES -->
					<div class="input-field col s12">
						<select id="fornecedorProduto" name="fornecedorProduto">

							<%
                                    out.print("<option value='" + produto.getFornecedor().getId() + "'>" + produto.getFornecedor().getNome() + 
                                        "</option>");

    								List<EntidadeDominio> listaFornecedores = (List<EntidadeDominio>)request.getAttribute("listaFornecedores");
    								
                            		for (EntidadeDominio e : listaFornecedores){
                            			Fornecedor fornecedorList = (Fornecedor) e;
                            			out.print("<option value='" + fornecedorList.getId() + "'>" + fornecedorList.getNome() + 
                            					"</option>");
                            		}
    							
    							%>

						</select> <label for="fornecedorProduto">Fornecedor do Produto</label>
					</div>


					<div class="col s12">
						<label for="marcaProduto">Marca do Produto</label> <input
							type="text" id="marcaProduto" name="marcaProduto"
							value="<% out.print(produto.getMarca()); %>" required>
					</div>


				</fieldset>

				<fieldset>
					<legend>Categoria do Produto</legend>

					<!-- JSP PARA COMPLETAR AS OPÇÕES -->
					<div class="input-field col s12">
						<select id="categoriaProduto" name="categoriaProduto">

							<%
                                
                                	out.print("<option value='" + produto.getCategoria().getId() + "'>" + produto.getCategoria().getNome() + "</option>");
                                	
                                	List<EntidadeDominio> categorias = (List<EntidadeDominio>)request.getAttribute("listaCategorias");
                            		
                            		Categoria categoria = null;
                            		
                            		for(EntidadeDominio e : categorias){
                            			categoria = (Categoria) e;
                            			out.print("<option value='" + categoria.getId() + "'>" + categoria.getNome() + "</option>");
                            			
                            		}
                                	
                                
                                
                                %>

						</select> <label for="categoriaProduto">Categoria do Produto</label>
					</div>


				</fieldset>
				<br>
				<br>
				<center>
					<input class="waves-light btn grey" type="submit" id="OPERACAO"
						name="OPERACAO" value="ALTERAR PRODUTO">
				</center>
				<br>
				<br>

			</form>

		</div>
	</div>




	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>