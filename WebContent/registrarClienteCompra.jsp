<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<div class="container">
		<div class="row center">
			<h3>Cadastrar!</h3>
			<h6 style="color: red;">Todos os campos s�o obrigat�rio!</h6>
			<form action="CadastarCliente" method="POST" class="center">
				<fieldset>
					<legend>Informa��es B�sicas</legend>
					<div class="col s12">
						<label for="nomeCliente">Nome:</label> <input type="text"
							id="nomeCliente" name="nomeCliente" required>
					</div>
					<div class="col s12 m7">
						<label for="CPF">CPF:</label> <input type="text" id="CPF"
							name="CPF" required>
					</div>
					<div class="col s12 m5">
						<label for="dataNascimento">Data Nascimento:</label> <input
							type="text" id="dataNascimento" name="dataNascimento" required>
					</div>

				</fieldset>

				<fieldset>
					<legend>Informa�oes de Contato</legend>
					<div>
						<div class="col s3 m1">
							<label for="DDD">DDD</label> <input class="input-field"
								type="number" id="DDD" name="DDD" maxlength="99" minlength="10"
								required>
						</div>
						<div class="col s9 m3">
							<label for="telefone">Telefone</label> <input class="input-field"
								type="number" id="telefone" name="telefone" required>
						</div>

					</div>

				</fieldset>

				<!--  ENDEREÇOS  -->

				<fieldset>
					<legend>Endere�os</legend>
					<fieldset>
						<legend>Endere�o de Entrega</legend>
						<div class="col s12 m7">
							<label for="ruaEntrega">Rua:</label> <input class="input-field"
								type="text" id="ruaEntrega" name="ruaEntrega" required>
						</div>
						<div class="col s12 m7">
							<label for="numEntrega">Numero:</label> <input class="input-field"
								type="text" id="numEntrega" name="numEntrega">
						</div>
						<div class="col s12 m5">
							<label for="cepEntrega">CEP:</label> <input class="input-field"
								type="text" id="cepEntrega" name="cepEntrega" required>
						</div>
						<div class="col s12 m8">
							<label for="bairroEntrega">Bairro:</label> <input
								class="input-field" type="text" id="bairroEntrega"
								name="bairroEntrega" required>
						</div>
						<div class="input-field col s12 m4">
							<select id="estadoEntrega" name="estadoEntrega" required>
								<option value="" disabled selected>Escolha uma op��o</option>
								<option value="AC">Acre</option>
								<option value="AL">Alagoas</option>
								<option value="AP">Amap�</option>
								<option value="AM">Amazonas</option>
								<option value="BA">Bahia</option>
								<option value="CE">Cear�</option>
								<option value="DF">Distrito Federal</option>
								<option value="ES">Espírito Santo</option>
								<option value="GO">Goi�s</option>
								<option value="MA">Maranh�o</option>
								<option value="MT">Mato Grosso</option>
								<option value="MS">Mato Grosso do Sul</option>
								<option value="MG">Minas Gerais</option>
								<option value="PA">Par�</option>
								<option value="PB">Paraíba</option>
								<option value="PR">Paran�</option>
								<option value="PE">Pernambuco</option>
								<option value="PI">Piauí</option>
								<option value="RJ">Rio de Janeiro</option>
								<option value="RN">Rio Grande do Norte</option>
								<option value="RS">Rio Grande do Sul</option>
								<option value="RO">Rond�nia</option>
								<option value="RR">Roraima</option>
								<option value="SC">Santa Catarina</option>
								<option value="SP">S�o Paulo</option>
								<option value="SE">Sergipe</option>
								<option value="TO">Tocantins</option>
							</select> <label for="estadoEntrega">Fornecedor do Produto</label>
						</div>

						<div class="col s12 m8">
							<label for="cidadeEntrega">Cidade:</label> <input
								class="input-field" type="text" id="cidadeEntrega"
								name="cidadeEntrega" required>
						</div>
						<div class="col s12 m4">
							<label for="paisEntrega">Pais:</label> <select name="paisEntrega"
								id="paisEntrega" required>
								<option value="Brasil">Brasil</option>
							</select>
						</div>

						<div class="col s12 m6">
							<label for="logradouroEntrega">Logradouro:</label> <input
								class="input-field" type="text" id="logradouroEntrega"
								name="logradouroEntrega" required>
						</div>

						<div class="col s12 m6">
							<label for="tipoLogradouroEntrega">Tipo Logradouro:</label> <select
								name="tipoLogradouroEntrega" id="tipoLogradouroEntrega" required>
								<option value="Rua">Rua</option>
								<option value="Avenida">Avenida</option>
								<option value="Estrada">Estrada</option>
							</select>
						</div>

						<div class="col s12">
							<label for="apelidoEntrega">Apelido para esse Endere�o</label> <input
								class="input-field" type="text" id="apelidoEntrega"
								name="apelidoEntrega" required>
						</div>

						<div class="col s12">
							<label for="observacoesEntrega">Observa��es</label>
							<textarea id="observacoesEntrega" name="observacoesEntrega"></textarea>
						</div>

					</fieldset>

					<fieldset>
						<legend>Endere�o de Cobran�a</legend>
						<br />
						<div class="col s12">
							<label> <input type="checkbox" name="usarEnderecoEntrega"
								id="usarEnderecoEntrega" checked="checked" /> <span>Usar
									o Endere�o de Entrega como Endere�o de Cobran�a?</span>
							</label>
						</div>
						<br /> <br />
						<div class="col s12 m7">
							<label for="ruaCobranca">Rua:</label> <input class="input-field"
								type="text" id="ruaCobranca" name="ruaCobranca">
						</div>
						<div class="col s12 m7">
							<label for="numCobranca">Numero:</label> <input class="input-field"
								type="text" id="numCobranca" name="numCobranca">
						</div>
						<div class="col s12 m3">
							<label for="cepCobranca">CEP:</label> <input class="input-field"
								type="text" id="cepCobranca" name="cepCobranca">
						</div>
						<div class="col s12 m8">
							<label for="bairroCobranca">Bairro:</label> <input
								class="input-field" type="text" id="bairroCobranca"
								name="bairroCobranca">
						</div>
						<div class="input-field col s12 m4">
							<select id="estadoCobranca" name="estadoCobranca">
								<option value="" disabled selected>Escolha uma op��o</option>
								<option value="AC">Acre</option>
								<option value="AL">Alagoas</option>
								<option value="AP">Amap�</option>
								<option value="AM">Amazonas</option>
								<option value="BA">Bahia</option>
								<option value="CE">Cear�</option>
								<option value="DF">Distrito Federal</option>
								<option value="ES">Espírito Santo</option>
								<option value="GO">Goi�s</option>
								<option value="MA">Maranh�o</option>
								<option value="MT">Mato Grosso</option>
								<option value="MS">Mato Grosso do Sul</option>
								<option value="MG">Minas Gerais</option>
								<option value="PA">Par�</option>
								<option value="PB">Paraíba</option>
								<option value="PR">Paran�</option>
								<option value="PE">Pernambuco</option>
								<option value="PI">Piauí</option>
								<option value="RJ">Rio de Janeiro</option>
								<option value="RN">Rio Grande do Norte</option>
								<option value="RS">Rio Grande do Sul</option>
								<option value="RO">Rond�nia</option>
								<option value="RR">Roraima</option>
								<option value="SC">Santa Catarina</option>
								<option value="SP">S�o Paulo</option>
								<option value="SE">Sergipe</option>
								<option value="TO">Tocantins</option>
							</select> <label for="estadoCobranca">Fornecedor do Produto</label>
						</div>

						<div class="col s12 m8">
							<label for="cidadeCobranca">Cidade:</label> <input
								class="input-field" type="text" id="cidadeCobranca"
								name="cidadeCobranca">
						</div>
						<div class="col s12 m4">
							<label for="paisCobranca">Pais:</label> <select
								name="paisCobranca" id="paisCobranca">
								<option value="Brasil">Brasil</option>
							</select>
						</div>

						<div class="col s12 m6">
							<label for="logradouroEntrega">Pais:</label> <input
								class="input-field" type="text" id="logradouroEntrega"
								name="logradouroEntrega">
						</div>

						<div class="col s12 m6">
							<label for="tipoLogradouroEntrega">Pais:</label> <select
								name="tipoLogradouroEntrega" id="tipoLogradouroEntrega">
								<option value="Rua">Rua</option>
								<option value="Avenida">Avenida</option>
								<option value="Estrada">Estrada</option>
							</select>
						</div>

						<div class="col s12">
							<label for="apelidoCobranca">Apelido para esse Endere�o</label> <input
								class="input-field" type="text" id="apelidoCobranca"
								name="apelidoCobranca">
						</div>

						<div class="col s12">
							<label for="observacoesCobranca">Observa��es</label>
							<textarea id="observacoesCobranca" name="observacoesCobranca"></textarea>
						</div>

					</fieldset>


				</fieldset>

				<fieldset>
					<legend>Cart�o de Cr�dito</legend>
					<div class="col s12 m6">
						<label for="nomeBanco">Nome do Banco:</label> <input type="text" id="nomeBanco"
							name="nomeBanco" required>
					</div>
					
					<div class="col s12 m6">
						<label for="numAgencia">N�mero da Ag�ncia:</label> <input type="text" id="numAgencia"
							name="numAgencia" required>
					</div>

					<div class="col s12">
						<label for="nomeImpresso">Nome Impresso:</label> <input type="text" id="nomeImpresso"
							name="nomeImpresso" required>
					</div>
					
					<div class="col s12 m6">
						<label for="numCartao">N�mero do Cart�o:</label> <input type="text" id="numCartao"
							name="numCartao" required>
					</div>
					
					<div class="col s12 m6">
						<label for="codSeguranca">C�digo de Seguran�a:</label> <input type="text" id="codSeguranca"
							name="codSeguranca" required>
					</div>
					
					
				</fieldset>
				
				
				<fieldset>
					<legend>Usu�rio</legend>
					<div class="col s12 m8">
						<label for="email">Email:</label> <input type="text" id=email
							name="email" required>
					</div>

					<div class="col s12 m6">
						<label for="senha">Senha:</label> <input type="password"
							pattern="((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,15})"
							title="A senha deve conter no minimo 8 caracteres,
                                     ter letras maisculas e minusculas, n�meros al�m de conter caracteres especiais!"
							id="senha" name="senha" required>
					</div>
					<div class="col s12 m6">
						<label for="confirmacaoSenha">Confirma��o de Senha:</label> <input
							type="password" id="confirmacaoSenha" name="confirmacaoSenha" onblur="validarSenha()"
							name="logconfirmacaoSenhain" required>
					</div>


				</fieldset>


				<br>
				<br> <input class="waves-light btn grey" type="submit"
					id="OPERACAO" name="OPERACAO" value="CADASTRAR CLIENTE COMPRA">

				<br>
				<br>
			</form>
		</div>


	</div>

	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>