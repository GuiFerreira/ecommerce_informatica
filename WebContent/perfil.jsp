<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<%@page import="com.google.gson.*" %>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<script  src="https://code.jquery.com/jquery-3.4.1.js" 
			integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous">
		</script>
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<!-- HEADER -->
	<%@include file="navBar.jsp"%>
	<%
		Gson gson = new GsonBuilder().create();
	%>

	<!-- BODY -->
	<main class="corpo"> 
		<div class="center">
			<div class="row">
				<h3>Seja bem-vindo, <% out.print(clienteLogado.getNome()); %></h3>
				<div class="col s1 m1 l1">
				
				</div>
				<div class="col s10 m10 l10">
					<ul class="collapsible" style="text-align: center;">
						<!--  minhas informa��es -->
					    <li>
					      <div class="collapsible-header"><i class="material-icons">person</i><b>Informa��es pessoais</b></div>
					      <div class="collapsible-body">
					      	<!--  INFORMA��ES DO CLIENTE  -->
					      		<h6><b>Infoma��es pessoais:</b></h6> <br/>
					      		<div class="center-align">
					      			<div class="col m6">
							      		<label>Nome completo:</label>
							      		<span><% out.print(clienteLogado.getNome()); %></span> <br/>
					      			</div>
					      			<div class="col m6">
							      		<label>CPF:</label>
							      		<span><% out.print(clienteLogado.getCpf()); %></span>
					      			</div>
					      			<div class="col m6">
							      		<label>Data nascimento:</label>
							      		<span><% out.print(clienteLogado.getDataNascimento()); %></span>
					      			</div>
					      			<div class="col m6">
							      		<label>Email:</label>
							      		<span><% out.print(clienteLogado.getUsuario().getEmail()); %></span>
					      			</div>
					      			
						      	</div>
					      		
								<a class="waves-effect waves-light btn grey modal-trigger" href="#modalAlterarInfo">Alterar</a>
								<a class="waves-effect waves-light btn grey modal-trigger" href="#modalAlterarSenha">Alterar Senha</a>
								
					      </div>
					    </li>
					    <!--  meus contatos  -->
					    <li>
					    	<div class="collapsible-header"><i class="material-icons">phone_iphone</i><b>Meu contato</b></div>
					    	<div class="collapsible-body">
					    	<!--  MEU CONTATO -->
					      		<h6><b>Meu Contato:</b></h6> <br/>
					      		<div class="col s3 m1">
					      			<label>DDD:</label>
					      			<span><% out.print(clienteLogado.getContato().getDDD()); %></span>
					      		</div>
					      		<div class="col s9 m3">
						      		<label for="ddd">Telefone:</label>
						      		<span><% out.print(clienteLogado.getContato().getTelefone()); %></span>
				      			</div>
					      		
					      		<a class="waves-effect waves-light btn grey modal-trigger" href="#modalAlterarContato">
					      			Alterar</a>
								
							
					      	</div>
					    </li>
					    <!--  meus endere�os  -->
					    <li>
							<div class="collapsible-header"><i class="material-icons">place</i><b>Meus endere�os</b></div>
							<div class="collapsible-body">
							<!--  MEUS ENDERE�OS-->
					      		<h6><b>Meus endere�os:</b></h6> <br/>
								<%
									HashMap<Integer, EnderecoCliente> mapEnderecos = Util.getMapEnderecos(clienteLogado.getListaEnderecoCobranca(), clienteLogado.getListaEnderecoEntrega());
									HashMap<Integer, String> mapTipoEndereco = new HashMap<Integer, String>();
									mapTipoEndereco.put(0, "Entrega");
									mapTipoEndereco.put(1, "Cobran�a");
									mapTipoEndereco.put(2, "Entrega/Cobran�a");
									
									if (mapEnderecos.size() != 0)
									for (Integer idEndereco : mapEnderecos.keySet()){
										EnderecoCliente endCliente = mapEnderecos.get(idEndereco);
										
										int tipoEndereco = endCliente.getEndCobranca();
										
										%>
											<fieldset>
												<legend><% out.print(endCliente.getApelidoEndereco()); %></legend>
												<div class="col s12">
													<label>Rua:</label>
													<span> <% out.print(endCliente.getRua());%> </span>
												</div>

												<div class="col s12">
													<label>Tipo endere�o:</label>
													<span><% out.print(mapTipoEndereco.get(tipoEndereco)); %></span>
												</div>
											
												<div class="col s12 m8">
													<label>Bairro:</label>
													<span> <% out.print(endCliente.getBairro()); %> </span>
												</div>
											
												<div class="col s12 m4">
													<label>Numero:</label>
													<span><% out.print(endCliente.getNumero()); %></span>
												</div>
											
												<div class="col s12 m8">
													<label>Cidade:</label>
													<span><% out.print(endCliente.getCidade().getNome()); %></span>
												</div>
											
												<div class="col s12 m4">
													<label>CEP:</label>
													<span><% out.print(endCliente.getCep()); %></span>
												</div>
											
												<div class="col s10 m4">
													<label>Estado:</label>
													<span><% out.print(endCliente.getCidade().getEstado().getNome()); %></span>
												</div>
											
												<div class="col s2 m2">
													<label>Sigla:</label>
													<span><% out.print(endCliente.getCidade().getEstado().getSigla()); %></span>
												</div>
											
												<div class="col s10 m4">
													<label>Pais:</label>
													<span><% out.print(endCliente.getCidade().getEstado().getPais().getNome()); %> </span>
												</div>
											
												<div class="col s2 m2">
													<label>Sigla:</label>
													<span><% out.print(endCliente.getCidade().getEstado().getPais().getSigla()); %> </span>
												</div>
											
												<div class="col s12 m6">
													<label>Logradouro:</label>
													<span><% out.print(endCliente.getLogradouro().getLogradouro()); %> </span>
												</div>
											
												<div class="col s12 m6">
													<label>Tipo de Logradouro:</label>
													<span><% out.print(endCliente.getLogradouro().getTpLogradouro()); %> </span>
												</div>
											
												<div class="col s12">
													<label>Observacoes:</label>
													<span> <% out.print(endCliente.getObsercacoes()); %></span>
												</div>
												
												<a onclick="setEndereco(<% out.print(gson.toJson(endCliente, EnderecoCliente.class).replace("\"", "'")); %>)" class="waves-effect waves-light btn grey modal-trigger" href="#modalAlterarEndereco">Alterar</a>
											
											</fieldset>
										
										
										<%
										
									}
									
									if (mapEnderecos.size() == 0)
										out.print("<h6><b>N�o existe nenhum endere�o cadastrado...</b></h6>");
									
									out.print("<br/><a class='waves-effect waves-light btn grey modal-trigger' href='cadastrarEndereco.jsp?tipoEndereco=2'>Criar Endere�o</a>");
									
								%>								
							
							</div>
					    </li>
					    <!--  meus cart�es de cr�dito  -->
					    <li>
					    	<div class="collapsible-header"><i class="material-icons">payment</i><b>Meus cart�es</b></div>
					      	<div class="collapsible-body">
					      		<!--  MEU CART�ES -->
					      		<h6><b>Meus Cart�es:</b></h6> <br/>
					      		<%
									
					      			if (clienteLogado.getListaCartoes() != null)
					      			for (Cartao cartao : clienteLogado.getListaCartoes()){
										
										%>
											<fieldset>
												<legend><% out.print(cartao.getNomeImpresso()); %></legend>
												<div class="col s6">
													<label>Nome Impresso no cart�o: </label>
													<span> <% out.print(cartao.getNomeImpresso());%> </span>
												</div>
												<div class="col s6">
													<label>N�mero do cart�o</label>
													<span> <% out.print(cartao.getNumero());%> </span>
												</div>
												<div class="col s6">
													<label>Nome do Banco:</label>
													<span> <% out.print(cartao.getBanco().getNome());%> </span>
												</div>
												<div class="col s6">
													<label>N�mero da Ag�ncia:</label>
													<span> <% out.print(cartao.getBanco().getNumAgencia());%> </span>
												</div>
												
												<a onclick="setCartao(<% out.print(gson.toJson(cartao, Cartao.class).replace("\"", "'")); %>)" class="waves-effect waves-light btn grey modal-trigger" href="#modalAlterarCartao">Alterar</a>
											
											</fieldset>
										
										
										<%
										
									}
									
									if (clienteLogado.getListaCartoes() != null)
										out.print("<h6><b>N�o existe nenhum cart�o cadastrado...</b></h6>");
									
									out.print("<br/><a class='waves-effect waves-light btn grey modal-trigger' href='cadastrarCartao.jsp'>Cadastrar Cart�o</a>");
									
								%>	
					      	</div>
					    </li>
					    
					    <!--  meus cupons  -->
					    <li>
							<div class="collapsible-header"><i class="material-icons">attach_money</i><b>Meus Cupons</b></div>
							<div class="collapsible-body">
								<!--  MEUS CUPONS -->
					      		<h6><b>Meus Cupons:</b></h6> <br/>
								<div class="center-align">
									<div class="col s12 m12 l12">
										<div class="col s4 m4 l4">
											<p style="color: blue">Cupom <b>Usado</b></p>									
										</div>
										<div class="col s4 m4 l4">
											<p style="color: red">Cupom <b>Venceu e N�o foi Usado</b></p>								
										</div>
										<div class="col s4 m4 l4">
											<p style="color: green">Cupom <b>PODE</b> ser Usado</p>								
										</div>
									
									</div>
									
									<table>
										<thead>
											<tr>
												<td>C�digo do Cupom</td>	
												<td>Nome</td>
												<td>Tipo Cupom</td>
												<td>Valor do Desconto</td>
												<td>Vencimento</td>
												<td>Usado</td>
											</tr>
											
										</thead>
										
										<tbody class="center-align">
										<%
											
											if (clienteLogado.getListCupons() != null && !clienteLogado.getListCupons().isEmpty())
												for (Cupom cupom : clienteLogado.getListCupons()){
													String corVencimento = "", corUsado = "";
													if (cupom.isUsado()){
														corVencimento = corUsado = " style='color: blue;'";
													}
													else {
														if (Util.venceu(cupom.getVencimento())){
															corVencimento = corUsado = " style='color: red;'";
															
														}
														
														else {
															corVencimento = corUsado = " style='color: green;'";
														}
																
																
														
													}
														
													out.print("<tr>");
													
													out.print("<td>" + cupom.getCodigo() + "</td>");
													out.print("<td>" + cupom.getNome() + "</td>");
													out.print("<td>" + cupom.getNomeTipoCupom() + "</td>");
													out.print("<td>" + cupom.getValor() + "</td>");
													out.print("<td" + corVencimento + ">" + cupom.getVencimento() + "</td>");
													out.print("<td" + corUsado + ">" + (cupom.isUsado() ? "Usado" : "N�o Usou") + "</td>");
													
													out.print("</tr>");
													
												}
											
										
										%>
										</tbody>
										
									</table>
											
								</div>
							</div>
					    </li>
					    
					    
					    <!--  meus pedidos  -->
					    <li>
							<div class="collapsible-header"><i class="material-icons">shopping_cart</i><b>Meus pedidos</b></div>
							<div class="collapsible-body">
								<!--  MEUS PEDIDOS -->
					      		<h6><b>Meus Pedidos:</b></h6> <br/>
								<div class="center-align">
									<table>
										<thead>
											<tr>
												<td>C�digo do Pedido</td>	
												<td>Data da Compra</td>
												<td>Enviar para</td>
												<td>Preco Total</td>
												<td>Status</td>
												<td>Detalhes do Pedido</td>
												<td>Opera��es</td>
											</tr>
											
										</thead>
										
										<tbody class="center-align">
										<%
											
											if (clienteLogado.getListPedidos() != null && !clienteLogado.getListPedidos().isEmpty()){
												session.setAttribute("listPedidos", clienteLogado.getListPedidos());										
												for (Pedido pedido : clienteLogado.getListPedidos()){
													
													out.print("<tr>");
													
													out.print("<td>" + pedido.getCodPedido() + "</td>");
													out.print("<td>" + pedido.getDataCompra() + "</td>");
													out.print("<td>" + pedido.getEndEntrega().getApelidoEndereco() + "</td>");
													out.print("<td>" + pedido.getPrecoTotal() + "</td>");
													out.print("<td>" + pedido.getStatusCompra().toString() + "</td>");
													out.print("<input type='hidden' name='idPedido' value='" + pedido.getId() + "'/>");
													out.print("<td><a href='Pedido?codigo=" + pedido.getCodPedido() + "'>Detalhes</a></td>");
													
													if (pedido.getStatusCompra() == StatusCompra.ENTREGUE || pedido.getStatusCompra().getOrdemStatus() == 8)
														out.print("<td>" + "<a href='SolicitarTroca?codigo=" + pedido.getCodPedido() + "'>Solicitar Troca</a>" + "</td>");
													else if (pedido.getStatusCompra().getOrdemStatus() <= 3)
														out.print("<td>" + "<a href='SolicitarCancelamento?codigo=" + pedido.getCodPedido() + "'>Cancelar</a>" + "</td>");
													else
														out.print("<td>Nenhuma a��o</td>");
													
													out.print("</tr>");
													
												}
											}
											
										
										%>
										</tbody>
										
									</table>
											
								</div>
							</div>
					    </li>
				  </ul>
				
				</div>
				<div class="col s1 m1 l1">
				
				</div>
			</div>
		</div>
	</main>
	
	<!-- Modal Structure -->
	<!--  ALTERAR INFORMA��ES DO CLIENTE  -->
	<div id="modalAlterarInfo" class="modal modal-fixed-footer">
		<div class="center">
		<div class="row">
			<div class="col s12">
			<br/>
			<h4>Alterar Informa��es Pessoais</h4>
				<form action="AlterarCliente" method="POST">
				    <div class="modal-content">
						<div class="col m6">
				      		<label for="nome">Nome completo:</label>
				      		<input class="input-field" type="text" value="<% out.print(clienteLogado.getNome()); %>" id="nome" name="nome" required/>
		      			</div>
		      			<div class="col m6">
				      		<label for="cpf">CPF:</label>
				      		<input class="input-field" type="text" id="cpf" name="cpf" value="<% out.print(clienteLogado.getCpf()); %>" required/>
		      			</div>
		      			<div class="col m6">
				      		<label for="dataNascimento">Data nascimento:</label>
				      		<input class="input-field" type="text" id="dataNascimento" name="dataNascimento" value="<% out.print(clienteLogado.getDataNascimento()); %>" required/>
		      			</div>
		      			<div class="col m6">
				      		<label for="email">Email:</label>
				      		<input class="input-field" type="text" id="email" name="email" value="<% out.print(clienteLogado.getUsuario().getEmail()); %>" required/>
		      			</div>
				    </div>
				    <div class="center-align modal-footer">
						<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="ALTERAR CLIENTE">
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>
	
	
	<!-- Modal Structure --> 
	<!--  ALTERAR SOMENTE SENHA-->
	<div id="modalAlterarSenha" class="modal modal-fixed-footer">
		<div class="center">
		<div class="row">
			<div class="col s12">
			<br/>
			<h4>Alterar Senha</h4>
				<form action="AlterarSenha" method="POST">
				    <div class="modal-content">
						<div class="col m6">
				      		<label for="senhaAntiga">Senha Antiga:</label>
				      		<input class="input-field" type="text" id="senhaAntiga" name="senhaAntiga" required/>
		      			</div>
						<div class="col m6">
				      		<label for="novaSenha">Nova Senha:</label>
				      		<input class="input-field" type="text" id="novaSenha" name="novaSenha" required/>
		      			</div>
						<div class="col m6">
				      		<label for="confirmacaoSenha">Confirma��o de Senha:</label>
				      		<input class="input-field" type="text" id="confirmacaoSenha" name="confirmacaoSenha" required/>
		      			</div>
				    </div>
				    <div class="modal-footer">
						<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="ALTERAR SENHA">
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>
	
	<!-- Modal Structure --> 
	<!--  ALTERAR INFORMA��ES DO CONTATO -->
	<div id="modalAlterarContato" class="modal">
		<div class="center">
		<div class="row">
			<div class="col s12">
			<br/>
			<h4>Alterar Contato</h4>
				<form action="AlterarContato" method="POST">
				    <div class="modal-content">
						<div class="col s3 m1">
							<input type="hidden" id="TESTE" name="" value="TESTE"/>
				      		<label for="ddd">DDD:</label>
				      		<input class="input-field" type="number" value="<% out.print(clienteLogado.getContato().getDDD()); %>" id="ddd" name="ddd" maxlength="99" minlength="10" required/>
				      		
		      			</div>
		      			<div class="col s9 m3">
				      		<label for="ddd">Telefone:</label>
				      		<input class="input-field" value="<% out.print(clienteLogado.getContato().getTelefone()); %>" type="number" id="ddd" name="ddd" maxlength="99" minlength="10" required/>
		      			</div>
				    </div>
				    <div class="modal-footer">
						<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="ALTERAR CONTATO">
					</div>
				</form>
			</div>
		</div>
		</div>
	</div>
	
	<!-- Modal Structure --> 
	<!--  ALTERAR DADOS DO CART�O -->
	<div id="modalAlterarCartao" class="modal">
		<div class="row">
		<div class="center">
		<br/>
			<h4>Alterar Cart�o</h4>
			<form action="AlterarCartao" method="POST">
				<div class="col s12">
				    <div class="modal-content">
						<div class="col s6">
							<label for="nomeImpressoCartao">Nome Impresso no cart�o: </label>
							<input type="text" id="nomeImpressoCartao" name="nomeImpressoCartao" value="" required/>
						</div>
						<div class="col s6">
							<label>N�mero do cart�o</label>
							<input type="text" id="numeroCartao" name="numeroCartao" value="" required/>
						</div>
						<div class="col s6">
							<label>Nome do Banco:</label>
							<input type="text" id="nomeBancoCartao" name="nomeBancoCartao" value="" required/>
						</div>
						<div class="col s6">
							<label>N�mero da Ag�ncia:</label>
							<input type="text" id="numeroAgenciaCartao" name="numeroAgenciaCartao" value="" required/>
						</div>
						<div class="col s6">
							<label>C�digo de Seguran�a:</label>
							<input type="text" id="codigoSegurancaCartao" name="codigoSegurancaCartao" value="" required/>
						</div>
						<input type="hidden" name="idCartao" id="idCartao" value=""/>
				    </div>
				    <br/><br/>
				    <div class="modal-footer">
						<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="ALTERAR CART�O">
					</div>
				</div>
			</form>
		</div>
		</div>
	</div>
	
	<!-- Modal Structure --> 
	<!--  ALTERAR DADOS DO ENDERECO -->
	<div id="modalAlterarEndereco" class="modal">
		<div class="row">
		<div class="center">
		<br/>
			<h4>Alterar Endere�o</h4>
			<form action="AlterarEndereco" method="POST">
				<div class="col s12">
				    <div class="modal-content">
						<div class="col m6">
							<label>Rua:</label>
							<input type="text" id="ruaEndereco" name="ruaEndereco" value="" required/>
						</div>

						<div class="col m6">
							<select id="tipoEndereco" name="tipoEndereco">
								<option value="0">Entrega</option>
								<option value="1">Cobran�a</option>
								<option value="2">Entrega/Cobran�a</option>
							</select>
						</div>
					
						<div class="col m6">
							<label>Bairro:</label>
							<input type="text" id="bairroEndereco" name="bairroEndereco" value="" required/>
						</div>
					
						<div class="col s12 m4">
							<label>Numero:</label>
							<input type="text" id="numeroEndereco" name="numeroEndereco" value="" required/>
						</div>
					
						<div class="col s12 m8">
							<label>Cidade:</label>
							<input type="text" id="cidadeEndereco" name="cidadeEndereco" value="" required/>
						</div>
					
						<div class="col s12 m4">
							<label>CEP:</label>
							<input type="text" id="cepEndereco" name="cepEndereco" value="" required/>
						</div>
					
						<div class="col s10 m4">
							<label>Estado:</label>
							<input type="text" id="estadoEndereco" name="estadoEndereco" value="" required/>
						</div>
					
						<div class="col s2 m2">
							<label>Sigla:</label>
							<input type="text" id="siglaEstadoEndereco" name="siglaEstadoEndereco" value="" required/>
						</div>
					
						<div class="col s10 m4">
							<label>Pais:</label>
							<input type="text" id="paisEndereco" name="paisEndereco" value="" required/>
						</div>
					
						<div class="col s2 m2">
							<label>Sigla:</label>
							<input type="text" id="siglaPaisEndereco" name="siglaPaisEndereco" value="" required/>
						</div>
					
						<div class="col s12 m6">
							<label>Logradouro:</label>
							<input type="text" id="logradouroEndereco" name="logradouroEndereco" value="" required/>
						</div>
					
						<div class="col s12 m6">
							<label>Tipo de Logradouro:</label>
							<input type="text" id="tipoLogradouroEndereco" name="tipoLogradouroEndereco" value="" required/>
						</div>
						
						<div class="col s12">
							<label>Apelido:</label>
							<input type="text" id="apelidoEndereco" name="apelidoEndereco" value="" required/>
						</div>
						
						<div class="col s12">
							<label>Observa��es:</label>
							<input type="text" id="observacoesEndereco" name="observacoesEndereco" value="" />
						</div>
						
						<input type="hidden" name="idEndereco" id="idEndereco" value=""/>
					
				    </div>
				    <br/><br/>
				    <div class="modal-footer">
						<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="ALTERAR ENDERECO">
					</div>
				</div>
			</form>
		</div>
		</div>
	</div>
	

	<!-- FOOTER -->
	<%@include file="footer.jsp"%>


	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
	<script type="text/javascript">
		
		function setEndereco(jsonEndereco){
			console.log(jsonEndereco);
			document.getElementById('ruaEndereco').value = jsonEndereco.rua;
			document.getElementById('bairroEndereco').value = jsonEndereco.bairro;
			document.getElementById('numeroEndereco').value = jsonEndereco.numero;
			document.getElementById('cepEndereco').value = jsonEndereco.cep;
			document.getElementById('estadoEndereco').value = jsonEndereco.cidade.estado.nome;
			document.getElementById('siglaEstadoEndereco').value = jsonEndereco.cidade.estado.sigla;
			document.getElementById('paisEndereco').value = jsonEndereco.cidade.estado.pais.nome;
			document.getElementById('cidadeEndereco').value = jsonEndereco.cidade.nome;
			document.getElementById('siglaPaisEndereco').value = jsonEndereco.cidade.estado.pais.sigla;
			document.getElementById('logradouroEndereco').value = jsonEndereco.logradouro.logradouro;
			document.getElementById('tipoLogradouroEndereco').value = jsonEndereco.logradouro.tpLogradouro;
			document.getElementById('apelidoEndereco').value = jsonEndereco.apelidoEndereco;
			document.getElementById('observacoesEndereco').value = jsonEndereco.obsercacoes;
			document.getElementById('idEndereco').value = jsonEndereco.id;
			
		}
		
		function setCartao(jsonCartao){
			console.log(jsonCartao);
			console.log(jsonCartao);
			document.getElementById("nomeImpressoCartao").value = jsonCartao.nomeImpresso;
			document.getElementById("numeroCartao").value = jsonCartao.numero;
			document.getElementById("nomeBancoCartao").value = jsonCartao.banco.nome;
			document.getElementById("numeroAgenciaCartao").value = jsonCartao.banco.numAgencia;
			document.getElementById("idCartao").value = jsonCartao.id;
			
		}
		
	
		document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.collapsible');
		    var instances = M.Collapsible.init(elems, null);
		  });
		
		document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.modal');
		    var instances = M.Modal.init(elems, null);
		  });
	</script>
</body>

</html>