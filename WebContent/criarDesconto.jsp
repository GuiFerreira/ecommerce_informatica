<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<div class="container">
		<div class="center row">
			<h3>Criar desconto para uma categoria de produtos</h3>

			<br>
			<br>
			<form action="Desconto" method="POST">
				<div class="input-field col s12 m6">
					<select id="idCategoria" name="idCategoria" required>
						<option value="" disabled selected>Escolha uma categoria</option>
						<%
							
								List<EntidadeDominio> listCategorias = (List<EntidadeDominio>) session.getAttribute("listCategorias");
	    			
	    						for(EntidadeDominio e : listCategorias){
	    							Categoria categoria = (Categoria) e;
	    							out.print("<option value='" + categoria.getId() + "'>");
	    							out.print(categoria.getNome());
	    							out.print("</option>");
	    							
	    						}
		    						
							
							%>

					</select>
				</div>

				<div class="col s12 m3">
					<label for="qntComprada">Descondo em porcentagem (%)</label> 
					<input type="number" id="qntDescontoPorcentagem" name="qntDescontoPorcentagem" placeholder="Desconto em %" required>
				</div>

				<br>

				<div class="col s12 m3">
					<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO"
						value="BUSCAR PRODUTOS">
				</div>

			</form>

		</div>
		
		<div class="col m12">
			<div class="center-align col m12">
				<h4>Lista dos produtos com a categoria escolhida:</h4>
			</div>
			
			<table>
				<thead>
					<tr>
						<td>C�digo do Produto</td>
						<td>Nome do Produto</td>
						<td>Pre�o do Produto</td>
						<td>Entra no Desconto?</td>
						<td>Desconto</td>
						
						
					</tr>
				</thead>
			
			</table>
		
		</div>


	</div>

	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>