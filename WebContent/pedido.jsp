<!DOCTYPE html>
<html>
<head>
</head>
<body>

	<!-- HEADER -->
	<%@include file="navBar.jsp"%>
	<%
		Pedido pedido = (Pedido) session.getAttribute("pedido");
		double precoDosProdutos = 0;
	
	%>
	
	<!-- BODY -->
	<main class="corpo"> 
		<div class="center">
			<div class="row">
			<div class="container">
				<h3>Informa��es do Pedido</h3>
				<% out.print("<h5>C�digo do pedido: " + pedido.getCodPedido() + "</h5>"); %>

				<fieldset>
					<legend><b>Itens do Pedido</b></legend>
					<table>
						<thead>
							<tr>
								<td>Imagem do Produto</td>
								<td>Nome do Produto</td>
								<td>Quantidade</td>
								<td>Pre�o Unit�rio</td>
								<td>Pre�o Total</td>
							</tr>
						</thead>
						<tbody>
							<%
		                     
								for (ItensPedido itemPedido : pedido.getListProdutosPedido()){
									Produto produto = itemPedido.getProduto();
									precoDosProdutos += itemPedido.getPrecoTotal();
									out.print("<tr>");
										out.print("<td><img class='imgCarrinho' src='" + produto.getUriImagem() + "'></td>");
										out.print("<td><a href='produto?id="+ produto.getId() + "'>" + produto.getNome() + "</a></td>");
										out.print("<td>" + itemPedido.getQuantidade() + "</td>");
										out.print("<td>R$ " + String.format("%.2f", itemPedido.getPrecoItem()) + "</td>");
										out.print("<td>R$ " + String.format("%.2f", itemPedido.getPrecoTotal()) + "</td>");
									out.print("</tr>");
								}
	                               
	                                   
	                               
	                        %>
						</tbody>
					</table>
					
					
					<!-- ITENS DE TROCA --> 
				<fieldset>
					<legend><b>Itens TROCADOS</b></legend>
					<table>
						<thead>
							<tr>
								<td>Imagem do Produto</td>
								<td>Nome do Produto</td>
								<td>Quantidade Cancelada</td>
								<td>Status</td>
							</tr>
						</thead>
						
							<tbody>
								<%
			                     
									if (pedido.getListTrocaPedido() != null && !pedido.getListTrocaPedido().isEmpty())
										for (TrocaPedido troca : pedido.getListTrocaPedido()){
											for (ItensTroca itemTroca : troca.getListItensTroca()){
												Produto produto = itemTroca.getProduto();
												out.print("<tr>");
													out.print("<td><img class='imgCarrinho' src='" + produto.getUriImagem() + "'></td>");
													out.print("<td><a href='produto?id="+ produto.getId() + "'>" + produto.getNome() + "</a></td>");
													out.print("<td>" + itemTroca.getQntTrocada() + "</td>");
													out.print("<td>" + itemTroca.getStatus().toString() + "</td>");
												out.print("</tr>");
												
												
											}
										}
		                               
		                                   
		                               
		                        %>
							</tbody>
						
					</table>
					
				</fieldset>
					
					
					
					<fieldset>
						<legend><b>Cupom: </b></legend>
						<%
							if (pedido.getCupom() == null || pedido.getCupom().getId() == 0){
								out.print("<h6>N�o foi usado Cupom nesse Pedido</h6>");
							}
							else {
								
						%>
							<table>
								<thead>
									<tr>
										<td>C�digo do Cupom</td>	
										<td>Nome</td>
										<td>Tipo Cupom</td>
										<td>Valor do Desconto</td>
										<td>Vencimento</td>
										<td>Usado</td>
									</tr>
									
								</thead>
								
								<tbody class="center-align">
								<%
									
									Cupom cupom = pedido.getCupom();
										String corVencimento = "", corUsado = "";
										if (cupom.isUsado()){
											corVencimento = corUsado = " style='color: blue;'";
										}
										else {
											if (Util.venceu(cupom.getVencimento())){
												corVencimento = corUsado = " style='color: red;'";
												
											}
											
											else {
												corVencimento = corUsado = " style='color: green;'";
											}
													
													
											
										}
											
										out.print("<tr>");
										
										out.print("<td>" + cupom.getCodigo() + "</td>");
										out.print("<td>" + cupom.getNome() + "</td>");
										out.print("<td>" + cupom.getNomeTipoCupom() + "</td>");
										out.print("<td>" + cupom.getValor() + "</td>");
										out.print("<td" + corVencimento + ">" + cupom.getVencimento() + "</td>");
										out.print("<td" + corUsado + ">" + (cupom.isUsado() ? "Usado" : "N�o Usou") + "</td>");
										
										out.print("</tr>");
										
									
									
								
								%>
								</tbody>
								
							</table>
						
						<%
							}
						%>
					
					</fieldset>
					
					<fieldset>
						<legend><b>Pre�os:</b></legend>
						<h6>Pre�o Produtos = R$ <% out.print(String.format("%.2f", precoDosProdutos)); %></h6>
						<h6>Pre�o Frete = R$ <% out.print(String.format("%.2f", pedido.getValorFrete())); %></h6>
						<h6>Pre�o Cupom = R$ <% out.print(String.format("%.2f", pedido.getCupom().getValor())); %></h6>
						<h6>Pre�o total = R$ <% out.print(String.format("%.2f", pedido.getPrecoTotal())); %></h6>
					
					</fieldset>
					
	
				</fieldset>
				
				
				
				
				<%
				
					if (pedido.getCliente().getId() == clienteLogado.getId()) {
				
				%>
				
						<fieldset>
							<legend><b>Cart�es:</b></legend>
							<%
								
								for (CartaoPedido cartao : pedido.getListCartoes()){
									out.print("<div class='col s4 m4 l4'>");
									out.print("<fieldset>");
										out.print("<legend><b>N�mero: " + cartao.getCartao().getNumero() + "</b></legend>");
										out.print("<div class='left-align'>");
										out.print("<b>N�mero:</b>   " + cartao.getCartao().getNumero() + "<br/>");
										out.print("<b>Banco:</b>   " + cartao.getCartao().getBanco().getNome() + "<br/>");
										out.print("<b>Nome Impresso:</b>   " + cartao.getCartao().getNomeImpresso()+ "<br/>");
										out.print("<b>Quantidade Parcelas:</b> " + cartao.getQntParcelas()+ "<br/>");
										out.print("<b>Valor da Parcelas:</b> R$ " + String.format("%.2f",cartao.getPrecoParcela())+ "<br/>");
										out.print("<b>Valor Total:</b> R$ " + String.format("%.2f", cartao.getValorTotal())+ "<br/>");
										out.print("</div>");
									
									out.print("</fieldset>");
									out.print("</div>");
									
								}
							
							
							%>
						
						</fieldset>
				
				
				
				<%
				
					}
				
				%>
				
				<fieldset>
					<legend>Endere�os</legend>
					<div class="col s6 m6 l6">
						<fieldset>
							<legend><b>Entrega:</b></legend>
							<div class="left-align">
							<%
								out.print("<b>Endereco:</b> " + pedido.getEndEntrega().getRua() + ", " + pedido.getEndEntrega().getBairro() + "<br/>");
								out.print("<b>N�mero:</b> " + pedido.getEndEntrega().getNumero() + "<br/>");
								out.print("<b>Bairro:</b> " + pedido.getEndEntrega().getBairro() + "</br>");
								out.print("<b>Cidade:</b> " + pedido.getEndEntrega().getCidade().getNome() + "<br/>");
							
							%>
							</div>
							
						</fieldset>
					</div>
					
					
					<div class="col s6 m6 l6">
						<fieldset>
							<legend>Cobran�a:</legend>
							<div class="left-align">
							<%
								out.print("<b>Endereco:</b> " + pedido.getEndCobranca().getRua() + ", " + pedido.getEndEntrega().getBairro() + "<br/>");
								out.print("<b>N�mero:</b> " + pedido.getEndCobranca().getNumero() + "<br/>");
								out.print("<b>Bairro:</b> " + pedido.getEndCobranca().getBairro() + "</br>");
								out.print("<b>Cidade:</b> " + pedido.getEndCobranca().getCidade().getNome() + "<br/>");
							
							%>
							</div>
							
						</fieldset>
						
					</div>
				
				</fieldset>
				
				
				
				
				
			
			</div>
			</div>
		</div>
	</main>
	
	<%@include file="footer.jsp"%>
	
	
</body>
</html>