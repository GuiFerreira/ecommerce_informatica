<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<h3 class="center">Produtos</h3>
	<br>
	
	<div class=""><div class="row">
	
	<div class="col s2 m2 l2">
		<div class="center">
			<h5>Categorias</h5>
		</div>
		<div style="padding-left: 10px;">
			<a href='FiltrarCategoriaProduto?categoria=Todas'>Todas as Categorias</a><br/>
			<%
				List<EntidadeDominio> listEntidade = (List<EntidadeDominio>) session.getAttribute("listCategorias");
				for (EntidadeDominio entidade : listEntidade){
					Categoria categoria = (Categoria) entidade;
					out.println("<a href='FiltrarCategoriaProduto?categoria=" + categoria.getNome().replace(" ", "_") + "'>" + categoria.getNome() + "</a><br/>");
				}
			%>
		</div>
	
	</div>
	
	<div class="col s10 m10 l10">
		<!-- JSP AKI -->
		<div class="row">
						<!-- EXIBIR ALGUNS PRODUTOS NA TELA -->
					<% 
                    	
                    		List<EntidadeDominio> listEntidades = (List<EntidadeDominio>) session.getAttribute("listProdutos");
    		
    						if (listEntidades != null){
    							Produto produto = null;
    							
    							for (EntidadeDominio e : listEntidades){
    								produto = (Produto) e;
    								
    								out.print("<div class='col s6 m3'>");
    									out.print("<div class='card grey darken-1 z-depth-5'>");
		    								out.print("<a href='produto?id="+ produto.getId() + "'>");
			    								out.print("<div class='card-image'>");
			    									out.print("<img class='imgProduto' src='" + produto.getUriImagem() +
			    												"' alt='" + produto.getNome() + "'>");
			    								out.print("</div>");
			    								out.print("<div class='tituloProduto'>" + produto.getNome());
			    								out.print("</div>");
		    								out.print("</a>");
		    								out.print("<div class='descricaoProduto'>" + produto.getDescricao());		    									
		    								out.print("</div>");
		    								out.print("<div class='precoProduto'>R$ " + String.format("%.2f", produto.getPrecoComprado() * produto.getCategoria().getMargemLucro()));		    									
		    								out.print("</div>");		    								
	    								out.print("</div>");
    								out.print("</div>");
    								
    								
    							}
    							
    						}
    						
    						if (listEntidades != null && listEntidades.size() == 0){
    							out.print("<div class='col s12 m12 l12'><h4 class='center'> N�o temos nenhum produto com essa categoria no momento...</h4></div>");
    						}
    		
    		
                    	%>

		</div>

	</div>
	</div></div>

	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>