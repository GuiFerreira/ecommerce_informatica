<%@page import="br.com.ecommerceinformatica.fatec.dominio.Fornecedor"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo"> <%
			Fornecedor fornecedor = (Fornecedor) session.getAttribute("fornecedor");
		%>

	<div class="container">
		<div class="row center">
			<form action="AlterarFornecedor" method="POST">

				<h3>Alterar Fornecedor</h3>
				<p style="color: red;">Campos com * sao obrigatorios</p>
				<br /> <input id="idFornecedor" name="idFornecedor"
					value="<% out.print(fornecedor.getId()); %>" style='display: none' />
				<input id="idContato" name="idContato"
					value="<% out.print(fornecedor.getContato().getId()); %>"
					style="display: none" /> <input id="idEndereco" name="idEndereco"
					value="<% out.print(fornecedor.getEndereco().getId()); %>"
					style="display: none" />

				<fieldset>
					<legend>Dados do Fornecedor</legend>
					<div class="col s12 m8 offset-m2">
						<label for="nomeFornecedor">Nome do Fornecedor *</label> <input
							class="input-field" type="text" id="nomeFornecedor"
							name="nomeFornecedor"
							value="<% out.print(fornecedor.getNome()); %>" required>
					</div>
					<div class="col s12 m8 offset-m2">
						<label for="cnpj">CNPJ do Fornecedor * </label> <input
							class="input-field" type="text" id="cnpj" name="cnpj"
							value="<% out.print(fornecedor.getCNPJ()); %>" required>
					</div>
				</fieldset>

				<fieldset>
					<legend>Contato do Fornecedor *</legend>
					<div class="col s12 m4">
						<div class="row">
							<div class="col s3">
								<label for="DDD">DDD *</label> <input class="input-field"
									type="number" id="DDD" name="DDD" maxlength="99" minlength="10"
									value="<% out.print(fornecedor.getContato().getDDD()); %>"
									required>
							</div>
							<div class="col s9">
								<label for="telefone">Telefone *</label> <input
									class="input-field" type="number" id="telefone" name="telefone"
									value="<% out.print(fornecedor.getContato().getTelefone()); %>"
									required>
							</div>
						</div>
					</div>

					<div class="col s12 m8">
						<label for="email">Email do Fornecedor *</label> <input
							class="input-field" type="text" id="email" name="email"
							value="<% out.print(fornecedor.getContato().getEmail()); %>"
							required>
					</div>


				</fieldset>

				<fieldset>
					<legend>Endere�o do Fornecedor</legend>
					<div class="col s12">
						<label for="rua">Rua *</label> <input class="input-field"
							type="text" id="rua" name="rua"
							value="<% out.print(fornecedor.getEndereco().getRua()); %>"
							required>
					</div>

					<div class="col s12 m8">
						<label for="bairro">Bairro *</label> <input class="input-field"
							type="text" id="bairro" name="bairro"
							value="<% out.print(fornecedor.getEndereco().getBairro()); %>"
							required>
					</div>

					<div class="col s12 m4">
						<label for="numero">Numero *</label> <input class="input-field"
							type="text" id="numero" name="numero"
							value="<% out.print(fornecedor.getEndereco().getNumero()); %>"
							required>
					</div>

					<div class="col s12 m8">
						<label for="cidade">Cidade *</label> <input class="input-field"
							type="text" id="cidade" name="cidade"
							value="<% out.print(fornecedor.getEndereco().getCidade().getNome()); %>"
							required>
					</div>

					<div class="col s12 m4">
						<label for="cep">CEP *</label> <input class="input-field"
							type="text" id="cep" name="cep"
							value="<% out.print(fornecedor.getEndereco().getCep()); %>"
							required>
					</div>


					<div class="col s10 m5">
						<label for="estado">Estado *</label> <input class="input-field"
							type="text" id="estado" name="estado"
							value="<% out.print(fornecedor.getEndereco().getCidade().getEstado().getNome()); %>"
							required>
					</div>

					<div class="col s2 m1">
						<label for="siglaEstado">Sigla *</label> <input
							class="input-field" type="text" id="siglaEstado"
							name="siglaEstado"
							value="<% out.print(fornecedor.getEndereco().getCidade().getEstado().getSigla()); %>"
							required>
					</div>

					<div class="col s10 m5">
						<label for="pais">Pais *</label> <input class="input-field"
							type="text" id="pais" name="pais"
							value="<% out.print(fornecedor.getEndereco().getCidade().getEstado().getPais().getNome()); %>"
							required>
					</div>

					<div class="col s2 m1">
						<label for="siglaPais">Sigla *</label> <input class="input-field"
							type="text" id="siglaPais" name="siglaPais"
							value="<% out.print(fornecedor.getEndereco().getCidade().getEstado().getPais().getSigla()); %>"
							required>
					</div>

					<div class="col s12 m6">
						<label for="logradouro">Logradouro *</label> <input
							class="input-field" type="text" id="logradouro" name="logradouro"
							value="<% out.print(fornecedor.getEndereco().getLogradouro().getLogradouro()); %>"
							required>
					</div>

					<div class="col s12 m6">
						<label for="tipoLogradouro">Tipo Logradouro *</label> <input
							class="input-field" type="text" id="tipoLogradouro"
							name="tipoLogradouro"
							value="<% out.print(fornecedor.getEndereco().getLogradouro().getTpLogradouro()); %>"
							required>
					</div>

					<div class="col s12">
						<label for="observacoes">Observacoes</label>
						<textarea id="observacoes" name="observacoes">
							<% out.print(fornecedor.getEndereco().getObsercacoes()); %>
						</textarea>

					</div>

				</fieldset>
				<br /> <input class="waves-light btn grey" type="submit"
					id="OPERACAO" name="OPERACAO" value="ALTERAR FORNECEDOR">

			</form>
		</div>
	</div>


	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>