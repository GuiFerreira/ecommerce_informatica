<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>

<head>
<meta charset="UTF-8">
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>

	<!-- BODY -->
	<main class="corpo">
	<div class="container">
	<div class="center">
		<div class="row s4 m4 l4"></div>
		
			<h3>Suporte ao Cliente!</h3>
		
		<!--  USU�RIO N�O EST� LOGADO  -->
		<% 	if (clienteLogado == null){ 	%>
		
			<p>
				Para solicitar alguma ajuda ao time de vendas, por favor fazer o login: <br/>
				<a href="login.jsp" >fa�a login</a>
			</p>		
		
		<%		} else{  %>
		
		<!--  USU�RIO EST� LOGADO  -->
			
			<form action="SUPPORTE" method="POST">
				
				<label for="assunto">Assunto:</label>
				<input type="text" id="assunto" name="name" required/>
							
				<br/>	
				<label for="textoSuporte">Corpo do texto:</label>
				<textarea rows="10" style="height:auto!important" id="textoSuporte" name="textoSuporte" required></textarea>
				
				<br/><br/>				
				<input class="waves-light btn grey" type="submit" id="OPERACAO"
						name="OPERACAO" value="ENVIAR">
			
			</form>
		
		<%	}	%>
	</div>
	</div>
	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>