<%@page import="com.sun.mail.imap.protocol.Status"%>
<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />
    <link type="text/css" rel="stylesheet" href="css/estilo.css" media="screen,projection" />

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- Meu JS-->
    <script type="text/javascript" src="js/main.js"></script>

</head>

<body>
	
	<%@include file="navBar.jsp" %>
	
	<%
		
		if (!adm)
			response.sendRedirect("index.jsp");
		
    		
    	String funcionalidade = "An�lise:";
//     	String funcionalidade = session.getAttribute("funcionalidade");
	%>
	
    <!-- BODY -->
    <main class="corpo">
		<div class="row">
           	<div class="col m3">
           		<div class="center-align col m12">
           			<h5>Funcionlidades:</h5>
           		</div>
           		<div class="left-align col m12">
	            	<form action="ademirRequest" method="GET">
	            		<fieldset>
							<legend><b>Pesquisar</b></legend>	            		
			                <input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="Pesquisar"><br/><br/>
	            		</fieldset>
	            		<br/>
	                	<fieldset>
	                		<legend><b>Cadastrar</b></legend>
			                <input class="waves-light btn grey" type="submit" id="Cadastrar Produto" name="OPERACAO" value="Cadastrar Produto"><br/><br/>
			                <input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="Cadastrar Fornecedor"><br/><br/>
			                <input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="Cadastrar Categoria"><br/><br/>
							<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="Registrar Compra Mercadoria"><br/><br/>
							<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="Desconto Produto"><br/><br/>
	                		
	                	</fieldset>
						
	
	            	</form>
	            </div>
           	
           	</div>
			<div class="center-align col m9">
				 <ul class="collapsible">
					<li>
						<div class="center-align collapsible-header">Lista de Pedidos Status</div>
						<div class="collapsible-body">
							<h6><b>Pedidos:</b></h6> <br/>
							<div class="center-align">
								<table>
									<thead>
										<tr>
											<td>C�digo do Pedido</td>	
											<td>Data da Compra</td>
											<td>Enviar para</td>
											<td>Preco Total</td>
											<td>Status</td>
											<td>Editar Status</td>
										</tr>
										
									</thead>
									
									<tbody class="center-align">
									<%
										List<EntidadeDominio> listEntidadesPedidos = Util.getListPedidoADM(request);
										// tem q tirar isso aqui tbm
										session.setAttribute("listPedidos", listEntidadesPedidos);
										
										for (EntidadeDominio entidade : listEntidadesPedidos){
											Pedido pedido = (Pedido) entidade;
											out.print("<tr>");
											
											out.print("<td><a href='Pedido?codigo=" + pedido.getCodPedido() + "'>" + pedido.getCodPedido() + "</a></td>");
											out.print("<td>" + pedido.getDataCompra() + "</td>");
											out.print("<td>" + pedido.getEndEntrega().getCidade().getNome() + ": " + pedido.getEndEntrega().getRua() + " - " + pedido.getEndEntrega().getNumero()+ "</td>");
											out.print("<td>" + pedido.getPrecoTotal() + "</td>");
											out.print("<td>");
											%>
												<form action="AlterarStatusPedido" method="POST">
													<input type="hidden" name="idPedido" value="<% out.print(pedido.getId()); %>"/>
													<select id="statusPedido" name="statusPedido" <% out.print((!pedido.getStatusCompra().getPermitirAlteracaoADM()) ? "disabled=true" : ""); %>>
														<option value="<% out.print(pedido.getStatusCompra().toString()); %>"><% out.print(pedido.getStatusCompra().toString()); %></option>
														<% 
														
															for (StatusCompra valor : StatusCompra.values()) {
																if (pedido.getStatusCompra().toString().equals(valor.toString()) || !valor.getPermitirAlteracaoADM() ||
																		(pedido.getStatusCompra().getOrdemStatus() + 1) != valor.getOrdemStatus())
																	continue;
																
																out.print("<option value='" + valor.toString() + "'>" + valor.toString() + "</option>");
																
															}
														
														%>
													</select>
												
											
											<%
											out.print("</td>");
											out.print("<td>");
												%>
													<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="EDITAR PEDIDO" <% out.print((!pedido.getStatusCompra().getPermitirAlteracaoADM()) ? "disabled=true" : ""); %>><br/>
												 </form>
												<%
											out.print("</td>");
											out.print("</tr>");
											
										}
										
									
									%>
									</tbody>
									
								</table>
										
							</div>
						</div>
					</li>
					
					<li>
						<div class="center-align collapsible-header">Lista de Itens de Troca</div>
						<div class="collapsible-body">
							<h6><b>Itens de Troca:</b></h6> <br/>
							<div class="center-align">
								<table class="center-align">
									<thead>
										<tr>
											<td>C�dTroca</td>
											<td>C�dPedido</td>
											<td>Imagem</td>
											<td>Nome Produto</td>
											<td>Data Solicita��o</td>
											<td>Status</td>
											<td>Editar</td>
											<td>A��o</td>
										</tr>
									</thead>
									<tbody class="center-align">
									<%
										List<EntidadeDominio> listItensTroca = Util.getListItensTrocaADM(request);
										// tem q tirar isso aqui tbm
										session.setAttribute("listItensTroca", listItensTroca);
										
										for (EntidadeDominio entidade : listItensTroca){
											TrocaPedido troca = (TrocaPedido) entidade;
											for (ItensTroca item : troca.getListItensTroca()){
												out.print("<tr>");
												out.print("<td>" + troca.getId() + "</td>");
												out.print("<td><a href='Pedido?codigo=" + troca.getPedido().getCodPedido() + "'>" + troca.getPedido().getCodPedido() + "</a></td>");
												out.print("<td><img class='imgCarrinho' src='" + item.getProduto().getUriImagem() + "'></td>");
												out.print("<td>" + item.getProduto().getNome() + "</td>");
												out.print("<td>" + troca.getDataSolicitacao() + "</td>");
												out.print("<td>" + item.getStatus().toString() + "</td>");
												out.print("<td>");
												%>
													<form action="AlterarStatusTroca" method="POST">
														<input type="hidden" name="idPedido" value="<% out.print(troca.getPedido().getId()); %>"/>
														<input type="hidden" name="idTroca" value="<% out.print(troca.getId()); %>"/>
														<input type="hidden" name="idProduto" value="<% out.print(item.getProduto().getId()); %>"/>
														<input type="hidden" name="idCliente" value="<% out.print(troca.getIdCliente()); %>"/>
														<select name="statusTrocaPedido">
															<option value="<% out.print(item.getStatus().toString()); %>"><% out.print(item.getStatus().toString()); %></option>
															<% 
															
																for (StatusTrocaCancelamento valor : StatusTrocaCancelamento.values()) {
																	if (item.getStatus().toString().equals(valor.toString()) || valor.tipoStatus != item.getStatus().tipoStatus ||
																			(item.getStatus().getOrdemStatus() + 1) != valor.getOrdemStatus())
																		continue;
																	
																	out.print("<option value='" + valor.toString() + "'>" + valor.toString() + "</option>");
																	
																}
															
															%>
														</select>
													
												
													<%
												out.print("</td>");
												out.print("<td>");
													%>
														<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="EDITAR TROCA"><br/>
													 </form>
													<%
												out.print("</td>");
												
												
											}
											
											
										}
										
									
									%>
									</tbody>
									
								</table>
										
							</div>
						</div>
					</li>
					
					
					<li>
						<div class="center-align collapsible-header">An�lise</div>
						<div class="collapsible-body">
							<div class="col m12">
								<h3><% out.print(funcionalidade); %></h3>
								<br/><br/>
								<fieldset>
									<div class="col m12">
										<h6>Escolha o per�odo para gerar o gr�fico:</h6>
										<form action="FiltroPeriodoVenda" method="POST">
											<div class="center-align col m4">
												<div class="center-align col m6 input-field">
													<label for="dataInicio">Data In�cio:</label>
													<input type="text" id="dataInicio" name="dataInicio" class="datepicker"required>
												</div>
											</div>
											<div class="center-align col m4">
												<div class="center-align col m6 input-field">
													<label for="dataFim">Data Fim:</label>
													<input type="text" id="dataFim" name="dataFim" class="datepicker" required>
												</div>
											</div>
											<div class="center-align col m4">
												<div class="center-align col m6 input-field">
													<input class="waves-light btn grey" type="submit" id="OPERACAO" name="OPERACAO" value="Filtro Grafico Mes X QntVendida"/>
												</div>
											</div>
										</form>
									</div>
								</fieldset>
								
								<fieldset>
									<legend><b>Quantidade de Produtos Vendidos X Per�odo de Tempo:</b></legend>
									<div class="right-align col m12">
										<div class="col m8">
											<img class="responsive-img" src="img/graficos/QntVendidaReaisXMes.jpg" alt="era pra ser um gr�fico" >
										</div>
									</div>
								</fieldset>
								
								<fieldset>
									<legend><b>Categorias mais Vendidas: </b></legend>
									<div class="right-align col m12">
										<div class="col m8">
											<img class="responsive-img" src="img/graficos/CategoriasMaisVendidas.jpg" alt="era pra ser um gr�fico" >
										</div>
									</div>
								</fieldset>
								
								<fieldset>
									<legend><b>Quantidade de Clientes Cadastrados e Qnt Pedido X Per�odo de Tempo: </b></legend>
									<div class="right-align col m12">
										<div class="col m8">
											<img class="responsive-img" src="img/graficos/QntClientesEPedidos.jpg" alt="era pra ser um gr�fico" >
										</div>
									</div>
								</fieldset>
								
								
							</div>
							
							
						</div>
					</li>
				</ul>
				
				
			</div>
           
		</div>
           
    </main>
	
	<%@include file="footer.jsp" %>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript">
		document.addEventListener('DOMContentLoaded', function() {
			var elems = document.querySelectorAll('.datepicker');
			var instances = M.Datepicker.init(elems, {"format" : "dd/mm/yyyy"});
		});
		
	  document.addEventListener('DOMContentLoaded', function() {
		    var elems = document.querySelectorAll('.collapsible');
		    var instances = M.Collapsible.init(elems, null);
		  });
    
    </script>
</body>

</html>