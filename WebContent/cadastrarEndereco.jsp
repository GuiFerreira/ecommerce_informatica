<%@page import="br.com.ecommerceinformatica.fatec.dominio.*"%>
<html>

<head>
<!--Import Google Icon Font-->
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet" href="css/materialize.min.css"
	media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/estilo.css"
	media="screen,projection" />

<!-- Compiled and minified CSS -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- Meu JS-->
<script type="text/javascript" src="js/main.js"></script>

</head>

<body>

	<%@include file="navBar.jsp"%>
	
	<%
		Integer tipoEndereco = Integer.parseInt(request.getParameter("tipoEndereco"));
		
		HashMap<Integer, String> mapTipoEndereco = new HashMap<Integer, String>();
		mapTipoEndereco.put(0, "Entrega");
		mapTipoEndereco.put(1, "Cobran�a");
		mapTipoEndereco.put(2, "Entrega/Cobran�a");
		
	
	%>
	
	<!-- BODY -->
	<main class="corpo">
	<div class="container">
		<div class="row">
		<h3>Cadastrar Endere�o!</h3>
		<h6 style="color: red;">Todos os campos s�o obrigat�rio!</h6>
			<form action="CadastarEndereco" method="POST" class="center">
				<fieldset>
					<legend>Endere�o</legend>
					<select id="tipoEndereco" name="tipoEndereco">
						<option value="<% out.print(tipoEndereco); %>"><% out.print(mapTipoEndereco.get(tipoEndereco)); %></option>
						<% if (tipoEndereco != 0){ %><option value="0"><% out.print(mapTipoEndereco.get(0)); %></option> <% } %>
						<% if (tipoEndereco != 1){ %><option value="1"><% out.print(mapTipoEndereco.get(1)); %></option> <% } %>
						<% if (tipoEndereco != 2){ %><option value="2"><% out.print(mapTipoEndereco.get(2)); %></option> <% } %>
					</select>
					<div class="col s12 m7">
						<label for="ruaEntrega">Rua:</label> <input class="input-field"
							type="text" id="ruaEntrega" name="ruaEntrega" required>
					</div>
					<div class="col s12 m7">
						<label for="numEntrega">Numero:</label> <input class="input-field"
							type="text" id="numEntrega" name="numEntrega">
					</div>
					<div class="col s12 m5">
						<label for="cepEntrega">CEP:</label> <input class="input-field"
							type="text" id="cepEntrega" name="cepEntrega" required>
					</div>
					<div class="col s12 m8">
						<label for="bairroEntrega">Bairro:</label> <input
							class="input-field" type="text" id="bairroEntrega"
							name="bairroEntrega" required>
					</div>
					<div class="input-field col s12 m4">
						<select id="estadoEntrega" name="estadoEntrega" required>
							<option value="" disabled selected>Escolha uma op��o</option>
							<option value="AC">Acre</option>
							<option value="AL">Alagoas</option>
							<option value="AP">Amap�</option>
							<option value="AM">Amazonas</option>
							<option value="BA">Bahia</option>
							<option value="CE">Cear�</option>
							<option value="DF">Distrito Federal</option>
							<option value="ES">Espírito Santo</option>
							<option value="GO">Goi�s</option>
							<option value="MA">Maranh�o</option>
							<option value="MT">Mato Grosso</option>
							<option value="MS">Mato Grosso do Sul</option>
							<option value="MG">Minas Gerais</option>
							<option value="PA">Par�</option>
							<option value="PB">Paraíba</option>
							<option value="PR">Paran�</option>
							<option value="PE">Pernambuco</option>
							<option value="PI">Piauí</option>
							<option value="RJ">Rio de Janeiro</option>
							<option value="RN">Rio Grande do Norte</option>
							<option value="RS">Rio Grande do Sul</option>
							<option value="RO">Rond�nia</option>
							<option value="RR">Roraima</option>
							<option value="SC">Santa Catarina</option>
							<option value="SP">S�o Paulo</option>
							<option value="SE">Sergipe</option>
							<option value="TO">Tocantins</option>
						</select> <label for="estadoEntrega">Estado</label>
					</div>
		
					<div class="col s12 m8">
						<label for="cidadeEntrega">Cidade:</label> <input
							class="input-field" type="text" id="cidadeEntrega"
							name="cidadeEntrega" required>
					</div>
					<div class="col s12 m4">
						<label for="paisEntrega">Pais:</label> <select name="paisEntrega"
							id="paisEntrega" required>
							<option value="Brasil">Brasil</option>
						</select>
					</div>
		
					<div class="col s12 m6">
						<label for="logradouroEntrega">Logradouro:</label> <input
							class="input-field" type="text" id="logradouroEntrega"
							name="logradouroEntrega" required>
					</div>
		
					<div class="col s12 m6">
						<label for="tipoLogradouroEntrega">Tipo Logradouro:</label> <select
							name="tipoLogradouroEntrega" id="tipoLogradouroEntrega" required>
							<option value="Rua">Rua</option>
							<option value="Avenida">Avenida</option>
							<option value="Estrada">Estrada</option>
						</select>
					</div>
		
					<div class="col s12">
						<label for="apelidoEntrega">Apelido para esse Endere�o</label> <input
							class="input-field" type="text" id="apelidoEntrega"
							name="apelidoEntrega" required>
					</div>
		
					<div class="col s12">
						<label for="observacoesEntrega">Observa��es</label>
						<textarea id="observacoesEntrega" name="observacoesEntrega"></textarea>
					</div>
					
					<br> 
					<input class="waves-light btn grey" type="submit"
						id="OPERACAO" name="OPERACAO" value="CADASTRAR ENDERECO">
		
				</fieldset>
			</form>
		
		
		
		</div>


	</div>

	</main>

	<%@include file="footer.jsp"%>

	<!--JavaScript at end of body for optimized loading-->
	<script type="text/javascript" src="js/materialize.min.js"></script>
</body>

</html>