CREATE TABLE EMAILTROCARSENHA(
	email_cod integer not null,
	email_cliente text not null,
	email_usou boolean
);

CREATE TABLE CATEGORIAS(
    cat_id serial,
    cat_nome text,
    cat_margem_lucro real,

    cat_data_cadastro text,
    
    ativo boolean
);

CREATE TABLE CONTATOS(
    con_id serial,
    con_ddd text,
    con_telefone text,
    con_email text,
    ativo boolean

    con_data_cadastro text
);

CREATE TABLE ENDERECOS(
    end_id serial,
    end_rua text,
    end_bairro text,
    end_numero text,
    end_cep text,
    end_pais text,
    end_sigla_pais text,
    end_estado text,
    end_sigla_estado text,
    end_cidade text,
    end_tipo_logradouro text,
    end_logradouro text,
    end_observacoes text,
    end_apelido text,
    
    end_data_cadastro text,
    
	ativo boolean
);

CREATE TABLE FORNECEDORES(
    for_id serial,
    for_nome text,
    for_cnpj text,

    for_con_id integer,
    for_end_id integer,

    for_data_cadastro text,
    
    ativo boolean

);

CREATE TABLE PRODUTOS(
    pro_id serial,
    pro_nome text not null,
    pro_codigo text not null,
    pro_descricao text not null,
    pro_especificacoes text not null,
    pro_marca text not null,
    pro_desconto real,
    pro_imagem text,

    pro_for_id integer,
    pro_cat_id integer,

    pro_data_cadastro text,

    pro_preco_compra real,
    
    ativo boolean
);

CREATE TABLE USUARIOS(
    usu_id serial,
    usu_email text,
    usu_senha text,
    usu_adm boolean,
    
    ativo boolean
);

CREATE TABLE CLIENTES(
    cli_id serial,
    cli_nome text not null,
    cli_cpf text not null,
    cli_data_nascimento text not null,
    
    cli_con_id integer,
    cli_usu_id integer,
    
    ativo boolean

);

CREATE TABLE CLIENTES_ENDERECOS(
    cliend_cli_id integer,
    cliend_end_id integer,
    cliend_cobranca integer
);

CREATE TABLE CUPOM(
    cup_id serial,
    cup_nome text,
    cup_valor real,
    cup_data_vencimento text,
    cup_usado boolean,
    cup_desconto boolean,
    cup_troca boolean,
    cup_cli_id int
);

CREATE TABLE PEDIDO(
    ped_id serial,
    ped_preco_total real,
    ped_codigo text,
    ped_status_compra text,
    ped_cli_id integer,
    ped_end_cobranca_id integer,
    ped_end_entrega_id integer,
    ped_dias_entrega integer,
    ped_cupom_desconto_id integer,
    ped_valor_frete real,
    ped_data_compra text,
    ped_data_entregue text,
    ped_car_num text,
    ped_qnt_parcela integer,
    ped_preco_parcela real
);

CREATE TABLE ITENS_PEDIDO(
    iped_ped_id integer,
    iped_pro_id integer,
    iped_qnt_produto integer,
    iped_preco_unitario real,
    iped_preco_total real,
    iped_data_compra text
);

CREATE TABLE ESTOQUE (
    est_pro_id integer,
    est_qnt_estoque integer,
    est_data_ultima_compra text
);

CREATE TABLE CARTAO_CREDITO(
    car_numero text,
    car_cli_id integer,
    car_cod_seguranca text,
    car_nome_impresso text,
    car_nome_banco text,
    car_agencia text,
    
    ativo boolean

);

CREATE TABLE CARTOES_PEDIDO(
	carped_ped_id integer,
	carped_car_id text,
	carped_valor_total real,
	carped_valor_parcela real,
	carped_qnt_parcela int
);

CREATE TABLE SYSTEM_CONFIG(
	sys_nome text,
	sys_valor text

);



-- CHAVES PRIMARIAS
ALTER TABLE CARTAO_CREDITO ADD CONSTRAINT PK_CARTAO_CREDITO PRIMARY KEY (car_numero);

ALTER TABLE ESTOQUE ADD CONSTRAINT PK_ESTOQUE PRIMARY KEY (est_pro_id);

ALTER TABLE USUARIOS ADD CONSTRAINT PK_USUARIO PRIMARY KEY (usu_id);

ALTER TABLE CUPOM ADD CONSTRAINT PK_CUPOM PRIMARY KEY (cup_id);

ALTER TABLE PEDIDO ADD CONSTRAINT PK_PEDIDO PRIMARY KEY (ped_id);

ALTER TABLE PRODUTOS ADD CONSTRAINT PK_PROD PRIMARY KEY (pro_id);

ALTER TABLE FORNECEDORES ADD CONSTRAINT PK_FORN PRIMARY KEY (for_id);

ALTER TABLE ENDERECOS ADD CONSTRAINT PK_ENDE PRIMARY KEY (end_id);

ALTER TABLE CONTATOS ADD CONSTRAINT PK_CONT PRIMARY KEY (con_id);

ALTER TABLE CATEGORIAS ADD CONSTRAINT PK_CATE PRIMARY KEY (cat_id);

ALTER TABLE CLIENTES ADD CONSTRAINT PK_CLI PRIMARY KEY (cli_id);

ALTER TABLE CLIENTES_ENDERECOS ADD CONSTRAINT PK_CLIEND PRIMARY KEY (cliend_cli_id, cliend_end_id);

ALTER TABLE CARTOES_PEDIDO ADD CONSTRAINT PK_CARTOESPEDIDO_PEDIDO PRIMARY KEY (carped_ped_id, carped_car_id);



-- CHAVES ESTRANGEIRAS
ALTER TABLE ESTOQUE ADD CONSTRAINT FK_ESTOQUE_PRODUTO FOREIGN KEY (est_pro_id) REFERENCES PRODUTOS (pro_id);

ALTER TABLE PEDIDO ADD CONSTRAINT FK_PEDIDO_CLIENTE FOREIGN KEY (ped_cli_id) REFERENCES CLIENTES(cli_id);
ALTER TABLE PEDIDO ADD CONSTRAINT FK_PEDIDO_END_COBRANCA FOREIGN KEY (ped_end_cobranca_id) REFERENCES ENDERECOS(end_id);
ALTER TABLE PEDIDO ADD CONSTRAINT FK_PEDIDO_END_ENTREGA FOREIGN KEY (ped_end_entrega_id) REFERENCES ENDERECOS(end_id);

ALTER TABLE ITENS_PEDIDO ADD CONSTRAINT FK_ITEN_PEDIDO_PEDIDO FOREIGN KEY (iped_ped_id) REFERENCES PEDIDO (ped_id);
ALTER TABLE ITENS_PEDIDO ADD CONSTRAINT FK_ITEN_PEDIDO_PRODUTO FOREIGN KEY (iped_pro_id) REFERENCES PRODUTOS (pro_id);

ALTER TABLE PRODUTOS ADD CONSTRAINT FK_PROD_FOR FOREIGN KEY (pro_for_id) REFERENCES FORNECEDORES(for_id);
ALTER TABLE PRODUTOS ADD CONSTRAINT FK_PROD_CAT FOREIGN KEY (pro_cat_id) REFERENCES CATEGORIAS(cat_id);

ALTER TABLE FORNECEDORES ADD CONSTRAINT FK_FORN_CON FOREIGN KEY (for_con_id) REFERENCES CONTATOS(con_id);
ALTER TABLE FORNECEDORES ADD CONSTRAINT FK_FORN_END FOREIGN KEY (for_end_id) REFERENCES ENDERECOS(end_id);

ALTER TABLE CLIENTES_ENDERECOS ADD CONSTRAINT FK_CLIEND_CLI FOREIGN KEY (cliend_cli_id) REFERENCES CLIENTES(cli_id);
ALTER TABLE CLIENTES_ENDERECOS ADD CONSTRAINT FK_CLIEND_END FOREIGN KEY (cliend_end_id) REFERENCES ENDERECOS(end_id);

ALTER TABLE CLIENTES ADD CONSTRAINT FK_CLI_CON FOREIGN KEY (cli_con_id) REFERENCES CONTATOS(con_id);

-- SQL PARA DELETAR AS TABELAS
-- DROP TABLE CUPOM;
-- DROP TABLE ITENS_PEDIDO;
-- DROP TABLE CARTOES_PEDIDO;
-- DROP TABLE PEDIDO;
-- DROP TABLE CARTAO_CREDITO;
-- DROP TABLE ESTOQUE;
-- DROP TABLE PRODUTOS;
-- DROP TABLE FORNECEDORES;
-- DROP TABLE CATEGORIAS;
-- DROP TABLE CLIENTES_ENDERECOS;
-- DROP TABLE CLIENTES;
-- DROP TABLE CONTATOS;
-- DROP TABLE ENDERECOS;
-- DROP TABLE USUARIOS;








