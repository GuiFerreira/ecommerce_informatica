-- INSERIR FORNECEDORES
INSERT INTO enderecos (end_rua, end_bairro, end_numero, end_cep, end_pais,
					   end_sigla_pais,end_estado, end_sigla_estado,end_cidade,end_data_cadastro,
					   end_tipo_logradouro, end_logradouro, end_observacoes)
VALUES ('Rua Abacaxi', 'Bairro Limão', '123A', '08830808', 'Morango', 'MG', 'Abacate', 
	   		'AB', 'Uva', '10/03/2019', 'Rua', 'Rua', 'Apartamento 21 Bloco A');

INSERT INTO contatos (con_ddd,
						con_telefone,
						con_email,
						con_data_cadastro)
VALUES ('11', '953402016', 'kimaster@kimaster.com.br', '10/03/2019');



INSERT INTO fornecedores (for_nome,
							for_cnpj,
							for_con_id,
							for_end_id,
							for_data_cadastro)
VALUES ('Kimaster', '12345678910', 1, 1, '10/03/2019');


INSERT INTO enderecos (end_rua, end_bairro, end_numero, end_cep, end_pais,
					   end_sigla_pais,end_estado, end_sigla_estado,end_cidade,end_data_cadastro,
					   end_tipo_logradouro, end_logradouro, end_observacoes)
VALUES ('Rua Amora', 'Bairro Pessego', '133A', '08831343', 'Goiaba', 'GB', 'Kiwi', 
	   		'KW', 'Uva', '09/03/2019', 'Rua', 'Rua', 'Ao lado do Shopping');

INSERT INTO contatos (con_ddd,
						con_telefone,
						con_email,
						con_data_cadastro)
VALUES ('13', '989765825', 'carrara@carrara.com.br', '09/03/2019');



INSERT INTO fornecedores (for_nome,
							for_cnpj,
							for_con_id,
							for_end_id,
							for_data_cadastro)
VALUES ('Carrara', '12345678910', 2, 2, '09/03/2019');


INSERT INTO enderecos (end_rua, end_bairro, end_numero, end_cep, end_pais,
					   end_sigla_pais,end_estado, end_sigla_estado,end_cidade,end_data_cadastro,
					   end_tipo_logradouro, end_logradouro, end_observacoes)
VALUES ('Rua Lasanha', 'Bairro Pizza', '9889', '05891343', 'Fruta', 'FT', 'Laranja', 
	   		'LJ', 'Uva', '11/03/2019', 'Rua', 'Rua', '');

INSERT INTO contatos (con_ddd,
						con_telefone,
						con_email,
						con_data_cadastro)
VALUES ('27', '975693140', 'nice@nice.com.br', '11/03/2019');



INSERT INTO fornecedores (for_nome,
							for_cnpj,
							for_con_id,
							for_end_id,
							for_data_cadastro)
VALUES ('Nice', '5692014578', 3, 3, '11/03/2019');



-- INSERINDO CATEGORIAS
INSERT INTO categorias (cat_nome,
    cat_margem_lucro,
    cat_data_cadastro)
VALUES('Monitor', 5, '11/03/2019');
	
INSERT INTO categorias (cat_nome,
    cat_margem_lucro,
    cat_data_cadastro)
VALUES('Mouse', 4.7, '11/03/2019');
	
INSERT INTO categorias (cat_nome,
    cat_margem_lucro,
    cat_data_cadastro)
VALUES('Teclado', 8.6, '11/03/2019');
	
INSERT INTO categorias (cat_nome,
    cat_margem_lucro,
    cat_data_cadastro)
VALUES('Processador', 6.3, '11/03/2019');
	
INSERT INTO categorias (cat_nome,
    cat_margem_lucro,
    cat_data_cadastro)
VALUES('HeadSet', 3, '11/03/2019');



-- INSERT PRODUTOS
INSERT INTO PRODUTOS (pro_nome, pro_codigo, pro_descricao, pro_especificacoes, pro_marca, pro_desconto,
                        pro_imagem, pro_for_id, pro_cat_id, pro_data_cadastro, pro_preco_compra) 
VALUES('Logitech G502','1234','Mouse Gamer G502', 'Mouse Gamer G502 16.000 DPI', 'Logitech', 0, 'img/produtos/logitechg G502.png',
                1, 2, '27/03/2019', 100);

INSERT INTO PRODUTOS (pro_nome, pro_codigo, pro_descricao, pro_especificacoes, pro_marca, pro_desconto,
                        pro_imagem, pro_for_id, pro_cat_id, pro_data_cadastro, pro_preco_compra) 
VALUES('HeadSet HyperX Cloud Stinger','1235','HyperX Cloud Stinger', 'HyperX Cloud Stinger', 'HyperX', 0, 'img/produtos/hyperxcloudstinger.png',
                2, 5, '27/03/2019', 120);


-- INSERT ENDERECOS PARA CLIENTE
INSERT INTO enderecos (end_rua, end_bairro, end_numero, end_cep, end_pais,
					   end_sigla_pais,end_estado, end_sigla_estado,end_cidade,end_data_cadastro,
					   end_tipo_logradouro, end_logradouro, end_observacoes)
VALUES ('Rua Teste CLiente', 'Bairro Teste CLiente', '123', '433', 'Fruta', 'FT', 'Laranja', 
	   		'LJ', 'Uva', '11/03/2019', 'Rua', 'Rua', '');

INSERT INTO contatos (con_ddd,
						con_telefone,
						con_email,
						con_data_cadastro)
VALUES ('11', '975612234', 'aaaaa@nice.com.br', '13/03/2019');

INSERT INTO usuarios(usu_email, usu_senha, usu_adm)
VALUES ('aaaaa@nice.com.br', 'Qwer1234!', false);

INSERT INTO clientes (cli_nome, 
                        cli_cpf, 
                        cli_data_nascimento, 
                        cli_con_id, 
                        cli_usu_id)
VALUES ('Joao', '376.190.018-08', '17/01/1999', 4, 1);

--

INSERT INTO enderecos (end_rua, end_bairro, end_numero, end_cep, end_pais,
					   end_sigla_pais,end_estado, end_sigla_estado,end_cidade,end_data_cadastro,
					   end_tipo_logradouro, end_logradouro, end_observacoes)
VALUES ('Rua KKKKK', 'Bairro KKKKK', '432', '121', 'Fruta', 'TY', 'Laranja', 
	   		'LJ', 'Uva', '11/03/2019', 'Rua', 'Rua', '');

INSERT INTO contatos (con_ddd,
						con_telefone,
						con_email,
						con_data_cadastro)
VALUES ('11', '978762234', 'bbbbbb@nice.com.br', '13/03/2019');

INSERT INTO usuarios(usu_email, usu_senha, usu_adm)
VALUES ('bbbbbb@nice.com.br', 'Qwer1234!', false);

INSERT INTO clientes (cli_nome, 
                        cli_cpf, 
                        cli_data_nascimento, 
                        cli_con_id, 
                        cli_usu_id)
VALUES ('Guilherme', '376.190.018-08', '17/01/1999', 5, 2);


